package com.vf.i.mvc.service;

import java.util.List;

import com.jfinal.kit.StrKit;
import com.vf.s.common.model.biz.Point;

public class ScenePointService {

	public List<Point> gets(String layerId) {
		String whereForBizPoint = "FROM " + Point.TABLE_NAME + " P  WHERE 1=1 ";
		if (!StrKit.isBlank(layerId)) {
			whereForBizPoint += " and P.LAYERID ='" + layerId + "' ";
		}
		return Point.dao.find("SELECT P.* , 'Point' as type  " + whereForBizPoint);
	}

	public Point get(String entityId) {
		return Point.dao.findById(entityId);
	}

}
