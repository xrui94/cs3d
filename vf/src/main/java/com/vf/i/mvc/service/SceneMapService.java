package com.vf.i.mvc.service;

import java.util.List;

import com.jfinal.kit.StrKit;
import com.vf.s.common.model.biz.Provider;
import com.vf.s.common.model.scene.SceneProvider;

public class SceneMapService {

	public List<Provider> gets(String layerId) {
		String whereForMap = "SELECT A.*,'Map' as type  FROM " + Provider.TABLE_NAME + " A , " + SceneProvider.TABLE_NAME
				+ " B WHERE A.ID=B.PROVIDERID";
		if (!StrKit.isBlank(layerId)) {
			whereForMap += " AND B.LAYERID='" + layerId + "' ";
		}
		whereForMap += " ORDER BY B.SORT ASC ";
		return  Provider.dao.find(whereForMap);
	}

	public Provider get(String entityId) {
		return Provider.dao.findFirst("select A.*,B.SORT,B.layerId from "+Provider.TABLE_NAME+" A,"+SceneProvider.TABLE_NAME+" B where A.ID=B.PROVIDERID AND B.id=? ",entityId);
	}

}
