package com.vf.i.mvc.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.jfinal.kit.StrKit;
import com.vf.core.render.RenderBean;
import com.vf.s.common.model.biz.*;
import com.vf.s.common.model.scene.BizSceneFeature;
import com.vf.s.common.model.scene.SceneLayer;
import com.vf.s.common.model.scene.BizSceneModel;

public class SceneLayerService {

	private final static ScenePointService pointService=new ScenePointService();
	private final static SceneMapService  mapService=new SceneMapService();
	private final static SceneModelService  modelService=new SceneModelService();
	private final static ScenePolylineService  polylineService=new ScenePolylineService();
	private final static ScenePolygonService  polygonService=new ScenePolygonService();
	private final static SceneFeatureService  featureService=new SceneFeatureService();
	private final static SceneVideoPlaneService  videoPlaneService=new SceneVideoPlaneService();
	private final static SceneVideoShed3dService  videoShed3dService=new SceneVideoShed3dService();
	private final static SceneVideoModelService  videoModelService=new SceneVideoModelService();
	

	public List<SceneLayer> gets(String layerId) {
		return SceneLayer.dao.find("select * from "+SceneLayer.TABLE_NAME+" where parentid=? ",layerId);
	}
	
	public SceneLayer get(String layerId) {
		return SceneLayer.dao.findById(layerId);
	}
	
	
	public RenderBean getEntities(SceneLayer layer) {
		
		RenderBean renderBean = new RenderBean();
		
		Map<String,Object> data = new HashMap<String,Object>();
		
		
		//点集合
		if(StrKit.equals("Points", layer.getType())) {
			List<Point> list=pointService.gets(layer.getId());
			if(list!=null && list.size()>0) {
				
				data.put("layer", layer);
				data.put("entities", list);
				renderBean.setCode(200);
				renderBean.setMessage("成功！");
				renderBean.setData(data);
			}else {
				renderBean.setCode(500);
				renderBean.setMessage("未查询到数据！");
			}
		}
		//底图集合
		else if(StrKit.equals("Maps",  layer.getType())) {
			List<Provider> list=mapService.gets(layer.getId());
			if(list!=null && list.size()>0) {
				data.put("layer", layer);
				data.put("entities", list);
				renderBean.setCode(200);
				renderBean.setMessage("成功！");
				renderBean.setData(data);
			}else {
				renderBean.setCode(500);
				renderBean.setMessage("未查询到数据！");
			}
		}
		//模型集合
		else if(StrKit.equals("Models",  layer.getType())) {
			List<BizSceneModel> list=modelService.gets(layer.getId());
			if(list!=null && list.size()>0) {
				data.put("layer", layer);
				data.put("entities", list);
				renderBean.setCode(200);
				renderBean.setMessage("成功！");
				renderBean.setData(data);
			}else {
				renderBean.setCode(500);
				renderBean.setMessage("未查询到数据！");
			}
		}
		//线集合
		else if(StrKit.equals("Polylines",  layer.getType())) {
			List<BizLine> list=polylineService.gets(layer.getId());
			if(list!=null && list.size()>0) {
				data.put("layer", layer);
				data.put("entities", list);
				renderBean.setCode(200);
				renderBean.setMessage("成功！");
				renderBean.setData(data);
			}else {
				renderBean.setCode(500);
				renderBean.setMessage("未查询到数据！");
			}
		}
		//面集合
		else if(StrKit.equals("Polygons",  layer.getType())) {
			List<BizPolygon> list=polygonService.gets(layer.getId());
			if(list!=null && list.size()>0) {
				data.put("layer", layer);
				data.put("entities", list);
				renderBean.setCode(200);
				renderBean.setMessage("成功！");
				renderBean.setData(data);
			}else {
				renderBean.setCode(500);
				renderBean.setMessage("未查询到数据！");
			}
		}
		//单体集合
		else if(StrKit.equals("Features",  layer.getType())) {
			List<BizSceneFeature> list=featureService.gets(layer.getId());
			if(list!=null && list.size()>0) {
				data.put("layer", layer);
				data.put("entities", list);
				renderBean.setCode(200);
				renderBean.setMessage("成功！");
				renderBean.setData(data);
			}else {
				renderBean.setCode(500);
				renderBean.setMessage("未查询到数据！");
			}
		}
		else if(StrKit.equals("VideoPlanes",  layer.getType())) {
			List<BizVideoPlane> list=videoPlaneService.gets(layer.getId());
			if(list!=null && list.size()>0) {
				data.put("layer", layer);
				data.put("entities", list);
				renderBean.setCode(200);
				renderBean.setMessage("成功！");
				renderBean.setData(data);
			}else {
				renderBean.setCode(500);
				renderBean.setMessage("未查询到数据！");
			}
		}
		else if(StrKit.equals("VideoShed3ds",  layer.getType())) {
			List<BizVideoShed3d> list=videoShed3dService.gets(layer.getId());
			if(list!=null && list.size()>0) {
				data.put("layer", layer);
				data.put("entities", list);
				renderBean.setCode(200);
				renderBean.setMessage("成功！");
				renderBean.setData(data);
			}else {
				renderBean.setCode(500);
				renderBean.setMessage("未查询到数据！");
			}
		}
		else if(StrKit.equals("VideoModels",  layer.getType())) {
			List<BizVideoModel> list=videoModelService.gets(layer.getId());
			if(list!=null && list.size()>0) {
				data.put("layer", layer);
				data.put("entities", list);
				renderBean.setCode(200);
				renderBean.setMessage("成功！");
				renderBean.setData(data);
			}else {
				renderBean.setCode(500);
				renderBean.setMessage("未查询到数据！");
			}
		}
		else if(StrKit.equals("Roams",  layer.getType())) {
		}
		else {
		}
		return renderBean;
	}

	public RenderBean getEntity(String entityId,String type) {
		RenderBean renderBean = new RenderBean();
		Map<String,Object> data = new HashMap<String,Object>();

		//点集合
		if(StrKit.equals("Point", type)) {
			Point entity=pointService.get(entityId);
			if(entity!=null) {
				
				data.put("layer", get(entity.getLayerId()));
				data.put("entity", entity);
				
				renderBean.setCode(200);
				renderBean.setMessage("成功！");
				renderBean.setData(entity);
			}else {
				renderBean.setCode(500);
				renderBean.setMessage("未查询到数据！");
			}
		}
		//底图集合
		else if(StrKit.equals("Map",  type)) {
//			BizMap entity=mapService.get(entityId);
//			if(entity!=null) {
//				data.put("layer", get(entity.getStr("layerId")));
//				data.put("entity", entity);
//				renderBean.setCode(200);
//				renderBean.setMessage("成功！");
//				renderBean.setData(entity);
//			}else {
//				renderBean.setCode(500);
//				renderBean.setMessage("未查询到数据！");
//			}
		}
		//模型集合
		else if(StrKit.equals("Model",  type)) {
			BizSceneModel entity=modelService.get(entityId);
			if(entity!=null) {
				data.put("layer", get(entity.getLayerId()));
				data.put("entity", entity);
				renderBean.setCode(200);
				renderBean.setMessage("成功！");
				renderBean.setData(entity);
			}else {
				renderBean.setCode(500);
				renderBean.setMessage("未查询到数据！");
			}
		}
		//线集合
		else if(StrKit.equals("Polyline",  type)) {
			BizLine entity=polylineService.get(entityId);
			if(entity!=null) {
				data.put("layer", get(entity.getLayerId()));
				data.put("entity", entity);
				renderBean.setCode(200);
				renderBean.setMessage("成功！");
				renderBean.setData(entity);
			}else {
				renderBean.setCode(500);
				renderBean.setMessage("未查询到数据！");
			}
		}
		//面集合
		else if(StrKit.equals("Polygon",  type)) {
			BizPolygon entity=polygonService.get(entityId);
			if(entity!=null) {
				data.put("layer", get(entity.getLayerId()));
				data.put("entity", entity);
				renderBean.setCode(200);
				renderBean.setMessage("成功！");
				renderBean.setData(entity);
			}else {
				renderBean.setCode(500);
				renderBean.setMessage("未查询到数据！");
			}
		}
		//单体集合
		else if(StrKit.equals("Feature",  type)) {
			BizSceneFeature entity=featureService.get(entityId);
			if(entity!=null) {
				data.put("layer", get(entity.getLayerId()));
				data.put("entity", entity);
				renderBean.setCode(200);
				renderBean.setMessage("成功！");
				renderBean.setData(entity);
			}else {
				renderBean.setCode(500);
				renderBean.setMessage("未查询到数据！");
			}
		}
		else if(StrKit.equals("VideoPlane",  type)) {
			BizVideoPlane entity=videoPlaneService.get(entityId);
			if(entity!=null) {
				data.put("layer", get(entity.getLayerId()));
				data.put("entity", entity);
				renderBean.setCode(200);
				renderBean.setMessage("成功！");
				renderBean.setData(entity);
			}else {
				renderBean.setCode(500);
				renderBean.setMessage("未查询到数据！");
			}
		}
		else if(StrKit.equals("VideoShed3d",  type)) {
			BizVideoShed3d entity=videoShed3dService.get(entityId);
			if(entity!=null) {
				data.put("layer", get(entity.getLayerId()));
				data.put("entity", entity);
				renderBean.setCode(200);
				renderBean.setMessage("成功！");
				renderBean.setData(entity);
			}else {
				renderBean.setCode(500);
				renderBean.setMessage("未查询到数据！");
			}
		}
		else if(StrKit.equals("VideoModel",  type)) {
			BizVideoModel entity=videoModelService.get(entityId);
			if(entity!=null) {
				data.put("layer", get(entity.getLayerId()));
				data.put("entity", entity);
				renderBean.setCode(200);
				renderBean.setMessage("成功！");
				renderBean.setData(entity);
			}else {
				renderBean.setCode(500);
				renderBean.setMessage("未查询到数据！");
			}
		}
		else if(StrKit.equals("Roams",  type)) {
		}
		else {
		}
		return renderBean;
	}







}
