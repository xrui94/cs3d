package com.vf.i.mvc.service;

import java.util.List;

import com.jfinal.kit.StrKit;
import com.vf.s.common.model.biz.BizLine;

public class ScenePolylineService {

	public List<BizLine> gets(String layerId) {
		String whereForBizPoint="FROM "+BizLine.TABLE_NAME+" P  WHERE 1=1 ";
		if(!StrKit.isBlank(layerId)) {
			whereForBizPoint+=" and P.LAYERID ='"+layerId+"' ";
		}
		return BizLine.dao.find("SELECT P.* ,'Polyline' as type "+whereForBizPoint);
	}

	public BizLine get(String entityId) {
		return BizLine.dao.findById(entityId);
	}

}
