package com.vf.i.mvc.controller;

import java.util.List;

import com.jfinal.aop.Inject;
import com.jfinal.kit.StrKit;
import com.jfinal.log.Log;
import com.vf.core.controller.BaseController;
import com.vf.i.mvc.service.SceneLayerService;
import com.vf.s.common.model.scene.SceneLayer;

public class SceneLayerController extends BaseController {

	protected final Log LOG = Log.getLog(getClass());

	@Inject
	private SceneLayerService srv;

	public void gets() {
		String sceneId = this.getPara("sceneId");
		String layerId = this.getPara("layerId");
		if (StrKit.isBlank(sceneId) || StrKit.isBlank(layerId)) {
			this.renderError("参数必填！");
			return;
		}

		List<SceneLayer> layers=srv.gets(layerId);
		if (layers != null && layers.size() > 0) {
			renderSuccess("成功！", layers);
		} else {
			this.renderError("未查询到数据！");
		}
	}
	
	public void get() {
		String layerId = this.getPara("layerId");
		if (StrKit.isBlank(layerId)) {
			this.renderError("参数必填！");
			return;
		}
		SceneLayer layer=srv.get(layerId);
		if (layer != null ) {
			renderSuccess("成功！", layer);
		} else {
			this.renderError("未查询到数据！");
		}
	}
	
	
	public void getEntities() {
		String layerId = this.getPara("layerId");
		SceneLayer layer=SceneLayer.dao.findById(layerId);
		if(layer!=null) {
			this.renderJson(srv.getEntities(layer));
		}else {
			renderError("无效图层！");
		}
	}
	
	public void getEntity() {
		String entityId = this.getPara("entityId");
		String type = this.getPara("type");
		this.renderJson(srv.getEntity(entityId,type));
	}
	
	//暂
	public void getGroup() {
		String menuId = this.getPara("menuId");
		String entityId = this.getPara("entityId");
		String type = this.getPara("type");
		this.renderJson(srv.getEntity(entityId,type));
	}
	
	
	public void getLayersByLevel() {
		String configId = this.getPara("configId");
		int level = this.getParaToInt("level",1);
		
		if(StrKit.isBlank(configId)) {
			this.renderError("参数必填!");
			return;
		}
		
		
		
		
		
	}
	
	


	public void getCluster() {
		String layerId = this.getPara("layerId");
		if (StrKit.isBlank(layerId)) {
			this.renderError("参数必填！");
			return;
		}
		SceneLayer layer=SceneLayer.dao.findById(layerId);
		if(layer!=null) {
			if(StrKit.equals("Points", layer.getType())) {
				this.renderJson(srv.getEntities(layer));
			}else {
				this.renderError("聚合只对点有效！");
			}
		}else {
			renderError("无效图层！");
		}
		
	}

}
