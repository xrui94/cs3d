package com.vf.i.mvc.service;

import com.vf.s.common.model.scene.Scene;

public class SceneService {

	public Scene get(String sceneId) {
		return Scene.dao.findById(sceneId);
	}
	
	

}
