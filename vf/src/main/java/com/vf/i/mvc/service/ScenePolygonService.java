package com.vf.i.mvc.service;

import java.util.List;

import com.jfinal.kit.StrKit;
import com.vf.s.common.model.biz.BizPolygon;

public class ScenePolygonService {

	public List<BizPolygon> gets(String layerId) {
		String whereForBizPoint="FROM "+BizPolygon.TABLE_NAME+" P  WHERE 1=1 ";
		if(!StrKit.isBlank(layerId)) {
			whereForBizPoint+=" and P.LAYERID ='"+layerId+"' ";
		}
		return BizPolygon.dao.find("SELECT P.*  "+whereForBizPoint);
	}

	public BizPolygon get(String entityId) {
		return BizPolygon.dao.findById(entityId);
	}

}
