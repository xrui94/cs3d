package com.vf.i.mvc.service;

import java.util.LinkedList;
import java.util.List;

import com.jfinal.kit.StrKit;
import com.vf.s.common.model.biz.BizVideo;
import com.vf.s.common.model.biz.BizVideoPlane;

public class SceneVideoPlaneService {

	public List<BizVideoPlane> gets(String layerId) {
		String whereForBizPoint="FROM "+BizVideoPlane.TABLE_NAME+" P   WHERE 1=1 ";
		if(!StrKit.isBlank(layerId)) {
			whereForBizPoint+=" and P.LAYERID ='"+layerId+"' ";
		}
		List<BizVideoPlane> bizBizVideoPlane=BizVideoPlane.dao.find("SELECT P.*  "+whereForBizPoint);
		List<BizVideoPlane> list=new LinkedList<BizVideoPlane>();
		for(BizVideoPlane videoPlane :bizBizVideoPlane) {
			BizVideo bizVideo=BizVideo.dao.findById(videoPlane.getVideoId());
			videoPlane.setVideo(bizVideo);
			list.add(videoPlane);
		}
		return list;
	}

	public BizVideoPlane get(String entityId) {
		BizVideoPlane bizVideoPlane=BizVideoPlane.dao.findById(entityId);
		if(bizVideoPlane!=null) {
			BizVideo bizVideo=BizVideo.dao.findById(bizVideoPlane.getVideoId());
			bizVideoPlane.setVideo(bizVideo);
			return bizVideoPlane;
		}else {
			return null;
		}
	}

}
