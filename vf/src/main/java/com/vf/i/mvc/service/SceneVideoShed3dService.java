package com.vf.i.mvc.service;

import java.util.LinkedList;
import java.util.List;

import com.jfinal.kit.StrKit;
import com.vf.s.common.model.biz.BizVideo;
import com.vf.s.common.model.biz.BizVideoShed3d;

public class SceneVideoShed3dService {

	public List<BizVideoShed3d> gets(String layerId) {
		String whereForBizPoint="FROM "+BizVideoShed3d.TABLE_NAME+" P   WHERE 1=1 ";
		if(!StrKit.isBlank(layerId)) {
			whereForBizPoint+=" and P.LAYERID ='"+layerId+"' ";
		}
		List<BizVideoShed3d> bizLineList=BizVideoShed3d.dao.find("SELECT P.*  "+whereForBizPoint);
		List<BizVideoShed3d> list=new LinkedList<BizVideoShed3d>();
		for(BizVideoShed3d videoShed3d :bizLineList) {
			BizVideo bizVideo=BizVideo.dao.findById(videoShed3d.getVideoId());
			videoShed3d.setVideo(bizVideo);
			list.add(videoShed3d);
		}
		return list;
	}

	public BizVideoShed3d get(String entityId) {
		BizVideoShed3d bizVideoPlane=BizVideoShed3d.dao.findById(entityId);
		if(bizVideoPlane!=null) {
			BizVideo bizVideo=BizVideo.dao.findById(bizVideoPlane.getVideoId());
			bizVideoPlane.setVideo(bizVideo);
			return bizVideoPlane;
		}
		return null;
	}

}
