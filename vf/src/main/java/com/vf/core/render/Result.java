package com.vf.core.render;


public class Result {

	private Object code;
	private String msg;
	private Object data;

	public Result() {
	}

	public Result(Object code, String msg) {
		this.code = code;
		this.msg = msg;
	}

	public Result(Object code, String msg, Object data) {
		super();
		this.code = code;
		this.msg = msg;
		this.data = data;
	}

	public Object getCode() {
		return code;
	}

	public void setCode(Object code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}
}
