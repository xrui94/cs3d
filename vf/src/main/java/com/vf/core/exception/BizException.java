package com.vf.core.exception;

public class BizException extends RuntimeException {

	private static final long serialVersionUID = 7505235826750387729L;
	private int code;
	private String message;

	public BizException(int code, String message) {
		super(message);
		this.code = code;
		this.message = message;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
