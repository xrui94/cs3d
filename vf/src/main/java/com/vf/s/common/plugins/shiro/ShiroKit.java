/**
 * @开发版权 云南赢中吉洋智能技术有限公司（YNYZ）
 * @项目名称 JCORE
 * @版本信息 v1.0
 * @开发人员 zhous
 * @开发日期 2018-11-20
 * @修订日期
 * @描述  ShiroKit
 */
package com.vf.s.common.plugins.shiro;

import java.util.concurrent.ConcurrentMap;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;

import com.jfinal.log.Log;

public class ShiroKit {

	protected final static Log LOG = Log.getLog(ShiroKit.class.getName());

	
	/**
	 * 登录成功时所用的页面。
	 */
	private static String successUrl = "/";

	/**
	 * 登录成功时所用的页面。
	 */
	private static String loginUrl = "/login.html";


	/**
	 * 登录成功时所用的页面。
	 */
	private static String unauthorizedUrl ="/401.jsp";
	
	/**
	 * Session中保存的请求的Key值
	 */
	private static String SAVED_REQUEST_KEY = "jfinalShiroSavedRequest";


	/**
	 * 用来记录那个action或者actionpath中是否有shiro认证注解。
	 */
	private static ConcurrentMap<String, AuthzHandler> authzMaps = null;

	/**
	 * 禁止初始化
	 */
	private ShiroKit() {}

	static void init(ConcurrentMap<String, AuthzHandler> maps) {
		authzMaps = maps;
	}

	public static AuthzHandler getAuthzHandler(String actionKey){
		return authzMaps.get(actionKey);
	}

	/**
	 * Session中保存的请求的Key值
	 * @return
	 */
	public static final String getSavedRequestKey(){
		return SAVED_REQUEST_KEY;
	}

	public static SimpleUser getLoginUser() {
		Subject subject = SecurityUtils.getSubject();
		if(subject!=null){
			return (SimpleUser)subject.getPrincipal();
		}else{
			SecurityUtils.getSubject().logout();
			return null;
		}
	}

	public static String getSuccessUrl() {
		return successUrl;
	}

	public static void setSuccessUrl(String successUrl) {
		ShiroKit.successUrl = successUrl;
	}

	public static String getLoginUrl() {
		return loginUrl;
	}

	public static void setLoginUrl(String loginUrl) {
		ShiroKit.loginUrl = loginUrl;
	}

	public static String getUnauthorizedUrl() {
		return unauthorizedUrl;
	}

	public static void setUnauthorizedUrl(String unauthorizedUrl) {
		ShiroKit.unauthorizedUrl = unauthorizedUrl;
	}
	
}
