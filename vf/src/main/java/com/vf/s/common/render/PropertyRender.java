package com.vf.s.common.render;

import java.util.List;

import com.vf.s.common.model.biz.*;

public class PropertyRender {
	
	private BizText bizText;
	private List<BizPage> bizPages;
	private List<BizProperty> bizPropertys;
	private List<BizPicture> bizPictures;
	private List<BizPanorama> bizPanoramas;
	private List<BizVideo> bizVideos;
	
	public BizText getBizText() {
		return bizText;
	}
	public void setBizText(BizText bizText) {
		this.bizText = bizText;
	}
	public List<BizPage> getBizPages() {
		return bizPages;
	}
	public void setBizPages(List<BizPage> bizPages) {
		this.bizPages = bizPages;
	}
	public List<BizProperty> getBizPropertys() {
		return bizPropertys;
	}
	public void setBizPropertys(List<BizProperty> bizPropertys) {
		this.bizPropertys = bizPropertys;
	}
	public List<BizPicture> getBizPictures() {
		return bizPictures;
	}
	public void setBizPictures(List<BizPicture> bizPictures) {
		this.bizPictures = bizPictures;
	}
	public List<BizPanorama> getBizPanoramas() {
		return bizPanoramas;
	}
	public void setBizPanoramas(List<BizPanorama> bizPanoramas) {
		this.bizPanoramas = bizPanoramas;
	}
	public List<BizVideo> getBizVideos() {
		return bizVideos;
	}
	public void setBizVideos(List<BizVideo> bizVideos) {
		this.bizVideos = bizVideos;
	}
	
}
