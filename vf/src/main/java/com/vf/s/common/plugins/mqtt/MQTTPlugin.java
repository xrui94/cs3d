package com.vf.s.common.plugins.mqtt;

import java.util.List;
import java.util.UUID;

import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import com.jfinal.plugin.IPlugin;
import com.vf.s.common.model.biz.BizMqtt;

public class MQTTPlugin implements IPlugin{
    
    private static final String clientid =UUID.randomUUID().toString().toUpperCase();
    private MqttClient client;
    private MqttConnectOptions options;
    private BizMqtt bizMqtt;
    public MQTTPlugin(BizMqtt bizMqtt) {
    	this.bizMqtt=bizMqtt;
    }
	
	@Override
	public boolean start() {
        try {
            // host为主机名，clientid即连接MQTT的客户端ID，一般以唯一标识符表示，MemoryPersistence设置clientid的保存形式，默认为以内存保存
            client = new MqttClient(this.bizMqtt.getHost(), clientid, new MemoryPersistence());
            // MQTT的连接设置
            options = new MqttConnectOptions();
            // 设置是否清空session,这里如果设置为false表示服务器会保留客户端的连接记录，这里设置为true表示每次连接到服务器都以新的身份连接
            options.setCleanSession(true);
            // 设置连接的用户名
          //  options.setUserName(userName);
            // 设置连接的密码
           // options.setPassword(passWord.toCharArray());
            // 设置超时时间 单位为秒
            options.setConnectionTimeout(10);
            // 设置会话心跳时间 单位为秒 服务器会每隔1.5*20秒的时间向客户端发送个消息判断客户端是否在线，但这个方法并没有重连的机制
            options.setKeepAliveInterval(20);
            // 设置回调
            client.setCallback(new PushCallback());
            
          //  MqttTopic topic = client.getTopic(TOPIC);
            //setWill方法，如果项目中需要知道客户端是否掉线可以调用该方法。设置最终端口的通知消息
          //  options.setWill(topic, "close".getBytes(), 2, true);

            client.connect(options);
            String[] topic1 =getTopics();
            //订阅消息
            int[] Qos  = {1};
            client.subscribe(topic1, Qos);
            return false;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
		
	}

	@Override
	public boolean stop() {
		if(client!=null) {
			if(client.isConnected()) {
				try {
					client.disconnect();
					client.close();
				} catch (MqttException e) {
					e.printStackTrace();
				}
			}
		}
		return false;
	}
	
	private String[] getTopics() {
		List<BizMqtt> list=BizMqtt.dao.find("SELECT * FROM "+BizMqtt.TABLE_NAME+" WHERE PARENTID=? ",this.bizMqtt.getId());
		//List<String> topics=new ArrayList<String>();
		String[] topics=new String[list.size()];
		for(int i=0;i<list.size();i++) {
			topics[i]=list.get(i).getCode();
		}
		return topics;
	}

}
