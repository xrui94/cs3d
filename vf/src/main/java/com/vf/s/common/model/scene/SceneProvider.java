package com.vf.s.common.model.scene;

import java.util.List;

import com.vf.s.common.model.biz.Provider;
import com.vf.s.common.model.scene.base.BaseSceneProvider;

/**
 * Generated by JFinal.
 */
@SuppressWarnings("serial")
public class SceneProvider extends BaseSceneProvider<SceneProvider> {
	public static final SceneProvider dao = new SceneProvider().dao();
	public static final String TABLE_NAME = "BIZ_SCENE_PROVIDER";
	
	
	private List<Provider> providers;
	public List<Provider> getProviders() {
		if (super.get("providers") == null) {
			super.put("providers", this.providers);
		}
		return super.get("providers");
	}
	public void setProviders(List<Provider> providers) {
		super.put("providers", providers);
	}
	
	

}
