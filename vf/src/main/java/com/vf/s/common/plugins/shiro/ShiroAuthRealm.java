package com.vf.s.common.plugins.shiro;




import java.io.UnsupportedEncodingException;
import java.util.Base64;

import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authc.credential.DefaultPasswordService;
import org.apache.shiro.authc.credential.PasswordService;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.cache.Cache;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.subject.SimplePrincipalCollection;

import com.jfinal.kit.StrKit;
import com.jfinal.log.Log;
import com.vf.s.common.model.sys.SysUser;
public class ShiroAuthRealm extends AuthorizingRealm {

	protected final Log LOG = Log.getLog(getClass());

	

	public ShiroAuthRealm() {
		setAuthenticationTokenClass(UsernamePasswordToken.class);
	}

	/**
	 * 认证回调函数,登录时调用.
	 */
	@Override
	protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) {
		UsernamePasswordToken authcToken = (UsernamePasswordToken) token;
		String username = authcToken.getUsername();
		if (StrKit.isBlank(username)) {
			throw new UnknownAccountException("账号不可以为空!");
		}
		
		String password = String.valueOf(authcToken.getPassword());
		byte[] bytes = Base64.getDecoder().decode(username);
		byte[] bytes1 = Base64.getDecoder().decode(password);
	    try {
	    	username=new String(bytes, "UTF-8");
	    	password=new String(bytes1, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			throw new UnknownAccountException("账号密码请加密传输!");
		}
		
		SysUser sysAccount = SysUser.dao.findUserByUsername(username);
		if (null == sysAccount) {
			throw new UnknownAccountException("用户名或密码错误!");
		} else {
			LOG.debug("验证密码.");
			PasswordService svc = new DefaultPasswordService();
			try {
				if (svc.passwordsMatch(password, sysAccount.getPASSWORD())) {
					try {
						sysAccount.setLOGINTIMES(0);
						sysAccount.update();
					}catch (Exception e) {
					}
					return new SimpleAuthenticationInfo(
							new SimpleUser(sysAccount.getID(), sysAccount.getUSERNAME(), sysAccount.getNAME()),String.valueOf(authcToken.getPassword()), getName());
				} else {
					if(sysAccount.getLOGINTIMES()>=sysAccount.getERRORTIMES()) {
						throw new AuthenticationException("错误尝试次数过多，账户已锁定，请联系管理员解锁或24小时后再次尝试!");
					}else {
						try {
							sysAccount.setLOGINTIMES(sysAccount.getLOGINTIMES()+1);
							sysAccount.update();
						}catch (Exception e) {
						}
						throw new AuthenticationException("用户名或密码错误!");
					}
				}
			}catch (Exception e) {
				throw new UnknownAccountException("用户名或密码错误!");
			}
		}
	}

	/**
	 * 授权查询回调函数, 进行鉴权但缓存中无用户的授权信息时调用.
	 */
	@Override
	protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
		SimpleUser simpleUser = (SimpleUser) principals.fromRealm(getName()).iterator().next();
		SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();
		if (null == simpleUser) {
			return info;
		}
		return info;
	}

	/**
	 * 更新用户授权信息缓存.
	 */
	public void clearCachedAuthorizationInfo(String principal) {
		SimplePrincipalCollection principals = new SimplePrincipalCollection(principal, getName());
		clearCachedAuthorizationInfo(principals);
	}

	/**
	 * 清除所有用户授权信息缓存.
	 */
	public void clearAllCachedAuthorizationInfo() {
		Cache<Object, AuthorizationInfo> cache = getAuthorizationCache();
		if (cache != null) {
			for (Object key : cache.keys()) {
				cache.remove(key);
			}
		}
	}

}
