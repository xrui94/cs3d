package com.vf.s.common.model.biz.base;

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.IBean;

/**
 * Generated by JFinal, do not modify this file.
 */
@SuppressWarnings({"serial", "unchecked"})
public abstract class BaseBizText<M extends BaseBizText<M>> extends Model<M> implements IBean {

	public M setId(java.lang.String id) {
		set("id", id);
		return (M)this;
	}
	
	public java.lang.String getId() {
		return getStr("id");
	}

	public M setSceneId(java.lang.String sceneId) {
		set("sceneId", sceneId);
		return (M)this;
	}
	
	public java.lang.String getSceneId() {
		return getStr("sceneId");
	}

	public M setObjId(java.lang.String objId) {
		set("objId", objId);
		return (M)this;
	}
	
	public java.lang.String getObjId() {
		return getStr("objId");
	}

	public M setName(java.lang.String name) {
		set("name", name);
		return (M)this;
	}
	
	public java.lang.String getName() {
		return getStr("name");
	}

	public M setDesc(java.lang.String desc) {
		set("desc", desc);
		return (M)this;
	}
	
	public java.lang.String getDesc() {
		return getStr("desc");
	}

	public M setCreateTime(java.util.Date createTime) {
		set("createTime", createTime);
		return (M)this;
	}
	
	public java.util.Date getCreateTime() {
		return get("createTime");
	}

	public M setEvent(java.lang.String event) {
		set("event", event);
		return (M)this;
	}
	
	public java.lang.String getEvent() {
		return getStr("event");
	}

}
