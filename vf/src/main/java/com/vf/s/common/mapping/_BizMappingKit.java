package com.vf.s.common.mapping;

import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.vf.s.common.model.biz.*;

public class _BizMappingKit {
	public static void mapping(ActiveRecordPlugin arp) {
		
		arp.addMapping(Provider.TABLE_NAME, "id", Provider.class);
		arp.addMapping(VideoClassify.TABLE_NAME, "id", VideoClassify.class);
		arp.addMapping(IconClassify.TABLE_NAME, "id", IconClassify.class);
		
		
		arp.addMapping(BizLayer.TABLE_NAME, "id", BizLayer.class);
		arp.addMapping(BizLayerGroup.TABLE_NAME, "id", BizLayerGroup.class);
		arp.addMapping(BizStylePoint.TABLE_NAME, "id", BizStylePoint.class);
		arp.addMapping(BizStyleLine.TABLE_NAME, "id", BizStyleLine.class);
		arp.addMapping(BizStylePolygon.TABLE_NAME, "id", BizStylePolygon.class);
		arp.addMapping(BizIcon.TABLE_NAME, "id", BizIcon.class);
		arp.addMapping(BizModel.TABLE_NAME, "id", BizModel.class);
		arp.addMapping(BizModelClassify.TABLE_NAME, "id", BizModelClassify.class);
		
		
		arp.addMapping(Point.TABLE_NAME, "id", Point.class);
		arp.addMapping(BizLine.TABLE_NAME, "id", BizLine.class);
		arp.addMapping(BizPolygon.TABLE_NAME, "id", BizPolygon.class);
		arp.addMapping(BizFlight.TABLE_NAME, "id", BizFlight.class);
		arp.addMapping(BizDemand.TABLE_NAME, "id", BizDemand.class);
		arp.addMapping(BizVideo.TABLE_NAME, "id", BizVideo.class);
		arp.addMapping(BizPanorama.TABLE_NAME, "id", BizPanorama.class);
		arp.addMapping(BizPicture.TABLE_NAME, "id", BizPicture.class);
		arp.addMapping(BizArticle.TABLE_NAME, "id", BizArticle.class);
		
		arp.addMapping(BizProperty.TABLE_NAME, "id", BizProperty.class);
		arp.addMapping(BizKml.TABLE_NAME, "id", BizKml.class);
		arp.addMapping(BizEvent.TABLE_NAME, "id", BizEvent.class);
		arp.addMapping(BizText.TABLE_NAME, "id", BizText.class);
		arp.addMapping(BizPage.TABLE_NAME, "id", BizPage.class);
		arp.addMapping(BizView.TABLE_NAME, "id", BizView.class);
		arp.addMapping(BizMqtt.TABLE_NAME, "id", BizMqtt.class);
		arp.addMapping(BizWsAuth.TABLE_NAME, "id", BizWsAuth.class);
		arp.addMapping(BizWsCapacity.TABLE_NAME, "id", BizWsCapacity.class);
		arp.addMapping(BizWsLog.TABLE_NAME, "id", BizWsLog.class);
		

		arp.addMapping(BizVideoPlane.TABLE_NAME, "id", BizVideoPlane.class);
		arp.addMapping(BizVideoShed3d.TABLE_NAME, "id", BizVideoShed3d.class);
		arp.addMapping(BizVideoModel.TABLE_NAME, "id", BizVideoModel.class);
		arp.addMapping(BizStylePointPosition.TABLE_NAME, "id", BizStylePointPosition.class);
		
		arp.addMapping(BizVector.TABLE_NAME, "id", BizVector.class);
		
	}
}
