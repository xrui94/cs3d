package com.vf.s.common.primitive;

import com.vf.s.common.model.biz.Point;
import com.vf.s.common.model.biz.BizStylePoint;

public class PointPrimitive {
	
	private Point point;
	private BizStylePoint style;

	public Point getPoint() {
		return point;
	}

	public void setPoint(Point point) {
		this.point = point;
	}

	public BizStylePoint getStyle() {
		return style;
	}

	public void setStyle(BizStylePoint style) {
		this.style = style;
	}

}
