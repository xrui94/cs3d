package com.vf.s.common.model.biz;

import com.vf.s.common.model.biz.base.BaseBizWsAuth;

/**
 * Generated by JFinal.
 */
@SuppressWarnings("serial")
public class BizWsAuth extends BaseBizWsAuth<BizWsAuth> {
	public static final BizWsAuth dao = new BizWsAuth().dao();
	public static final String TABLE_NAME = "BIZ_WS_AUTH";

}
