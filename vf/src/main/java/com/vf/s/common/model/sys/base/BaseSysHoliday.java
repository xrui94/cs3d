package com.vf.s.common.model.sys.base;

import com.jfinal.plugin.activerecord.Model;

import java.util.List;

import com.jfinal.plugin.activerecord.IBean;

/**
 * Generated by JFinal, do not modify this file.
 */
@SuppressWarnings({"serial", "unchecked"})
public abstract class BaseSysHoliday<M extends BaseSysHoliday<M>> extends Model<M> implements IBean {

	private List<M> children;

	public List<M> getChildren() {
		if (super.get("children") == null) {
			super.put("children", this.children);
		}
		return super.get("children");
	}
	public void setChildren(List<M> children) {
		super.put("children", children);
	}
	
	public M setID(java.lang.String ID) {
		set("ID", ID);
		return (M)this;
	}
	
	public java.lang.String getID() {
		return getStr("ID");
	}

	public M setPARENTID(java.lang.String PARENTID) {
		set("PARENTID", PARENTID);
		return (M)this;
	}
	
	public java.lang.String getPARENTID() {
		return getStr("PARENTID");
	}

	public M setNAME(java.lang.String NAME) {
		set("NAME", NAME);
		return (M)this;
	}
	
	public java.lang.String getNAME() {
		return getStr("NAME");
	}

	public M setYEAR(java.lang.String YEAR) {
		set("YEAR", YEAR);
		return (M)this;
	}
	
	public java.lang.String getYEAR() {
		return getStr("YEAR");
	}

	public M setTYPE(java.lang.String TYPE) {
		set("TYPE", TYPE);
		return (M)this;
	}
	
	public java.lang.String getTYPE() {
		return getStr("TYPE");
	}

	public M setSTARTTIME(java.util.Date STARTTIME) {
		set("STARTTIME", STARTTIME);
		return (M)this;
	}
	
	public java.util.Date getSTARTTIME() {
		return get("STARTTIME");
	}

	public M setENDTIME(java.util.Date ENDTIME) {
		set("ENDTIME", ENDTIME);
		return (M)this;
	}
	
	public java.util.Date getENDTIME() {
		return get("ENDTIME");
	}

}
