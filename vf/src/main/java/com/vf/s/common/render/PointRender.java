package com.vf.s.common.render;

import java.util.List;

import com.vf.s.common.model.biz.Point;
import com.vf.s.common.model.biz.BizStylePoint;

public class PointRender {
	private Point point;
	private PropertyRender property;
	private List<BizStylePoint> styles;
	public Point getPoint() {
		return point;
	}
	public void setPoint(Point point) {
		this.point = point;
	}
	public List<BizStylePoint> getStyles() {
		return styles;
	}
	public void setStyles(List<BizStylePoint> styles) {
		this.styles = styles;
	}
	public PropertyRender getProperty() {
		return property;
	}
	public void setProperty(PropertyRender property) {
		this.property = property;
	}
	
	
	
}
