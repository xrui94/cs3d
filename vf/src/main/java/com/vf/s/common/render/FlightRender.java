package com.vf.s.common.render;


import com.vf.s.common.model.biz.BizFlight;

public class FlightRender {
	private BizFlight flight;
	private PointRender point;
	public BizFlight getFlight() {
		return flight;
	}
	public void setFlight(BizFlight flight) {
		this.flight = flight;
	}
	public PointRender getPoint() {
		return point;
	}
	public void setPoint(PointRender point) {
		this.point = point;
	}
	
	
	
}
