/**
 * @开发版权 云南赢中吉洋智能技术有限公司（YNYZ）
 * @项目名称 vfcore
 * @版本信息 v1.0
 * @开发人员 zhous
 * @开发日期 2019-09-23
 * @描述  CaptchaFormAuthenticationInterceptor
 * @修改记录:
 * 日期                          作者                       版本                             描述
 * -------------------------------------------------------------
 * 2019年9月24日      zhous       V1.0.0        create 
 */
package com.vf.s.common.plugins.shiro;

import javax.servlet.http.HttpServletRequest;

import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.web.filter.authc.FormAuthenticationFilter;
import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;

public class ShiroFormAuthenticationInterceptor extends FormAuthenticationFilter implements Interceptor {



    protected AuthenticationToken createToken(HttpServletRequest request) {
        String username = getUsername(request);
        String password = getPassword(request);
        return new UsernamePasswordToken(username, password);
    }
    
    public void intercept(Invocation ai) {
        HttpServletRequest request = ai.getController().getRequest();
        AuthenticationToken authenticationToken = createToken(request);
        request.setAttribute("shiroToken", authenticationToken);
        ai.invoke();
    }

}
