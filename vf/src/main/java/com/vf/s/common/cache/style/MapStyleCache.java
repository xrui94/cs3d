package com.vf.s.common.cache.style;

import java.util.HashMap;
import java.util.Map;

public class MapStyleCache {
	private static Map<String, Object> MAP_STYLE = new HashMap<String, Object>();

	public static void put(String key, Object stylePoint) {
		MAP_STYLE.put(key, stylePoint);
	}

	public static Object get(String key) {
		return MAP_STYLE.get(key);
	}

	public static void remove(String key) {
		MAP_STYLE.remove(key);
	}
	public static Map<String, Object> gets() {
		return MAP_STYLE;
	}

}
