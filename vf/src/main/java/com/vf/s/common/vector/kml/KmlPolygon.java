package com.vf.s.common.vector.kml;

import de.micromata.opengis.kml.v_2_2_0.Coordinate;
import de.micromata.opengis.kml.v_2_2_0.Placemark;

import java.util.List;

/**
 * @program: ParseKMLForJava
 * @description:
 * @author: Mr.Yue
 * @create: 2018-12-04 21:12
 **/
public class KmlPolygon {
	private List<Coordinate> points;
	private Placemark placemark;

	public Placemark getPlacemark() {
		return placemark;
	}

	public void setPlacemark(Placemark placemark) {
		this.placemark = placemark;
	}

	public List<Coordinate> getPoints() {
		return points;
	}

	public void setPoints(List<Coordinate> points) {
		this.points = points;
	}
}