package com.vf.s.common.mapping;

import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.vf.s.common.model.sys.*;

public class _SysMappingKit {
	public static void mapping(ActiveRecordPlugin arp) {
		arp.addMapping(SysDic.TABLE_NAME, "ID", SysDic.class);
		arp.addMapping("sys_dic_item", "ID", SysDicItem.class);
		arp.addMapping("sys_log", "id", SysLog.class);
		arp.addMapping("sys_menu", "ID", SysMenu.class);
		arp.addMapping("sys_org", "ID", SysOrg.class);
		arp.addMapping("sys_role", "ID", SysRole.class);
		arp.addMapping("sys_role_permissions", "ID", SysRolePermissions.class);
		arp.addMapping("sys_user", "ID", SysUser.class);
		arp.addMapping("sys_user_role", "ID", SysUserRole.class);
		arp.addMapping(SysRegion.TABLE_NAME, "ID", SysRegion.class);
		arp.addMapping(SysCron4j.TABLE_NAME, "id", SysCron4j.class);
	}
}
