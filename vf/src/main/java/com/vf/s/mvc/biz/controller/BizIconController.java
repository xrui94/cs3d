package com.vf.s.mvc.biz.controller;

import java.io.File;
import java.util.Date;
import java.util.List;

import javax.imageio.ImageIO;

import com.jfinal.kit.PathKit;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.upload.UploadFile;
import com.vf.core.controller.BaseController;
import com.vf.core.util.UuidUtil;
import com.vf.s.common.model.biz.BizIcon;
import com.vf.s.common.model.biz.IconClassify;
import com.vf.s.common.plugins.shiro.ShiroKit;
import com.vf.s.common.plugins.shiro.SimpleUser;

public class BizIconController extends BaseController {
	
	public void index() {
		this.render("list.html");
	}

	public void listData() {
		int pageNumber = getParaToInt("page", 1);
		int pageSize = getParaToInt("limit", 10);
		String name = getPara("name");
		String classifyId = getPara("classifyId");
		String isPublic = getPara("isPublic");
		
		String userId="";
		SimpleUser simpleUser = ShiroKit.getLoginUser();
		if (simpleUser != null) {
			userId=simpleUser.getId();
		}
		
		String sqlExceptSelect = " from " + BizIcon.TABLE_NAME + " o  where 1=1 ";
		
		if(StrKit.equals("1",isPublic)) {
			sqlExceptSelect += " and o.createUserId = '" + userId + "'   ";	
		}
		else if(StrKit.equals("0",isPublic)) {
			sqlExceptSelect += " and o.isPublic = '0'   ";	
		}else {
			if(!StrKit.equals(simpleUser.getUsername(),"system")) {
				sqlExceptSelect += " and ( o.createUserId = '" + userId + "' or  o.isPublic = '0' ) ";
			}
		}
		
		if (!StrKit.isBlank(name)) {
			sqlExceptSelect += " and o.name like '%" + name + "%'  ";
		}
		if (!StrKit.isBlank(classifyId)) {
			sqlExceptSelect += " and o.classifyId = '"+ classifyId + "'  ";
		}
		sqlExceptSelect += "  order by   o.createTime desc ";
		Page<BizIcon> page = BizIcon.dao.paginate(pageNumber, pageSize, "select o.*,(select NAME from "+IconClassify.TABLE_NAME+" where id=o.classifyId) classifyName ", sqlExceptSelect);
		renderLayuiPage(0, page.getList(), "", page.getTotalRow());
	}

	public void allList() {
		List<BizIcon> BizIcons = BizIcon.dao.findAll();
		if(BizIcons!=null && BizIcons.size()>0) {
			renderSuccess("查询成功！", BizIcons);
		}else {
			renderError("无数据！");
		}
	}
	
	public void save() {
		BizIcon model = getModel(BizIcon.class);
		if (StrKit.notBlank(model.getId())) {
			if (model.update()) {
				renderSuccess("更新成功！", model);
			} else {
				renderError("更新失败！");
			}
		} else {
			model.setId(UuidUtil.getUUID());
			if (model.save()) {
				renderSuccess("保存成功！", model);
			} else {
				renderError("保存失败！");
			}
		}
	}
	
	/***
	 * 删除
	 * @throws Exception
	 */
	public void delete() {
		BizIcon model = BizIcon.dao.findById(this.getPara("id"));
		if (model != null) {
			if (model.delete()) {
				renderSuccess("删除成功！");
			} else {
				renderError("删除失败！");
			}
		} else {
			renderError("数据不存在,请刷新后再试!");
		}
	}

	private String rootPath = PathKit.getWebRootPath();
	public void upload() {
		UploadFile uploadFile = getFile();
		try {
			
			/*
			 * String classifyId = getPara("classifyId");
			 * 
			 * if(StrKit.isBlank(classifyId)) { renderError("上传失败，图标类型不能为空!"); return; }
			 */
			
			SimpleUser simpleUser = ShiroKit.getLoginUser();
			
			String path = "/upload/icon" + "/" + uploadFile.getFileName().replace(" ", "");
			File file = new File(rootPath+path);
			if (!file.getParentFile().exists()) {
				file.getParentFile().mkdirs();
			}
			if (file.exists()) {
				file.delete();
			}
			java.awt.image.BufferedImage image = ImageIO.read(uploadFile.getFile());
			double width = image.getWidth();
			double height = image.getHeight();
			uploadFile.getFile().renameTo(file);
			BizIcon bizIcon = new BizIcon();
			bizIcon.setId(UuidUtil.getUUID());
			bizIcon.setName(uploadFile.getFileName());
			bizIcon.setCreateTime(new Date());
			bizIcon.setWidth(width);
			bizIcon.setHeight(height);
			bizIcon.setUrl(path);
			bizIcon.setClassifyId(getPara("classifyId"));
			bizIcon.setCreateTime(new Date(System.currentTimeMillis()));
			bizIcon.setCreateUserId(simpleUser==null?"":simpleUser.getId());
			bizIcon.setCreateUserName(simpleUser==null?"":simpleUser.getId());
			if(bizIcon.save()) {
				renderSuccess("上传成功！",bizIcon);
			}else {
				renderError("上传失败！");
			}
		} catch (Exception e) {
			renderError(e.getMessage());
		}
	}

}
