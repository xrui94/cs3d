package com.vf.s.mvc.biz.controller;



import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Supplier;

import com.vf.core.controller.BaseController;
import com.vf.s.common.model.biz.BizLayer;
import com.vf.s.common.model.biz.Point;

public class BizPointStyleController extends BaseController {
	
	ExecutorService executor = Executors.newFixedThreadPool(20);
	public void saveForLayer() throws Exception {
		String layerId=this.getPara("layerId");
		renderSuccess("异步处理中！！！");
	    CompletableFuture<Integer> future = CompletableFuture.supplyAsync(new Supplier<Integer>() {
	        @Override
	        public Integer get() {
	            try {
	        		List<BizLayer> bizLayers=BizLayer.dao.find("SELECT * FROM "+BizLayer.TABLE_NAME+" WHERE PARENTID=? AND TYPE='point' ",layerId);
	        		Point bizPoint;
	        		for(BizLayer bizLayer:bizLayers) {
	        			bizPoint=Point.dao.findById(bizLayer.getId());
	        			if(bizPoint!=null) {
	        				bizPoint.update();
	        			}
	        		}
	            } catch (Exception e) {
	                e.printStackTrace();
	            }
	            return 3;
	        }
	    }, executor);
	    future.thenAccept(e -> System.out.println(e));
	}
}
