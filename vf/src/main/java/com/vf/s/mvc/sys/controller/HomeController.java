package com.vf.s.mvc.sys.controller;

import java.util.List;

import com.jfinal.aop.Inject;
import com.jfinal.kit.PropKit;
import com.jfinal.log.Log;
import com.vf.core.controller.BaseController;
import com.vf.s.common.model.sys.SysMenu;
import com.vf.s.common.plugins.shiro.ShiroKit;
import com.vf.s.mvc.sys.service.MenuService;

public class HomeController extends BaseController{
	
	protected final Log LOG = Log.getLog(getClass());

	@Inject
	private MenuService srv;
	
	protected String rtcSerUrl=PropKit.use("app-config-dev.txt").get("rtcSerUrl");
	
	public void index() {
		this.setAttr("user", ShiroKit.getLoginUser());
		this.setAttr("rtcSerUrl",rtcSerUrl);
		render("index.html");
	}
	
	public void menus() {
		List<SysMenu> menuList=srv.getMenusForUser();
		if(menuList!=null&& menuList.size()>0) {
			renderSuccess("成功", menuList);
		}else {
			this.renderError("暂无菜单！");
		}
	}

}
