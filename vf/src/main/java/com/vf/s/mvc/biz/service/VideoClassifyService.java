package com.vf.s.mvc.biz.service;

import java.util.ArrayList;
import java.util.List;

import com.jfinal.core.Controller;
import com.jfinal.log.Log;
import com.vf.core.model.ZtreeNode;
import com.vf.core.render.RenderLayuiBean;
import com.vf.s.common.model.biz.VideoClassify;

public class VideoClassifyService {

	protected final Log LOG = Log.getLog(getClass());

	public List<VideoClassify> getMenusForUser() {
		return getChildrenAllTree("root");
	}

	public List<VideoClassify> getChildrenAllTree(String pId) {
		List<VideoClassify> list = getChildrenByPid(pId);
		for (VideoClassify o : list) {
			o.setChildren(getChildrenAllTree(o.getId()));
		}
		return list;
	}

	/***
	 * 根据id 查询孩子
	 * 
	 * @param id
	 * @return
	 */
	public List<VideoClassify> getChildrenByPid(String id) {
		String sql = "SELECT * FROM "+VideoClassify.TABLE_NAME+" M WHERE M.PARENTID='" + id + "'  ";
		sql = sql + " ORDER BY M.SORT";
		return VideoClassify.dao.find(sql);
	}

	public RenderLayuiBean listData(Controller menuController) {
		List<VideoClassify> list = VideoClassify.dao.find(
				"SELECT T.*, (CASE  WHEN (SELECT COUNT(ID) AS COUNT FROM "+VideoClassify.TABLE_NAME+" WHERE PARENTID=T.ID)>0 THEN TRUE ELSE  FALSE END ) AS HAVECHILD   FROM "+VideoClassify.TABLE_NAME+"  T  ORDER BY T.SORT ASC");
		RenderLayuiBean renderBean = new RenderLayuiBean();
		renderBean.setCode(0);
		renderBean.setMsg("查询成功");
		renderBean.setData(list);
		renderBean.setCount(list.size());
		return renderBean;
	}

	/***
	 * 菜单转成ZTreeNode
	 * @param olist 数据 open 是否展开所有 ifOnlyLeaf 是否只选叶子
	 * @return
	 */
	public List<ZtreeNode> toZTreeNode(List<VideoClassify> olist, Boolean open, Boolean ifOnlyLeaf) {
		List<ZtreeNode> list = new ArrayList<ZtreeNode>();
		for (VideoClassify o : olist) {
			ZtreeNode node = toZTreeNode(o);
			if (o.getChildren() != null && o.getChildren().size() > 0) {// 如果有孩子
				node.setChildren(toZTreeNode(o.getChildren(), open, ifOnlyLeaf));
				if (ifOnlyLeaf) {// 如果只选叶子
					node.setNocheck(true);
				}
				node.setIsParent(true);
			}
			node.setOpen(open);
			list.add(node);
		}
		return list;
	}

	public ZtreeNode toZTreeNode(VideoClassify org) {
		ZtreeNode node = new ZtreeNode();
		node.setId(org.getId());
		node.setName(org.getName());
		node.setParentId(org.getParentId());
		return node;
	}


}
