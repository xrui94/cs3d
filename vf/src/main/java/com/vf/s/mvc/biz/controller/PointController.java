package com.vf.s.mvc.biz.controller;



import com.jfinal.aop.Inject;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.vf.core.controller.BaseController;
import com.vf.s.common.model.biz.BizPanorama;
import com.vf.s.common.model.biz.Point;
import com.vf.s.common.model.biz.BizVideo;
import com.vf.s.common.primitive.PointPrimitive;
import com.vf.s.mvc.biz.service.PointService;
import com.vf.s.mvc.biz.service.BizPropertyService;

public class PointController extends BaseController {
	
	@Inject
	private PointService srv;
	
	@Inject
	private BizPropertyService propertyService;
	
	public void list() {
		render("list.html");
	}
	
	public void index() {
		this.set("sceneId", this.get("sceneId"));
		Point point=Point.dao.findById(get("pointId"));
		this.set("Point", point);
		render("list.html");
	}
	public void operation() {
		this.set("pointId", this.get("pointId"));
		this.set("Point",Point.dao.findById(get("pointId")));
		render("operation.html");
	}
	
	/**
	 * 编辑点信息
	 */
	public void edit() {
		this.set("Point", Point.dao.findById(get("pointId")));
		render("edit.html");
	}
	
	
	public void listData() {
		int pageNumber = getParaToInt("page", 1);
		int pageSize = getParaToInt("limit", 10);
		String name = getPara("name");
		String layerId = getPara("layerId");
		
		String select = " select *  ";
		String sqlExceptSelect = " from " + Point.TABLE_NAME + " o  where 1=1 ";
		if (!StrKit.isBlank(name)) {
			sqlExceptSelect += " and o.name like '%" + name + "%'  ";
		}
		
		if (!StrKit.isBlank(layerId)) {
			sqlExceptSelect += " and o.layerId = '" + layerId + "'  ";
		}
		Page<Point> page = Point.dao.paginate(pageNumber, pageSize, select, sqlExceptSelect);
		renderLayuiPage(0, page.getList(), "", page.getTotalRow());
	}
	

	
	public void pointForProVideo() {
		String id = this.get("id");
		if(!StrKit.isBlank(id)) {
			this.set("bizVideo", BizVideo.dao.findById(id));
		}
		render("pointForProVideo.html");
	}
	
	public void pointForProVSrc() {
		render("pointForProVSrc.html");
	}
	
	public void pointForProImgSrc() {
		render("pointForProImgSrc.html");
	}
	
	public void pointForProPanorama() {
		String id = this.get("id");
		if(!StrKit.isBlank(id)) {
			this.set("bizPanorama", BizPanorama.dao.findById(id));
		}
		this.set("pointId", this.get("pointId"));
		render("pointForProPanorama.html");
	}
	
	public void pointForPropertyPSrc() {
		String id = this.get("id");
		if(!StrKit.isBlank(id)) {
			this.set("bizVideo", BizVideo.dao.findById(id));
		}
		this.set("pointId", this.get("pointId"));
		render("pointForPropertyPSrc.html");
	}
	
	public void delete() throws Exception {
		String id = getPara("id");
		if (Point.dao.deleteById(id)) {
			renderSuccess("删除成功！");
		} else {
			renderError("删除失败！");
		}
	}

	
	public void getById() throws Exception {
		String id = getPara("id");
		Point point=Point.dao.findById(id);
		if (point!=null) {
			renderSuccess("成功！",point);
		} else {
			renderError("数据已经不存在！");
		}
	}
	
	/**
	 * 点操作菜单
	 */
	public void pointForMenu() {
		this.set("sceneId", this.get("sceneId"));
		Point point=Point.dao.findById(get("pointId"));
		this.set("Point", point);
		render("pointForMenu.html");
	}
	
	/**
	 * 保存点
	 */
	public void save() {
		Point model = getModel(Point.class);
		Point dbPoint=Point.dao.findById(model.getId());
		if(dbPoint!=null) {
			if (model.update() ) {
				if(model.getX()!=null && model.getY()!=null) {
					Db.update("update "+Point.TABLE_NAME+" set location=POINT('"+model.getX()+"','"+model.getY()+"')  where id=? ",model.getId());
				}
				renderSuccess("更新成功！",Point.dao.findById(model.getId()));
			} else {
				renderError("更新失败！");
			}
		}else {
			if(model.save()) {
				if(model.getX()!=null && model.getY()!=null) {
					Db.update("update "+Point.TABLE_NAME+" set location=POINT('"+model.getX()+"','"+model.getY()+"')  where id=? ",model.getId());
				}
				renderSuccess("保存成功！",Point.dao.findById(model.getId()));
			} else {
				renderError("保存失败！");
			}
		}
	}
	
	/**
	 * 点样式
	 */
	public void pointForStyle() {
		this.set("sceneId", this.get("sceneId"));
		Point point=Point.dao.findById(get("pointId"));
		this.set("Point", point);
		render("pointForStyle.html");
	}
	
	public void setStyle() {
		Point model = getModel(Point.class);
		if(model!=null ) {
			if (model.update()) {
				PointPrimitive pointPrimitive=new PointPrimitive();
				pointPrimitive.setPoint(Point.dao.findById(model.getId()));
				renderSuccess("设置成功！",pointPrimitive);
			} else {
				renderError("设置失败！");
			}
		}else {
			renderError("点已不存在！");
		}
	}
	
	
	/***
	 * 保存样式
	 */
	
	public void saveForStyle() {
		Point model = getModel(Point.class);
		Point dbPoint=Point.dao.findById(model.getId());
		if(dbPoint!=null) {
			if (model.update()) {
				renderSuccess("更新成功！",model);
			} else {
				renderError("更新失败！");
			}
		}
		
//		String pointId=this.getPara("pointId");
//		String styleId=this.getPara("styleId");
//		String event=this.getPara("event");
//		Point Point=Point.dao.findById(pointId);
//		if(Point!=null ) {
//			if(StrKit.equals(event, "DEFAULT")) {
//				Point.setDefaultStyleId(styleId);
//			}
//			else  if(StrKit.equals(event, "HOVER")) {
//				Point.setHoverStyleId(styleId);
//			}
//			else  if(StrKit.equals(event, "SELECTED")) {
//				Point.setSelectedStyleId(styleId);
//			}else {
//				Point.setDefaultStyleId(styleId);
//			}
//			if (Point.update()) {
//				renderSuccess("成功！",Point);
//			} else {
//				renderError("失败！");
//			}
//		}else {
//			renderError("点或样式不存在！");
//		}
	}
	
	
	
	
	
}
