package com.vf.s.mvc.biz.controller;

import com.jfinal.kit.PropKit;

import java.util.List;

import com.jfinal.aop.Before;
import com.jfinal.aop.Inject;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.tx.Tx;
import com.vf.core.controller.BaseController;
import com.vf.core.util.UuidUtil;
import com.vf.s.common.model.biz.BizLayerGroup;
import com.vf.s.common.model.scene.Scene;
import com.vf.s.common.plugins.shiro.ShiroKit;
import com.vf.s.common.plugins.shiro.SimpleUser;
import com.vf.s.mvc.biz.service.BizLineService;
import com.vf.s.mvc.biz.service.PointService;
import com.vf.s.mvc.biz.service.BizPolygonService;

public class BizSceneContoller  extends BaseController{
	
	protected String rtcSerUrl=PropKit.use("app-config-dev.txt").get("rtcSerUrl");
	
	
	@Inject
	private PointService srvForBizPoint;
	
	@Inject
	private BizPolygonService srvForBizPolygon;
	@Inject
	private BizLineService srvForBizLine;
	
	public void index() {
		List<BizLayerGroup> items=BizLayerGroup.dao.find("SELECT * FROM "+BizLayerGroup.TABLE_NAME);
		this.setAttr("groups", items);
		render("list.html");
	}
	
	public void editor() {
		this.setAttr("scene", Scene.dao.findById(getPara("id")));
		render("editor.html");
	}
	
	public void listData() {
		int pageNumber = getParaToInt("page", 1);
		int pageSize = getParaToInt("limit", 10);
		String keyword = getPara("keyword");
		String isPublic = getPara("isPublic");
		
		String userId="";
		SimpleUser simpleUser = ShiroKit.getLoginUser();
		if (simpleUser != null && StrKit.equals(simpleUser.getUsername(),"system")) {
			userId=simpleUser.getId();
		}

		String sqlExceptSelect = " FROM " + Scene.TABLE_NAME + " o  WHERE 1=1 ";
		if (!StrKit.isBlank(keyword)) {
			sqlExceptSelect += " AND ( O.NAME LIKE '%" + keyword + "%' OR   O.CODE '%" + keyword + "%' ) ";
		}
		
		if(StrKit.equals("1",isPublic)) {
			sqlExceptSelect += " and o.createUserId = '" + userId + "'   ";	
		}
		else if(StrKit.equals("0",isPublic)) {
			sqlExceptSelect += " and o.isPublic = '0'   ";	
		}else {
			sqlExceptSelect += " and ( o.createUserId = '" + userId + "' or  o.isPublic = '0' ) ";	
		}
		
		sqlExceptSelect += "  ORDER BY   O.SORT ASC ";
		Page<Scene> page = Scene.dao.paginate(pageNumber, pageSize, "select * ", sqlExceptSelect);
		renderLayuiPage(0, page.getList(), "", page.getTotalRow());
	}
	
	/***
	 * 保存
	 */
	public void save() {
		Scene role = getModel(Scene.class);
		if (StrKit.notBlank(role.getId())) {
			if (role.update()) {
				renderSuccess("更新成功！");
			} else {
				renderError("更新失败！");
			}
		} else {
			SimpleUser simpleUser=ShiroKit.getLoginUser();
			if(simpleUser==null) {
				renderError("会话过期，退出重新登录！！！");
				return;
			}
			role.setCreateUserId(simpleUser.getId());
			role.setCreateUserName(simpleUser.getName());
			role.setId(UuidUtil.getUUID());
			if (role.save()) {
				renderSuccess("保存成功！");
			} else {
				renderError("保存失败！");
			}
		}

	}

	/***
	 * 删除
	 * 
	 * @throws Exception
	 */
	@Before(Tx.class)
	public void delete() throws Exception {
		String id = getPara("id");
		Scene model = Scene.dao.findById(id);
		if (model != null) {
			if (model.delete()) {
				renderSuccess("删除成功！");
			} else {
				renderError("删除失败！");
			}
		} else {
			renderError("数据不存在,请刷新后再试!");
		}
	}
	


	/**
	 * 获取场景信息页面
	 */
	public void getEditPage() {
		String id = getPara("sceneId");
		this.setAttr("sceneId", id);
		this.setAttr("bizScene", Scene.dao.findById(id));
		render("info.html");
	}
	
	public void findById() throws Exception {
		String id = getPara("id");
		Scene model = Scene.dao.findById(id);
		if (model != null) {
			renderSuccess("成功！",model);
		} else {
			renderError("数据不存在,请刷新后再试!");
		}
	}
	
	public void preview() {
		String id = getPara("id");
		Scene model = Scene.dao.findById(id);
		if(model!=null) {
			model.setViewTimes(model.getViewTimes()+1);
			model.update();
		}
		this.setAttr("id", id);
		render("preview.html");
	}
	
	public void introduce() {
		String id = getPara("sceneId");
		Scene model = Scene.dao.findById(id);
		this.setAttr("model", model);
		render("introduce.html");
	}
	

}
