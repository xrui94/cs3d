package com.vf.s.mvc.biz.controller;

import java.util.List;

import com.jfinal.kit.StrKit;
import com.vf.core.controller.BaseController;
import com.vf.core.render.RenderLayuiBean;
import com.vf.core.util.UuidUtil;
import com.vf.s.common.model.biz.BizEvent;

public class BizEventController extends BaseController {
	
	
	public void index() {
		this.render("list.html");
	}
	
	public void listData() {
		List<BizEvent> list = BizEvent.dao.find(
				"SELECT T.*, (CASE  WHEN (SELECT COUNT(ID) AS COUNT FROM BIZ_EVENT WHERE PARENTID=T.ID)>0 THEN TRUE ELSE  FALSE END ) AS HAVECHILD   FROM BIZ_EVENT  T  ORDER BY T.SORT ASC");
		RenderLayuiBean renderBean = new RenderLayuiBean();
		renderBean.setCode(0);
		renderBean.setMsg("查询成功");
		renderBean.setData(list);
		renderBean.setCount(list.size());
		this.renderJson(renderBean);
	}
	

	public void save() {
		BizEvent role = getModel(BizEvent.class);
		if (StrKit.notBlank(role.getId())) {
			BizEvent namerole=BizEvent.dao.findFirst("SELECT * FROM "+BizEvent.TABLE_NAME +" WHERE NAME=? AND ID!=?",role.getName(),role.getId());
			if(namerole!=null) {
				renderError("名称已存在！");
				return ;
			}
			if (role.update()) {
				renderSuccess("更新成功！");
			} else {
				renderError("更新失败！");
			}
		} else {
			BizEvent namerole=BizEvent.dao.findFirst("SELECT * FROM "+BizEvent.TABLE_NAME +" WHERE NAME=?",role.getName());
			if(namerole!=null) {
				renderError("名称已存在！");
				return ;
			}
			role.setId(UuidUtil.getUUID());
			if (role.save()) {
				renderSuccess("保存成功！");
			} else {
				renderError("保存失败！");
			}
		}

	}
	
	/***
	 * 删除
	 * @throws Exception
	 */
	public void delete() {
		BizEvent model = BizEvent.dao.findById(this.getPara("id"));
		if (model != null) {
			if (model.delete()) {
				renderSuccess("删除成功！");
			} else {
				renderError("删除失败！");
			}
		} else {
			renderError("数据不存在,请刷新后再试!");
		}
	}


}
