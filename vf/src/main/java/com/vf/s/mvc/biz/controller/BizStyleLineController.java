package com.vf.s.mvc.biz.controller;


import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Page;
import com.vf.core.controller.BaseController;
import com.vf.core.util.UuidUtil;
import com.vf.s.common.cache.style.MapStyleCache;
import com.vf.s.common.model.biz.BizStyleLine;

public class BizStyleLineController extends BaseController {
	
	public void index() {
		this.render("list.html");
	}

	public void listData() {
		int pageNumber = getParaToInt("page", 1);
		int pageSize = getParaToInt("limit", 10);
		String name = getPara("name");
		String sqlExceptSelect = " from " + BizStyleLine.TABLE_NAME + " o  where 1=1 ";
		if (!StrKit.isBlank(name)) {
			sqlExceptSelect += " and o.name like '%" + name + "%'  ";
		}
		sqlExceptSelect += " order by  o.name ";
		Page<BizStyleLine> page = BizStyleLine.dao.paginate(pageNumber, pageSize, "select * ", sqlExceptSelect);
		renderLayuiPage(0, page.getList(), "", page.getTotalRow());
	}

	public void save() {
		BizStyleLine role = getModel(BizStyleLine.class);
		if (StrKit.notBlank(role.getId())) {
			BizStyleLine namerole=BizStyleLine.dao.findFirst("SELECT * FROM "+BizStyleLine.TABLE_NAME +" WHERE NAME=? AND ID!=?",role.getName(),role.getId());
			if(namerole!=null) {
				renderError("名称已存在！");
				return ;
			}
			if (role.update()) {
				MapStyleCache.put(role.getId(), BizStyleLine.dao.findById(role.getId()));
				renderSuccess("更新成功！");
			} else {
				renderError("更新失败！");
			}
		} else {
			BizStyleLine namerole=BizStyleLine.dao.findFirst("SELECT * FROM "+BizStyleLine.TABLE_NAME +" WHERE NAME=?",role.getName());
			if(namerole!=null) {
				renderError("名称已存在！");
				return ;
			}
			role.setId(UuidUtil.getUUID());
			if (role.save()) {
				MapStyleCache.put(role.getId(), BizStyleLine.dao.findById(role.getId()));
				renderSuccess("保存成功！");
			} else {
				renderError("保存失败！");
			}
		}

	}
	
	/***
	 * 删除
	 * @throws Exception
	 */
	public void delete() {
		BizStyleLine model = BizStyleLine.dao.findById(this.getPara("id"));
		if (model != null) {
			if (model.delete()) {
				MapStyleCache.remove(model.getId());
				renderSuccess("删除成功！");
			} else {
				renderError("删除失败！");
			}
		} else {
			renderError("数据不存在,请刷新后再试!");
		}
	}
	
	public void copy() {
		BizStyleLine model = BizStyleLine.dao.findById(this.getPara("id"));
		if (model != null) {
			model.setId(UuidUtil.getUUID());
			model.setName(model.getName()+"【复制】");
			if (model.save()) {
				MapStyleCache.put(model.getId(), BizStyleLine.dao.findById(model.getId()));
				renderSuccess("复制【"+model.getName()+"】成功！");
			} else {
				renderError("复制失败！");
			}
		} else {
			renderError("数据不存在,请刷新后再试!");
		}
	}


}
