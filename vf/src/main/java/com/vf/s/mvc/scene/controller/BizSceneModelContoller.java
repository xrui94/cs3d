package com.vf.s.mvc.scene.controller;

import java.util.List;

import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Page;
import com.vf.core.controller.BaseController;
import com.vf.core.util.UuidUtil;
import com.vf.s.common.model.biz.BizModel;
import com.vf.s.common.model.biz.BizVideo;
import com.vf.s.common.model.scene.BizSceneModel;

public class BizSceneModelContoller  extends BaseController{
	
	public void select() {
		this.setAttr("layerId",getPara("layerId"));
		render("select.html");
	}
	
	public void setting() {
		this.set("modelId", this.get("modelId"));
		BizSceneModel bizSceneModel=BizSceneModel.dao.findById(this.get("modelId"));
		if(StrKit.equals("3DTILES", bizSceneModel.getType())) {
			this.set("bizSceneModel", bizSceneModel);
			render("setting3DTileset.html");
		}
		else if(StrKit.equals("GLTF", bizSceneModel.getType())) {
			this.set("bizSceneModel", bizSceneModel);
			render("settingGLTF.html");
		}
	}
	
	public void listData() {
		int pageNumber = getParaToInt("page", 1);
		int pageSize = getParaToInt("limit", 10);
		String keywords = getPara("keywords");
		String classifyId = getPara("classifyId");
		String select = " select *  ";
		String sqlExceptSelect = " from " + BizModel.TABLE_NAME + " o  where 1=1 ";
		if (!StrKit.isBlank(keywords)) {
			sqlExceptSelect += " and o.name like '%" + keywords + "%'  ";
		}
		if (!StrKit.isBlank(classifyId)) {
			sqlExceptSelect += " and o.classifyId = '" + classifyId + "'  ";
		}
		Page<BizModel> page = BizModel.dao.paginate(pageNumber, pageSize, select, sqlExceptSelect);
		renderLayuiPage(0, page.getList(), "", page.getTotalRow());
	}
	
	public void save() {
		BizSceneModel model = getModel(BizSceneModel.class);
		if(!StrKit.isBlank(model.getId()) && BizSceneModel.dao.findById(model.getId())!=null) {
			if (model.update()) {
				renderSuccess("更新成功！", BizSceneModel.dao.findById(model.getId()));
			} else {
				renderError("更新失败！");
			}
		}else {
			if(StrKit.isBlank(model.getId()))
				model.setId(UuidUtil.getUUID());
			if (model.save()) {
				renderSuccess("保存成功！", BizSceneModel.dao.findById(model.getId()));
			} else {
				renderError("保存失败！");
			}
		}
	}
	
	public void delete(){
		String id = getPara("id");
		BizSceneModel model=BizSceneModel.dao.findById(id);
		if(model!=null) {
			if(model.delete()) {
				renderSuccess("删除成功！");
			}else {
				renderError("删除失败！");
			}
		}else {
			renderError("数据不存在！");
		}
	}
	
	public void getList() {
		String layerId = getPara("layerId");
		String whereForBizModelParam="FROM "+BizSceneModel.TABLE_NAME+" A, "+BizModel.TABLE_NAME+" B WHERE A.MODELID=B.ID  ";
		if(!StrKit.isBlank(layerId)) {
			whereForBizModelParam+=" and  A.LAYERID='"+layerId+"' ";
		}
		whereForBizModelParam+=" ORDER BY A.SORT ASC ";
		//模型数据
		List<BizSceneModel> models=BizSceneModel.dao.find("SELECT A.*,B.URL AS url  "+whereForBizModelParam);
		this.renderSuccess("成功！", models);
	}
	
	public void getById() {
		String id = getPara("id");
		BizSceneModel sceneModel = BizSceneModel.dao.findById(id);
		if(sceneModel==null) {
			renderError("模型不存在!");
			return;
		}
		BizModel bizModel=BizModel.dao.findById(sceneModel.getModelId());
		if(bizModel!=null) {
			if(StrKit.equals(bizModel.getType(),"OBJ")) {
				sceneModel.setVideo(BizVideo.dao.findById(bizModel.getVideoId()));
			}
			sceneModel.setUrl(bizModel.getUrl());
			sceneModel.setType(bizModel.getType());
			this.renderSuccess("成功！", sceneModel);
		}else {
			renderError("模型已不存在，请及时移除!");
		}
	}
	
	public void getModelById() {
		String modelId = getPara("id");
		BizModel bizModel=BizModel.dao.findById(modelId);
		if(bizModel!=null) {
			if(StrKit.equals(bizModel.getType(),"OBJ")) {
				bizModel.setVideo(BizVideo.dao.findById(bizModel.getVideoId()));
			}
			this.renderSuccess("成功！", bizModel);
		}else {
			renderError("模型已不存在，请及时移除!");
		}
	}
}
