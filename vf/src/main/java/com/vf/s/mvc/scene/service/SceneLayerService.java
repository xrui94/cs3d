package com.vf.s.mvc.scene.service;

import java.util.ArrayList;
import java.util.List;

import com.jfinal.kit.StrKit;
import com.vf.core.model.ZtreeNode;
import com.vf.s.common.model.biz.BizLine;
import com.vf.s.common.model.biz.BizModel;
import com.vf.s.common.model.biz.Point;
import com.vf.s.common.model.biz.BizPolygon;
import com.vf.s.common.model.biz.BizVideoModel;
import com.vf.s.common.model.biz.BizVideoPlane;
import com.vf.s.common.model.biz.BizVideoShed3d;
import com.vf.s.common.model.biz.Provider;
import com.vf.s.common.model.scene.BizSceneFeature;
import com.vf.s.common.model.scene.SceneLayer;
import com.vf.s.common.model.scene.BizSceneModel;
import com.vf.s.common.model.scene.BizSceneRoam;
import com.vf.s.common.model.scene.SceneProvider;


public class SceneLayerService {

	public List<SceneLayer> getChildrenAllTree(String pId,String type) {
		List<SceneLayer> list = getChildrenByPid(pId,type);
		for (SceneLayer o : list) {
			o.setChildren(getChildrenAllTree(o.getId(),type));
		}
		return list;
	}

	public List<SceneLayer> getChildrenByPid(String id,String type) {
		String sql = "SELECT * FROM "+SceneLayer.TABLE_NAME+" M WHERE M.PARENTID='" + id + "'  ";
		if(!StrKit.isBlank(type)) {
			sql+=" and  m.type='"+type+"' ";
		}
		sql = sql + " ORDER BY M.SORT";
		return SceneLayer.dao.find(sql);
	}
	
	public List<ZtreeNode> toZTreeNode(List<SceneLayer> olist, boolean open) {
		List<ZtreeNode> list = new ArrayList<ZtreeNode>();
		for (SceneLayer o : olist) {
			ZtreeNode node = toZTreeNode(o);
			if (o.getChildren() != null && o.getChildren().size() > 0) {
				node.setChildren(toZTreeNode(o.getChildren(), open));
				node.setIsParent(true);
			}
			node.setOpen(open);
			list.add(node);
		}
		return list;
	}

	public ZtreeNode toZTreeNode(SceneLayer sceneMenu) {
		ZtreeNode node = new ZtreeNode();
		node.setId(sceneMenu.getId());
		node.setName(sceneMenu.getName());
		node.setLabel(sceneMenu.getName());
		node.setType(sceneMenu.getType());
		node.setParentId(sceneMenu.getParentId());
		return node;
	}
	public List<?> findChildren(String id,String type) {
		if(StrKit.equals("Maps", type)) {
			List<Provider> olist=Provider.dao.find("SELECT a.name,a.code,b.id FROM "+Provider.TABLE_NAME+" A ,"+SceneProvider.TABLE_NAME+" B  WHERE A.ID=B.PROVIDERID and  B.LAYERID=? ORDER BY B.SORT ASC ",id);
			return olist;
		}
		else if(StrKit.equals("Models", type)) {
			List<BizSceneModel> olist=BizSceneModel.dao.find("SELECT A.*,B.URL AS URI FROM "+BizSceneModel.TABLE_NAME+" A, "+BizModel.TABLE_NAME+" B "
					+ " WHERE A.MODELID=B.ID AND  A.LAYERID=?  ",id);
			return olist;
		}
		else if(StrKit.equals("Points", type)) {
			List<Point> olist=Point.dao.find("SELECT * FROM "+Point.TABLE_NAME+" WHERE LAYERID=? ",id);
			return olist;
		}
		else if(StrKit.equals("Polylines", type)) {
			List<BizLine> olist=BizLine.dao.find("SELECT * FROM "+BizLine.TABLE_NAME+" WHERE LAYERID=? ",id);
			return olist;
		}
		else if(StrKit.equals("Polygons", type)) {
			List<BizPolygon> olist=BizPolygon.dao.find("SELECT * FROM "+BizPolygon.TABLE_NAME+" WHERE LAYERID=? ",id);
			return olist;
		}
		else if(StrKit.equals("Features", type)) {
			List<BizSceneFeature> olist=BizSceneFeature.dao.find("SELECT * FROM "+BizSceneFeature.TABLE_NAME+" WHERE LAYERID=? ORDER BY SORT ASC",id);
			return olist;
		}
		else if(StrKit.equals("VideoPlanes", type)) {
			List<BizVideoPlane> olist=BizVideoPlane.dao.find("SELECT * FROM "+BizVideoPlane.TABLE_NAME+" WHERE LAYERID=? ",id);
			return olist;
		}
		else if(StrKit.equals("VideoShed3ds", type)) {
			List<BizVideoShed3d> olist=BizVideoShed3d.dao.find("SELECT * FROM "+BizVideoShed3d.TABLE_NAME+" WHERE LAYERID=? ",id);
			return olist;

		}
		else if(StrKit.equals("VideoModels", type)) {
			List<BizVideoModel> olist=BizVideoModel.dao.find("SELECT * FROM "+BizVideoModel.TABLE_NAME+" WHERE LAYERID=?",id);
			return olist;

		}
		else if(StrKit.equals("Roams", type)) {
			List<BizSceneRoam> olist=BizSceneRoam.dao.find("SELECT * FROM "+BizSceneRoam.TABLE_NAME+" WHERE LAYERID=?  order by sort asc ",id);
			return olist;
		}else {
			String sql = "SELECT a.*,(CASE WHEN (select count(id) from "+SceneLayer.TABLE_NAME+" where a.id=parentId ) >0 THEN true ELSE false END) AS isParent FROM "+SceneLayer.TABLE_NAME+" a where a.parentId=? ";
			sql += " order by a.sort asc ";
			List<SceneLayer> olist = SceneLayer.dao.find(sql,id);
			return olist;
		}
	}
}
