package com.vf.s.mvc.scene.controller;

import java.util.LinkedList;
import java.util.List;

import com.jfinal.kit.StrKit;
import com.vf.core.controller.BaseController;
import com.vf.s.common.model.biz.BizVideo;
import com.vf.s.common.model.biz.BizVideoPlane;
public class BizSceneVPlaneContoller  extends BaseController{
	
	public void getList() {
		String layerId = getPara("layerId");
		String whereForBizPoint="FROM "+BizVideoPlane.TABLE_NAME+" P   WHERE 1=1 ";
		if(!StrKit.isBlank(layerId)) {
			whereForBizPoint+=" and P.LAYERID ='"+layerId+"' ";
		}
		List<BizVideoPlane> bizBizVideoPlane=BizVideoPlane.dao.find("SELECT P.*  "+whereForBizPoint);
		List<BizVideoPlane> list=new LinkedList<BizVideoPlane>();
		for(BizVideoPlane videoPlane :bizBizVideoPlane) {
			BizVideo bizVideo=BizVideo.dao.findById(videoPlane.getVideoId());
			videoPlane.setVideo(bizVideo);
			list.add(videoPlane);
		}
		if(list.size()>0) {
			this.renderSuccess("成功！", list);
		}else {
			renderError("信息不存在！");
		}
	}
	
	public void getById() {
		BizVideoPlane bizVideoPlane=BizVideoPlane.dao.findById(this.getPara("id"));
		if(bizVideoPlane!=null) {
			BizVideo bizVideo=BizVideo.dao.findById(bizVideoPlane.getVideoId());
			bizVideoPlane.setVideo(bizVideo);
			renderSuccess("成功！",bizVideoPlane);
		}else {
			renderError("信息不存在！");
		}
	}
		
}
