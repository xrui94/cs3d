package com.vf.s.mvc.biz.controller;

import com.jfinal.aop.Inject;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Page;
import com.vf.core.controller.BaseController;
import com.vf.core.util.PinYinUtil;
import com.vf.core.util.UuidUtil;
import com.vf.s.common.model.biz.BizLayerGroup;
import com.vf.s.mvc.biz.service.BizLayerService;

public class BizLayerGroupController extends BaseController {

	@Inject
	private BizLayerService srv;

	public void index() {
		render("list.html");
	}

	public void listData() {
		int pageNumber = getParaToInt("page", 1);
		int pageSize = getParaToInt("limit", 10);
		String name = getPara("name");
		String sqlExceptSelect = " from " + BizLayerGroup.TABLE_NAME + " o  where 1=1 ";
		if (!StrKit.isBlank(name)) {
			sqlExceptSelect += " and o.name like '%" + name + "%'  ";
		}
		Page<BizLayerGroup> page = BizLayerGroup.dao.paginate(pageNumber, pageSize, "select * ", sqlExceptSelect);
		renderLayuiPage(0, page.getList(), "", page.getTotalRow());
	}

	public void save() {
		BizLayerGroup model = getModel(BizLayerGroup.class);
		if (StrKit.notBlank(model.getId())) {
			if (StrKit.isBlank(model.getCode())) {
				model.setCode(PinYinUtil.getPinyin(model.getName()));
			}
			if (model.update()) {
				renderSuccess("更新成功！", model);
			} else {
				renderError("更新失败！");
			}
		} else {
			model.setId(UuidUtil.getUUID());
			if (StrKit.isBlank(model.getCode())) {
				model.setCode(PinYinUtil.getPinyin(model.getName()));
			}
			if (model.save()) {
				renderSuccess("保存成功！", model);
			} else {
				renderError("保存失败！");
			}
		}
	}

	public void delete() throws Exception {
		String id = getPara("id");
		BizLayerGroup bizLayer = BizLayerGroup.dao.findById(id);
		if (bizLayer != null) {
			if (bizLayer.delete()) {
				renderSuccess("删除成功", bizLayer);
			} else {
				renderError("删除失败！");
			}
		} else {
			renderError("无效操作，数据不存在!");
		}
	}

}
