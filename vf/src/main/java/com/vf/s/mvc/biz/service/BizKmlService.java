package com.vf.s.mvc.biz.service;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;


import com.jfinal.kit.JsonKit;
import com.jfinal.kit.StrKit;
import com.vf.core.util.UuidUtil;
import com.vf.s.common.model.biz.BizLayer;
import com.vf.s.common.model.biz.BizLine;
import com.vf.s.common.model.biz.Point;
import com.vf.s.common.model.biz.BizPolygon;
import com.vf.s.common.model.biz.BizProperty;
import com.vf.s.common.vector.kml.KmlLine;
import com.vf.s.common.vector.kml.KmlPoint;
import com.vf.s.common.vector.kml.KmlPolygon;

import de.micromata.opengis.kml.v_2_2_0.Coordinate;
import de.micromata.opengis.kml.v_2_2_0.ExtendedData;
import de.micromata.opengis.kml.v_2_2_0.Placemark;
import de.micromata.opengis.kml.v_2_2_0.SchemaData;

public class BizKmlService {
	
	
	public void saveForBizPoint(String fileName,KmlPoint point,int sort) {
		List<Coordinate> points=point.getPoints();
		Placemark placemark=point.getPlacemark();
		
		BizLayer bizLayer;
		ExtendedData extendedData=placemark.getExtendedData();
		if(extendedData!=null) {
			List<SchemaData> schemaDatas=placemark.getExtendedData().getSchemaData();
			if(schemaDatas!=null && schemaDatas.size()>=0) {
				for (SchemaData schemaData : schemaDatas) {
					bizLayer=getBizLayer(schemaData.getSchemaUrl().trim());
					if(points!=null && points.size()>0) {
						for (Coordinate coordinate : points) {
							savePoint(bizLayer, placemark, coordinate,sort);
						}
					}
				}
			}else {
				bizLayer=getBizLayer(fileName);
				if(points!=null && points.size()>0) {
					for (Coordinate coordinate : points) {
						savePoint(bizLayer, placemark, coordinate,sort);
					}
				}
			}
		}else {
			bizLayer=getBizLayer(fileName);
			if(points!=null && points.size()>0) {
				for (Coordinate coordinate : points) {
					savePoint(bizLayer, placemark, coordinate,sort);
				}
			}
		}
	}
	
	public void saveForBizLine(String fileName,KmlLine line) {
		List<Coordinate> points=line.getPoints();
		Placemark placemark=line.getPlacemark();
		BizLayer bizLayer;
		ExtendedData extendedData=placemark.getExtendedData();
		int sort=0;
		if(extendedData!=null) {
			List<SchemaData> schemaDatas=placemark.getExtendedData().getSchemaData();
			if(schemaDatas!=null && schemaDatas.size()>=0) {
				for (SchemaData schemaData : schemaDatas) {
					bizLayer=getBizLayer(schemaData.getSchemaUrl().trim());
					if(points!=null && points.size()>0) saveLine(  bizLayer, placemark, points, sort);
				}
			}else {
				bizLayer=getBizLayer(fileName);
				if(points!=null && points.size()>0) saveLine(  bizLayer, placemark, points, sort);
			}
		}else {
			bizLayer=getBizLayer(fileName);
			if(points!=null && points.size()>0) {
				saveLine(  bizLayer, placemark, points, sort);
			}
		}
	}
	
	
	
	public void saveForKmlPolygon(String fileName,KmlPolygon polygon) {
		List<Coordinate> points=polygon.getPoints();
		Placemark placemark=polygon.getPlacemark();
		
		BizLayer bizLayer;
		ExtendedData extendedData=placemark.getExtendedData();
		int sort=0;
		if(extendedData!=null) {
			List<SchemaData> schemaDatas=placemark.getExtendedData().getSchemaData();
			if(schemaDatas!=null && schemaDatas.size()>=0) {
				for (SchemaData schemaData : schemaDatas) {
					bizLayer=getBizLayer(schemaData.getSchemaUrl().trim());
					if(points!=null && points.size()>0) savePolygon(  bizLayer, placemark,points, sort) ;
				}
			}else {
				bizLayer=getBizLayer(fileName);
				if(points!=null && points.size()>0) savePolygon(  bizLayer, placemark,points, sort) ;
			}
		}else {
			bizLayer=getBizLayer(fileName);
			if(points!=null && points.size()>0) {
				savePolygon(  bizLayer, placemark,points, sort) ;
			}
		}
	}
	
	private BizLayer getBizLayer(String name) {
		BizLayer bizLayer=BizLayer.dao.findFirst("SELECT * FROM "+BizLayer.TABLE_NAME+" WHERE code=? ",name);
		if(bizLayer==null) {
			bizLayer=new BizLayer();
			bizLayer.setId(UuidUtil.getUUID());
			bizLayer.setCode(name);
			bizLayer.setName(name);
			bizLayer.setParentId("root");
			bizLayer.setType("folder");
			bizLayer.save();
		}
		return bizLayer;
	}
	
	private void savePoint(BizLayer bizLayer,Placemark placemark,Coordinate coordinate,int sort) {
		Point bizPoint=Point.dao.findFirst("SELECT * FROM "+Point.TABLE_NAME+" WHERE x=? AND  y=? AND  z=?  AND name=?  ",coordinate.getLongitude(),coordinate.getLatitude(),coordinate.getAltitude(),placemark.getName());
		if(bizPoint==null) {
			bizPoint=new Point();
			bizPoint.setId(UuidUtil.getUUID());
			bizPoint.setName(placemark.getName());
			bizPoint.setX(coordinate.getLongitude());
			bizPoint.setY(coordinate.getLatitude());
			bizPoint.setZ(coordinate.getAltitude());
			bizPoint.setLayerId(bizLayer.getId());
			bizPoint.save();
			
			if(StrKit.notBlank(placemark.getPhoneNumber())) {
				BizProperty bizProperty=new BizProperty();
				bizProperty.setId(UuidUtil.getUUID());
				bizProperty.setKey("电话");
				bizProperty.setValue(placemark.getPhoneNumber());
				bizProperty.setObjId(bizPoint.getId());
				bizProperty.setEvent("MOUSE_HOVER");
				bizProperty.setSort(1);
				bizProperty.save();
			}
			
			if(StrKit.notBlank(placemark.getDescription())) {
				BizProperty bizProperty=new BizProperty();
				bizProperty.setId(UuidUtil.getUUID());
				bizProperty.setKey("地址");
				bizProperty.setValue(placemark.getDescription());
				bizProperty.setObjId(bizPoint.getId());
				bizProperty.setEvent("MOUSE_HOVER");
				bizProperty.setSort(2);
				bizProperty.save();
			}
		}
	}
	
	private void saveLine( BizLayer bizLayer,Placemark placemark,List<Coordinate> points,int sort) {
		BizLine bizLine=new BizLine();
		bizLine.setId(UuidUtil.getUUID());
		bizLine.setName(placemark.getName());
		bizLine.setLayerId(bizLayer.getId());
		bizLine.setSort(sort);
		List<Map<String, Object>> ps=new LinkedList<Map<String, Object>>();
		for (Coordinate coordinate : points) {
			Map<String, Object> obj=new HashMap<String, Object>();
			obj.put("x", coordinate.getLongitude());
			obj.put("y", coordinate.getLatitude());
			obj.put("z", coordinate.getAltitude());
			ps.add(obj);
		}
		bizLine.setPoints(JsonKit.toJson(ps));
		bizLine.save();
//		BizLayer bizLayerForPoint=new BizLayer();
//		bizLayerForPoint.setId(bizLine.getId());
//		bizLayerForPoint.setSceneId(bizLine.getSceneId());
//		bizLayerForPoint.setName(bizLine.getName());
//		bizLayerForPoint.setParentId(bizLayer.getId());
//		bizLayerForPoint.setType("line");
//		bizLayerForPoint.setSort(bizLine.getSort());
//		bizLayerForPoint.setDataId(bizLine.getId());
//		bizLayerForPoint.save();
		sort++;
	}
	
	private void savePolygon(BizLayer bizLayer,Placemark placemark,List<Coordinate> points,int sort) {
		BizPolygon bizPolygon=new BizPolygon();
		bizPolygon.setId(UuidUtil.getUUID());
		bizPolygon.setName(placemark.getName());
		bizPolygon.setLayerId(bizLayer.getId());
		bizPolygon.setSort(sort);
		List<Map<String, Object>> ps=new LinkedList<Map<String, Object>>();
		for (Coordinate coordinate : points) {
			Map<String, Object> obj=new HashMap<String, Object>();
			obj.put("x", coordinate.getLongitude());
			obj.put("y", coordinate.getLatitude());
			obj.put("z", coordinate.getAltitude());
			ps.add(obj);
		}
		bizPolygon.setPoints(JsonKit.toJson(ps));
		bizPolygon.save();
		sort++;
	}
	
	
	
	
}
