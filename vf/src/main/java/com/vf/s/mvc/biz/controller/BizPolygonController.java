package com.vf.s.mvc.biz.controller;


import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Supplier;

import com.jfinal.aop.Inject;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Page;
import com.vf.core.controller.BaseController;
import com.vf.s.common.model.biz.BizLayer;
import com.vf.s.common.model.biz.BizPolygon;
import com.vf.s.common.model.scene.Scene;
import com.vf.s.mvc.biz.service.BizPolygonService;

public class BizPolygonController extends BaseController {
	
	@Inject
	private BizPolygonService srv;
	
	public void index() {
		render("list.html");
	}
	
	public void operation() {
		this.set("bizPolygon", BizPolygon.dao.findById(get("polygonId")));
		render("operation.html");
	}
	
	public void edit() {
		this.set("bizPolygon", BizPolygon.dao.findById(get("polygonId")));
		render("edit.html");
	}
	
	public void list() {
		List<Scene> scenes=Scene.dao.find("SELECT * FROM "+Scene.TABLE_NAME);
		this.set("scenes",scenes);
		render("polygonForList.html");
	}
	
	public void listData() {
		int pageNumber = getParaToInt("page", 1);
		int pageSize = getParaToInt("limit", 10);
		String name = getPara("name");
		String sceneId = getPara("sceneId");
		
		String select = " select *  ";
		String sqlExceptSelect = " from " + BizPolygon.TABLE_NAME + " o  where 1=1 ";
		if (!StrKit.isBlank(name)) {
			sqlExceptSelect += " and o.name like '%" + name + "%'  ";
		}
		
		if (!StrKit.isBlank(sceneId)) {
			sqlExceptSelect += " and o.sceneId = '" + sceneId + "'  ";
		}
		
		Page<BizPolygon> page = BizPolygon.dao.paginate(pageNumber, pageSize, select, sqlExceptSelect);
		renderLayuiPage(0, page.getList(), "", page.getTotalRow());
	}
	
	
	public void save() {
		BizPolygon model = getModel(BizPolygon.class);
		BizPolygon dbBizPolygon=BizPolygon.dao.findById(model.getId());
		if(dbBizPolygon!=null) {
			if (model.update()) {
				renderSuccess("更新成功！",BizPolygon.dao.findById(model.getId()));
			} else {
				renderError("更新失败！");
			}
		}else {
			if (model.save()) {
				renderSuccess("保存成功！",BizPolygon.dao.findById(model.getId()));
			} else {
				renderError("保存失败！");
			}
		}
	}
	
	public void getById() throws Exception {
		String id = getPara("id");
		BizPolygon bizPolygon = BizPolygon.dao.findById(id);
		if(bizPolygon!=null) {
			renderSuccess("查询成功！", bizPolygon);
		}else {
			renderError("未查到数据！");
		}
	}
	
	public void delete() throws Exception {
		String id = getPara("id");
		boolean isDel=BizPolygon.dao.deleteById(id);
		if (isDel) {
			renderSuccess("删除成功！");
		} else {
			renderError("删除失败！");
		}
	}
	

	
	/**
	 * 样式
	 */
	public void polygonForStyle() {
		this.set("sceneId", this.get("sceneId"));
		BizPolygon bizPolygon=BizPolygon.dao.findById(get("polygonId"));
		this.set("bizPolygon", bizPolygon);
		render("polygonForStyle.html");
	}
	
	/**
	 * 菜单
	 */
	public void polygonForMenu() {
		this.set("sceneId", this.get("sceneId"));
		BizPolygon bizPolygon=BizPolygon.dao.findById(get("polygonId"));
		this.set("bizPolygon", bizPolygon);
		render("polygonForMenu.html");
	}
	
	public void saveForStyle() {
		BizPolygon model = getModel(BizPolygon.class);
		BizPolygon dbBizPolygon=BizPolygon.dao.findById(model.getId());
		if(dbBizPolygon!=null) {
			if (model.update()) {
				renderSuccess("更新成功！",model);
			} else {
				renderError("更新失败！");
			}
		}
	}
	
	ExecutorService executor = Executors.newFixedThreadPool(20);
	public void batchSaveStyle() throws Exception {
		String layerId=this.getPara("layerId");
		renderSuccess("异步处理中！！！");
	    CompletableFuture<Integer> future = CompletableFuture.supplyAsync(new Supplier<Integer>() {
	        @Override
	        public Integer get() {
	            try {
	        		List<BizLayer> bizLayers=BizLayer.dao.find("SELECT * FROM "+BizLayer.TABLE_NAME+" WHERE PARENTID=? AND TYPE='polygon' ",layerId);
	        		BizPolygon bizPolygon;
	        		for(BizLayer bizLayer:bizLayers) {
	        			bizPolygon=BizPolygon.dao.findById(bizLayer.getId());
	        			if(bizPolygon!=null) {
	        				bizPolygon.update();
	        			}
	        		}
	            } catch (Exception e) {
	                e.printStackTrace();
	            }
	            return 3;
	        }
	    }, executor);
	    future.thenAccept(e -> System.out.println(e));
	}
	
}
