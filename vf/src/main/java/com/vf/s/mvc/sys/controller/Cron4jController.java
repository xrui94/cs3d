package com.vf.s.mvc.sys.controller;

import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Page;
import com.vf.core.controller.BaseController;
import com.vf.core.util.UuidUtil;
import com.vf.s.common.model.sys.SysCron4j;

public class Cron4jController extends BaseController{
	
	public void index() {
		this.render("list.html");
	}
	
	public void listData() {
		int pageNumber = getParaToInt("page", 1);
		int pageSize = getParaToInt("limit", 10);
		String keyword = getPara("keyword");

		String sqlExceptSelect = " FROM " + SysCron4j.TABLE_NAME + " o  WHERE 1=1 ";
		if (!StrKit.isBlank(keyword)) {
			sqlExceptSelect += " AND ( O.NAME LIKE '%" + keyword + "%' OR   O.CODE LIKE '%" + keyword + "%' ) ";
		}
		sqlExceptSelect += "  ORDER BY   O.LASTRUNTIME DESC ";
		Page<SysCron4j> page = SysCron4j.dao.paginate(pageNumber, pageSize, "select * ", sqlExceptSelect);
		renderLayuiPage(0, page.getList(), "", page.getTotalRow());
	}
	
	public void save() {
		SysCron4j cron4j = getModel(SysCron4j.class);
		if (StrKit.notBlank(cron4j.getId())) {
			if (cron4j.update()) {
				renderSuccess("更新成功！");
			} else {
				renderError("更新失败！");
			}
		} else {
			cron4j.setId(UuidUtil.getUUID());
			if (cron4j.save()) {
				renderSuccess("保存成功！");
			} else {
				renderError("保存失败！");
			}
		}
	}

	public void delete() {
		String id = getPara("id");
		SysCron4j cron4j = SysCron4j.dao.findById(id);
		if (cron4j != null) {
			if (cron4j.delete()) {
				renderSuccess("删除成功！");
			} else {
				renderError("删除失败！");
			}
		} else {
			renderError("数据不存在,请刷新后再试!");
		}
	}
	
}
