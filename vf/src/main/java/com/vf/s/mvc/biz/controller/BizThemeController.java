package com.vf.s.mvc.biz.controller;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.jfinal.kit.StrKit;
import com.vf.core.controller.BaseController;
import com.vf.core.render.HttpClientResult;
import com.vf.core.util.HttpUtil;
import com.vf.s.common.model.scene.Scene;

public class BizThemeController extends BaseController {
	
	public void index() {
		String id = getPara("sceneId");
		Scene model = Scene.dao.findById(id);
		if(model!=null) {
			model.setViewTimes(model.getViewTimes()+1);
			model.update();
		}
		this.setAttr("id", id);
		render("default/preview.html");
	}
	
	public void introduceForDefault() {
		String id = getPara("sceneId");
		Scene model = Scene.dao.findById(id);
		this.setAttr("model", model);
		render("default/introduce.html");
	}
	
	public void layerTreeForDefault() {
		String id = getPara("sceneId");
		this.setAttr("sceneId", id);
		render("default/layerTree.html");
	}
	
	public void flightTreeForDefault() {
		String id = getPara("sceneId");
		this.setAttr("sceneId", id);
		render("default/flightTree.html");
	}
	
	public void pointInfo() {
		String id = getPara("id");
		HttpClientResult result;
		try {
			result = HttpUtil.doGet("https://ejoydg.tianview.com/sg-front-api-data-xiaodu_org_info.html?org_uuid="+id, null, null);
			if (result.getCode() == 200) {
				if (!StrKit.isBlank(result.getContent())) {
					JSONObject object =  JSONObject.parseObject(result.getContent());
					if(object.getInteger("error")==0) {
						JSONObject data =  JSONObject.parseObject(object.getString("data"));
						if(!data.isEmpty()) {
							JSONArray clean_list = JSONArray.parseArray(data.getString("clean_list"));
							if(!clean_list.isEmpty()) {
								this.setAttr("clean_list", clean_list);
							}
						}
					}
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		render("default/pointInfo.html");
	}
}
