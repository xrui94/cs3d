package com.vf.s.mvc.biz.controller;


import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Page;
import com.vf.core.controller.BaseController;
import com.vf.core.util.UuidUtil;
import com.vf.s.common.model.biz.BizLayer;
import com.vf.s.common.model.biz.BizPage;
import com.vf.s.common.model.biz.BizText;
import com.vf.s.common.model.biz.BizView;
import com.vf.s.common.render.ViewRender;

public class BizViewController extends BaseController {
	
	

	
	/**
	 * 忘记加上图层检索
	 */
	public void listData() {
		int pageNumber = getParaToInt("page", 1);
		int pageSize = getParaToInt("limit", 10);
		String name = getPara("name");
		String parentId = getPara("parentId");
		String sceneId = getPara("sceneId");
		
		String select = " select *  ";
		String sqlExceptSelect = " from " + BizView.TABLE_NAME + " o  where 1=1 ";
		if (!StrKit.isBlank(name)) {
			sqlExceptSelect += " and o.name like '%" + name + "%'  ";
		}
		
		if (!StrKit.isBlank(sceneId)) {
			sqlExceptSelect += " and o.sceneId = '" + sceneId + "'  ";
		}
		
		if (!StrKit.isBlank(parentId)) {
			select ="select o.*,(CASE WHEN (select COUNT(id) from biz_flight b WHERE  o.id=b.id and b.parentId='"+parentId+"') >0 THEN true ELSE false END)  as LAY_CHECKED  ";
			sqlExceptSelect += " order by  LAY_CHECKED desc  ";
		}
		Page<BizView> page = BizView.dao.paginate(pageNumber, pageSize, select, sqlExceptSelect);
		renderLayuiPage(0, page.getList(), "", page.getTotalRow());
		
	}
	
	
	public void delete() throws Exception {
		String id = getPara("id");
		Page<BizLayer> page=BizLayer.dao.paginate(1, 1, "SELECT * "," FROM "+BizLayer.TABLE_NAME+" WHERE PARENTID=? ",id);
		if (page.getTotalRow()==0) {
			boolean isDel=BizLayer.dao.deleteById(id);
			if (isDel) {
				renderSuccess("删除成功！");
			} else {
				renderError("删除失败！");
			}
		} else {
			renderError("当前节点有子节点,不允许删除!");
			return;
		}
	}

	
	public void getById() throws Exception {
		String id = getPara("id");
		BizView bizView=BizView.dao.findById(id);
		if (bizView!=null) {
			renderSuccess("成功！",bizView);
		} else {
			renderError("数据已经不存在！");
		}
	}
	
	public void getRenderById() throws Exception {
		String id = getPara("id");
		String event = getPara("event");
		BizView bizView=BizView.dao.findById(id);
		if (bizView!=null) {
			ViewRender viewRender=new ViewRender();
			viewRender.setView(bizView);
			viewRender.setText(BizText.dao.findById(bizView.getId()));
			viewRender.setPages(BizPage.dao.find("SELECT * FROM "+BizPage.TABLE_NAME+" WHERE objId=? AND event=? ",bizView.getId(),event));
			renderSuccess("成功！",viewRender);
		} else {
			renderError("数据已经不存在！");
		}
	}
	
	
	
	
	/**
	 * 编辑点信息
	 */
	public void viewForEdit() {
		this.set("sceneId", this.get("sceneId"));
		BizView bizView=BizView.dao.findById(get("viewId"));
		this.set("bizView", bizView);
		render("viewForEdit.html");
	}
	
	/**
	 * 保存点
	 */
	public void save() {
		BizView model = getModel(BizView.class);
		if (StrKit.notBlank(model.getId())) {
			BizLayer bizLayer=BizLayer.dao.findById(model.getId());
			bizLayer.setName(model.getName());
			bizLayer.setCode(model.getCode());
			bizLayer.setSort(model.getSort());
			if (model.update() && bizLayer.update()) {
				renderSuccess("更新成功！",model);
			} else {
				renderError("更新失败！");
			}
		} else {
			model.setId(UuidUtil.getUUID());
			BizLayer bizLayer=new BizLayer();
			bizLayer.setId(model.getId());
			bizLayer.setName(model.getName());
			bizLayer.setSceneId(model.getSceneId());
			bizLayer.setCode(model.getCode());
			bizLayer.setType("view");
			bizLayer.setSort(model.getSort());
			bizLayer.setParentId("root");
			if (model.save() && bizLayer.save()) {
				renderSuccess("保存成功！",model);
			} else {
				renderError("保存失败！");
			}
		}
	}
	
	
	
	
}
