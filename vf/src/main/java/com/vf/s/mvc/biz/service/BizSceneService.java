package com.vf.s.mvc.biz.service;

import com.jfinal.plugin.activerecord.Db;
import com.vf.s.common.model.biz.*;

public class BizSceneService {
	
	public void deleteRelation(String sceneId) {
		//图层移出
		Db.delete("DELETE FROM "+BizLayer.TABLE_NAME+" WHERE sceneId=? ",sceneId);
		Db.delete("DELETE FROM "+Point.TABLE_NAME+" WHERE sceneId=? ",sceneId);
		Db.delete("DELETE FROM "+BizLine.TABLE_NAME+" WHERE sceneId=? ",sceneId);
		Db.delete("DELETE FROM "+BizPolygon.TABLE_NAME+" WHERE sceneId=? ",sceneId);
		Db.delete("DELETE FROM "+BizFlight.TABLE_NAME+" WHERE sceneId=? ",sceneId);
	}

}
