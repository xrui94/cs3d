package com.vf.s.mvc.scene.controller;

import java.util.List;

import com.jfinal.aop.Before;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.tx.Tx;
import com.vf.core.controller.BaseController;
import com.vf.core.util.UuidUtil;
import com.vf.s.common.model.scene.BizSceneFeature;

public class BizSceneFeatureContoller extends BaseController {
	
	public void setting() {
		this.setAttr("feature",BizSceneFeature.dao.findById(this.getPara("featureId")));
		render("setting.html");
	}
	
	/**
	 * 保存点
	 */
	public void save() {
		BizSceneFeature model = getModel(BizSceneFeature.class);
		BizSceneFeature dbBizModelFeature=BizSceneFeature.dao.findById(model.getId());
		if (dbBizModelFeature!=null ) {
			if (model.update() ) {
				renderSuccess("更新成功！",BizSceneFeature.dao.findById(model.getId()));
			} else {
				renderError("更新失败！");
			}
		} else {
			if (model.save()) {
				renderSuccess("保存成功！",BizSceneFeature.dao.findById(model.getId()));
			} else {
				renderError("保存失败！");
			}
		}
	}

	/***
	 * 删除
	 * 
	 * @throws Exception
	 */
	@Before(Tx.class)
	public void delete() throws Exception {
		String id = getPara("id");
		BizSceneFeature model = BizSceneFeature.dao.findById(id);
		if (model != null) {
			if (model.delete()) {
				renderSuccess("删除成功！");
			} else {
				renderError("删除失败！");
			}
		} else {
			renderError("数据不存在,请刷新后再试!");
		}
	}

	public void findById() throws Exception {
		String id = getPara("id");
		BizSceneFeature model = BizSceneFeature.dao.findById(id);
		if (model != null) {
			renderSuccess("成功！", model);
		} else {
			renderError("数据不存在,请刷新后再试!");
		}
	}
	
	
	public void getList() {
		String layerId = getPara("layerId");
		String whereForBizPoint="FROM "+BizSceneFeature.TABLE_NAME+" P  WHERE 1=1 ";
		if(!StrKit.isBlank(layerId)) {
			whereForBizPoint+=" and P.LAYERID ='"+layerId+"' ";
		}
		List<BizSceneFeature> bizPointList=BizSceneFeature.dao.find("SELECT P.*  "+whereForBizPoint);
		if(bizPointList!=null && bizPointList.size()>0) {
			this.renderSuccess("成功！", bizPointList);
		}else {
			renderError("未查询到数据！");
		}
	}
	
	public void getById() {
		BizSceneFeature bizPoint=BizSceneFeature.dao.findById(getPara("id"));
		if(bizPoint!=null) {
			this.renderSuccess("成功！", bizPoint);
		}else {
			renderError("未查询到数据！");	
		}
	}
	
	public void copy() {
		String id=this.getPara("id");
		String interval=this.getPara("interval");
		String incremental=this.getPara("incremental");
		String decreasing=this.getPara("decreasing");
		
		BizSceneFeature bizPoint=BizSceneFeature.dao.findById(id);
		if(bizPoint!=null) {
			double interval1=0.0;
			if(!StrKit.isBlank(interval)) {
				interval1=Double.parseDouble(interval);
			}
			
			if(!StrKit.isBlank(incremental)) {
				int incremental1=Integer.parseInt(incremental);
				Double z=Double.parseDouble(bizPoint.getZ());
				for(int i=0;i<incremental1;i++) {
					z+=interval1+bizPoint.getDimensionsZ();
					BizSceneFeature feature=new BizSceneFeature();
					feature.setId(UuidUtil.getUUID());
					feature.setLayerId(bizPoint.getLayerId());
					feature.setX(bizPoint.getX());
					feature.setY(bizPoint.getY());
					feature.setZ(z+"");
					feature.setDimensionsX(bizPoint.getDimensionsX());
					feature.setDimensionsY(bizPoint.getDimensionsY());
					feature.setDimensionsZ(bizPoint.getDimensionsZ());
					feature.setRotationX(bizPoint.getRotationX());
					feature.setRotationY(bizPoint.getRotationY());
					feature.setRotationZ(bizPoint.getRotationZ());
					feature.setNormalColor(bizPoint.getNormalColor());
					feature.setHoverColor(bizPoint.getHoverColor());
					feature.setName(bizPoint.getName()+"【"+(bizPoint.getSort()+i+1)+"】");
					feature.setCode(bizPoint.getCode());
					feature.setSort(bizPoint.getSort()+i+1);
					feature.save();
				}
			}
			
			
			if(!StrKit.isBlank(decreasing)) {
				int decreasing1=Integer.parseInt(decreasing);
				Double z=Double.parseDouble(bizPoint.getZ());
				for(int i=0;i<decreasing1;i++) {
					z-=interval1+bizPoint.getDimensionsZ();
					BizSceneFeature feature=new BizSceneFeature();
					feature.setId(UuidUtil.getUUID());
					feature.setLayerId(bizPoint.getLayerId());
					feature.setX(bizPoint.getX());
					feature.setY(bizPoint.getY());
					feature.setZ(z+"");
					feature.setDimensionsX(bizPoint.getDimensionsX());
					feature.setDimensionsY(bizPoint.getDimensionsY());
					feature.setDimensionsZ(bizPoint.getDimensionsZ());
					feature.setRotationX(bizPoint.getRotationX());
					feature.setRotationY(bizPoint.getRotationY());
					feature.setRotationZ(bizPoint.getRotationZ());
					feature.setNormalColor(bizPoint.getNormalColor());
					feature.setHoverColor(bizPoint.getHoverColor());
					feature.setName(bizPoint.getName()+"【"+(bizPoint.getSort()-i-1)+"】");
					feature.setCode(bizPoint.getCode());
					feature.setSort(bizPoint.getSort()-i-1);
					feature.save();
				}
			}
			this.renderSuccess("成功！", bizPoint);
		}else {
			renderError("复制失败！");	
		}
	}
	

}
