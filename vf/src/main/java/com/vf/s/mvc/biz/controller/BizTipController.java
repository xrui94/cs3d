package com.vf.s.mvc.biz.controller;

import java.util.List;

import com.vf.core.controller.BaseController;
import com.vf.s.common.model.biz.BizPage;
import com.vf.s.common.model.biz.BizPanorama;
import com.vf.s.common.model.biz.BizPicture;
import com.vf.s.common.model.biz.BizProperty;
import com.vf.s.common.model.biz.BizText;
import com.vf.s.common.model.biz.BizVideo;
import com.vf.s.common.render.MouseEventRender;
import com.vf.s.common.render.PropertyRender;

public class BizTipController extends BaseController {
	public void index() {
		String objId=this.getPara("objId");
		String event=this.getPara("event");
		MouseEventRender mouseEventRender=new MouseEventRender();
		//文本
		BizText bizText=BizText.dao.findFirst("SELECT * FROM "+BizText.TABLE_NAME+" WHERE ID=? AND EVENT=? ",objId,event);
		//页面
		List<BizPage> bizPages=BizPage.dao.find("SELECT * FROM "+BizPage.TABLE_NAME+" WHERE  objId=? AND event=? ",objId,event);
		//属性
		List<BizProperty> bizPropertys=BizProperty.dao.find("SELECT * FROM "+BizProperty.TABLE_NAME+" WHERE  objId=? AND event=? ",objId,event);
		
		mouseEventRender.setBizText(bizText);
		mouseEventRender.setBizPages(bizPages);
		mouseEventRender.setBizPropertys(bizPropertys);
		this.set("mouseEventRender", mouseEventRender);
		this.render("tipsForLayer.html");
	}
	public void tipsForLayer() {
		String objId=this.getPara("objId");
		String event=this.getPara("event");
		this.set("sceneId", this.getPara("sceneId"));
		this.set("objId", this.getPara("objId"));
		PropertyRender propertyRender=new PropertyRender();
		//文本
		BizText bizText=BizText.dao.findFirst("SELECT * FROM "+BizText.TABLE_NAME+" WHERE ID=? AND EVENT=? ",objId,event);
		//页面
		List<BizPage> bizPages=BizPage.dao.find("SELECT * FROM "+BizPage.TABLE_NAME+" WHERE  objId=? AND event=? ",objId,event);
		//属性
		List<BizProperty> bizPropertys=BizProperty.dao.find("SELECT * FROM "+BizProperty.TABLE_NAME+" WHERE  objId=? AND event=? ",objId,event);
		List<BizPicture> bizPictures=BizPicture.dao.find("SELECT * FROM "+BizPicture.TABLE_NAME+" WHERE  objId=? AND event=? ",objId,event);
		List<BizPanorama> bizPanoramas=BizPanorama.dao.find("SELECT * FROM "+BizPanorama.TABLE_NAME+" WHERE  objId=? AND event=? ",objId,event);
		List<BizVideo> bizVideos=BizVideo.dao.find("SELECT * FROM "+BizVideo.TABLE_NAME+" WHERE  objId=? AND event=? ",objId,event);
		
		propertyRender.setBizText(bizText);
		propertyRender.setBizPages(bizPages);
		propertyRender.setBizPropertys(bizPropertys);
		propertyRender.setBizPictures(bizPictures);
		propertyRender.setBizPanoramas(bizPanoramas);
		propertyRender.setBizVideos(bizVideos);
		this.set("propertyRender", propertyRender);
		this.render("tipsForLayer.html");
	}
	
}
