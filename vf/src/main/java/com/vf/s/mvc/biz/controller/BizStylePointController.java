package com.vf.s.mvc.biz.controller;


import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Page;
import com.vf.core.controller.BaseController;
import com.vf.core.util.UuidUtil;
import com.vf.s.common.cache.style.MapStyleCache;
import com.vf.s.common.model.biz.BizStylePoint;

public class BizStylePointController extends BaseController {
	
	public void index() {
		this.render("list.html");
	}

	public void listData() {
		int pageNumber = getParaToInt("page", 1);
		int pageSize = getParaToInt("limit", 10);
		String name = getPara("name");
		String sqlExceptSelect = " from " + BizStylePoint.TABLE_NAME + " o  where 1=1 ";
		if (!StrKit.isBlank(name)) {
			sqlExceptSelect += " and o.name like '%" + name + "%'  ";
		}
		sqlExceptSelect += " order by o.createTime  desc ";
		Page<BizStylePoint> page = BizStylePoint.dao.paginate(pageNumber, pageSize, "select * ", sqlExceptSelect);
		renderLayuiPage(0, page.getList(), "", page.getTotalRow());
	}

	public void save() {
		BizStylePoint model = getModel(BizStylePoint.class);
		if (StrKit.notBlank(model.getId()) && BizStylePoint.dao.findById(model.getId())!=null) {
			if (model.update()) {
				MapStyleCache.put(model.getId(), BizStylePoint.dao.findById(model.getId()));
				renderSuccess("更新成功！",BizStylePoint.dao.findById(model.getId()));
			} else {
				renderError("更新失败！");
			}
		} else {
			if(StrKit.isBlank(model.getId()))
				model.setId(UuidUtil.getUUID());
			if (model.save()) {
				MapStyleCache.put(model.getId(), BizStylePoint.dao.findById(model.getId()));
				renderSuccess("保存成功！",BizStylePoint.dao.findById(model.getId()));
			} else {
				renderError("保存失败！");
			}
		}
	}
	
	public void findById() {
		BizStylePoint model = BizStylePoint.dao.findById(this.getPara("id"));
		if (model!=null) {
			renderSuccess("成功！",model);
		} else {
			renderError("记录不存在！");
		}
	}
	
	/***
	 * 删除
	 * @throws Exception
	 */
	public void delete() {
		BizStylePoint model = BizStylePoint.dao.findById(this.getPara("id"));
		if (model != null) {
			if (model.delete()) {
				MapStyleCache.remove(model.getId());
				renderSuccess("删除成功！");
			} else {
				renderError("删除失败！");
			}
		} else {
			renderError("数据不存在,请刷新后再试!");
		}
	}


	public void copy() {
		BizStylePoint model = BizStylePoint.dao.findById(this.getPara("id"));
		if (model != null) {
			model.setId(UuidUtil.getUUID());
			model.setName(model.getName()+"【复制】");
			if (model.save()) {
				MapStyleCache.put(model.getId(), BizStylePoint.dao.findById(model.getId()));
				renderSuccess("复制【"+model.getName()+"】成功！");
			} else {
				renderError("复制失败！");
			}
		} else {
			renderError("数据不存在,请刷新后再试!");
		}
	}
	
	
}
