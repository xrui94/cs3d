package com.vf.s.mvc.biz.controller;


import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Page;
import com.vf.core.controller.BaseController;
import com.vf.s.common.model.biz.BizWsLog;

public class BizWsLogController extends BaseController {
	
	public void index() {
		this.render("list.html");
	}

	public void listData() {
		int pageNumber = getParaToInt("page", 1);
		int pageSize = getParaToInt("limit", 10);
		String name = getPara("name");
		String sqlExceptSelect = " from " + BizWsLog.TABLE_NAME + " o  where 1=1 ";
		if (!StrKit.isBlank(name)) {
			sqlExceptSelect += " and o.name like '%" + name + "%'  ";
		}
		sqlExceptSelect += " order by o.createTime  desc ";
		Page<BizWsLog> page = BizWsLog.dao.paginate(pageNumber, pageSize, "select * ", sqlExceptSelect);
		renderLayuiPage(0, page.getList(), "", page.getTotalRow());
	}

	
	/***
	 * 删除
	 * @throws Exception
	 */
	public void delete() {
		BizWsLog model = BizWsLog.dao.findById(this.getPara("id"));
		if (model != null) {
			if (model.delete()) {
				renderSuccess("删除成功！");
			} else {
				renderError("删除失败！");
			}
		} else {
			renderError("数据不存在,请刷新后再试!");
		}
	}
	
}
