package com.vf.s.mvc.biz.controller;


import com.jfinal.aop.Inject;
import com.vf.core.controller.BaseController;
import com.vf.s.common.model.biz.BizPolygon;
import com.vf.s.mvc.biz.service.BizPolygonService;

public class BizOperationController extends BaseController {
	
	@Inject
	private BizPolygonService srv;
	
	public void index() {
		this.set("sceneId", this.get("sceneId"));
		BizPolygon bizPolygon=BizPolygon.dao.findById(get("polygonId"));
		this.set("bizPolygon", bizPolygon);
		render("polygon.html");
	}
	
	
	public void menu() {
		this.set("sceneId", this.get("sceneId"));
		this.set("type", this.get("type"));
		render("menu.html");
	}
	
	public void polygon() {
		this.set("sceneId", this.get("sceneId"));
		this.set("type", this.get("type"));
		render("polygon.html");
	}
	
	
	
	
}
