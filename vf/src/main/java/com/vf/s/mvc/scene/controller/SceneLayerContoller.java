package com.vf.s.mvc.scene.controller;

import java.util.ArrayList;
import java.util.List;

import com.jfinal.aop.Inject;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.vf.core.controller.BaseController;
import com.vf.core.model.ZtreeNode;
import com.vf.core.util.UuidUtil;
import com.vf.s.common.cache.style.MapLayerCache;
import com.vf.s.common.model.biz.BizLayer;
import com.vf.s.common.model.biz.BizLine;
import com.vf.s.common.model.biz.BizModel;
import com.vf.s.common.model.biz.BizPanorama;
import com.vf.s.common.model.biz.BizPicture;
import com.vf.s.common.model.biz.Point;
import com.vf.s.common.model.biz.BizPolygon;
import com.vf.s.common.model.biz.BizProperty;
import com.vf.s.common.model.biz.BizText;
import com.vf.s.common.model.biz.BizVideo;
import com.vf.s.common.model.biz.BizVideoModel;
import com.vf.s.common.model.biz.BizVideoPlane;
import com.vf.s.common.model.biz.BizVideoShed3d;
import com.vf.s.common.model.biz.Provider;
import com.vf.s.common.model.scene.BizSceneFeature;
import com.vf.s.common.model.scene.SceneLayer;
import com.vf.s.common.model.scene.BizSceneModel;
import com.vf.s.common.model.scene.BizSceneRoam;
import com.vf.s.common.model.scene.BizSceneRoamPoint;
import com.vf.s.common.model.scene.SceneProvider;
import com.vf.s.mvc.scene.service.SceneLayerService;
import com.vf.s.mvc.sys.service.DicService;

public class SceneLayerContoller extends BaseController {

	@Inject
	private SceneLayerService sceneLayerService;
	@Inject
	private DicService dicSrv;
	
	public void edit() {
		this.setAttr("properties",dicSrv.findItemList("sceneProperties"));
		this.setAttr("layer", SceneLayer.dao.findById(this.getPara("layerId")));
		render("edit.html");
	}

	public void group() {
		this.setAttr("parentLayer", SceneLayer.dao.findById(this.getPara("parentId")));
		this.setAttr("layer", SceneLayer.dao.findById(this.getPara("id")));
		render("group.html");
	}

	public void into() {
		this.setAttr("layer", SceneLayer.dao.findById(this.getPara("id")));
		render("into.html");
	}

	public void points() {
		this.setAttr("layer", SceneLayer.dao.findById(this.getPara("id")));
		render("points.html");
	}

	public void polylines() {
		this.setAttr("layer", SceneLayer.dao.findById(this.getPara("id")));
		render("polylines.html");
	}

	public void listData() {
		int pageNumber = getParaToInt("page", 1);
		int pageSize = getParaToInt("limit", 10);
		String name = getPara("name");
		String select = " select *  ";
		String sqlExceptSelect = " from " + BizLayer.TABLE_NAME + " o  where 1=1 ";
		if (!StrKit.isBlank(name)) {
			sqlExceptSelect += " and o.name like '%" + name + "%'  ";
		}
		Page<BizLayer> page = BizLayer.dao.paginate(pageNumber, pageSize, select, sqlExceptSelect);
		renderLayuiPage(0, page.getList(), "", page.getTotalRow());
	}

	public void save() {
		SceneLayer model = getModel(SceneLayer.class);
		if (StrKit.notBlank(model.getId()) && SceneLayer.dao.findById(model.getId()) != null) {
			if (StrKit.isBlank(model.getDefaultStyleId())) {
				model.setDefaultStyleId(UuidUtil.getUUID());
			}
			if (StrKit.isBlank(model.getHoverStyleId())) {
				model.setHoverStyleId(UuidUtil.getUUID());
			}
			if (StrKit.isBlank(model.getSelectedStyleId())) {
				model.setSelectedStyleId(UuidUtil.getUUID());
			}

			if (model.update()) {
				model = SceneLayer.dao.findById(model.getId());
				renderSuccess("更新成功！", model);
				MapLayerCache.put(model.getId(), model);
			} else {
				renderError("更新失败！");
			}
		} else {
			if (StrKit.isBlank(model.getId()))
				model.setId(UuidUtil.getUUID());
			if (StrKit.isBlank(model.getDefaultStyleId())) {
				model.setDefaultStyleId(UuidUtil.getUUID());
			}
			if (StrKit.isBlank(model.getHoverStyleId())) {
				model.setHoverStyleId(UuidUtil.getUUID());
			}
			if (StrKit.isBlank(model.getSelectedStyleId())) {
				model.setSelectedStyleId(UuidUtil.getUUID());
			}
			if (StrKit.isBlank(model.getType())) {
				model.setType("Layers");
			}
			if (model.save()) {
				model = SceneLayer.dao.findById(model.getId());
				renderSuccess("保存成功！", model);
				MapLayerCache.put(model.getId(), model);
			} else {
				renderError("保存失败！");
			}
		}
	}

	public void saveLayer() {
		BizLayer model = getModel(BizLayer.class);
		if (StrKit.notBlank(model.getId())) {
			if (model.update()) {
				renderSuccess("更新成功！", model);
			} else {
				renderError("更新失败！");
			}
		} else {
			model.setId(UuidUtil.getUUID());
			if (model.save()) {
				renderSuccess("保存成功！", model);
			} else {
				renderError("保存失败！");
			}
		}
	}

	public void getAsyncTreeData() {
		String id = this.get("id", "root");
		String sceneId = this.get("sceneId");
		String type = this.get("type");
		String code = this.get("code");

		List<ZtreeNode> nodelist = new ArrayList<ZtreeNode>();
		// 点集合
		if (StrKit.equals("Points", type)) {
			List<Point> olist = Point.dao.find("SELECT * FROM " + Point.TABLE_NAME + " WHERE LAYERID=? ", id);
			for (Point o : olist) {
				ZtreeNode node = new ZtreeNode();
				node.setId(o.getId());
				node.setName(o.getName());
				node.setType("Point");
				node.setCode(o.getCode());
				node.setIsParent(false);
				node.setParentId(o.getLayerId());
				nodelist.add(node);
			}
		}
		// 底图集合
		else if (StrKit.equals("Maps", type)) {
			List<Provider> olist = Provider.dao.find("SELECT a.name,a.code,b.id FROM " + Provider.TABLE_NAME + " A ,"
					+ SceneProvider.TABLE_NAME + " B  WHERE A.ID=B.PROVIDERID and  B.LAYERID=? ORDER BY B.SORT ASC ",
					id);
			for (Provider o : olist) {
				ZtreeNode node = new ZtreeNode();
				node.setId(o.getId());
				node.setName(o.getName());
				node.setType("Map");
				node.setCode(o.getCode());
				node.setIsParent(false);
				node.setParentId(id);
				nodelist.add(node);
			}
		}
		// 模型集合
		else if (StrKit.equals("Models", type)) {
			List<BizSceneModel> olist = BizSceneModel.dao
					.find("SELECT A.*,B.URL AS URI FROM " + BizSceneModel.TABLE_NAME + " A, " + BizModel.TABLE_NAME
							+ " B " + " WHERE A.MODELID=B.ID AND  A.LAYERID=?  ORDER BY A.SORT ASC  ", id);
			for (BizSceneModel o : olist) {
				ZtreeNode node = new ZtreeNode();
				node.setId(o.getId());
				node.setName(o.getName());
				node.setType("Model");
				node.setCode(o.getCode());
				node.setIsParent(false);
				node.setParentId(id);
				nodelist.add(node);
			}
		}
		// 点集合
		else if (StrKit.equals("Polylines", type)) {
			List<BizLine> olist = BizLine.dao.find("SELECT * FROM " + BizLine.TABLE_NAME + " WHERE LAYERID=? ", id);
			for (BizLine o : olist) {
				ZtreeNode node = new ZtreeNode();
				node.setId(o.getId());
				node.setName(o.getName());
				node.setType("Polyline");
				node.setCode(o.getCode());
				node.setIsParent(false);
				node.setParentId(o.getLayerId());
				nodelist.add(node);
			}
		}
		// 面集合
		else if (StrKit.equals("Polygons", type)) {
			List<BizPolygon> olist = BizPolygon.dao.find("SELECT * FROM " + BizPolygon.TABLE_NAME + " WHERE LAYERID=? ",
					id);
			for (BizPolygon o : olist) {
				ZtreeNode node = new ZtreeNode();
				node.setId(o.getId());
				node.setName(o.getName());
				node.setType("Polygon");
				node.setCode(o.getCode());
				node.setIsParent(false);
				node.setParentId(o.getLayerId());
				nodelist.add(node);
			}
		}
		// 单体集合
		else if (StrKit.equals("Features", type)) {
			List<BizSceneFeature> olist = BizSceneFeature.dao
					.find("SELECT * FROM " + BizSceneFeature.TABLE_NAME + " WHERE LAYERID=? ORDER BY SORT ASC", id);
			for (BizSceneFeature o : olist) {
				ZtreeNode node = new ZtreeNode();
				node.setId(o.getId());
				node.setName(o.getName());
				node.setType("Feature");
				node.setCode(o.getCode());
				node.setIsParent(false);
				node.setParentId(o.getLayerId());
				nodelist.add(node);
			}
		} else if (StrKit.equals("VideoPlanes", type)) {
			List<BizVideoPlane> olist = BizVideoPlane.dao
					.find("SELECT * FROM " + BizVideoPlane.TABLE_NAME + " WHERE LAYERID=? and sceneId=? ", id, sceneId);
			for (BizVideoPlane o : olist) {
				ZtreeNode node = new ZtreeNode();
				node.setId(o.getId());
				node.setName(o.getName());
				node.setType("VideoPlane");
				node.setCode(o.getCode());
				node.setIsParent(false);
				node.setParentId(o.getLayerId());
				nodelist.add(node);
			}
		} else if (StrKit.equals("VideoShed3ds", type)) {
			List<BizVideoShed3d> olist = BizVideoShed3d.dao
					.find("SELECT * FROM " + BizVideoShed3d.TABLE_NAME + " WHERE LAYERID=? and sceneId=?", id, sceneId);
			for (BizVideoShed3d o : olist) {
				ZtreeNode node = new ZtreeNode();
				node.setId(o.getId());
				node.setName(o.getName());
				node.setType("VideoShed3d");
				node.setCode(o.getCode());
				node.setIsParent(false);
				node.setParentId(o.getLayerId());
				nodelist.add(node);
			}
		} else if (StrKit.equals("VideoModels", type)) {
			List<BizVideoModel> olist = BizVideoModel.dao
					.find("SELECT * FROM " + BizVideoModel.TABLE_NAME + " WHERE LAYERID=? and sceneId=?", id, sceneId);
			for (BizVideoModel o : olist) {
				ZtreeNode node = new ZtreeNode();
				node.setId(o.getId());
				node.setName(o.getName());
				node.setType("VideoModel");
				node.setCode(o.getCode());
				node.setIsParent(false);
				node.setParentId(o.getLayerId());
				nodelist.add(node);
			}
		} else if (StrKit.equals("Roams", type)) {
			List<BizSceneRoam> olist = BizSceneRoam.dao
					.find("SELECT * FROM " + BizSceneRoam.TABLE_NAME + " WHERE LAYERID=?  order by sort asc ", id);
			for (BizSceneRoam o : olist) {
				ZtreeNode node = new ZtreeNode();
				node.setId(o.getId());
				node.setName(o.getName());
				node.setType("Roam");
				node.setCode(o.getCode());
				node.setIsParent(false);
				node.setParentId(o.getLayerId());
				nodelist.add(node);
			}
		} else {
			String sql = "SELECT a.*,(CASE WHEN (select count(id) from " + SceneLayer.TABLE_NAME
					+ " where a.id=parentId ) >0 THEN true ELSE false END) AS isParent FROM " + SceneLayer.TABLE_NAME
					+ " a where a.parentId=? ";
			if (!StrKit.isBlank(code)) {
				sql += " and a.code='" + code + "' ";
			}
			String parentId = sceneId;
			if (!StrKit.isBlank(id) && !StrKit.equals(id, "root")) {
				parentId = id;
			}
			sql += " order by a.sort asc ";
			List<SceneLayer> olist = SceneLayer.dao.find(sql, parentId);
			for (SceneLayer o : olist) {
				ZtreeNode node = SceneLayer.dao.toZTreeNode(o);
				if (StrKit.equals("Points", o.getType())) {
					node.setIsParent(false);
					node.setCount(Db.queryInt("select count(*) from " + Point.TABLE_NAME + " where LAYERID=? ",
							node.getId()));
				} else if (StrKit.equals("Polylines", o.getType())) {
					node.setIsParent(false);
					node.setCount(Db.queryInt("select count(*) from " + BizLine.TABLE_NAME + " where LAYERID=? ",
							node.getId()));
				}
				else if (StrKit.equals("Polygons", o.getType())) {
					/* node.setIsParent(false); */
					node.setIsParent(true);
					node.setCount(Db.queryInt("select count(*) from " + BizPolygon.TABLE_NAME + " where LAYERID=? ",
							node.getId()));
				} else {
					node.setIsParent(true);
				}
				node.setParentId(o.getParentId());
				node.setDefaultStyleId(o.getDefaultStyleId());
				node.setHoverStyleId(o.getHoverStyleId());
				node.setSelectedStyleId(o.getSelectedStyleId());
				nodelist.add(node);
			}
		}
		renderJson(nodelist);
	}

	public void delete() {
		String id = getPara("id");
		String type = getPara("type");
		if (StrKit.equals("Point", type)) {
			if (Point.dao.deleteById(id)) {
				renderSuccess("删除成功");
				Db.delete("DELETE FROM " + BizProperty.TABLE_NAME + " WHERE OBJID=? ", id);
				Db.delete("DELETE FROM " + BizPicture.TABLE_NAME + " WHERE OBJID=? ", id);
				Db.delete("DELETE FROM " + BizVideo.TABLE_NAME + " WHERE OBJID=? ", id);
				Db.delete("DELETE FROM " + BizPanorama.TABLE_NAME + " WHERE OBJID=? ", id);
				Db.delete("DELETE FROM " + BizText.TABLE_NAME + " WHERE OBJID=? ", id);
				return;
			} else {
				renderError("失败!");
				return;
			}
		}
		if (StrKit.equals("Map", type)) {
			if (SceneProvider.dao.deleteById(id)) {
				renderSuccess("删除成功");
				return;
			} else {
				renderError("失败!");
				return;
			}
		}
		if (StrKit.equals("Model", type)) {
			if (BizSceneModel.dao.deleteById(id)) {
				renderSuccess("删除成功");
				Db.delete("DELETE FROM " + BizProperty.TABLE_NAME + " WHERE OBJID=? ", id);
				Db.delete("DELETE FROM " + BizPicture.TABLE_NAME + " WHERE OBJID=? ", id);
				Db.delete("DELETE FROM " + BizVideo.TABLE_NAME + " WHERE OBJID=? ", id);
				Db.delete("DELETE FROM " + BizPanorama.TABLE_NAME + " WHERE OBJID=? ", id);
				Db.delete("DELETE FROM " + BizText.TABLE_NAME + " WHERE OBJID=? ", id);
				return;
			} else {
				renderError("失败!");
				return;
			}
		}
		if (StrKit.equals("Polyline", type)) {
			if (BizLine.dao.deleteById(id)) {
				renderSuccess("删除成功");
				Db.delete("DELETE FROM " + BizProperty.TABLE_NAME + " WHERE OBJID=? ", id);
				Db.delete("DELETE FROM " + BizPicture.TABLE_NAME + " WHERE OBJID=? ", id);
				Db.delete("DELETE FROM " + BizVideo.TABLE_NAME + " WHERE OBJID=? ", id);
				Db.delete("DELETE FROM " + BizPanorama.TABLE_NAME + " WHERE OBJID=? ", id);
				Db.delete("DELETE FROM " + BizText.TABLE_NAME + " WHERE OBJID=? ", id);
				Db.delete("DELETE FROM " + BizText.TABLE_NAME + " WHERE OBJID=? ", id);
				return;
			} else {
				renderError("失败!");
				return;
			}
		}
		if (StrKit.equals("Polygon", type)) {
			if (BizPolygon.dao.deleteById(id)) {
				renderSuccess("删除成功");
				Db.delete("DELETE FROM " + BizProperty.TABLE_NAME + " WHERE OBJID=? ", id);
				Db.delete("DELETE FROM " + BizPicture.TABLE_NAME + " WHERE OBJID=? ", id);
				Db.delete("DELETE FROM " + BizVideo.TABLE_NAME + " WHERE OBJID=? ", id);
				Db.delete("DELETE FROM " + BizPanorama.TABLE_NAME + " WHERE OBJID=? ", id);
				Db.delete("DELETE FROM " + BizText.TABLE_NAME + " WHERE OBJID=? ", id);
				Db.delete("DELETE FROM " + BizText.TABLE_NAME + " WHERE OBJID=? ", id);
				Db.delete("DELETE FROM BIZ_STYLE_OBJ WHERE OBJID=? ",id);
				return;
			} else {
				renderError("失败!");
				return;
			}
		}
		if (StrKit.equals("Feature", type)) {
			if (BizSceneFeature.dao.deleteById(id)) {
				renderSuccess("删除成功");
				Db.delete("DELETE FROM " + BizProperty.TABLE_NAME + " WHERE OBJID=? ", id);
				Db.delete("DELETE FROM " + BizPicture.TABLE_NAME + " WHERE OBJID=? ", id);
				Db.delete("DELETE FROM " + BizVideo.TABLE_NAME + " WHERE OBJID=? ", id);
				Db.delete("DELETE FROM " + BizPanorama.TABLE_NAME + " WHERE OBJID=? ", id);
				Db.delete("DELETE FROM " + BizText.TABLE_NAME + " WHERE OBJID=? ", id);
				Db.delete("DELETE FROM " + BizText.TABLE_NAME + " WHERE OBJID=? ", id);
				return;
			} else {
				renderError("失败!");
				return;
			}
		}
		if (StrKit.equals("VideoPlane", type)) {
			if (BizVideoPlane.dao.deleteById(id)) {
				renderSuccess("删除成功");
				Db.delete("DELETE FROM " + BizProperty.TABLE_NAME + " WHERE OBJID=? ", id);
				Db.delete("DELETE FROM " + BizPicture.TABLE_NAME + " WHERE OBJID=? ", id);
				Db.delete("DELETE FROM " + BizVideo.TABLE_NAME + " WHERE OBJID=? ", id);
				Db.delete("DELETE FROM " + BizPanorama.TABLE_NAME + " WHERE OBJID=? ", id);
				Db.delete("DELETE FROM " + BizText.TABLE_NAME + " WHERE OBJID=? ", id);
				return;
			} else {
				renderError("失败!");
				return;
			}
		}
		if (StrKit.equals("VideoShed3d", type)) {
			if (BizVideoShed3d.dao.deleteById(id)) {
				renderSuccess("删除成功");
				Db.delete("DELETE FROM " + BizProperty.TABLE_NAME + " WHERE OBJID=? ", id);
				Db.delete("DELETE FROM " + BizPicture.TABLE_NAME + " WHERE OBJID=? ", id);
				Db.delete("DELETE FROM " + BizVideo.TABLE_NAME + " WHERE OBJID=? ", id);
				Db.delete("DELETE FROM " + BizPanorama.TABLE_NAME + " WHERE OBJID=? ", id);
				Db.delete("DELETE FROM " + BizText.TABLE_NAME + " WHERE OBJID=? ", id);
				return;
			} else {
				renderError("失败!");
				return;
			}
		}
		if (StrKit.equals("VideoModel", type)) {
			if (BizVideoModel.dao.deleteById(id)) {
				renderSuccess("删除成功");
				Db.delete("DELETE FROM " + BizProperty.TABLE_NAME + " WHERE OBJID=? ", id);
				Db.delete("DELETE FROM " + BizPicture.TABLE_NAME + " WHERE OBJID=? ", id);
				Db.delete("DELETE FROM " + BizVideo.TABLE_NAME + " WHERE OBJID=? ", id);
				Db.delete("DELETE FROM " + BizPanorama.TABLE_NAME + " WHERE OBJID=? ", id);
				Db.delete("DELETE FROM " + BizText.TABLE_NAME + " WHERE OBJID=? ", id);
				return;
			} else {
				renderError("失败!");
				return;
			}
		}
		if (StrKit.equals("Roam", type)) {
			if (BizSceneRoam.dao.deleteById(id)) {
				renderSuccess("删除成功");
				Db.delete("DELETE FROM " + BizSceneRoamPoint.TABLE_NAME + " WHERE ROAMID=? ", id);
				return;
			} else {
				renderError("失败!");
				return;
			}
		} else {
			SceneLayer bizSceneLayer = SceneLayer.dao.findById(id);
			if (bizSceneLayer != null) {
				if (bizSceneLayer.delete()) {
					renderSuccess("删除成功", bizSceneLayer);
					MapLayerCache.remove(id);
				} else {
					renderError("删除失败！");
				}
			} else {
				renderError("数据不存在!");
				return;
			}
		}
	}

	public void getTree() {
		List<SceneLayer> menuList = sceneLayerService.getChildrenAllTree(this.getPara("sceneId"), this.getPara("type"));
		List<ZtreeNode> nodelist = sceneLayerService.toZTreeNode(menuList, true);
		renderJson(nodelist);
	}

	public void findChildren() {
		List<?> nodes = sceneLayerService.findChildren(this.getPara("id"), this.getPara("type"));
		if (nodes != null && nodes.size() > 0) {
			renderSuccess("成功", nodes);
		} else {
			renderError("无数据!");
		}
	}

}
