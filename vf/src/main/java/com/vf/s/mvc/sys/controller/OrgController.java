package com.vf.s.mvc.sys.controller;

import java.util.List;

import com.jfinal.aop.Inject;
import com.jfinal.kit.StrKit;
import com.jfinal.log.Log;
import com.vf.core.controller.BaseController;
import com.vf.core.model.ZtreeNode;
import com.vf.core.render.RenderLayuiBean;
import com.vf.core.util.UuidUtil;
import com.vf.s.common.model.sys.SysOrg;
import com.vf.s.mvc.sys.service.OrgService;

public class OrgController extends BaseController{
	
	protected final Log LOG = Log.getLog(getClass());
	
	@Inject
	private OrgService srv;

	public void index() {
		render("list.html");
	}
	
	/**
	 * 加载菜单列表
	 */
	public void listData() {
		RenderLayuiBean renderBean=srv.listData(this);
		this.renderJson(renderBean);
	}
	
	/***
	 * 保存
	 */
	public void save() {
		SysOrg menu = getModel(SysOrg.class);
		if (StrKit.notBlank(menu.getID())) {
			SysOrg codeMenu=SysOrg.dao.findFirst("SELECT * FROM "+SysOrg.TABLE_NAME +" WHERE CODE=? AND ID!=?",menu.getCODE(),menu.getID());
			if(codeMenu!=null) {
				renderError("标识已存在！");
				return ;
			}
			SysOrg nameMenu=SysOrg.dao.findFirst("SELECT * FROM "+SysOrg.TABLE_NAME +" WHERE NAME=? AND ID!=?",menu.getNAME(),menu.getID());
			if(nameMenu!=null) {
				renderError("名称已存在！");
				return ;
			}
			if (menu.update()) {
				renderSuccess("更新成功！");
			} else {
				renderError("更新失败！");
			}
		} else {
			SysOrg codeMenu=SysOrg.dao.findFirst("SELECT * FROM "+SysOrg.TABLE_NAME +" WHERE CODE=?",menu.getCODE());
			if(codeMenu!=null) {
				renderError("标识已存在！");
				return ;
			}
			SysOrg nameMenu=SysOrg.dao.findFirst("SELECT * FROM "+SysOrg.TABLE_NAME +" WHERE NAME=?",menu.getNAME());
			if(nameMenu!=null) {
				renderError("名称已存在！");
				return ;
			}
			menu.setID(UuidUtil.getUUID());
			if(StrKit.isBlank(menu.getPARENTID()))
				menu.setPARENTID("root");
			if (menu.save()) {
				renderSuccess("保存成功！");
			} else {
				renderError("保存失败！");
			}
		}

	}

	/***
	 * 删除
	 * 
	 * @throws Exception
	 */
	public void delete() throws Exception {
		String id = getPara("id");
		List<SysOrg> list = srv.getChildrenByPid(id,"");
		if (list.size() <= 0) {
			SysOrg menu = SysOrg.dao.findById(id);
			if (menu != null) {
				if (menu.delete()) {
					renderSuccess("删除成功！");
				} else {
					renderError("删除失败！");
				}
			} else {
				renderError("数据不存在,请刷新后再试!");
			}
		} else {
			renderError("当前节点有子节点,不允许删除!");
			return;
		}
	}
	
	 /***
     * 获取树
     */
    public void selectTree(){
    	Boolean open = true;//是否展开所有
    	Boolean ifOnlyLeaf = false;//是否只选叶子
    	if(StrKit.notBlank(getPara("ifOnlyLeaf"))){//是否查询所有孩子
    		if("1".equals(getPara("ifOnlyLeaf"))){
    			ifOnlyLeaf = true;
    		}
    	}
		List<SysOrg> menuList = srv.getChildrenAllTree("root",null);
    	List<ZtreeNode> nodelist =srv.toZTreeNode(menuList,open,ifOnlyLeaf);//数据库中的菜单
    	renderJson(nodelist);
    }
    

}
