package com.vf.s.mvc.biz.controller;


import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Supplier;

import com.jfinal.aop.Inject;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Page;
import com.vf.core.controller.BaseController;
import com.vf.s.common.model.biz.BizLayer;
import com.vf.s.common.model.biz.BizLine;
import com.vf.s.common.model.scene.Scene;
import com.vf.s.mvc.biz.service.BizLineService;

public class BizLineController extends BaseController {
	
	@Inject
	private BizLineService srv;
	public void index() {
		render("list.html");
	}
	
	public void operation() {
		this.set("bizLine", BizLine.dao.findById(get("polylineId")));
		render("operation.html");
	}

	public void edit() {
		this.set("bizLine", BizLine.dao.findById(get("lineId")));
		render("edit.html");
	}
	
	public void list() {
		List<Scene> scenes=Scene.dao.find("SELECT * FROM "+Scene.TABLE_NAME);
		this.set("scenes",scenes);
		render("polyLineForList.html");
	}
	
	public void listData() {
		int pageNumber = getParaToInt("page", 1);
		int pageSize = getParaToInt("limit", 10);
		String name = getPara("name");
		String sceneId = getPara("sceneId");
		
		String select = " select *  ";
		String sqlExceptSelect = " from " + BizLine.TABLE_NAME + " o  where 1=1 ";
		if (!StrKit.isBlank(name)) {
			sqlExceptSelect += " and o.name like '%" + name + "%'  ";
		}
		
		if (!StrKit.isBlank(sceneId)) {
			sqlExceptSelect += " and o.sceneId = '" + sceneId + "'  ";
		}
		Page<BizLine> page = BizLine.dao.paginate(pageNumber, pageSize, select, sqlExceptSelect);
		renderLayuiPage(0, page.getList(), "", page.getTotalRow());
	}
	
	/**
	 * 保存
	 */
	public void save() {
		BizLine model = getModel(BizLine.class);
		BizLine dbBizLine=BizLine.dao.findById(model.getId());
		if(dbBizLine!=null) {
			if (model.update()) {
				renderSuccess("更新成功！",BizLine.dao.findById(model.getId()));
			} else {
				renderError("更新失败！");
			}
		}else {
			if (model.save()) {
				renderSuccess("保存成功！",BizLine.dao.findById(model.getId()));
			} else {
				renderError("保存失败！");
			}
		}
	}
	
	public void delete() throws Exception {
		String id = getPara("id");
		boolean isDel=BizLine.dao.deleteById(id);
		if (isDel) {
			renderSuccess("删除成功！");
		} else {
			renderError("删除失败！");
		}
	}
	

	
	
	/**
	 * 样式
	 */
	public void polyLineForStyle() {
		this.set("sceneId", this.get("sceneId"));
		BizLine bizLine=BizLine.dao.findById(get("lineId"));
		this.set("bizLine", bizLine);
		render("polyLineForStyle.html");
	}
	
	
	/***
	 * 保存样式
	 */
	
	public void saveForStyle() {
		BizLine model = getModel(BizLine.class);
		BizLine dbBizLine=BizLine.dao.findById(model.getId());
		if(dbBizLine!=null) {
			if (model.update()) {
				renderSuccess("更新成功！",model);
			} else {
				renderError("更新失败！");
			}
		}
//		String lineId=this.getPara("lineId");
//		String styleId=this.getPara("styleId");
//		String event=this.getPara("event");
//		BizLine bizLine=BizLine.dao.findById(lineId);
//		if(bizLine!=null ) {
//			if(StrKit.equals(event, "DEFAULT")) {
//				bizLine.setDefaultStyleId(styleId);
//			}
//			else  if(StrKit.equals(event, "HOVER")) {
//				bizLine.setHoverStyleId(styleId);
//			}
//			else  if(StrKit.equals(event, "SELECTED")) {
//				bizLine.setSelectedStyleId(styleId);
//			}else {
//				bizLine.setDefaultStyleId(styleId);
//			}
//			if (bizLine.update()) {
//				renderSuccess("成功！",bizLine);
//			} else {
//				renderError("失败！");
//			}
//		}else {
//			renderError("点或样式不存在！");
//		}
	}
	
	/**
	 * 菜单
	 */
	public void polyLineForMenu() {
		this.set("sceneId", this.get("sceneId"));
		BizLine bizLine=BizLine.dao.findById(get("lineId"));
		this.set("bizLine", bizLine);
		render("polyLineForMenu.html");
	}
	
	ExecutorService executor = Executors.newFixedThreadPool(20);
	public void batchSaveStyle() throws Exception {
		String layerId=this.getPara("layerId");
		renderSuccess("异步处理中！！！");
	    CompletableFuture<Integer> future = CompletableFuture.supplyAsync(new Supplier<Integer>() {
	        @Override
	        public Integer get() {
	            try {
	        		List<BizLayer> bizLayers=BizLayer.dao.find("SELECT * FROM "+BizLayer.TABLE_NAME+" WHERE PARENTID=? AND TYPE='line' ",layerId);
	        		BizLine bizLine;
	        		for(BizLayer bizLayer:bizLayers) {
	        			bizLine=BizLine.dao.findById(bizLayer.getId());
	        			if(bizLine!=null) {
	        				bizLine.update();
	        			}
	        		}
	            } catch (Exception e) {
	                e.printStackTrace();
	            }
	            return 3;
	        }
	    }, executor);
	    future.thenAccept(e -> System.out.println(e));
	}
	
}
