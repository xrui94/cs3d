package com.vf.s.mvc.scene.controller;


import java.util.List;

import com.jfinal.core.NotAction;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Page;
import com.vf.core.controller.BaseController;
import com.vf.core.util.UuidUtil;
import com.vf.s.common.model.biz.Provider;
import com.vf.s.common.model.scene.SceneProvider;
public class SceneProviderContoller  extends BaseController{
	
	public void setting() {
		render("setting.html");
	}
	
	public void select() {
		this.setAttr("layerId",getPara("layerId"));
		render("select.html");
	}
	
	public void listData() {
		int pageNumber = getParaToInt("page", 1);
		int pageSize = getParaToInt("limit", 10);
		String sceneId = getPara("sceneId");
		String keywords = getPara("keywords");
		String type = getPara("type");
		String isPublic = getPara("isPublic");
		String select = " select o.* ,(CASE WHEN (select count(id) from "+SceneProvider.TABLE_NAME+" a where a.providerId=o.id and a.sceneId='"+sceneId+"' ) >0 THEN true ELSE false END) AS selected  ";
		String sqlExceptSelect = " from " + Provider.TABLE_NAME + " o  where 1=1 ";
		if (!StrKit.isBlank(keywords)) {
			sqlExceptSelect += " and o.name like '%" + keywords + "%'  ";
		}
		if (!StrKit.isBlank(type)) {
			sqlExceptSelect += " and o.dataType = '" + type + "'  ";
		}
		if (!StrKit.isBlank(isPublic)) {
			sqlExceptSelect += " and o.isPublic = '" + isPublic + "'  ";
		}
		Page<Provider> page = Provider.dao.paginate(pageNumber, pageSize, select, sqlExceptSelect);
		renderLayuiPage(0, page.getList(), "", page.getTotalRow());
	}
	
	public void save() {
		SceneProvider model = getModel(SceneProvider.class);
		if(SceneProvider.dao.findById(model.getId())!=null) {
			if (model.update()) {
				renderSuccess("更新成功！", model);
			} else {
				renderError("更新失败！");
			}
		}
		else {
			if(null==SceneProvider.dao.findFirst("select * from "+SceneProvider.TABLE_NAME+" where providerId=? and sceneId=? ",model.getProviderId(),model.getSceneId())) {
				model.setId(UuidUtil.getUUID());
				if (model.save()) {
					renderSuccess("保存成功！", model);
				} else {
					renderError("保存失败！");
				}
			}else {
				renderError("不允许重复添加！");
			}
		}
	}
	
	public void removeById() {
		SceneProvider sceneProvider=SceneProvider.dao.findFirst("select * from "+SceneProvider.TABLE_NAME+" where SCENEID=? and PROVIDERID=? ",this.getPara("sceneId"),this.getPara("id"));
		if(sceneProvider!=null) {
			if(sceneProvider.delete()) {
				
				
				renderSuccess("移出成功！", Provider.dao.findById(sceneProvider.getProviderId()));
			}else {
				renderError("移出失败！");
			}
		}else {
			renderError("不数据不存在！");
		}
	}
	
	@NotAction
	public int getSort(String sceneId) {
		List<SceneProvider> list=SceneProvider.dao.find("SELECT * FROM "+SceneProvider.TABLE_NAME+" a , "+Provider.TABLE_NAME+" b WHERE and a.sceneId=? and a.Providerid=b.id ans b.dataType!='terrain'  ORDER BY a.SORT asc  ",sceneId);
		if(list!=null) {
			return list.get(list.size()-1).getSort()+1;
		}else {
			return 0;
		}
	}
	
	@NotAction
	public void updateSort(int index) {
	}
	
//	public void delete(){
//		String id = getPara("id");
//		String mapId = getPara("mapId");
//		String layerId = getPara("layerId");
//		if(StrKit.isBlank(id)) {
//			BizSceneMap bizSceneMap=BizSceneMap.dao.findFirst("SELECT * FROM "+BizSceneMap.TABLE_NAME+" WHERE MAPID=? AND LAYERID=? ",mapId,layerId);
//			if(bizSceneMap!=null) {
//				if(bizSceneMap.delete()) {
//					renderSuccess("删除成功！");
//				}else {
//					renderError("删除失败！");
//				}
//			}else {
//				renderError("数据不存在！");
//			}
//		}else {
//			BizSceneMap model=BizSceneMap.dao.findById(id);
//			if(model!=null) {
//				if(model.delete()) {
//					renderSuccess("删除成功！");
//				}else {
//					renderError("删除失败！");
//				}
//			}else {
//				renderError("数据不存在！");
//			}
//		}
//	}
	
//	public void listData() {
//		int pageNumber = getParaToInt("page", 1);
//		int pageSize = getParaToInt("limit", 10);
//		String name = getPara("name");
//		String type = getPara("type");
//		String sceneId = getPara("sceneId");
//		String select = " select *  ";
//		String sqlExceptSelect = " from " + BizMap.TABLE_NAME + " o  where 1=1 ";
//		if (!StrKit.isBlank(name)) {
//			sqlExceptSelect += " and o.name like '%" + name + "%'  ";
//		}
//		if (!StrKit.isBlank(type)) {
//			sqlExceptSelect += " and o.type = '" + type + "'  ";
//		}
//		Page<BizMap> page = BizMap.dao.paginate(pageNumber, pageSize, select, sqlExceptSelect);
//		renderLayuiPage(0, page.getList(), "", page.getTotalRow());
//	}
//	
	public void getList() {
		String sceneId = getPara("sceneId");
		String layerId = getPara("layerId");
		String whereForMap="SELECT A.* FROM "+Provider.TABLE_NAME+" A , "+SceneProvider.TABLE_NAME+" B WHERE A.ID=B.PROVIDERID AND B.SCENEID=? ";
		if(!StrKit.isBlank(layerId)) {
			whereForMap+=" AND B.LAYERID='"+layerId+"' ";
		}
		whereForMap+=" ORDER BY B.SORT ASC ";
		List<Provider> maps=Provider.dao.find(whereForMap,sceneId);
		
		if(maps!=null  && maps.size()>0) {
			this.renderSuccess("成功！", maps);
		}else {
			this.renderError("未查询到底图资源！");
		}
	}
	
	public void getById() {
		String layerId = getPara("id");
		Provider bizMap=Provider.dao.findFirst("select A.*,B.SORT from "+Provider.TABLE_NAME+" A,"+SceneProvider.TABLE_NAME+" B where A.ID=B.PROVIDERID AND B.id=? ",layerId);
		if(bizMap!=null) {
			this.renderSuccess("成功！", bizMap);
		}else {
			renderError("数据不存在!");
		}
	}
}
