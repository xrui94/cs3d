package com.vf.s.mvc.scene.controller;

import java.util.List;

import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Page;
import com.vf.core.controller.BaseController;
import com.vf.core.util.UuidUtil;
import com.vf.s.common.model.scene.*;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class BizSceneRoamController extends BaseController {
	
	public void index() {
		this.render("list.html");
	}
	
	public void setting() {
		this.setAttr("roam",BizSceneRoam.dao.findById(this.getPara("roamId")));
		render("setting.html");
	}
	
	public void operation() {
		this.set("sceneId", this.get("sceneId"));
		this.set("roamId", this.get("roamId"));
		BizSceneRoam bizSceneRoam=BizSceneRoam.dao.findById(get("roamId"));
		this.set("bizSceneRoam", bizSceneRoam);
		render("operation.html");
	}
	
	public void edit() {
		this.set("sceneId", this.get("sceneId"));
		BizSceneRoam bizSceneRoam=BizSceneRoam.dao.findById(get("roamId"));
		this.set("bizSceneRoam", bizSceneRoam);
		render("edit.html");
	}
	
	public void doingRoam() {
		this.set("sceneId", this.get("sceneId"));
		BizSceneRoam bizSceneRoam=BizSceneRoam.dao.findById(get("roamId"));
		this.set("bizSceneRoam", bizSceneRoam);
		this.set("roamId", get("roamId"));
		render("doingRoam.html");
	}
	
	public void listData() {
		int pageNumber = getParaToInt("page", 1);
		int pageSize = getParaToInt("limit", 10);
		String name = getPara("name");
		String sceneId = getPara("sceneId");
		String layersId = getPara("layersId");
		
		String sqlExceptSelect = " from " + BizSceneRoam.TABLE_NAME + " o  where 1=1 ";
		if (!StrKit.isBlank(name)) {
			sqlExceptSelect += " and o.name like '%" + name + "%'  ";
		}
		if (!StrKit.isBlank(layersId)) {
			sqlExceptSelect += " AND O.layersId='"+layersId+"' ";
		}
		if (!StrKit.isBlank(sceneId)) {
			sqlExceptSelect += " AND O.SCENEID='"+sceneId+"' ";
		}
		Page<BizSceneRoam> page = BizSceneRoam.dao.paginate(pageNumber, pageSize, "select * ", sqlExceptSelect);
		renderLayuiPage(0, page.getList(), "", page.getTotalRow());
	}
	
	public void save() {
		BizSceneRoam model = getModel(BizSceneRoam.class);
		if(!StrKit.isBlank(model.getId())) {
			if (model.update() ) {
				renderSuccess("更新成功！",BizSceneRoam.dao.findById(model.getId()));
			} else {
				renderError("更新失败！");
			}
		}else {
			model.setId(UuidUtil.getUUID());
			if(model.save()) {
				renderSuccess("保存成功！",BizSceneRoam.dao.findById(model.getId()));
			} else {
				renderError("保存失败！");
			}
		}
	}

	/***
	 * 删除
	 * @throws Exception
	 */
	public void delete() {
		BizSceneRoam model = BizSceneRoam.dao.findById(this.getPara("id"));
		if (model != null) {
			if (model.delete()) {
				renderSuccess("删除成功！");
			} else {
				renderError("删除失败！");
			}
		} else {
			renderError("数据不存在,请刷新后再试!");
		}
	}
	
	public void savePoint() {
		BizSceneRoamPoint model = getModel(BizSceneRoamPoint.class);
		if(!StrKit.isBlank(model.getId())) {
			if (model.update() ) {
				renderSuccess("更新成功！",BizSceneRoamPoint.dao.findById(model.getId()));
			} else {
				renderError("更新失败！");
			}
		}else {
			model.setId(UuidUtil.getUUID());
			if(model.save()) {
				renderSuccess("保存成功！",BizSceneRoamPoint.dao.findById(model.getId()));
			} else {
				renderError("保存失败！");
			}
		}
	}
	
	public void getById() {
		String roamId=this.getPara("roamId");
		BizSceneRoam bizSceneRoam=BizSceneRoam.dao.findById(roamId);
		if(bizSceneRoam!=null) {
			List<BizSceneRoamPoint> points=BizSceneRoamPoint.dao.find("SELECT * FROM "+BizSceneRoamPoint.TABLE_NAME+" WHERE roamId=? ",roamId);
			bizSceneRoam.setPoints(points);
			renderSuccess("成功！",bizSceneRoam);
		}else {
			renderError("数据不存在！");
		}
	}
	
	public void savePoints() {
		String roamId=this.getPara("roamId");
		String layerId=this.getPara("layerId");
		String points=this.getPara("points");
		try {
			JSONArray array=JSONArray.fromObject(points);
			if(array!=null) {
				BizSceneRoam bizSceneRoam;
				if(StrKit.isBlank(roamId)) {
					roamId=UuidUtil.getUUID();
					bizSceneRoam=new BizSceneRoam();
					bizSceneRoam.setId(roamId);
					bizSceneRoam.setLayerId(layerId);
					bizSceneRoam.save();
				}
				
				BizSceneRoamPoint BizSceneRoamPoint;
				for(int i=0;i<array.size();i++) {
					JSONObject json = JSONObject.fromObject(array.get(i));
					BizSceneRoamPoint=new BizSceneRoamPoint();
					BizSceneRoamPoint.setId(UuidUtil.getUUID());
					BizSceneRoamPoint.setRoamId(roamId);
					BizSceneRoamPoint.setName("【"+i+"】");
					BizSceneRoamPoint.setX(json.getString("x"));
					BizSceneRoamPoint.setY(json.getString("y"));
					BizSceneRoamPoint.setZ(json.getString("z"));
					BizSceneRoamPoint.setSort(i);
					BizSceneRoamPoint.save();
				}
				renderSuccess("成功！");
			}else {
				renderError("无数据！");
				return;
			}
		}catch (Exception e) {
			renderError("格式异常！");
			return;
		}
	}
	
	public void listDataForPoint() {
		String roamId = getPara("roamId");
		List<BizSceneRoamPoint> list=BizSceneRoamPoint.dao.find("SELECT * FROM "+BizSceneRoamPoint.TABLE_NAME+" WHERE ROAMID=? order by sort asc ",roamId);
		renderLayuiPage(0, list, "", list.size());
	}
	

}
