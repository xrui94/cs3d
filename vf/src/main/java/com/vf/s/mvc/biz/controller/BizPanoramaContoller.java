package com.vf.s.mvc.biz.controller;

import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Page;
import com.vf.core.controller.BaseController;
import com.vf.core.util.UuidUtil;
import com.vf.s.common.model.biz.BizPanorama;
import com.vf.s.common.plugins.shiro.ShiroKit;

public class BizPanoramaContoller  extends BaseController{
	
	public void index() {
		render("list.html");
	}
	
	public void listData() {
		int pageNumber = getParaToInt("page", 1);
		int pageSize = getParaToInt("limit", 10);
		String keyword = getPara("keyword");
		String objId = getPara("objId");
		String event = getPara("event");

		String sqlExceptSelect = " FROM " + BizPanorama.TABLE_NAME + " o  WHERE 1=1 ";
		if (!StrKit.isBlank(keyword)) {
			sqlExceptSelect += " AND ( O.NAME LIKE '%" + keyword + "%' ) ";
		}
		if (!StrKit.isBlank(objId)) {
			sqlExceptSelect += " AND O.OBJID='"+objId+"' ";
		}
		if (!StrKit.isBlank(event)) {
			sqlExceptSelect += " AND O.EVENT='"+event+"' ";
		}
		sqlExceptSelect += "  ORDER BY   O.CREATETIME DESC ";
		Page<BizPanorama> page = BizPanorama.dao.paginate(pageNumber, pageSize, "select * ", sqlExceptSelect);
		renderLayuiPage(0, page.getList(), "", page.getTotalRow());
	}
	
	/***
	 * 保存
	 */
	public void save() {
		BizPanorama model = getModel(BizPanorama.class);
		if (StrKit.notBlank(model.getId())) {
			if (model.update()) {
				renderSuccess("更新成功！");
			} else {
				renderError("更新失败！");
			}
		} else {
			model.setId(UuidUtil.getUUID());
			model.setCreateUserId(ShiroKit.getLoginUser().getId());
			model.setCreateUserName(ShiroKit.getLoginUser().getName());
			if (model.save()) {
				renderSuccess("保存成功！");
			} else {
				renderError("保存失败！");
			}
		}

	}
	
	/***
	 * 删除
	 * 
	 * @throws Exception
	 */
	public void delete() throws Exception {
		String id = getPara("id");
		BizPanorama model = BizPanorama.dao.findById(id);
		if (model != null) {
			if (model.delete()) {
				renderSuccess("删除成功！");
			} else {
				renderError("删除失败！");
			}
		} else {
			renderError("数据不存在,请刷新后再试!");
		}
	}
	
	
	public void findById() throws Exception {
		String id = getPara("id");
		BizPanorama model = BizPanorama.dao.findById(id);
		if (model != null) {
			renderSuccess("成功！",model);
		} else {
			renderError("数据不存在,请刷新后再试!");
		}
	}

}
