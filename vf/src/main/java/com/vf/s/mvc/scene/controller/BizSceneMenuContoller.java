package com.vf.s.mvc.scene.controller;

import java.util.ArrayList;
import java.util.List;

import com.jfinal.kit.StrKit;
import com.vf.core.controller.BaseController;
import com.vf.core.model.ZtreeNode;
import com.vf.core.util.PinYinUtil;
import com.vf.core.util.UuidUtil;
import com.vf.s.common.model.biz.BizLine;
import com.vf.s.common.model.biz.Point;
import com.vf.s.common.model.biz.BizPolygon;
import com.vf.s.common.model.biz.BizVideoModel;
import com.vf.s.common.model.biz.BizVideoPlane;
import com.vf.s.common.model.biz.BizVideoShed3d;
import com.vf.s.common.model.scene.BizSceneFeature;
import com.vf.s.common.model.scene.SceneLayer;
import com.vf.s.common.model.scene.BizSceneMenu;
import com.vf.s.common.model.scene.BizSceneModel;
public class BizSceneMenuContoller  extends BaseController{
	
	
	public void index() {
		this.setAttr("sceneId",getPara("sceneId"));
		render("list.html");
	}
	
	public void save() {
		BizSceneMenu model = getModel(BizSceneMenu.class);
		if(!StrKit.isBlank(model.getId())) {
			if(StrKit.isBlank(model.getCode())) {
				model.setCode(PinYinUtil.getPinyin(model.getName()));
			}
			if (model.update()) {
				renderSuccess("更新成功！", model);
			} else {
				renderError("更新失败！");
			}
		}else {
			model.setId(UuidUtil.getUUID());
			if(StrKit.isBlank(model.getCode())) {
				model.setCode(PinYinUtil.getPinyin(model.getName()));
			}
			if (model.save()) {
				renderSuccess("保存成功！", model);
			} else {
				renderError("保存失败！");
			}
		}
	}
	
	public void delete(){
		if(BizSceneMenu.dao.deleteById(getPara("id"))) {
			renderSuccess("删除成功！");
		}else {
			renderError("删除失败！");
		}
	}
	
	public void findById(){
		BizSceneMenu model=BizSceneMenu.dao.findById(getPara("id"));
		if(model!=null) {
			renderSuccess("查询成功！",model);
		}else {
			renderError("无数据！");
		}
	}
	
	
	
	public void getAsyncTreeData() {
		String id = this.get("id", "root");
		String sceneId = this.get("sceneId");
		String sql = "SELECT a.*,(CASE WHEN (select count(id) from "+BizSceneMenu.TABLE_NAME+" where a.id=parentId ) >0 THEN true ELSE false END) AS isParent FROM "+BizSceneMenu.TABLE_NAME+" a where a.parentId=? ";
		String parentId=sceneId;
		if(!StrKit.isBlank(id) && !StrKit.equals(id, "root") ) {
			parentId=id;
		}
		sql += " order by a.sort asc ";
		List<ZtreeNode> nodelist = new ArrayList<ZtreeNode>();
		List<BizSceneMenu> olist = BizSceneMenu.dao.find(sql,parentId);
		for (BizSceneMenu o : olist) {
			if(StrKit.equals("99", o.getType())) {
				ZtreeNode node = new ZtreeNode();
				node.setId(o.getId());
				node.setType(o.getType());
				node.setLayerId(o.getLayerId());
				node.setLayerType(o.getLayerType());
				if(StrKit.equals("Point", o.getLayerType())) {
					Point model=Point.dao.findById(o.getId());
					if(model!=null) {
						node.setName(o.getName());
						nodelist.add(node);
					}
				}
				else if(StrKit.equals("Polygon", o.getLayerType())) {
					BizPolygon model=BizPolygon.dao.findById(o.getId());
					if(model!=null) {
						node.setName(o.getName());
						nodelist.add(node);
					}
				}
				else if(StrKit.equals("Polyline", o.getLayerType())) {
					BizLine model=BizLine.dao.findById(o.getId());
					if(model!=null) {
						node.setName(o.getName());
						nodelist.add(node);
					}
				}
				else if(StrKit.equals("Model", o.getLayerType())) {
					BizSceneModel model=BizSceneModel.dao.findById(o.getId());
					if(model!=null) {
						node.setName(o.getName());
						nodelist.add(node);
					}
				}
				else if(StrKit.equals("Feature", o.getLayerType())) {
					BizSceneFeature model=BizSceneFeature.dao.findById(o.getId());
					if(model!=null) {
						node.setName(o.getName());
						nodelist.add(node);
					}
				}
				else if(StrKit.equals("VideoPlane", o.getLayerType())) {
					BizVideoPlane model=BizVideoPlane.dao.findById(o.getId());
					if(model!=null) {
						node.setName(o.getName());
						nodelist.add(node);
					}
				}
				else if(StrKit.equals("VideoShed3d", o.getLayerType())) {
					BizVideoShed3d model=BizVideoShed3d.dao.findById(o.getId());
					if(model!=null) {
						node.setName(o.getName());
						nodelist.add(node);
					}
				}
				else if(StrKit.equals("VideoModel", o.getLayerType())) {
					BizVideoModel model=BizVideoModel.dao.findById(o.getId());
					if(model!=null) {
						node.setName(o.getName());
						nodelist.add(node);
					}
				}
				else {
					SceneLayer bizSceneLayer=SceneLayer.dao.findById(o.getLayerId());
					if(bizSceneLayer!=null) {
						node.setName(o.getName());
						nodelist.add(node);
					}
				}
			}else {
				ZtreeNode node = new ZtreeNode();
				node.setId(o.getId());
				node.setName(o.getName());
				node.setType(o.getType());
				node.setLayerType(o.getLayerType());
				nodelist.add(node);
			}
		}
		renderJson(nodelist);
	}
	
	public void getTree() {
		boolean open = true;// 是否展开所有
		boolean ifOnlyLeaf = false;// 是否只选叶子
		if (StrKit.notBlank(getPara("ifOnlyLeaf"))) {// 是否查询所有孩子
			if ("1".equals(getPara("ifOnlyLeaf"))) {
				ifOnlyLeaf = true;
			}
		}
		String sceneId = getPara("sceneId");
		String type = getPara("type");
		List<BizSceneMenu> modelList = BizSceneMenu.dao.getChildrenAllTree(sceneId,type);
		List<ZtreeNode> nodelist = BizSceneMenu.dao.toZTreeNode(modelList, open, ifOnlyLeaf);// 数据库中的菜单
		renderJson(nodelist);
	}
	
	
	
}
