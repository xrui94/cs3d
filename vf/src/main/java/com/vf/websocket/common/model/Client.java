package com.vf.websocket.common.model;


public class Client {
	
	private String auth;
	private int source;
	
	public Client(String auth, int source) {
		super();
		this.auth = auth;
		this.source = source;
	}
	public String getAuth() {
		return auth;
	}
	public void setAuth(String auth) {
		this.auth = auth;
	}
	public int getSource() {
		return source;
	}
	public void setSource(int source) {
		this.source = source;
	}
	
}
