package com.vf.websocket.common.util;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.vf.websocket.common.model.Client;

public class ClientUtil {
	
	private static ConcurrentHashMap<Integer, Map<String, Client>> classifyMap = new ConcurrentHashMap<Integer, Map<String, Client>>();
	public static void put(int source,String auth,Client client) {
		
		Map<String, Client> clientMap;
		if(classifyMap.containsKey(source)) {
			clientMap=classifyMap.get(source);
		}
		else {
			clientMap=new HashMap<String, Client>();
		}
		
		if(!clientMap.containsKey(auth)) {
			clientMap.put(auth, client);
			classifyMap.put(source, clientMap);
		}
		
	}
	
	public static void remove(int source,String auth) {
		if(classifyMap.containsKey(source)) {
			classifyMap.get(source).remove(auth);
		}
	} 
	
	public static Client get(int source,String auth) {
		if(classifyMap.containsKey(source)) {
			return classifyMap.get(source).get(auth);
		}
		return null;
	}
	
	public static Map<String, Client> gets(int source) {
		if(classifyMap.containsKey(source)) {
			return classifyMap.get(source);
		}
		return null;
	}
	
	public static int getSize(int source) {
		if(classifyMap.containsKey(source)) {
			return classifyMap.get(source).size();
		}else {
			return 0;
		}
	}

}
