package com.vf.websocket.common.constant;

public class Status {
	
	public static final int SUCCESS=200; public static final String SUCCESS_MSG="成功";
	public static final int UNAUTHORIZED=401; public static final String UNAUTHORIZED_MSG="未授权";
	public static final int ERROR=500; public static final String ERROR_MSG="错误";
	public static final int NOT_FOUND=404; public static final String NOT_FOUND_MSG="404";
	
	

}
