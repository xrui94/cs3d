package com.vf.websocket.common.constant;

public class Scope {
	public static final int CONSUMER=0;
	public static final int PRODUCER=1;
	public static final int ALL=1;
	public static final int HEARTBEAT=99;
}
