CityBuider.prototype.selectScene = function() {
	var _this=this;
	 layer.load(2);
	 _this._Ajax('get',_this.ctx+"/city/scene/select",{cityId:_this.cityId},'html',function(e){
		layer.closeAll();
		layer.open({
			id:'setting',
           type: 1,
           anim:5,
           title:'场景资源',
           area: '300px',
           content:e,
           shade:0,
           offset: ['0px', '224px'],	
           fixed:true,
           move:false,
		    skin:'layui-layer  layui-layer-adminRight layui-layer-city',
           success: function (layero, dIndex) {
              $(layero).children('.layui-layer-content').css('overflow', 'hidden');
              
              form.on('submit(submitBaseBizCityForm)', function (data) {
	        	    layer.load(2);
	                var formData={}; 
	                for(var key in data.field){
	                	formData['bizCity.'+key]=data.field[key];
	                }
	                _this.ajaxFunc(_this.ctx+"/city/save",formData, "json", function(res){
		                layer.closeAll('loading');
		                if (200==res.code) {
		                    layer.msg(res.message, {icon: 1});
		                    $("#cityBuiderNameTd").html(res.data.name);
		                } else {
		                    layer.msg(res.message, {icon: 2});
		                }
	                });
	        	    return false;
	        	});
			},
			end:function(){
			}
		});	
	});
}