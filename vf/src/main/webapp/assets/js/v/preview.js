function Preview(option){
	this.ctx=option.ctx;
	this.sceneId=option.sceneId;
	this.container=option.container;
	this.globe;
	this.configs;
	this.record=new Map();
	this.level=new Map();
	this.init();
}

/**
 * 初始化
 */
Preview.prototype.init=function(){
	let _this=this;
	layer.load(2);
	
	//初始化场景
	let scene= _this.getScene(this.sceneId);
	this.scene=scene;
	if(scene){
		scene.url=_this.ctx;
    	_this.globe= new VFG.Viewer(_this.container,scene);
    	_this.LEFT_CLICK_PRIMITIVE();
    	_this.RIGHT_CLICK_PRIMITIVE();
    	_this.CAMERA_MOVE_END();
    	
    	//读取场景配置
    	this.configs=this.getConfig(this.sceneId);
    	
    	console.log(this.configs);
    	
    	//读取基础图层
    	let baseLayers=this.getBaseLayerGroup(this.configs);
    	
    	if(baseLayers!=null && baseLayers.children && baseLayers.children.length>0){
    		for(let menu of baseLayers.children){
    			console.log('开始-【',menu.name,'】-渲染');
    			this.render(menu);
    			console.log('完成-【',menu.name,'】-渲染');
    		}
    	}
    	
    	//读取场景菜单
    	let menuGroups=this.getMenuGroup(this.configs);
    	if(menuGroups!=null && menuGroups.children && menuGroups.children.length>0){
    		this.click(menuGroups.children[0]);
    	}
    	layer.closeAll('loading');
    	_this.showBuildingIndex();
    	
	}else{
		layer.closeAll('loading');
		layer.msg("未找到场景！", {icon: 2});
	}
}

/**
 * 场景菜单切换
 */
Preview.prototype.click=function(menu){
	let _this=this;
	//图层
	let layers=_this.getLayerGroup(menu);
	if(layers!=null && layers.children && layers.children.length>0){
		for(let menu of layers.children){
			if(menu.layerShow=='1' && menu.layerId){
				console.log('开始-【',menu.name,'】-渲染');
				_this.render(menu);
				console.log('完成-【',menu.name,'】-渲染');
			}
		}
	}
	
	//按钮
	var btns=_this.getBtnGroup(menu);
	//组件
	var assemblys=_this.getAssemblyGroup(menu);
}



Preview.prototype.addRecord=function(layerId,entities){
	var _this=this;
	if(!this.record.has(layerId)){
		this.record.set(layerId,entities)
	}
}

//记录资源
Preview.prototype.addRecord=function(layerId,entities){
	var _this=this;
	if(!this.record.has(layerId)){
		this.record.set(layerId,entities)
	}
}

//移除资源
/*Preview.prototype.removeRecord=function(){
	let _this=this;
　　 _this.record.forEach(function(value,key){
	 if(entities){
		 for(var entity of entities){
			if(this.globe&&entity){
				this.globe.removeById({
					id:entity.id,
					type:entity.type
				});
			}
		 }
	 }
　　 });
   this.record.clear();
}*/
/**
 * 渲染
 */

Preview.prototype.floors=null;
Preview.prototype.render=function(menu){
	var _this=this;
	if(menu && menu.layerId){
		var entities=this.getEntities(menu.layerId);
		_this.floors=entities.entities;
		if(this.globe&&entities){
			this.globe.render(entities);
		}
		return entities;
	}
}

/**
 * 移除
 */
Preview.prototype.remove=function(menu){
	var _this=this;
	if(menu && menu.layerId){
		var entities=this.getEntities(menu.layerId);
		if(this.globe&&entities){
			this.globe.render(entities);
		}
	}
}

/**
 * 点击
 */
Preview.prototype.LEFT_CLICK_PRIMITIVE=function(){
	var _this=this;
	if(_this.globe){
		_this.globe.LEFT_CLICK_PRIMITIVE(function(e){
			console.log(e);
			if(e.attribute && e.attribute=='Floor'){
				_this.hideFloors(e.id);
			}
		});
	}
}

Preview.prototype.RIGHT_CLICK_PRIMITIVE=function(){
	var _this=this;
	if(_this.globe){
		_this.globe.RIGHT_CLICK_PRIMITIVE(function(e){
			if(_this.isEnterFloor){
				_this.exitFloor();
			}
		});
	}
}



/**
 * 移动
 */
Preview.prototype.CAMERA_MOVE_END=function(){
	var _this=this;
	if(_this.globe){
		_this.globe.CAMERA_MOVE_END(function(e){
			_this.loadLayerLevel(e);
		});
	}
}

/**
 * 层级加载
 */
Preview.prototype.loadLayerLevel=function(e){
	var _this=this;
	if(e){
		console.log(e);
	}
	
}


Preview.prototype._Ajax = function(url, data, dataType, callback) {
	if(dataType == undefined || dataType == null){
		dataType = "html";
	}
	var result;
	$.ajax({
		type : "post",
		url : encodeURI(encodeURI(url)),
		data : data,
		dataType : dataType,
		contentType: "application/x-www-form-urlencoded; charset=UTF-8",
		async: false,
		cache: false,
		success:function(response){
			result = response;
			//扩展回调函数
			if( callback != null ){
				callback();
			}
		}
	});
	return result;
}

Preview.prototype.ajaxFunc = function(url, data, dataType, callback){
	if(dataType == undefined || dataType == null){
		dataType = "html";
	}
	var result;
	$.ajax({
		type : "post",
		url : encodeURI(encodeURI(url)),
		data : data,
		dataType : dataType,
		contentType: "application/x-www-form-urlencoded; charset=UTF-8",
		async: true,
		cache: false,
		success:function(response){
			result = response;
			//扩展回调函数
			if( callback != null ){
				callback(result);
			}
		}
	});
	return result;
};

/***************************************************/
Preview.prototype.showBuildingIndex=function (){
	var _this=this;
	var text;
	layer.open({
		id:'menuBuildingLayer',
        type: 1,
        anim:5,
        title:false,
        closeBtn: 0,
        area: '200px',
        content:$('#modelBuildingMenuScript').html(),
        shade:0,
        offset: 'l',	
        fixed:true,
        move:false,
		skin:'layui-layer',
        success: function (layero, dIndex) {
        	$(layero).children('.layui-layer-content').css('overflow', 'hidden');
		    form.render();   
			$('#openFloorsBtn').click(function () {
				console.log(_this.floors);
				_this.openFloors(_this.floors);
			});
			$('#closeFloorsBtn').click(function () {
				_this.closeFloors(_this.floors);
			});
			$('#exitFloorsBtn').click(function () {
				_this.exitFloor();
			});
			$('#homeBtn').click(function () {
				_this.globe.flyToHome(_this.scene);
			});
		},
		end:function(){
		}
	});
}
Preview.prototype.isOpenFloors=false;
Preview.prototype.openFloors=function (floors){
	let _this=this;
	if(!this.isOpenFloors){
		this.isOpenFloors=true;
		let index=0;
		let height=0;
		for(let floor of floors){
			if(floor.attribute=='Floor'){
				if(index!=0){
					height=5*index;
					let z=floor.z*1+height;
					_this.globe.changeModelHeight(floor.id,z);
				}
				index++;
			}
		}
		
		_this.globe.zoomOut(2);
	}
}

Preview.prototype.closeFloors=function (floors){
	let _this=this;
	if(this.isOpenFloors){
		this.isOpenFloors=false;
		let index=0;
		let height=0;
		for(let floor of floors){
			if(floor.attribute=='Floor'){
				if(index!=0){
					height=5*index;
					let z=floor.z*1-height;
					_this.globe.changeModelHeight(floor.id,z);
				}
				index++;
			}
		}
		_this.globe.flyToHome(_this.scene);
	}
}

Preview.prototype.isEnterFloor=false;
Preview.prototype.hideFloors=function (id){
	let _this=this;
	for(let floor of _this.floors){
		if(floor.id!=id){
			_this.globe.showModel({
				id:floor.id,
				show:false
			});
		}
	}
	_this.globe.flyToModel(id,function(){
		_this.isEnterFloor=true;
	});
}

Preview.prototype.exitFloor=function (){
	let _this=this;
	if(_this.isEnterFloor){
		_this.isEnterFloor=false;
		for(let floor of _this.floors){
			_this.globe.showModel({
				id:floor.id,
				show:true
			});
		}
		_this.globe.flyToHome(_this.scene);
	}
}



