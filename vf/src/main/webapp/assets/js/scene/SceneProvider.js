SceneEditor.prototype.showOrHideProvider=function(layerId,state,type){
	var _this=this;
	if('Maps'==type){
		layer.load(2);
	    _this.ajaxFunc(_this.ctx+"/scene/provider/getList",{
	    	sceneId:_this.sceneId,
	    	layerId:layerId
	    }, "json", function(res){
	    	layer.closeAll('loading');
			 if (undefined!=res && 200==res.code) {
				 var list=res.data;
				 if(state==true){
					 _this.globe.addProviders(list);
				 }else{
					 _this.globe.removeProviders(list);
				 }
			 }
			 else{
				 layer.alert(res.message, {icon: 3,skin: 'layer-ext-moon' })
			 }
	    });
	}else if('Map'==type){
		layer.load(2);
	    _this.ajaxFunc(_this.ctx+"/scene/provider/getById",{
	    	id:layerId
	    }, "json", function(res){
	    	layer.closeAll('loading');
			 if (undefined!=res && 200==res.code) {
				 var model=res.data; 
				 if(model){
					 if(state==true){
						 _this.globe.addProvider(model);
					 }else{
						 _this.globe.removeProvider(model.id);
					 }
				 }
			 }
			 else{
				 layer.alert(res.message, {icon: 3,skin: 'layer-ext-moon' })
			 }
	    });
	}else{
		layer.alert('未识别图层标识【"'+type+'"】！！', {icon: 1,skin: 'layer-ext-moon' })
	}
} 

/**
 * 在线底图
 */

SceneEditor.prototype.addOnLineProvider=function(node){
	 var _this=this;
	 layer.load(2);
	 _this._Ajax('get',_this.ctx+"/scene/provider/select",{sceneId:_this.sceneId,layerId:node.id},'html',function(e){
		layer.closeAll('loading');
	    layer.open({
	    	id:'bizMapLayer',
	        title:'在线底图',
	        type: 1,
	        area: '335px',
	        content:e,
	        shade:0,
	        offset: ['0px', '224px'],
	        fixed:true,
	        move:false,
	        skin:'layui-layer layui-layer-adminRight layui-layer-city',
		    content: e,
		    success: function (layero, dIndex) {
	            $(layero).children('.layui-layer-content').css('overflow', 'hidden');
	            $(layero).children('.layui-layer-content').css('height', 'calc(100% - 30px)');
	            form.render();
	           _this.loadOnProviders(1,node);
		   		$('#selectProviderSearchBtn').click(function () {
		   		  _this.loadOnProviders(1,node);
				});
			},
			end:function(){
			}
	    });  
	});
}

SceneEditor.prototype.loadOnProviders=function(curr,node){
	var _this=this;
	var search=form.val('selectProviderSearchForm');
    _this.ajaxFunc(_this.ctx+'/scene/provider/listData',{
    	page:curr?curr:1,
    	limit:10,
    	keywords:search?search.keywords:'',
    	type:search?search.type:'',
    	sceneId:_this.sceneId,	
    }, "json", function(res){
        if (undefined!=res && 0==res.code) {
        	res.ctx=_this.ctx;
        	console.log(res);
            laytpl(selectProviderScript.innerHTML).render(res, function (html) {
                $('#select-provider-list').html(html);
        	    $(".ImageList>.content>.item>.IconButton ").on('click',function () {
        	    	if(this.checked){
        	    		_this.addProvider(node,$(this).attr('id')) 
        	    	}else{
        	    		_this.removeProvider(node,$(this).attr('id')) 
        	    	}
        	    })
            });
            laypage.render({
              elem: 'selectProviderPageBar'
              ,count:res.count
              ,curr:curr
              ,layout: ['prev', 'next','count']
              ,jump: function(obj, first){
                if(!first){
                	_this.loadOnProviders(obj.curr,node);
                }
              }
            });	 
        }else{
        }
    });
}

SceneEditor.prototype.addProvider=function(node,providerId){
	var _this=this;
    _this.ajaxFunc(_this.ctx+"/biz/provider/getById",{id:providerId}, "json", function(res){
        layer.closeAll('loading');
        if (res!=null && 200==res.code) {
        	let data=res.data;
        	let id=VFG.Util.getUuid();
        	let formData={}; 
        	formData['sceneProvider.id']=id;
        	formData['sceneProvider.providerId']=providerId;
        	formData['sceneProvider.sceneId']=_this.sceneId;
        	formData['sceneProvider.layerId']=node.id;
    	    layer.load(2);
            _this.ajaxFunc(_this.ctx+"/scene/provider/save",formData, "json", function(res){
                layer.closeAll('loading');
                if (res!=null && 200==res.code) {
                    layer.msg(res.message);
                    _this.globe.addProvider(data);
                    _this.reAsyncLayerByPId(node.layerId);
                } else {
                    layer.msg(res!=null && res.message?res.message:'系统错误！');
                }
            });
        } else {
            layer.msg(res && res.message?res.message:'系统错误！');
        }
    });
}

SceneEditor.prototype.removeProvider=function(node,providerId){
	var _this=this;
    _this.ajaxFunc(_this.ctx+"/scene/provider/removeById",{
    	sceneId:_this.sceneId,	
    	id:providerId
    }, "json", function(res){
        if (res!=null && 200==res.code) {
			 _this.globe.removeProvider(providerId);
        	 layer.msg(res.message);
        	 _this.reAsyncLayerByPId(node.layerId)
        } else {
            layer.msg(res && res.message?res.message:'系统错误！');
        }
    });
}


/*SceneEditor.prototype.addOnLineProvider=function(node){
	 var _this=this;
	 layer.load(2);
	 _this._Ajax('get',_this.ctx+"/scene/provider/select",{sceneId:_this.sceneId,layerId:node.id},'html',function(e){
		layer.closeAll('loading');
	    layer.open({
	    	id:'bizMapLayer',
	        title:'在线底图',
	        type: 1,
	        area: '288px',
	        content:e,
	        shade:0,
	        offset: ['0px', '224px'],
	        fixed:true,
	        move:false,
	        skin:'layui-layer layui-layer-adminRight layui-layer-city',
		    content: e,
		    success: function (layero, dIndex) {
	            $(layero).children('.layui-layer-content').css('overflow', 'hidden');
	            $(".map-ul li").bind('click',function(e){
	            	var formData={}; 
	            	var id=$(e.currentTarget).attr("id");
	            	var type=$(e.currentTarget).attr("type");
	            	if($(e.currentTarget).hasClass("map-selected")){
		        	    layer.load(2);
		                _this.ajaxFunc(_this.ctx+"/scene/provider/delete",{mapId:id,layerId:node.id}, "json", function(res){
			                layer.closeAll('loading');
			                if (200==res.code) {
			                    layer.msg(res.message);
			                } else {
			                    layer.msg(res.message);
			                }
		                });
	            	}else{
		            	formData['bizSceneMap.id']=id;
		            	formData['bizSceneMap.mapId']=id;
		            	formData['bizSceneMap.sceneId']=_this.sceneId;
		            	formData['bizSceneMap.layerId']=node.id;
		        	    layer.load(2);
		                _this.ajaxFunc(_this.ctx+"/scene/provider/save",formData, "json", function(res){
			                layer.closeAll('loading');
			                if (200==res.code) {
			                    layer.msg(res.message);
			                    _this.reAsyncLayerByPId(res.data.layerId);
			                } else {
			                    layer.msg(res.message);
			                }
		                });
	            	}
	            	$(e.currentTarget).toggleClass("map-selected");
	            })
			},
			end:function(){
			}
	    });  
	});
}*/

SceneEditor.prototype.editProvider = function(node) {
	var _this=this;
	 layer.load(2);
	 _this._Ajax('get',_this.ctx+"/scene/provider/setting",{mapId:node.id},'html',function(e){
		layer.closeAll();
		layer.open({
		   id:'editProvider',
           type: 1,
           anim:5,
           title:'底图设置',
           area: '300px',
           content:e,
           shade:0,
           offset: ['0px', '224px'],	
           fixed:true,
           move:false,
		    skin:'layui-layer  layui-layer-adminRight layui-layer-city',
           success: function (layero, dIndex) {
              $(layero).children('.layui-layer-content').css('overflow', 'hidden');
              form.render();
              form.on('submit(submitBaseBizSceneMapMapForm)', function (data) {
	        	    layer.load(2);
	                var formData={}; 
	                for(var key in data.field){
	                	formData['bizSceneMap.'+key]=data.field[key];
	                }
	                _this.ajaxFunc(_this.ctx+"/scene/provider/save",formData, "json", function(res){
		                layer.closeAll('loading');
		                if (200==res.code) {
		                    layer.msg(res.message, {icon: 1});
		                    _this.reAsyncLayerByPId(res.data.layerId);
		                } else {
		                    layer.msg(res.message, {icon: 2});
		                }
	                });
	        	    return false;
	        	});
			},
			end:function(){
			}
		});	
	});
}


