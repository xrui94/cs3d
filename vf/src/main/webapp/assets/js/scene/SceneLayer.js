SceneEditor.prototype.initLayerTree=function(){
	var _this=this;
	var setting = {
		async: {
			enable: true,
			url: _this.ctx+"/scene/layer/getAsyncTreeData?sceneId="+_this.sceneId,
			autoParam:["id", "name=n", "level=lv","type=type"],
			dataFilter: filter,
		},
		view: {
			showLine: false,
			showIcon: false,
			selectedMulti: false,
			dblClickExpand: false,
			addHoverDom: addHoverDom,
			removeHoverDom:removeHoverDom
		},
		check: {
			enable: true,
		},
		edit: {
		},
		callback: {
			onCheck: onCheck,
			onClick: onClick
		}
	};
	
	function filter(treeId, parentNode, childNodes) {
		if (!childNodes) return null;
		for (var i=0, l=childNodes.length; i<l; i++) {
			childNodes[i].name = childNodes[i].name?childNodes[i].name.replace(/\.n/g, '.'):'未命名';
			if(childNodes[i].count!=0){
				childNodes[i].name=childNodes[i].name+"("+childNodes[i].count+")"
			}
			
			if(childNodes[i].type=='point'){
				childNodes[i].icon = _this.ctx+"/assets/plugins/zTree_v3/css/metroStyle/img/icon-point.png";
			}
			else if(childNodes[i].type=='line'){
				childNodes[i].icon = _this.ctx+"/assets/plugins/zTree_v3/css/metroStyle/img/icon-line.png";
			}
			else if(childNodes[i].type=='Layers'){
				childNodes[i].nocheck = true;
			}
		}
		return childNodes;
	}
	
	function onCheck(e, treeId, treeNode) {
		_this.showOrHideLayer(treeNode);
	}
	function onClick(e, treeId, treeNode) {
		_this.flyToNode(treeNode);
	}
	
	function addHoverDom(treeId, treeNode) {
		var sObj = $("#" + treeNode.tId + "_span");
		if( 'Layers'==treeNode.type &&  !$("#editLayersBtn_"+treeNode.tId).length>0){
			var addStr= "<span class='button edit' id='editLayersBtn_" + treeNode.tId+ "' title='编辑' onfocus='this.blur();'></span>";
			  addStr += "<span class='button into' id='importLayersBtn_" + treeNode.tId+ "' title='导入' onfocus='this.blur();'></span>";
			  addStr += "<span class='button remove' id='delLayersBtn_" + treeNode.tId+ "' title='删除' onfocus='this.blur();'></span>";
			  sObj.after(addStr);
			var editBtn = $("#editLayersBtn_"+treeNode.tId);
			if (editBtn) editBtn.bind("click", function(){
				_this.editLayer(treeNode);
				return false;
			});
			
			var importBtn = $("#importLayersBtn_"+treeNode.tId);
			if (importBtn) importBtn.bind("click", function(){
				_this.importLayer(treeNode);
				return false;
			});
			
			var delBtn = $("#delLayersBtn_"+treeNode.tId);
			if (delBtn) delBtn.bind("click", function(){
				_this.deleteLayer(treeNode);
				return false;
			});
		}
		if('Maps'==treeNode.type &&  !$("#delMapsBtn_"+treeNode.tId).length>0){
			var addStr= "<span class='button edit' id='editMapsBtn_" + treeNode.tId+ "' title='编辑' onfocus='this.blur();'></span>";
			addStr += "<span class='button into' id='selectMapsBtn_" + treeNode.tId+ "' title='导入底图' onfocus='this.blur();'></span>";
			addStr += "<span class='button remove' id='delMapsBtn_" + treeNode.tId+ "' title='删除' onfocus='this.blur();'></span>";
			sObj.after(addStr);
			var editBtn = $("#editMapsBtn_"+treeNode.tId);
			if (editBtn) editBtn.bind("click", function(){
				_this.editLayer(treeNode);                   
				return false;
			});
			var selectMapsBtn = $("#selectMapsBtn_"+treeNode.tId);
			if (selectMapsBtn) selectMapsBtn.bind("click", function(){
				_this.addOnLineProvider(treeNode);
				return false;
			});
			var delBtn = $("#delMapsBtn_"+treeNode.tId);
			if (delBtn) delBtn.bind("click", function(){
				_this.deleteLayer(treeNode);
				return false;
			});
		}
		else if('Map'==treeNode.type &&  !$("#delMapBtn_"+treeNode.tId).length>0){
			/*var addStr= "<span class='button edit' id='editMapBtn_" + treeNode.tId+ "' title='编辑' onfocus='this.blur();'></span>";*/
			var addStr = "<span class='button remove' id='delMapBtn_" + treeNode.tId+ "' title='删除' onfocus='this.blur();'></span>";
			sObj.after(addStr);
/*			var editBtn = $("#editMapBtn_"+treeNode.tId);
			if (editBtn) editBtn.bind("click", function(){
				_this.editProvider(treeNode);
				return false;
			});*/
			var delBtn = $("#delMapBtn_"+treeNode.tId);
			if (delBtn) delBtn.bind("click", function(){
				_this.deleteLayer(treeNode);
				return false;
			});
		}
		if('Models'==treeNode.type && !$("#editModelsBtn_"+treeNode.tId).length>0){
			var addStr= "<span class='button edit' id='editModelsBtn_" + treeNode.tId+ "' title='编辑' onfocus='this.blur();'></span>";
			addStr += "<span class='button into' id='selectModelsBtn_" + treeNode.tId+ "' title='导入模型' onfocus='this.blur();'></span>";
			addStr += "<span class='button remove' id='delModelsBtn_" + treeNode.tId+ "' title='删除' onfocus='this.blur();'></span>";
			sObj.after(addStr);
			var editBtn = $("#editModelsBtn_"+treeNode.tId);
			if (editBtn) editBtn.bind("click", function(){
				_this.editLayer(treeNode);
				return false;
			});
			
			var selectModelsBtn = $("#selectModelsBtn_"+treeNode.tId);
			if (selectModelsBtn) selectModelsBtn.bind("click", function(){
				_this.addOnLineModel(treeNode);
				return false;
			});
			var delBtn = $("#delModelsBtn_"+treeNode.tId);
			if (delBtn) delBtn.bind("click", function(){
				_this.deleteLayer(treeNode);
				return false;
			});
		}
		else if('Model'==treeNode.type &&  !$("#editModelBtn_"+treeNode.tId).length>0){
			var addStr= "<span class='button edit' id='editModelBtn_" + treeNode.tId+ "' title='编辑' onfocus='this.blur();'></span>";
			addStr+= "<span class='button remove' id='delModelBtn_" + treeNode.tId+ "' title='删除' onfocus='this.blur();'></span>";
			sObj.after(addStr);
			
			var editBtn = $("#editModelBtn_"+treeNode.tId);
			if (editBtn) editBtn.bind("click", function(){
				_this.editModel(treeNode);
				return false;
			});
			var delBtn = $("#delModelBtn_"+treeNode.tId);
			if (delBtn) delBtn.bind("click", function(){
				_this.deleteLayer(treeNode);
				return false;
			});
		}
		else if('Points'==treeNode.type &&  !$("#editPointsBtn_"+treeNode.tId).length>0){
			var addStr= "<span class='button edit' id='editPointsBtn_" + treeNode.tId+ "' title='编辑' onfocus='this.blur();'></span>";
			addStr += "<span class='button mark-point' id='markPointBtn_" + treeNode.tId+ "' title='添加' onfocus='this.blur();'></span>";
			addStr += "<span class='button remove' id='delPointsBtn_" + treeNode.tId+ "' title='删除' onfocus='this.blur();'></span>";
			sObj.after(addStr);
			var editBtn = $("#editPointsBtn_"+treeNode.tId);
			if (editBtn) editBtn.bind("click", function(){
				_this.editPointLayer(treeNode);
				return false;
			});
			
			var markPoint = $("#markPointBtn_"+treeNode.tId);
			if (markPoint) markPoint.bind("click", function(){
				_this.markPoint(treeNode) 
				return false;
			});
			
			var delBtn = $("#delPointsBtn_"+treeNode.tId);
			if (delBtn) delBtn.bind("click", function(){
				_this.deleteLayer(treeNode);
				return false;
			});
		}
		else if('Point'==treeNode.type &&  !$("#editPointBtn_"+treeNode.tId).length>0){
			var addStr= "<span class='button edit' id='editPointBtn_" + treeNode.tId+ "' title='编辑' onfocus='this.blur();'></span>";
			addStr += "<span class='button remove' id='delPointBtn_" + treeNode.tId+ "' title='删除' onfocus='this.blur();'></span>";
			sObj.after(addStr);
			var editBtn = $("#editPointBtn_"+treeNode.tId);
			if (editBtn) editBtn.bind("click", function(){
				_this.editPoint(treeNode) 
				return false;
			});
			var delBtn = $("#delPointBtn_"+treeNode.tId);
			if (delBtn) delBtn.bind("click", function(){
				_this.deleteLayer(treeNode);
				return false;
			});
		}
		if('Polylines'==treeNode.type &&  !$("#editLinesBtn_"+treeNode.tId).length>0){
			var addStr= "<span class='button edit' id='editLinesBtn_" + treeNode.tId+ "' title='编辑' onfocus='this.blur();'></span>";
			addStr += "<span class='button polyline' id='drawLinesBtn_" + treeNode.tId+ "' title='样式' onfocus='this.blur();'></span>";
			addStr += "<span class='button remove' id='delLinesBtn_" + treeNode.tId+ "' title='删除' onfocus='this.blur();'></span>";
			sObj.after(addStr);
			var editBtn = $("#editLinesBtn_"+treeNode.tId);
			if (editBtn) editBtn.bind("click", function(){
				_this.editPolylineLayer(treeNode);
				return false;
			});
			
			var drawLinesBtn = $("#drawLinesBtn_"+treeNode.tId);
			if (drawLinesBtn) drawLinesBtn.bind("click", function(){
				_this.drawPolyline(treeNode);
				return false;
			});
			
			var delBtn = $("#delLinesBtn_"+treeNode.tId);
			if (delBtn) delBtn.bind("click", function(){
				_this.deleteLayer(treeNode);
				return false;
			});
		}
		else if('Polyline'==treeNode.type &&  !$("#editLineBtn_"+treeNode.tId).length>0){
			var addStr= "<span class='button edit' id='editLineBtn_" + treeNode.tId+ "' title='编辑' onfocus='this.blur();'></span>";
			addStr += "<span class='button remove' id='delLineBtn_" + treeNode.tId+ "' title='删除' onfocus='this.blur();'></span>";
			sObj.after(addStr);
			var editBtn = $("#editLineBtn_"+treeNode.tId);
			if (editBtn) editBtn.bind("click", function(){
				_this.editPolyLine(treeNode.id,treeNode.name) 
				return false;
			});
			
			var delBtn = $("#delLineBtn_"+treeNode.tId);
			if (delBtn) delBtn.bind("click", function(){
				_this.deleteLayer(treeNode);
				return false;
			});
		}
		else if('Polygons'==treeNode.type &&  !$("#editPolygonsBtn_"+treeNode.tId).length>0){
			var addStr= "<span class='button edit' id='editPolygonsBtn_" + treeNode.tId+ "' title='编辑' onfocus='this.blur();'></span>";
			addStr += "<span class='button polygon' id='drawPolygonsBtn_" + treeNode.tId+ "' title='样式' onfocus='this.blur();'></span>";
			addStr += "<span class='button remove' id='delPolygonsBtn_" + treeNode.tId+ "' title='删除' onfocus='this.blur();'></span>";
			sObj.after(addStr);
			var editBtn = $("#editPolygonsBtn_"+treeNode.tId);
			if (editBtn) editBtn.bind("click", function(){
				_this.editPolygonLayer(treeNode);
				return false;
			});
			
			var drawPolygonsBtn = $("#drawPolygonsBtn_"+treeNode.tId);
			if (drawPolygonsBtn) drawPolygonsBtn.bind("click", function(){
				_this.drawPolygon(treeNode);
				return false;
			});
			
			var delBtn = $("#delPolygonsBtn_"+treeNode.tId);
			if (delBtn) delBtn.bind("click", function(){
				_this.deleteLayer(treeNode);
				return false;
			});
		}
		else if('Polygon'==treeNode.type &&  !$("#editPolygonBtn_"+treeNode.tId).length>0){
			var addStr= "<span class='button edit' id='editPolygonBtn_" + treeNode.tId+ "' title='编辑' onfocus='this.blur();'></span>";
			addStr += "<span class='button remove' id='delPolygonBtn_" + treeNode.tId+ "' title='删除' onfocus='this.blur();'></span>";
			sObj.after(addStr);
			var editBtn = $("#editPolygonBtn_"+treeNode.tId);
			if (editBtn) editBtn.bind("click", function(){
				_this.editPolygon(treeNode.id,treeNode.name) 
				return false;
			});
			
			var delBtn = $("#delPolygonBtn_"+treeNode.tId);
			if (delBtn) delBtn.bind("click", function(){
				_this.deleteLayer(treeNode);
				return false;
			});
		}
		else if('VideoModels'==treeNode.type &&  !$("#editVideoModelsBtn_"+treeNode.tId).length>0){
			var addStr= "<span class='button edit' id='editVideoModelsBtn_" + treeNode.tId+ "' title='编辑' onfocus='this.blur();'></span>";
			addStr += "<span class='button vplane' id='drawVideoModelsBtn_" + treeNode.tId+ "' title='视频墙' onfocus='this.blur();'></span>";
			addStr += "<span class='button remove' id='delVideoModelsBtn_" + treeNode.tId+ "' title='删除' onfocus='this.blur();'></span>";
			sObj.after(addStr);
			var editBtn = $("#editVideoModelsBtn_"+treeNode.tId);
			if (editBtn) editBtn.bind("click", function(){
				_this.editLayer(treeNode);
				return false;
			});
			var drawVideoModelsBtn = $("#drawVideoModelsBtn_"+treeNode.tId);
			if (drawVideoModelsBtn) drawVideoModelsBtn.bind("click", function(){
				_this.editVModel(treeNode);
				return false;
			});
			var delBtn = $("#delVideoModelsBtn_"+treeNode.tId);
			if (delBtn) delBtn.bind("click", function(){
				_this.deleteLayer(treeNode);
				return false;
			});
		}
		else if('VideoModel'==treeNode.type &&  !$("#editVideoModelBtn_"+treeNode.tId).length>0){
			var addStr= "<span class='button edit' id='editVideoModelBtn_" + treeNode.tId+ "' title='编辑' onfocus='this.blur();'></span>";
			addStr += "<span class='button remove' id='delVideoModelBtn_" + treeNode.tId+ "' title='删除' onfocus='this.blur();'></span>";
			sObj.after(addStr);
			var editBtn = $("#editVideoModelBtn_"+treeNode.tId);
			if (editBtn) editBtn.bind("click", function(){
				_this.editVModel(treeNode);
				return false;
			});
			var delBtn = $("#delVideoModelBtn_"+treeNode.tId);
			if (delBtn) delBtn.bind("click", function(){
				_this.deleteLayer(treeNode);
				return false;
			});
		}
		else if('VideoPlanes'==treeNode.type &&  !$("#editVideoPlanesBtn_"+treeNode.tId).length>0){
			var addStr= "<span class='button edit' id='editVideoPlanesBtn_" + treeNode.tId+ "' title='编辑' onfocus='this.blur();'></span>";
			addStr += "<span class='button vplane' id='drawVideoPlanesBtn_" + treeNode.tId+ "' title='视频墙' onfocus='this.blur();'></span>";
			addStr += "<span class='button remove' id='delVideoPlanesBtn_" + treeNode.tId+ "' title='删除' onfocus='this.blur();'></span>";
			sObj.after(addStr);
			var editBtn = $("#editVideoPlanesBtn_"+treeNode.tId);
			if (editBtn) editBtn.bind("click", function(){
				_this.editLayer(treeNode);
				return false;
			});
			var drawVideoPlanesBtn = $("#drawVideoPlanesBtn_"+treeNode.tId);
			if (drawVideoPlanesBtn) drawVideoPlanesBtn.bind("click", function(){
				_this.editVideoPlane(treeNode);
				return false;
			});
			var delBtn = $("#delVideoPlanesBtn_"+treeNode.tId);
			if (delBtn) delBtn.bind("click", function(){
				_this.deleteLayer(treeNode);
				return false;
			});
		}
		else if('VideoPlane'==treeNode.type &&  !$("#editVideoPlaneBtn_"+treeNode.tId).length>0){
			var addStr= "<span class='button edit' id='editVideoPlaneBtn_" + treeNode.tId+ "' title='编辑' onfocus='this.blur();'></span>";
			addStr += "<span class='button remove' id='delVideoPlaneBtn_" + treeNode.tId+ "' title='删除' onfocus='this.blur();'></span>";
			sObj.after(addStr);
			var editBtn = $("#editVideoPlaneBtn_"+treeNode.tId);
			if (editBtn) editBtn.bind("click", function(){
				_this.editVideoPlane(treeNode);
				return false;
			});
			var delBtn = $("#delVideoPlaneBtn_"+treeNode.tId);
			if (delBtn) delBtn.bind("click", function(){
				_this.deleteLayer(treeNode);
				return false;
			});
		}
		else if('VideoShed3ds'==treeNode.type &&  !$("#editVideoShed3dsBtn_"+treeNode.tId).length>0){
			var addStr= "<span class='button edit' id='editVideoShed3dsBtn_" + treeNode.tId+ "' title='编辑' onfocus='this.blur();'></span>";
			addStr += "<span class='button vplane' id='drawVideoShed3dsBtn_" + treeNode.tId+ "' title='视频墙' onfocus='this.blur();'></span>";
			addStr += "<span class='button remove' id='delVideoShed3dsBtn_" + treeNode.tId+ "' title='删除' onfocus='this.blur();'></span>";
			sObj.after(addStr);
			var editBtn = $("#editVideoShed3dsBtn_"+treeNode.tId);
			if (editBtn) editBtn.bind("click", function(){
				_this.editLayer(treeNode);
				return false;
			});
			var drawVideoShed3dsBtn = $("#drawVideoShed3dsBtn_"+treeNode.tId);
			if (drawVideoShed3dsBtn) drawVideoShed3dsBtn.bind("click", function(){
				_this.editVideoShed3d(treeNode);
				return false;
			});
			var delBtn = $("#delVideoShed3dsBtn_"+treeNode.tId);
			if (delBtn) delBtn.bind("click", function(){
				_this.deleteLayer(treeNode);
				return false;
			});
		}
		else if('VideoShed3d'==treeNode.type &&  !$("#editVideoShed3dBtn_"+treeNode.tId).length>0){
			var addStr= "<span class='button edit' id='editVideoShed3dBtn_" + treeNode.tId+ "' title='编辑' onfocus='this.blur();'></span>";
			addStr += "<span class='button remove' id='delVideoShed3dBtn_" + treeNode.tId+ "' title='删除' onfocus='this.blur();'></span>";
			sObj.after(addStr);
			var editBtn = $("#editVideoShed3dBtn_"+treeNode.tId);
			if (editBtn) editBtn.bind("click", function(){
				_this.editVideoShed3d(treeNode);
				return false;
			});
			var delBtn = $("#delVideoShed3dBtn_"+treeNode.tId);
			if (delBtn) delBtn.bind("click", function(){
				_this.deleteLayer(treeNode);
				return false;
			});
		}
		else if('Features'==treeNode.type &&  !$("#editFeaturestBtn_"+treeNode.tId).length>0){
			var addStr= "<span class='button edit' id='editFeaturestBtn_" + treeNode.tId+ "' title='编辑' onfocus='this.blur();'></span>";
			addStr += "<span class='button feature' id='drawFeaturesBtn_" + treeNode.tId+ "' title='绘制单体' onfocus='this.blur();'></span>";
			addStr += "<span class='button remove' id='delFeaturestBtn_" + treeNode.tId+ "' title='删除' onfocus='this.blur();'></span>";
			sObj.after(addStr);
			var editBtn = $("#editFeaturestBtn_"+treeNode.tId);
			if (editBtn) editBtn.bind("click", function(){
				_this.editLayer(treeNode);
				return false;
			});
			var drawFeaturesBtn = $("#drawFeaturesBtn_"+treeNode.tId);
			if (drawFeaturesBtn) drawFeaturesBtn.bind("click", function(){
				_this.drawFeature(treeNode);
				return false;
			});
			var delBtn = $("#delFeaturestBtn_"+treeNode.tId);
			if (delBtn) delBtn.bind("click", function(){
				_this.deleteLayer(treeNode);
				return false;
			});
		}
		else if('Feature'==treeNode.type &&  !$("#editFeaturetBtn_"+treeNode.tId).length>0){
			var addStr= "<span class='button edit' id='editFeaturetBtn_" + treeNode.tId+ "' title='编辑' onfocus='this.blur();'></span>";
			addStr += "<span class='button remove' id='delFeatureBtn_" + treeNode.tId+ "' title='删除' onfocus='this.blur();'></span>";
			sObj.after(addStr);
			var editBtn = $("#editFeaturetBtn_"+treeNode.tId);
			if (editBtn) editBtn.bind("click", function(){
				_this.editFeature(treeNode) 
				return false;
			});
			var delBtn = $("#delFeatureBtn_"+treeNode.tId);
			if (delBtn) delBtn.bind("click", function(){
				_this.deleteLayer(treeNode);
				return false;
			});
		}
		//漫游
		else if('Roams'==treeNode.type &&  !$("#editRoamsBtn_"+treeNode.tId).length>0){
			var addStr= "<span class='button edit' id='editRoamsBtn_" + treeNode.tId+ "' title='编辑' onfocus='this.blur();'></span>";
			addStr += "<span class='button roam' id='drawRoamsBtn_" + treeNode.tId+ "' title='漫游' onfocus='this.blur();'></span>";
			addStr += "<span class='button remove' id='delRoamsBtn_" + treeNode.tId+ "' title='删除' onfocus='this.blur();'></span>";
			sObj.after(addStr);
			var editBtn = $("#editRoamsBtn_"+treeNode.tId);
			if (editBtn) editBtn.bind("click", function(){
				_this.editLayer(treeNode);
				return false;
			});
			var drawRoamsBtn = $("#drawRoamsBtn_"+treeNode.tId);
			if (drawRoamsBtn) drawRoamsBtn.bind("click", function(){
				_this.addRoam(treeNode);
				return false;
			});
			var delBtn = $("#delRoamsBtn_"+treeNode.tId);
			if (delBtn) delBtn.bind("click", function(){
				_this.deleteLayer(treeNode);
				return false;
			});
		}
		else if('Roam'==treeNode.type &&  !$("#editRoamBtn_"+treeNode.tId).length>0){
			var addStr= "<span class='button edit' id='editRoamBtn_" + treeNode.tId+ "' title='编辑' onfocus='this.blur();'></span>";
			addStr += "<span class='button roamRoad' id='drawRoamBtn_" + treeNode.tId+ "' title='绘制漫游路线' onfocus='this.blur();'></span>";
			addStr += "<span class='button roamDoing' id='roamDoingBtn_" + treeNode.tId+ "' title='运行' onfocus='this.blur();'></span>";
			addStr += "<span class='button remove' id='delRoamBtn_" + treeNode.tId+ "' title='删除' onfocus='this.blur();'></span>";
			sObj.after(addStr);
			var editBtn = $("#editRoamBtn_"+treeNode.tId);
			if (editBtn) editBtn.bind("click", function(){
				_this.editRoam(treeNode);
				return false;
			});
			var drawRoamBtn = $("#drawRoamBtn_"+treeNode.tId);
			if (drawRoamBtn) drawRoamBtn.bind("click", function(){
				_this.addRoamRoad(treeNode);
				return false;
			});
			
			var roamDoingBtn = $("#roamDoingBtn_"+treeNode.tId);
			if (roamDoingBtn) roamDoingBtn.bind("click", function(){
				_this.doingRoam(treeNode);
				return false;
			});
			
			var delBtn = $("#delRoamBtn_"+treeNode.tId);
			if (delBtn) delBtn.bind("click", function(){
				_this.deleteLayer(treeNode);
				return false;
			});
		}
		
	};
	function removeHoverDom(treeId, treeNode) {
		$("#editLayersBtn_"+treeNode.tId).unbind().remove();
		$("#importLayersBtn_"+treeNode.tId).unbind().remove();
		$("#delLayersBtn_"+treeNode.tId).unbind().remove();

		$("#editGroupBtn_"+treeNode.tId).unbind().remove();
		$("#delGroupBtn_"+treeNode.tId).unbind().remove();
		$("#importGroupBtn_"+treeNode.tId).unbind().remove();
		
		$("#editPointsBtn_"+treeNode.tId).unbind().remove();
		$("#markPointBtn_"+treeNode.tId).unbind().remove();
		$("#delPointsBtn_"+treeNode.tId).unbind().remove();
		
		$("#editPointBtn_"+treeNode.tId).unbind().remove();
		$("#delPointBtn_"+treeNode.tId).unbind().remove();
		
		//在线底图
		$("#editMapsBtn_"+treeNode.tId).unbind().remove();
		$("#selectMapsBtn_"+treeNode.tId).unbind().remove();
		$("#delMapsBtn_"+treeNode.tId).unbind().remove();
		/*$("#editMapBtn_"+treeNode.tId).unbind().remove();*/
		$("#delMapBtn_"+treeNode.tId).unbind().remove();
		
		//模型
		$("#editModelsBtn_"+treeNode.tId).unbind().remove();
		$("#delModelsBtn_"+treeNode.tId).unbind().remove();
		$("#selectModelsBtn_"+treeNode.tId).unbind().remove();
		$("#delModelBtn_"+treeNode.tId).unbind().remove();
		$("#editModelBtn_"+treeNode.tId).unbind().remove();
		
		//面集合
		$("#editPolygonsBtn_"+treeNode.tId).unbind().remove();
		$("#drawPolygonsBtn_"+treeNode.tId).unbind().remove();
		$("#delPolygonsBtn_"+treeNode.tId).unbind().remove();
		$("#editPolygonBtn_"+treeNode.tId).unbind().remove();
		$("#delPolygonBtn_"+treeNode.tId).unbind().remove();
		
		//线
		$("#editLinesBtn_"+treeNode.tId).unbind().remove();
		$("#drawLinesBtn_"+treeNode.tId).unbind().remove();
		$("#delLinesBtn_"+treeNode.tId).unbind().remove();	
		$("#editLineBtn_"+treeNode.tId).unbind().remove();
		$("#delLineBtn_"+treeNode.tId).unbind().remove();
		
		//模型融合
		$("#editVideoModelsBtn_"+treeNode.tId).unbind().remove();
		$("#delVideoModelsBtn_"+treeNode.tId).unbind().remove();
		$("#drawVideoModelsBtn_"+treeNode.tId).unbind().remove();
		$("#editVideoModelBtn_"+treeNode.tId).unbind().remove();
		$("#delVideoModelBtn_"+treeNode.tId).unbind().remove();
		
		$("#editVideoPlanesBtn_"+treeNode.tId).unbind().remove();
		$("#delVideoPlanesBtn_"+treeNode.tId).unbind().remove();
		$("#drawVideoPlanesBtn_"+treeNode.tId).unbind().remove();
		$("#editVideoPlaneBtn_"+treeNode.tId).unbind().remove();
		$("#delVideoPlaneBtn_"+treeNode.tId).unbind().remove();
		
		
		$("#editVideoShed3dsBtn_"+treeNode.tId).unbind().remove();
		$("#delVideoShed3dsBtn_"+treeNode.tId).unbind().remove();
		$("#drawVideoShed3dsBtn_"+treeNode.tId).unbind().remove();
		$("#editVideoShed3dBtn_"+treeNode.tId).unbind().remove();
		$("#delVideoShed3dBtn_"+treeNode.tId).unbind().remove();
		
		//单体
		$("#editFeaturestBtn_"+treeNode.tId).unbind().remove();
		$("#delFeaturestBtn_"+treeNode.tId).unbind().remove();
		$("#drawFeaturesBtn_"+treeNode.tId).unbind().remove();
		
		$("#editFeaturetBtn_"+treeNode.tId).unbind().remove();
		$("#delFeatureBtn_"+treeNode.tId).unbind().remove();
		
		//漫游
		$("#editRoamsBtn_"+treeNode.tId).unbind().remove();
		$("#delRoamsBtn_"+treeNode.tId).unbind().remove();
		$("#drawRoamsBtn_"+treeNode.tId).unbind().remove();
		$("#editRoamPointBtn_"+treeNode.tId).unbind().remove();
		$("#delRoamPointBtn_"+treeNode.tId).unbind().remove();
		
		$("#editRoamBtn_"+treeNode.tId).unbind().remove();
		$("#delRoamBtn_"+treeNode.tId).unbind().remove();
		$("#drawRoamBtn_"+treeNode.tId).unbind().remove();
		$("#roamDoingBtn_"+treeNode.tId).unbind().remove();
		
	};
	
	$.fn.zTree.init($("#sceneLayer-"+_this.sceneId), setting);
}

SceneEditor.prototype.importLayer = function(node) {
	var _this=this;
	 layer.load(2);
	 _this._Ajax('get',_this.ctx+"/scene/layer/into",{id:node.id},'html',function(e){
		layer.closeAll();
		layer.open({
		  id:'importLayer',
         type: 1,
         anim:5,
         title:"【"+node.name+"】导入数据",
         area: '380px',
         content:e,
         shade:0,
         offset: ['0px', '224px'],	
         fixed:true,
         move:false,
		 skin:'layui-layer  layui-layer-adminRight layui-layer-city',
         success: function (layero, dIndex) {
            $(layero).children('.layui-layer-content').css('overflow', 'hidden');
            _this.initLayerTable(node);
		 },
		 end:function(){
		 }
		});	
	});
}

SceneEditor.prototype.initLayerTable = function(node) {
	var _this=this;
    var bizModelForGLTFTb = table.render({
        elem: '#tableBizLayer',
        url: _this.ctx+'/scene/layer/listData', 
        toolbar: false,
        cellMinWidth: 100,
        skin:'nob',// 无边框风格
        page: {
            layout: ['prev', 'page', 'next','count'] //自定义分页布局
            ,groups:5 //只显示 1 个连续页码
            ,first: false //不显示首页
            ,last: false //不显示尾页
        },
        where:{
        	//type:'GLTF',
        },
        cols: [[
            {field: 'name', sort: false, title: '名称', align: 'left'},
            {field: 'type', sort: false, title: '类型', align: 'left'},
        ]],
        done: function (res, curr, count) {
        }
    });
    
    form.on('submit(formSubSearchBizLayer)', function (data) {
    	bizModelForGLTFTb.reload({where: data.field}, 'data');
    });
    
    table.on('row(tableBizLayer)', function(obj){
      var data = obj.data;
      obj.tr.addClass('layui-table-click').siblings().removeClass('layui-table-click');
      _this.addLayerToScene(node,obj.data);
    });
}

SceneEditor.prototype.addLayerToScene = function (node,data) {
	var _this=this;
    layer.load(2);
    var formData={}; 
    formData['sceneLayer.id']=data.id;
    formData['sceneLayer.sceneId']=_this.sceneId;
    formData['sceneLayer.parentId']=node.id;
    formData['sceneLayer.name']=data.name;
    formData['sceneLayer.code']=data.code;
    formData['sceneLayer.type']=data.type;
    formData['sceneLayer.sort']=data.sort;
    formData['sceneLayer.defaultStyleId']=data.defaultStyleId;
    formData['sceneLayer.hoverStyleId']=data.hoverStyleId;
    formData['sceneLayer.selectedStyleId']=data.selectedStyleId;
    _this.ajaxFunc(_this.ctx+"/scene/layer/save",formData, "json", function(res){
        layer.closeAll('loading');
        if (200==res.code) {
            layer.msg(res.message, {icon: 1});
            _this.globe.updateLayer(res.data);
            _this.reAsyncLayerByPId(node.id);
        } else {
            layer.msg(res.message, {icon: 2});
        }
    });
}

/**
 * 添加组
 */
SceneEditor.prototype.addOrEditGroupLayer = function(parentId,id) {
	var _this=this;
	 layer.load(2);
	 _this._Ajax('get',_this.ctx+"/biz/layer/group",{parentId:parentId,id:id},'html',function(e){
		layer.closeAll();
		layer.open({
		  id:'addOrEditGroupLayer',
          type: 1,
          anim:5,
          title:(id?'编辑组':'新增组'),
          area: '300px',
          content:e,
          shade:0,
          offset: ['0px', '224px'],	
          fixed:true,
          move:false,
		    skin:'layui-layer  layui-layer-adminRight layui-layer-city',
          success: function (layero, dIndex) {
             $(layero).children('.layui-layer-content').css('overflow', 'hidden');
             form.render();
             form.on('submit(submitBizLayerForm)', function (data) {
	        	    layer.load(2);
	                var formData={}; 
	                for(var key in data.field){
	                	formData['sceneLayer.'+key]=data.field[key];
	                }
	                formData['sceneLayer.sceneId']=_this.sceneId;
	                _this.ajaxFunc(_this.ctx+"/scene/layer/saveLayer",formData, "json", function(res){
		                layer.closeAll('loading');
		                if (200==res.code) {
		                    layer.msg(res.message, {icon: 1});
		                    $("#bizLayerId").val(res.data.id);
		                    _this.reAsyncLayerByPId(id?res.data.parentId:_this.sceneId);
		                } else {
		                    layer.msg(res.message, {icon: 2});
		                }
	                });
	        	    return false;
	        	});
			},
			end:function(){
			}
		});	
	});
}

SceneEditor.prototype.editLayer = function(node) {
	var _this=this;
	 layer.load(2);
	 _this._Ajax('get',_this.ctx+"/scene/layer/edit",{layerId:(node?node.id:"")},'html',function(e){
		layer.closeAll();
		layer.open({
		   id:'editLayer',
           type: 1,
           anim:5,
           title:(node?'编辑':'新增'),
           area: '300px',
           content:e,
           shade:0,
           offset: ['0px', '224px'],	
           fixed:true,
           move:false,
		   skin:'layui-layer  layui-layer-adminRight layui-layer-city',
           success: function (layero, dIndex) {
              $(layero).children('.layui-layer-content').css('overflow', 'hidden');
              
	      	    treeSelect.render({
	    	        elem: '#parentId',
	    	        data: _this.ctx+'/scene/layer/getTree?sceneId='+_this.sceneId+'&type=Layers',
	    	        type: 'get',
	    	        placeholder: '请选择',
	    	        search: true,
	    	        click: function(d){
	    	        },
	    	        success: function (d) {
	    	        	if(node && node.parentId!=_this.sceneId){
	    	        		treeSelect.checkNode('parentId', node.parentId);
	    	        	}
	    	        }
	    	    });
              
	      	    form.render();
	              form.on('submit(submitBizLayerForm)', function (data) {
		        	    layer.load(2);
		                var formData={}; 
		                for(var key in data.field){
		                	formData['sceneLayer.'+key]=data.field[key];
		                }
		                if(!formData['sceneLayer.parentId']){
		                	formData['sceneLayer.parentId']=_this.sceneId;
		                }
		                formData['sceneLayer.sceneId']=_this.sceneId;
		                _this.ajaxFunc(_this.ctx+"/scene/layer/save",formData, "json", function(res){
			                layer.closeAll('loading');
			                if (200==res.code) {
			                    layer.msg(res.message, {icon: 1});
			                    $("#bizLayerId").val(res.data.id);
			                    _this.reAsyncLayerByPId(res.data.layerId);
			                } else {
			                    layer.msg(res.message, {icon: 2});
			                }
		                });
		        	    return false;
		        	});
			},
			end:function(){
			}
		});	
	});
}

SceneEditor.prototype.deleteLayer=function(treeNode){
	var _this=this;
    layer.confirm('确定要删除“【<span style="color:red;">' + treeNode.name + '</span>】”吗？', {
        skin: 'layui-layer-admin',
        shade: .1
    }, function (index) {
        layer.close(index);
        layer.load(2);
        _this.ajaxFunc(_this.ctx+"/scene/layer/delete",{sceneId:_this.sceneId,id:treeNode.id,type:treeNode.type}, "json", function(res){
	        layer.closeAll('loading');
	        if (200==res.code) {
	            layer.msg(res.message, {icon: 1});
	            _this.globe.removeLayer(treeNode.id);
	    		var treeObj = $.fn.zTree.getZTreeObj("sceneLayer-"+_this.sceneId);
	    		if(treeNode&&treeObj){
	    			var node = treeObj.getNodeByParam("id",treeNode.id, null);
	    			if(node){
	    				treeObj.removeNode(node);
	    			}
	    			_this.globe.removeById(treeNode.id,treeNode.type);
	    		}
	            return true;
	        } else {
	            layer.msg(res.message, {icon: 2});
	            return false;
	        }
        });
    }, function (index) {
        layer.close(index);
        return false;
    }); 
}

SceneEditor.prototype.reAsyncLayerByPId=function(id){
	var _this=this;
	var treeObj =  $.fn.zTree.getZTreeObj("sceneLayer-"+_this.sceneId);
	if (treeObj) {
		var  node= treeObj.getNodeByParam("id",id, null);
		treeObj.reAsyncChildNodes(node||null, "refresh");
	}
}

SceneEditor.prototype.addOrUpdateLayerNode=function(data){
	var _this=this;
	var treeObj =  $.fn.zTree.getZTreeObj("sceneLayer-"+_this.sceneId);
	if (treeObj) {
		var node=treeObj.getNodeByParam("id",data.id, null);
		if(node){
			if(node.name){
				node.name=data.name
				node.code=data.code
				node.sort=data.sort
			}
			treeObj.updateNode(node);
		}else{
			var parentNode=treeObj.getNodeByParam("id",data.id, null);
			treeObj.addNodes(parentNode, {
				id:data.id, 
				pId:data.layerId, 
				layerId:data.layerId, 
				name:data.name||"",
				type:data.type||null,
				code:data.code||null,
				defaultStyleId:data.defaultStyleId||null,
				hoverStyleId:data.hoverStyleId||null,
				selectedStyleId:data.selectedStyleId||null,
			});
		}
	}
}

SceneEditor.prototype.getLayerNodeById=function(id){
	var _this=this;
	var treeObj =  $.fn.zTree.getZTreeObj("sceneLayer-"+_this.sceneId);
	if (treeObj) {
		return treeObj.getNodeByParam("id",id, null);
	}
}

SceneEditor.prototype.addLayerNodeById=function(id){
	var _this=this;
	var treeObj =  $.fn.zTree.getZTreeObj("sceneLayer-"+_this.sceneId);
	if (treeObj) {
		return treeObj.getNodeByParam("id",id, null);
	}
}

SceneEditor.prototype.showOrHideLayer=function(treeNode){
	if(!treeNode){
		layer.alert('未选中图层节点！！', {icon: 1,skin: 'layer-ext-moon' })
		return;
	}
	var _this=this;
	if('Maps'==treeNode.type || 'Map'==treeNode.type ){
		_this.showOrHideProvider(treeNode.id,treeNode.checked,treeNode.type);
	}
	else if('Points'==treeNode.type || 'Point'==treeNode.type){
		_this.showOrHidePoint(treeNode.id,treeNode.checked,treeNode.type);
	}
	else if('Models'==treeNode.type || 'Model'==treeNode.type ){
		_this.showOrHideModel(treeNode.id,treeNode.checked,treeNode.type);
	}
	else if('Polylines'==treeNode.type || 'Polyline'==treeNode.type){
		_this.showOrHidePolyline(treeNode.id,treeNode.checked,treeNode.type);
	}
	else if('Polygons'==treeNode.type || 'Polygon'==treeNode.type){
		_this.showOrHidePolygon(treeNode.id,treeNode.checked,treeNode.type);
	}
	else if('VideoPlanes'==treeNode.type || 'VideoPlane'==treeNode.type){
		_this.showOrHideVideoPlane(treeNode.id,treeNode.checked,treeNode.type);
	}
	else if('VideoShed3ds'==treeNode.type || 'VideoShed3d'==treeNode.type){
		_this.showOrHideVideoShed3d(treeNode.id,treeNode.checked,treeNode.type);
	}
	else if('VideoModels'==treeNode.type || 'VideoModel'==treeNode.type){
		_this.showOrHideVModel(treeNode.id,treeNode.checked,treeNode.type);
	}
	else if('Features'==treeNode.type || 'Feature'==treeNode.type){
		_this.showOrHideFeature(treeNode.id,treeNode.checked,treeNode.type);
	}
}



