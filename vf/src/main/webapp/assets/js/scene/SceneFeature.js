SceneEditor.prototype.showOrHideFeature=function(layerId,state,type){
	var _this=this;
	if('Features'==type){
		layer.load(2);
	    _this.ajaxFunc(_this.ctx+"/scene/feature/getList",{
	    	layerId:layerId
	    }, "json", function(res){
	    	layer.closeAll('loading');
			 if (undefined!=res && 200==res.code) {
				 var list=res.data;
				 if(true==state){
					 _this.globe.addFeatures(list); 
				 }else{
					 _this.globe.removeFeatures(list); 
				 }
			 }
			 else{
			 }
	    });
	}
	else if ('Feature'==type){
		layer.load(2);
	    _this.ajaxFunc(_this.ctx+"/scene/feature/getById",{
	    	id:layerId
	    }, "json", function(res){
	    	layer.closeAll('loading');
			 if (undefined!=res && 200==res.code) {
				 var point=res.data;
				 if(true==state){
					 _this.globe.addFeature(point); 
				 }else{
					 _this.globe.removeFeature(point); 
				 }
			 }
			 else{
			 }
	    });
	}else{
		layer.alert('未识别图层标识【"'+type+'"】！！', {icon: 1,skin: 'layer-ext-moon' })
	} 
} 
SceneEditor.prototype.drawFeature = function (node) {
	var _this=this;
	if(this.isDraw){
		console.log('已经处于绘制状态！！！');
		return;
	}
	_this.isDraw=true;
	this.drawPoint=new Cesium.DrawPoint(_this.viewer,{
		pick:function(e){
			var position=VFG.Util.getC3ToLnLa(_this.viewer,e);
			
			var option={
				id:VFG.Util.getUuid(),
				name:'',
				code:'',
				layerId:node.id,
				type:'Feature',
				x:position.x,
				y:position.y,
				z:position.z,
				dimensionsX:10,
				dimensionsY:10,
				dimensionsZ:10,
			}
			
			_this.globe.add(option);
		    var formData={}; 
		    formData['bizSceneFeature.id']=option.id;
		    formData['bizSceneFeature.layerId']=node.id;
		    formData['bizSceneFeature.name']=option.name||'未定义';
		    formData['bizSceneFeature.x']=option.x;
		    formData['bizSceneFeature.y']=option.y;
		    formData['bizSceneFeature.z']=option.z;
		    formData['bizSceneFeature.dimensionsX']=option.dimensionsX;
		    formData['bizSceneFeature.dimensionsY']=option.dimensionsY;
		    formData['bizSceneFeature.dimensionsZ']=option.dimensionsZ;
		    var res = common_ajax.ajaxFunc(_this.ctx+"/scene/feature/save",formData, "json", null);
		    if (undefined!=res && 200==res.code) {
		    	layer.msg(res.message);
		    	_this.reAsyncLayerByPId(res.data.layerId);
		    }else{
		    	layer.msg(res?res.message:'保持失败！！');
		    }
		},
		end:function(){
			_this.isDraw=false;
			_this.drawPoint=null;
		}
	});
};

SceneEditor.prototype.editFeature = function (node) {
	var _this=this;
	_this._Ajax('get',_this.ctx+"/scene/feature/setting",{featureId:node.id},'html',function(e){
		   var layerIndex= layer.open({
		    	id:'feature',
		        title: node ? '编辑【'+node.name+'】':'新增',
		        type: 1,
		        area: '300px',
		        content:e,
		        shade:0,
		        offset: ['0px', '224px'],	
		        fixed:true,
		        move:false,
				skin:'layui-layer  layui-layer-adminRight layui-layer-city',
			    success: function (layero, dIndex) {
		            $(layero).children('.layui-layer-content').css('overflow', 'hidden');
                    colorpicker.render({
                        elem: '#modelFeatureNormalColorForDiv'
                        ,color:$("#modelFeatureNormalColor").val()
                        ,format: 'rgb'
                        ,predefine: true
                        ,alpha: true
                        ,done: function(color){
                          $('#modelFeatureNormalColor').val(color);
                          _this.globe.update({
                        	  id:node.id,
                        	  type:'Feature',
                        	  normalColor:color
                          });
                  	 	}   
                        ,change: function(color){
                            $('#modelFeatureNormalColor').val(color);
                            _this.globe.update({
                            	  id:node.id,
                            	  type:'Feature',
                            	  normalColor:color
                              });
                        }
                      });		            
		            
                    colorpicker.render({
                        elem: '#modelFeatureHoverColorForDiv'
                        ,color:$("#modelFeatureHoverColor").val()
                        ,format: 'rgb'
                        ,predefine: true
                        ,alpha: true
                        ,done: function(color){
                          $('#modelFeatureHoverColor').val(color);
                          _this.globe.update({
                          	  id:node.id,
                          	  type:'Feature',
                          	  hoverColor:color
                           });
                  	 	}   
                        ,change: function(color){
                            $('#modelFeatureHoverColor').val(color);
                            _this.globe.update({
                            	  id:node.id,
                            	  type:'Feature',
                            	  hoverColor:color
                             });
                        }
                      });
		           
	            	slider.render({
	              	     elem: '#rotateForModelFeatureX-slide'
	              	    ,max:360
	              	    ,min:0
	              	    ,step:0.1
	              	    ,input: true //输入框
	              	    ,value:$("#rotateForModelFeatureX-input").val()*1 || 0
	             	    ,change: function(value){
	     	                $("#rotateForModelFeatureX-input").val(value);
	     	                var ops=form.val('modelBizModelFeatureForm');
	                         _this.globe.update({
	                          	  id:node.id,
	                          	  type:'Feature',
	                          	  rotationX:ops.rotationX,
	                          	  rotationY:ops.rotationY,
	                          	  rotationZ:ops.rotationZ
	                         });	     	                
	             	    }
	              	});
	            	  
	            	slider.render({
	              	     elem: '#rotateForModelFeatureY-slide'
	              	    ,max:360
	              	    ,min:0
	              	    ,step:0.1
	              	    ,input: true //输入框
	              	    ,value:$("#rotateForModelFeatureY-input").val()*1 || 0
	             	    ,change: function(value){
	     	                $("#rotateForModelFeatureY-input").val(value);
	     	                var ops=form.val('modelBizModelFeatureForm');
	                         _this.globe.update({
	                          	  id:node.id,
	                          	  type:'Feature',
	                          	  rotationX:ops.rotationX,
	                          	  rotationY:ops.rotationY,
	                          	  rotationZ:ops.rotationZ
	                         });	     	                
	             	    }
	              	});
	            	  
	            	slider.render({
	              	     elem: '#rotateForModelFeatureZ-slide'
	              	    ,max:360
	              	    ,min:0
	              	    ,step:0.1
	              	    ,input: true //输入框
	              	    ,value:$("#rotateForModelHeading-input").val()*1 || 0
	             	    ,change: function(value){
	     	                $("#rotateForModelFeatureZ-input").val(value);
	     	                var ops=form.val('modelBizModelFeatureForm');
	                         _this.globe.update({
	                          	  id:node.id,
	                          	  type:'Feature',
	                          	  rotationX:ops.rotationX,
	                          	  rotationY:ops.rotationY,
	                          	  rotationZ:ops.rotationZ
	                         });
	             	    }
	              	});
	            	  
	              	$('#positionForModelFeatureZ').on('input', function() {
     	                var ops=form.val('modelBizModelFeatureForm');
                        _this.globe.update({
                    	  id:node.id,
                    	  type:'Feature',
          	    		  x:ops.x,
 	       	    		  y:ops.y,
 	       	    	      z:ops.z
                       });
	            	}); 
	              	$('#dimensionsForModelFeatureX').on('input', function() {
     	                var ops=form.val('modelBizModelFeatureForm');
                        _this.globe.update({
	                      	  id:node.id,
	                      	  type:'Feature',
	                      	  dimensionsX:ops.dimensionsX,
	                      	  dimensionsY:ops.dimensionsY,
	                      	  dimensionsZ:ops.dimensionsZ
                         });
	            	}); 
	              	$('#dimensionsForModelFeatureY').on('input', function() {
     	                var ops=form.val('modelBizModelFeatureForm');
                        _this.globe.update({
	                      	  id:node.id,
	                      	  type:'Feature',
	                      	  dimensionsX:ops.dimensionsX,
	                      	  dimensionsY:ops.dimensionsY,
	                      	  dimensionsZ:ops.dimensionsZ
                         });
	            	}); 
	              	$('#dimensionsForModelFeatureZ').on('input', function() {
     	                var ops=form.val('modelBizModelFeatureForm');
                        _this.globe.update({
	                      	  id:node.id,
	                      	  type:'Feature',
	                      	  dimensionsX:ops.dimensionsX,
	                      	  dimensionsY:ops.dimensionsY,
	                      	  dimensionsZ:ops.dimensionsZ
                         });
	            	}); 
	              	
	              	form.on('select(isView)', function (data) {
                        _this.globe.update({
                      	  id:node.id,
                      	  type:'Feature',
                      	  isView:data.value,
                        });	   
	                });
	              	
	              	$('#positionForModelFeatureName').on('input', function() {
     	                var ops=form.val('modelBizModelFeatureForm');
                        _this.globe.update({
	                      	  id:node.id,
	                      	  type:'Feature',
	                      	  name:ops.name,
                         });
	            	}); 
	              	
	              	//平移
	              	var TranslationAxis=null;
				    $('#modelBizModelFeatureTranslation').click(function () {
		              	var ops=form.val('modelBizModelFeatureForm');
		              	if(ops&& ops.x && ops.y&& ops.z){
		              		if(_this.globe.contain({id:ops.id,type:'Feature'})){
		              			try {
		              				if(TranslationAxis){
		              					return;
		              				}
		              				TranslationAxis=_this.globe.createTranslationAxis({
			              				id:node.id,
			              				position:{
			              					x:ops.x ,
			              					y: ops.y,
			              					z :ops.z
			              				},
			              				callback:function(e){
			              					var lnla=_this.globe.getLnLaFormC3(e);
			              					$("#positionForModelFeatureX").val(lnla.x);
			              					$("#positionForModelFeatureY").val(lnla.y);
			              					$("#positionForModelFeatureZ").val(lnla.z);
			              					
			                                _this.globe.update({
			                              	  id:node.id,
			                              	  type:'Feature',
			                    	    	  x:lnla.x,
			           	       	    		  y:lnla.y,
			           	       	    	      z:lnla.z
			                                 });
			              					
			              				},
			              				end:function(e){
			              					TranslationAxis=null;
			              				}
			              			});
								} catch (e) {
									console.log(e)
									// TODO: handle exception
								}

		              		}
		              	}
		              	return false;
				    });
				    
				    $('#modelBizModelFeatureCopy').click(function () {
				    	_this.copyFeature(node);
		              	return false;
				    });
				    
	              	
	              	$('#positionForModelFeatureCode').on('input', function() {
     	                var ops=form.val('modelBizModelFeatureForm');
                        _this.globe.update({
	                      	  id:node.id,
	                      	  type:'Feature',
	                      	  code:ops.code,
                         });
	            	}); 
	              	
	              	var ops=form.val('modelBizModelFeatureForm');
	              	if(ops&& ops.x && ops.y&& ops.z){
	              		if(!_this.globe.contain({id:ops.id,type:'Feature'})){
	              			ops.type='Feature';
	              			_this.globe.add(ops);
	              		}
	              	}
	              	
		        	form.on('submit(modelBizModelFeatureSubmit)', function (data) {
		        	    layer.load(2);
		                var formData={}; 
		                for(var key in data.field){
		                	formData['bizSceneFeature.'+key]=data.field[key];
		                }
		                _this.ajaxFunc(_this.ctx+"/scene/feature/save",formData, "json", function(res){
			                layer.closeAll('loading');
			                if (200==res.code) {
			        		    layer.msg(res.message);
			        		    _this.reAsyncLayerByPId(res.data.layerId);		                   
			                } else {
			                    layer.msg(res.message, {icon: 2});
			                }
		                });
		        	    return false;
		        	});
		        	form.render(); 
				},
				end:function(){
				}
	    	});  
	});
};

SceneEditor.prototype.copyFeature = function (node) {
	var _this=this;
	   layer.open({
	    	id:'copyFeature',
	        title: node ? '复制【'+node.name+'】':'复制',
	        type: 1,
	        area: '300px',
	        content:$('#modelCopyFeatureScript').html(),
	        shade:0,
	        offset: ['0px', '224px'],	
	        fixed:true,
	        move:false,
			skin:'layui-layer  layui-layer-adminRight layui-layer-city',
		    success: function (layero, dIndex) {
	            $(layero).children('.layui-layer-content').css('overflow', 'hidden');
	        	form.on('submit(modelSubmitCopyFeatureForm)', function (data) {
	        	    layer.load(2);
	                var formData={}; 
	                for(var key in data.field){
	                	formData[key]=data.field[key];
	                }
	                formData["id"]=node.id
	                _this.ajaxFunc(_this.ctx+"/scene/feature/copy",formData, "json", function(res){
		                layer.closeAll('loading');
		                if (200==res.code) {
		        		    layer.msg(res.message);
		        		    _this.reAsyncLayerByPId(res.data.layerId);		                   
		                } else {
		                    layer.msg(res.message, {icon: 2});
		                }
	                });
	        	    return false;
	        	});
	        	form.render(); 
			},
			end:function(){
			}
   	});
};
