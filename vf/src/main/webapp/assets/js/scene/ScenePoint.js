SceneEditor.prototype.showOrHidePoint=function(layerId,state,type){
	var _this=this;
	if('Points'==type){
		layer.load(2);
	    _this.ajaxFunc(_this.ctx+"/scene/point/getList",{
	    	layerId:layerId
	    }, "json", function(res){
	    	layer.closeAll('loading');
			 if (undefined!=res && 200==res.code) {
				 var list=res.data;
				 if(true==state){
					 _this.globe.addPoints(list); 
//					 _this.globe.addCluster({
//						 layerId:layerId,
//					     points:list		 
//					 });
				 }else{
					 _this.globe.removePoints(list); 
//					 _this.globe.removeCluster({
//						 layerId:layerId,
//					     points:list		 
//					 });
				 }
			 }
			 else{
			 }
	    });
	}
	else if ('Point'==type){
		layer.load(2);
	    _this.ajaxFunc(_this.ctx+"/scene/point/getById",{
	    	id:layerId
	    }, "json", function(res){
	    	layer.closeAll('loading');
			 if (undefined!=res && 200==res.code) {
				 var point=res.data;
				 if(true==state){
					 _this.globe.addPoint(point); 
					 _this.globe.flyToPointById(point.id,function(e){
						 
					 });
				 }else{
					 _this.globe.setViewPointById(point.id,function(e){
						 _this.globe.removePoint(point); 
					 });
				 }
			 }
			 else{
			 }
	    });
	}else{
		layer.alert('未识别图层标识【"'+type+'"】！！', {icon: 1,skin: 'layer-ext-moon' })
	} 
} 

/**
 * 打点
 */
SceneEditor.prototype.markPoint = function (treeNode) {
	var _this=this;
	if(this.isDraw){
		console.log('已经处于绘制状态！！！');
		return;
	}
	layer.closeAll();
	_this.isDraw=true;
	this.drawPoint=new Cesium.DrawPoint(_this.viewer,{
		pick:function(e){
			var position=VFG.Util.getC3ToLnLa(_this.viewer,e);
			var option={
					id:VFG.Util.getUuid(),
					layerId:treeNode.id,
					name:'',
					x:position.x,
					y:position.y,
					z:position.z,
				};
			_this.globe.addPoint(option);
			_this.savePoint(option)
		},
		end:function(){
			_this.isDraw=false;
			_this.drawPoint=null;
			
		}
	});
};

/**
 * 保存点信息
 */
SceneEditor.prototype.savePoint=function(option){
	var _this=this;
    var formData={}; 
    formData['point.id']=option.id;
    formData['point.name']=option.name;
    formData['point.x']=option.x;
    formData['point.y']=option.y;
    formData['point.z']=option.z;
    formData['point.layerId']=option.layerId;
    _this.ajaxFunc(_this.ctx+"/biz/point/save",formData, "json", function(res){
        if (undefined!=res && 200==res.code) {
        	layer.msg(res.message);
        	_this.globe.flyToPointById(option.id,function(e){
			 });
        	_this.reAsyncLayerByPId(res.data.layerId);
        }else{
        	layer.msg(res?res.message:'保持失败！！');
        }
    });
}

/**
 * 编辑点信息
 */
SceneEditor.prototype.editPoint = function (treeNode) {
	var _this=this;
	layer.load(2);
	_this._Ajax('get',_this.ctx+"/scene/point/edit",{pointId:treeNode.id},'html',function(e){
		layer.closeAll('loading');   
		layer.open({
	    	id:'editPoint',
	        type: 1,
	        anim:5,
	        title: name || '编辑【'+treeNode.name+'】点',
	        area: '300px',
	        shade:0,
	        offset: ['0px', '224px'],	
	        fixed:true,
	        move:false,
			skin:'layui-layer  layui-layer-adminRight layui-layer-city',
		    content: e,
		    success: function (layero, dIndex) {
	            $(layero).children('.layui-layer-content').css('overflow', 'hidden');
	        	form.on('submit(submitBizPointForm)', function (data) {
	        	    layer.load(2);
	                var formData={}; 
	                for(var key in data.field){
	                	formData['point.'+key]=data.field[key];
	                }
	                _this.ajaxFunc(_this.ctx+"/biz/point/save",formData, "json", function(res){
		                layer.closeAll('loading');
		                if (200==res.code) {
		                    layer.msg(res.message, {icon: 1});
		                    _this.reAsyncLayerByPId(res.data.layerId);
		                } else {
		                    layer.msg(res.message, {icon: 2});
		                }
	                });
	        	    return false;
	        	});
			    $('#settingBizPointVisualAngle').click(function () {
			        var visualPos=VFG.Util.getVisualAngle(_this.viewer);
			        if(visualPos){
			            var formData={}; 
			            formData['point.id']=treeNode.id;
			            formData['point.heading']=visualPos.heading;
			            formData['point.pitch']=visualPos.pitch;
			            formData['point.roll']=visualPos.roll;
			            formData['point.cameraX']=visualPos.position.x;
			            formData['point.cameraY']=visualPos.position.y;
			            formData['point.cameraZ']=visualPos.position.z;
			            _this.ajaxFunc(_this.ctx+"/biz/point/save",formData, "json", function(res){
				            if (undefined!=res && 200==res.code) {
				            	 layer.msg(res.message);
				            }
			            });
			        }
			    });
	        	form.render(); 
			},
			end:function(){
			}
    	});  
	});
};


/**
 * 点拖拽
 */
SceneEditor.prototype.dragPoint = function (pointId) {
	var _this=this;
    _this.isDrag=true;
    var position;
	var cesiumPick =new Cesium.Pick(_this.viewer,{
		LEFT_CLICK:function(movement){
			position=VFG.Util.getScreenToLnLa (_this.viewer, movement.position);
			if(position){
				_this.globe.update({
					id:pointId,
					type:'Point',
					x:position.x,
					y:position.y,
					z:position.z,
				});
			}
		},
		RIGHT_CLICK:function(e){
			_this.isDrag=false;
			if(position){
			    var formData={}; 
			    formData['point.id']=pointId;
			    formData['point.x']=position.x;
			    formData['point.y']=position.y;
			    formData['point.z']=position.z;
			    _this.ajaxFunc(_this.ctx+"/biz/point/save",formData, "json", function(res){
			        if (undefined!=res && 200==res.code) {
			        	layer.msg('坐标更新成功！');
			        }else{
			        	layer.msg('坐标更新失败！');
			        }
			    });
			}
		}
	});
}

/**
 * 操作点
 */
SceneEditor.prototype.operationPoint = function (screenPos,pointId,type) {
	var _this=this;
	 var point=_this.globe.getById(pointId,type);
	if(point){
		layer.load(2);
		var VFGWin;
		_this._Ajax('get',_this.ctx+"/biz/point/operation",{sceneId:_this.sceneId,pointId:pointId},'html',function(e){
			layer.closeAll('loading');   
			layer.open({
				id:pointId,
			    type: 1,
			    title: false,
			    shade: 0,
			    content:e,
			    resize :false,
			    success: function (layero, dIndex) {
		            $(layero).children('.layui-layer-content').css('overflow', 'hidden');
		        	VFGWin=new Cesium.VFGWin(_this.viewer,{
		            	position:Cesium.Cartesian3.fromDegrees(point.x*1,point.y*1,point.z*1),
		            	layero:layero,
		            	index:dIndex,
		            });
		        	
		        	$('#toolForBizOperationDrag').click(function () {
		        		layer.close(dIndex);
		        		_this.dragPoint(pointId);
		        	});	
		        	$('#toolForBizOperationEdit').click(function () {
		        		layer.close(dIndex);
		        		_this.editPoint({id:pointId,name:point.name});
		        	});	
		        	$('#toolForBizOperationStyle').click(function () {
		        	});	
		        	$('#toolForBizOperationDel').click(function () {
		        		layer.close(dIndex);
		        		_this.deleteLayer({
		        			id:pointId,
		        			type:type,
		        			name:point.name,
		        		})
		        	});	
				},
				end:function(){
					if(VFGWin){
						 VFGWin.destroy();
						 VFGWin=null;
					}
				}
	    	});  
		});
	}else{
		layer.alert('信息未知！！', {icon: 1,skin: 'layer-ext-moon' })
	}
}

SceneEditor.prototype.editPointLayer=function(node){
	var _this=this;
	 layer.load(2);
	 _this._Ajax('get',_this.ctx+"/scene/point/setting",{id:node.id},'html',function(e){
		layer.closeAll();
		layer.open({
		  id:'editPointLayer',
          type: 1,
          anim:5,
          title:'编辑【'+node.name+'】',
          area: '350px',
          content:e,
          shade:0,
          offset: ['0px', '224px'],	
          fixed:true,
          move:false,
		  skin:'layui-layer  layui-layer-adminRight layui-layer-city',
          success: function (layero, dIndex) {
             $(layero).children('.layui-layer-content').css('overflow', 'hidden');
             
      	     treeSelect.render({
    	        elem: '#parentId',
    	        data: _this.ctx+'/scene/layer/getTree?sceneId='+_this.sceneId+'&type=Layers',
    	        type: 'get',
    	        placeholder: '请选择',
    	        search: true,
    	        click: function(d){
    	        },
    	        success: function (d) {
    	        	if(node){
    	        		treeSelect.checkNode('parentId', node.parentId);
    	        	}
    	        }
    	     });
             
             var curStyleTabIndex=0;
         	 form.on('select(pointType)', function(data){
        		if("0"==data.value){
        			_this.findPointStyle(curStyleTabIndex,node);
        		}
        		else if("1"==data.value){
        		}
        		form.render();
        	 });
         	 form.on('select(pointHoverType)', function(data){
         		if("0"==data.value){
         			_this.findPointStyle(curStyleTabIndex,node);
         		}
         		else if("1"==data.value){
         		}
         		form.render();
         	 });
         	 form.on('select(pointSelectType)', function(data){
         		if("0"==data.value){
         			_this.findPointStyle(curStyleTabIndex,node);
         		}
         		else if("1"==data.value){
         		}
         		form.render();
         	 });         	 
         	 element.on('tab(pointStyleTabFilter)', function(data){
         		 curStyleTabIndex=data.index;
         	 });
             form.render();
             form.on('submit(submitBizLayerForm)', function (data) {
	        	    layer.load(2);
	                var formData={}; 
	                for(var key in data.field){
	                	formData['bizSceneLayer.'+key]=data.field[key];
	                }
	                _this.ajaxFunc(_this.ctx+"/scene/layer/save",formData, "json", function(res){
		                layer.closeAll('loading');
		                if (200==res.code) {
		                	 layer.msg(res.message, {icon: 1});
			                 _this.reAsyncLayerByPId(res.data.parentId);
		                } else {
		                    layer.msg(res.message, {icon: 2});
		                }
	                });
	        	    return false;
	        	});
			},
			end:function(){
			}
		});	
	});
}

SceneEditor.prototype.findPointStyle=function(curTabIndex,node){
	var _this=this;
	 layer.load(2);
	var id;
 	if(curTabIndex==0) id=node.defaultStyleId;
	else if(curTabIndex==1) id=node.hoverStyleId;
	else if(curTabIndex==2) id=node.selectedStyleId;
    _this.ajaxFunc(_this.ctx+"/scene/point/findStyleById",{id:id}, "json", function(res){
    	layer.closeAll('loading');
        if (res.code == 200) {
        	_this.addOrEditPointStyle(curTabIndex,node,res.data);
        }else{
        	_this.addOrEditPointStyle(curTabIndex,node,null);
        }
    });	
}

SceneEditor.prototype.addOrEditPointStyle=function (curTabIndex,node,data){
	var _this=this;
	var text;
	if(curTabIndex==0) text="默认";
	else if(curTabIndex==1) text="悬浮";
	else if(curTabIndex==2) text="选中";
	layer.open({
		id:'addOrEditPointStyle',
        type: 1,
        anim:5,
        title:'【'+node.name+'】-->【'+text+'】样式设置',
        area: '350px',
        content:$('#modelBizStylePoint').html(),
        shade:0,
        offset: ['0px', '224px'],	
        fixed:true,
        move:false,
		skin:'layui-layer  layui-layer-adminRight layui-layer-city',
        success: function (layero, dIndex) {
           $(layero).children('.layui-layer-content').css('overflow', 'hidden');
           form.val('modelBizStylePointForm', data);  // 回显数据
	       	colorpicker.render({//点颜色
	    		elem: '#colorForPoint',
	    		color: (data&&data.colorForPoint)?data.colorForPoint:null,
	    		format: 'rgb',
	    		predefine: true,
	    		alpha: true,
	    		done: function(color) {
	    			$('#colorForPoint-input').val(color); //向隐藏域赋值
	    			color || this.change(color); //清空时执行 change
	    		},
	    		change: function(color) {}
	    	});
	
	    	colorpicker.render({//点轮廓颜色
	    		elem: '#outlineColorForPoint',
	    		color: (data&&data.outlineColorForPoint)?data.outlineColorForPoint:null,
	    		format: 'rgb',
	    		predefine: true,
	    		alpha: true,
	    		done: function(color) {
	    			$('#outlineColorForPoint-input').val(color); //向隐藏域赋值
	    			color || this.change(color); //清空时执行 change
	    		},
	    		change: function(color) {}
	    	}); 
	    	
	    	colorpicker.render({//标签填充颜色
	    		elem: '#fillColorForLabel',
	    		color: (data&&data.fillColorForLabel)?data.fillColorForLabel:null,
	    		format: 'rgb',
	    		predefine: true,
	    		alpha: true,
	    		done: function(color) {
	    			$('#fillColorForLabel-input').val(color); //向隐藏域赋值
	    			color || this.change(color); //清空时执行 change
	    		},
	    		change: function(color) {}
	    	});
	    	colorpicker.render({//标签轮廓颜色
	    		elem: '#outlineColorForLabel',
	    		color: (data&&data.outlineColorForLabel)?data.outlineColorForLabel:null,
	    		format: 'rgb',
	    		predefine: true,
	    		alpha: true,
	    		done: function(color) {
	    			$('#outlineColorForLabel-input').val(color); //向隐藏域赋值
	    			color || this.change(color); //清空时执行 change
	    		},
	    		change: function(color) {}
	    	});
	    	colorpicker.render({//标签背景颜色
	    		elem: '#backgroundColorForLabel',
	    		color: (data&&data.backgroundColorForLabel)?data.backgroundColorForLabel:null,
	    		format: 'rgb',
	    		predefine: true,
	    		alpha: true,
	    		done: function(color) {
	    			$('#backgroundColorForLabel-input').val(color); //向隐藏域赋值
	    			color || this.change(color); //清空时执行 change
	    		},
	    		change: function(color) {}
	    	});
	    	
	    	//显示图片
	    	if(data&& data.imageForBillboard){
	    		$("#imageForBillboard-img").attr("src",_this.ctx+"/"+data.imageForBillboard);
	    	}
	    	
	    	//选择图标
			$('#imageForBillboard-select-btn').click(function () {
				_this.selectIcon('point',function(data){
		        	$("#imageForBillboard-img").attr("src",_this.ctx+"/"+data.url);
		        	$("#imageForBillboard-input").val(data.url);
		        	$("#widthForBillboard").val(data.width);
		        	$("#heightForBillboard").val(data.height);
				});
				return false;
			});
			
        	$('#displayNearForPointBtn').click(function () {
       		 	$("#displayNearForPoint").val(_this.getVisualHeight());
       		 	return false;
        	});			
        	$('#displayFarForPointBtn').click(function () {
       		 	$("#displayFarForPoint").val(_this.getVisualHeight());
       		 	return false;
        	});	
        	
        	$('#scalarNearForPointBtn').click(function () {
       		 	$("#scalarNearForPoint").val(_this.getVisualHeight());
       		 	return false;
        	});			
        	$('#scalarFarForPointBtn').click(function () {
       		 	$("#scalarFarForPoint").val(_this.getVisualHeight());
       		 	return false;
        	});	
        	
        	$('#translucencyNearForPointBtn').click(function () {
       		 	$("#translucencyNearForPoint").val(_this.getVisualHeight());
       		 	return false;
        	});			
        	$('#translucencyFarForPointBtn').click(function () {
       		 	$("#translucencyFarForPoint").val(_this.getVisualHeight());
       		 	return false;
        	});
        	
        	$('#displayNearForLabelBtn').click(function () {
       		 	$("#displayNearForLabel").val(_this.getVisualHeight());
       		 	return false;
        	});			
        	$('#displayFarForLabelBtn').click(function () {
       		 	$("#displayFarForLabel").val(_this.getVisualHeight());
       		 	return false;
        	});
        	
        	$('#scalarNearForLabelBtn').click(function () {
       		 	$("#scalarNearForLabel").val(_this.getVisualHeight());
       		 	return false;
        	});			
        	$('#scalarFarForLabelBtn').click(function () {
       		 	$("#scalarFarForLabel").val(_this.getVisualHeight());
       		 	return false;
        	});  
        	
        	$('#pixelOffsetScaleNearForLabelBtn').click(function () {
       		 	$("#pixelOffsetScaleNearForLabel").val(_this.getVisualHeight());
       		 	return false;
        	});			
        	$('#pixelOffsetScaleFarForLabelBtn').click(function () {
       		 	$("#pixelOffsetScaleFarForLabel").val(_this.getVisualHeight());
       		 	return false;
        	}); 
        	
        	$('#translucencyNearForLabelBtn').click(function () {
       		 	$("#translucencyNearForLabel").val(_this.getVisualHeight());
       		 	return false;
        	});			
        	$('#translucencyFarForLabelBtn').click(function () {
       		 	$("#translucencyFarForLabel").val(_this.getVisualHeight());
       		 	return false;
        	}); 
        	

        	$('#displayNearForBillboardBtn').click(function () {
       		 	$("#displayNearForBillboard").val(_this.getVisualHeight());
       		 	return false;
        	});			
        	$('#displayFarForBillboardBtn').click(function () {
       		 	$("#displayFarForBillboard").val(_this.getVisualHeight());
       		 	return false;
        	}); 
        	
        	$('#pixelOffsetScaleNearForBillboardBtn').click(function () {
       		 	$("#pixelOffsetScaleNearForBillboard").val(_this.getVisualHeight());
       		 	return false;
        	});			
        	$('#pixelOffsetScaleFarForBillboardBtn').click(function () {
       		 	$("#pixelOffsetScaleFarForBillboard").val(_this.getVisualHeight());
       		 	return false;
        	}); 
        	
        	$('#scalarNearForBillboardBtn').click(function () {
       		 	$("#scalarNearForBillboard").val(_this.getVisualHeight());
       		 	return false;
        	});			
        	$('#scalarFarForBillboardBtn').click(function () {
       		 	$("#scalarFarForBillboard").val(_this.getVisualHeight());
       		 	return false;
        	});
        	
        	$('#translucencyNearForBillboardBtn').click(function () {
       		 	$("#translucencyNearForBillboard").val(_this.getVisualHeight());
       		 	return false;
        	});			
        	$('#translucencyFarForBillboardBtn').click(function () {
       		 	$("#translucencyFarForBillboard").val(_this.getVisualHeight());
       		 	return false;
        	}); 
        	
		    form.on('submit(modelSubmitBizStylePointForm)', function (data) {
		        layer.load(2);
		        var formData={}; 
		        for(var key in data.field){
		        	formData['bizStylePoint.'+key]=data.field[key];
		        }
	        	if(curTabIndex==0)  formData['bizStylePoint.name']=node.name+"【默认】";
	        	else if(curTabIndex==1)  formData['bizStylePoint.name']=node.name+"【悬浮】";
	        	else if(curTabIndex==2)  formData['bizStylePoint.name']=node.name+"【选中】";
		        formData['bizStylePoint.code']=node.code;
	        	if(curTabIndex==0) formData['bizStylePoint.id']=node.defaultStyleId;
	        	else if(curTabIndex==1) formData['bizStylePoint.id']=node.hoverStyleId;
	        	else if(curTabIndex==2) formData['bizStylePoint.id']=node.selectedStyleId;
			    _this.ajaxFunc(_this.ctx+"/scene/point/saveStyle",formData, "json", function(res){
			    	layer.closeAll('loading');
	                if (res.code == 200) {
	                    layer.msg(res.message, {icon: 1});
	                    _this.globe.updateStyle(res.data);
	                    
	                } else {
	                    layer.msg(res.message, {icon: 2});
	                }
			    });
		        return false;
		    });
		    form.render();            
		},
		end:function(){
		}
	});
}



