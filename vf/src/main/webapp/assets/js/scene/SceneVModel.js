SceneEditor.prototype.showOrHideVModel=function(layerId,state,type){
	var _this=this;
	if('VideoModels'==type){
		layer.load(2);
	    _this.ajaxFunc(_this.ctx+"/scene/video/model/getList",{
	    	layerId:layerId
	    }, "json", function(res){
	    	layer.closeAll('loading');
			 if (undefined!=res && 200==res.code) {
				 var list=res.data;
				 if(state==true){
					 _this.globe.addVModels(list,_this.ctx);
				 }else{
					 _this.globe.removeVModels(list);
				 }
			 }
			 else{
			 }
	    });
	}
	else if ('VideoModel'==type){
		layer.load(2);
	    _this.ajaxFunc(_this.ctx+"/scene/video/model/getById",{
	    	id:layerId
	    }, "json", function(res){
	    	layer.closeAll('loading');
			 if (undefined!=res && 200==res.code) {
				 var model=res.data; 
				 if(model){
					 if(state==true){
						 _this.globe.addVModel(model,_this.ctx);
						 _this.globe.flyToVModelById(model.id);
					 }else{
						 _this.globe.removeVModel(model);
					 }
				 }
			 }
			 else{
			 }
	    });
	}else{
		layer.alert('未识别图层标识【"'+type+'"】！！', {icon: 1,skin: 'layer-ext-moon' })
	} 
} 

SceneEditor.prototype.operationVModel = function (screenPos,planeId,type) {
	var _this=this;
	 var plane=_this.globe.getPrimitiveById(planeId,type);
	if(plane){
		layer.load(2);
		var VFGWin;
		_this._Ajax('get',_this.ctx+"/biz/video/model/operation",{sceneId:_this.sceneId,planeId:planeId},'html',function(e){
			layer.closeAll('loading');   
			var layerIndex= layer.open({
					id:planeId,
				    type: 1,
				    title: false,
				    shade: 0,
				    content:e,
				    resize :false,
				    success: function (layero, dIndex) {
			            $(layero).children('.layui-layer-content').css('overflow', 'hidden');
			            var cartesian3=VFG.Util.getScreenToC3(_this.viewer, screenPos, null);
			        	VFGWin=new Cesium.VFGWin(_this.viewer,{
			            	position:cartesian3,
			            	layero:layero,
			            	index:dIndex,
			            });
			        	$('#toolForBizOperationDrag').click(function () {
			        		layer.close(dIndex);
			        		//_this.dragPoint(pointId);
			        	});	
			        	$('#toolForBizOperationEdit').click(function () {
			        		layer.close(dIndex);
			        		_this.editVModel({id:planeId,name:plane.option.name});
			        	});	
			        	$('#toolForBizOperationDel').click(function () {
			        		layer.close(dIndex);
			        		_this.deleteLayer({
			        			id:planeId,
			        			type:type,
			        			name:plane.option.name,
			        		})
			        	});	
					},
					end:function(){
						if(VFGWin){
							 VFGWin.destroy();
							 VFGWin=null;
						}
					}
		    	});  
		});
	}else{
		layer.alert('信息未知！！', {icon: 1,skin: 'layer-ext-moon' })
	}
}

SceneEditor.prototype.editVModel = function (treeNode) {
	var _this=this;
	layer.load(2);
	_this._Ajax('get',_this.ctx+"/biz/video/model/edit",{sceneId:_this.sceneId,modelId:treeNode.id},'html',function(e){
		layer.closeAll('loading');   
		var layerIndex= layer.open({
		    	id:'editVModel',
		        title: name || '编辑【'+treeNode.name+'】',
		        type: 1,
		        area: '350px',
		        shade:0,
		        offset: ['0px', '224px'],	
		        fixed:true,
		        move:false,
				skin:'layui-layer  layui-layer-adminRight layui-layer-city',
			    content: e,
			    success: function (layero, dIndex) {
		            $(layero).children('.layui-layer-content').css('overflow', 'hidden');
		            
				    $('#bizVideoModelGetBtnVideo').click(function () {
				    	_this.selectVideo(treeNode,function(e){
				    		$("#bizVideoModelName-input").val(e.name);
				    		$("#bizVideoModelCode").val(e.code);
				    		$("#bizVideoModelVideoId").val(e.id);
				    	});
				    });
				    $('#bizVideoModelGetBtnModel').click(function () {
				    	_this.selectModel(treeNode,"OBJ",function(e){
				    		$("#bizVideoModelModelName-input").val(e.name);
				    		$("#bizVideoModelModelId").val(e.id);
				    	});
				    });
				    
				    slider.render({
		              elem: '#bizVideoModelRotationX'
		              ,input: true //输入框
		              ,min: 0 //最小值
		              ,max: 360 //最大值
		              ,value:$("#bizVideoModelRotationX-input").val()*1
		              ,change: function(value){
		            	  $("#bizVideoModelRotationX-input").val(value);
		            	  var bizVideoModelId=$("#bizVideoModelId").val();
		            	  _this.globe.changeVModelRX(bizVideoModelId,value*1);
	            	  }
		            });
		            slider.render({
		              elem: '#bizVideoModelRotationY'
		              ,input: true //输入框
		              ,min: 0 //最小值
		              ,max: 360 //最大值
		              ,value:$("#bizVideoModelRotationY-input").val()*1
		              ,change: function(value){
		            	  $("#bizVideoModelRotationY-input").val(value);
		            	  var bizVideoModelId=$("#bizVideoModelId").val();
		            	  _this.globe.changeVModelRY(bizVideoModelId,value*1);
	            	  }
		            });
			            
		            slider.render({
		              elem: '#bizVideoModelRotationZ'
		              ,input: true //输入框
		              ,min: 0 //最小值
		              ,max: 360 //最大值
		              ,value:$("#bizVideoModelRotationZ-input").val()*1
		              ,change: function(value){
		            	  $("#bizVideoModelRotationZ-input").val(value);
		            	  var bizVideoModelId=$("#bizVideoModelId").val();
		            	  _this.globe.changeVModelRZ(bizVideoModelId,value*1);
	            	  }
		            });
		            
                	$('#bizVideoModelZ').on('input', function(e) {
                		var bizVideoModelId=$("#bizVideoModelId").val();
                		_this.globe.changeVModelHeight(bizVideoModelId,$("#bizVideoModelZ").val()*1);
                	});
                	
                	$('#bizVideoModelScaleX').on('input', function(e) {
                       _this.globe.changeVModelScale($("#bizVideoModelId").val(),{
                    	   scaleX:$("#bizVideoModelScaleX").val()*1,
                    	   scaleY:$("#bizVideoModelScaleY").val()*1,
                    	   scaleZ:$("#bizVideoModelScaleZ").val()*1,
                       });
                	});
                	
                	$('#bizVideoModelScaleY').on('input', function(e) {
                        _this.globe.changeVModelScale($("#bizVideoModelId").val(),{
                     	   scaleX:$("#bizVideoModelScaleX").val()*1,
                     	   scaleY:$("#bizVideoModelScaleY").val()*1,
                     	   scaleZ:$("#bizVideoModelScaleZ").val()*1,
                        });
                	});
                	
                	$('#bizVideoModelScaleZ').on('input', function(e) {
                        _this.globe.changeVModelScale($("#bizVideoModelId").val(),{
                     	   scaleX:$("#bizVideoModelScaleX").val()*1,
                     	   scaleY:$("#bizVideoModelScaleY").val()*1,
                     	   scaleZ:$("#bizVideoModelScaleZ").val()*1,
                        });
                	});
		            
				    var isPick=false;
				    $('#bizVideoModelGetBtnPosition').click(function () {
				    	if(!isPick){
							this.pickPosition=new Cesium.DrawPoint(_this.viewer,{
								pick:function(e){
									var position=VFG.Util.getC3ToLnLa(_this.viewer,e);
						    		$("#bizVideoModelX").val(position.x);
						    		$("#bizVideoModelY").val(position.y);
						    		$("#bizVideoModelZ").val(position.z);
					            	  var bizVideoModelId=$("#bizVideoModelId").val();
					            	  _this.globe.changeVModelPosition(bizVideoModelId,{
					            		  x:position.x,
					            		  y:position.y,
					            		  z:position.z,
					            	  });
								},
								end:function(){
									isPick=false;
								}
							});
				    	}
				    });
		        	form.on('submit(submitbizVModelForm)', function (data) {
		        	    layer.load(2);
		                var formData={}; 
		                for(var key in data.field){
		                	formData['bizVideoModel.'+key]=data.field[key];
		                }
		                formData['bizVideoModel.layerId']=data.field['layerId']||treeNode.id;
		                formData['bizVideoModel.sceneId']=_this.sceneId;
		                _this.ajaxFunc(_this.ctx+"/biz/video/model/save",formData, "json", function(res){
			                layer.closeAll('loading');
			                if (200==res.code) {
			                    var data=res.data;
			                    $("#bizVideoModelId").val(data.id)
			                    _this.reAsyncLayerByPId(res.data.layerId);
			                    layer.msg(res.message, {icon: 1});
			                } else {
			                    layer.msg(res.message, {icon: 2});
			                }
		                });
		        	    return false;
		        	});
				    $('#settingbizVModelVisualAngle').click(function () {
				        var visualPos=VFG.Util.getVisualAngle(_this.viewer);
				        if(visualPos){
				            var formData={}; 
				            formData['bizVideoModel.id']=treeNode.id;
				            formData['bizVideoModel.heading']=visualPos.heading;
				            formData['bizVideoModel.pitch']=visualPos.pitch;
				            formData['bizVideoModel.roll']=visualPos.roll;
				            formData['bizVideoModel.cameraX']=visualPos.position.x;
				            formData['bizVideoModel.cameraY']=visualPos.position.y;
				            formData['bizVideoModel.cameraZ']=visualPos.position.z;
				            _this.ajaxFunc(_this.ctx+"/biz/video/model/save",formData, "json", function(res){
					            if (undefined!=res && 200==res.code) {
					            	 layer.msg(res.message);
					            	 _this.reAsyncLayerByPId(res.data.layerId);
					            }
				            });
				        }
				    });
				    
				    var dragControls;
				    $('#setting-drag-btn').click(function () {
				    	if(dragControls==null){
				    		console.log('sdfs');
				        	var x=$("#bizVideoModelX").val();
				        	var y=$("#bizVideoModelY").val();
				        	var z=$("#bizVideoModelZ").val()*1+5;
				    		dragControls= new VFG.OrbitControls(_this.globe.viewer,{
				    			id:treeNode.id,
				    			position:Cesium.Cartesian3.fromDegrees(x,y,z),
				    			callback:function(e){
				    				var e=_this.globe.getLnLaFormC3(e);
				    	        	$("#bizVideoModelX").val(e.x);
				    	        	$("#bizVideoModelY").val(e.y);
				    	        	$("#bizVideoModelZ").val(e.z-5);
				    	        	e.z-=5;
				    	        	_this.globe.changeVModelPosition(treeNode.id,e);
				    			},
				    			end:function(e){
				    				var e=_this.globe.getLnLaFormC3(e);
				    	        	$("#bizVideoModelX").val(e.x);
				    	        	$("#bizVideoModelY").val(e.y);
				    	        	$("#bizVideoModelZ").val(e.z-5);
				    				dragControls=null;
				    			}
				    		 });
				    	}
				    	 return false;
				    });
				    
				    
		        	form.render(); 
				},
				end:function(){
				}
	    	});  
	});
};





