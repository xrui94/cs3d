SceneEditor.prototype.showOrHideModel=function(layerId,state,type){
	var _this=this;
	if('Models'==type){
		layer.load(2);
	    _this.ajaxFunc(_this.ctx+"/scene/model/getList",{
	    	layerId:layerId
	    }, "json", function(res){
	    	layer.closeAll('loading');
			 if (undefined!=res && 200==res.code) {
				 var list=res.data;
				 if(state==true){
					 _this.globe.addModels(list);
				 }else{
					 _this.globe.removeModels(list);
				 }
			 }
			 else{
			 }
	    });
	}else if('Model'==type){
		layer.load(2);
	    _this.ajaxFunc(_this.ctx+"/scene/model/getById",{
	    	id:layerId
	    }, "json", function(res){
	    	layer.closeAll('loading');
			 if (undefined!=res && 200==res.code) {
				 var model=res.data; 
				 if(model){
					 if(state==true){
						 _this.globe.addModel(model);
					 }else{
						 _this.globe.removeModel(model);
					 }
				 }
			 }
			 else{
			 }
	    });
	}else{
		layer.alert('未识别图层标识【"'+type+'"】！！', {icon: 1,skin: 'layer-ext-moon' })
	}
} 

/**
 * 在线模型
 */
SceneEditor.prototype.addOnLineModel=function(node){
	 var _this=this;
	 layer.load(2);
	 _this._Ajax('get',_this.ctx+"/scene/model/select",{sceneId:_this.sceneId,layerId:node.id},'html',function(e){
		layer.closeAll('loading');
	    layer.open({
	    	id:'addOnLineModel',
	        title:'模型资源库',
	        type: 1,
	        area: '335px',
	        content:e,
	        shade:0,
	        offset: ['0px', '224px'],
	        fixed:true,
	        move:false,
	        skin:'layui-layer layui-layer-adminRight layui-layer-city',
		    content: e,
		    success: function (layero, dIndex) {
	            $(layero).children('.layui-layer-content').css('overflow', 'hidden');
	            $(layero).children('.layui-layer-content').css('height', 'calc(100% - 30px)');
	            form.render();
	            _this.loadOnLineModel(1,node);
        	    treeSelect.render({
        	        elem: '#classifyId',
        	        data: _this.ctx+'/biz/model/classify/selectTree',
        	        type: 'get',
        	        placeholder: '请选择',
        	        search: true,
        	        click: function(d){
        	        },
        	        success: function (d) {
        	        }
        	    });
        	    
		   		$('#selectModelSearchBtn').click(function () {
		   		  _this.loadOnLineModel(1,node);
				});
        	    
			},
			end:function(){
			}
	    });  
	});
}

SceneEditor.prototype.loadOnLineModel=function(curr,node){
	var _this=this;
	var search=form.val('selectModelSearchForm');
    _this.ajaxFunc(_this.ctx+'/scene/model/listData',{
    	page:curr?curr:1,
    	limit:10,
    	keywords:search?search.keywords:'',
    	classifyId:search?search.classifyId:'',
    }, "json", function(res){
        if (undefined!=res && 0==res.code) {
        	res.ctx=_this.ctx;
            laytpl(selectModelsScript.innerHTML).render(res, function (html) {
                $('#select-model-list').html(html);
        	    $(".ImageList>.content>.item>.IconButton ").on('click',function () {
        	    	layer.load(2);
        	        _this.addModelToScene(node,$(this).attr('id')) 
        	        layer.closeAll('loading');
        	    })
            });
            laypage.render({
              elem: 'selectModelPageBar'
              ,count:res.count
              ,curr:curr
              ,layout: ['prev', 'next','count']
              ,jump: function(obj, first){
                if(!first){
                	_this.loadOnLineModel(obj.curr,node);
                }
              }
            });	 
        }else{
        }
    });
}

SceneEditor.prototype.addModelToScene = function (node,modelId) {
	var _this=this;
    _this.ajaxFunc(_this.ctx+"/scene/model/getModelById",{
    	id:modelId
    }, "json", function(res){
        if (undefined!=res && 200==res.code) {
        	var model=res.data;
        	if(model.type=='3DTILES'){
        		_this.add3DTILESToScene(node,model);
        	}
        	else if(model.type=='OBJ'){
        	}
        	else if(model.type=='GLTF'){
        		_this.addGltfToScene(node,model);
        	}
        }else{
        	layer.alert(undefined!=res?res.messge:'系统错误！', {icon: 3,skin: 'layer-ext-moon' })
        }
    });
}

SceneEditor.prototype.add3DTILESToScene = function (node,model) {
	var _this=this;
	var id=VFG.Util.getUuid();
	_this.globe.addModel({
		id:id,
		url:model.url,
		name:model.name,
		type:model.type,
		callBack:function(e){
			if(e.state==true){
		        var formData={}; 
		        formData['bizSceneModel.id']=id;
		        formData['bizSceneModel.sceneId']=_this.sceneId;
		        formData['bizSceneModel.modelId']=model.id;
		        formData['bizSceneModel.layerId']=node.id;
		        formData['bizSceneModel.name']=model.name;
		        formData['bizSceneModel.type']=model.type;
		        formData['bizSceneModel.x']=e.param.x;
		        formData['bizSceneModel.y']=e.param.y;
		        formData['bizSceneModel.z']=e.param.z;
		        formData['bizSceneModel.pitch']=e.param.rotation_x;
		        formData['bizSceneModel.roll']=e.param.rotation_z;
		        formData['bizSceneModel.heading']=e.param.rotation_y;
		        _this.ajaxFunc(_this.ctx+"/scene/model/save",formData, "json", function(res){
		            if (undefined!=res && 200==res.code) {
		        		layer.alert('成功！！', {icon: 1,skin: 'layer-ext-moon' })
		        		_this.reAsyncLayerByPId(res.data.layerId);
		            }else{
		        		layer.alert('失败！！', {icon: 3,skin: 'layer-ext-moon' })
		            }
		        });
			}else{
				layer.alert('模型添加失败！', {icon: 3,skin: 'layer-ext-moon' })
			}
		}
	})
}

SceneEditor.prototype.addGltfToScene = function (node,model) {
	var _this=this;
	if(this.isDraw){
		console.log('已经处于绘制状态！！！');
		return;
	}
	var primitive=null;
	_this.isDraw=true;
	var position;
	_this.drawModel=new Cesium.DrawModel(_this.viewer,{
		leftClick:function(e){
			var position=VFG.Util.getC3ToLnLa(_this.viewer,e);
			if(position){
				var id=VFG.Util.getUuid();
				primitive=_this.globe.addModel({
					id:id,
					url:model.url,
					name:model.name,
					x:position.x,
					y:position.y,
					z:position.z,
					type:model.type
				});
				
		        var formData={}; 
		        formData['bizSceneModel.id']=id;
		        formData['bizSceneModel.sceneId']=_this.sceneId;
		        formData['bizSceneModel.modelId']=model.id;
		        formData['bizSceneModel.layerId']=node.id;
		        formData['bizSceneModel.name']=model.name;
		        formData['bizSceneModel.type']=model.type;
		        formData['bizSceneModel.x']=position.x;
		        formData['bizSceneModel.y']=position.y;
		        formData['bizSceneModel.z']=position.z;
		        _this.ajaxFunc(_this.ctx+"/scene/model/save",formData, "json", function(res){
		            if (undefined!=res && 200==res.code) {
		        		layer.msg('成功！！')
		        		_this.reAsyncLayerByPId(res.data.layerId);
		            }else{
		        		layer.msg('失败！！')
		        		_this.globe.removeModelById(id);
		            }
		        });
			}
		},
		end:function(){
			_this.isDraw=false;
			_this.drawModel=null;
			_this.addOnLineModel(node);
		}
	});
}


SceneEditor.prototype.editModel = function(node) {
	var _this=this;
	 layer.load(2);
	 _this._Ajax('get',_this.ctx+"/scene/model/setting",{modelId:node.id},'html',function(e){
		layer.closeAll();
		layer.open({
		   id:'editModel',
           type: 1,
           anim:5,
           title:'模型设置',
           area: '300px',
           content:e,
           shade:0,
           offset: ['0px', '224px'],	
           fixed:true,
           move:false,
		   skin:'layui-layer  layui-layer-adminRight layui-layer-city',
           success: function (layero, dIndex) {
              $(layero).children('.layui-layer-content').css('overflow', 'hidden');
		    	var type=$("#modelBizSceneModelType").val();
		    	if('3DTILES'==type){
		    		_this.editModel3DTILES(node.id);
		    	}
		    	else if('GLTF'==type){
		    		_this.editModelGLTF(node.id);
		    	}
			},
			end:function(){
			}
		});	
	});
}

SceneEditor.prototype.editModel3DTILES=function(id){
	var _this=this;
	let isPick=false;
    $('#pick-position-btn').click(function () {
    	if(!isPick){
   		    $("#pick-position-btn").attr("disabled", true)
        	isPick=true;
        	_this.drawModel=new Cesium.DrawModel(_this.viewer,{
        		leftClick:function(e){
        			var position=VFG.Util.getC3ToLnLa(_this.viewer,e);
        			if(position){
        				$("#translationForModelPosX").val(position.x);
        				$("#translationForModelPosY").val(position.y);
        				$("#translationForModelPosZ").val(position.z);
        				 var ops=form.val('modelBizSceneModelForm');  // 回显数据
        		    	 var primitive=VFG.Model.getById(id)
        		    	 if(primitive){
        		    		 var modelMatrix=VFG.Util.getPrimitiveModelMatrix(ops);
        		    		 primitive._root.transform=modelMatrix;
        		    	 } 
        			}
        		},
        		end:function(){
        			isPick=false;
        			 $("#pick-position-btn").attr("disabled", false)
        		}
        	});
    	}
    	return false;
    });
    
    var dragControls;
    $('#setting-drag-btn').click(function () {
    	if(dragControls==null){
        	var x=$("#translationForModelPosX").val();
        	var y=$("#translationForModelPosY").val();
        	var z=$("#translationForModelPosZ").val()*1+50;
    		dragControls= new VFG.OrbitControls(_this.globe.viewer,{
    			id:id,
    			position:Cesium.Cartesian3.fromDegrees(x,y,z),
    			callback:function(e){
    				var e=_this.globe.getLnLaFormC3(e);
    	        	$("#translationForModelPosX").val(e.x);
    	        	$("#translationForModelPosY").val(e.y);
    	        	$("#translationForModelPosZ").val(e.z-50);
    	        	e.z-=50;
	   				 var ops=form.val('modelBizSceneModelForm');  // 回显数据
			    	 var primitive=VFG.Model.getById(id)
			    	 if(primitive){
			    		 var modelMatrix=VFG.Util.getPrimitiveModelMatrix(ops);
			    		 primitive._root.transform=modelMatrix;
			    	 } 
    			},
    			end:function(e){
    				var e=_this.globe.getLnLaFormC3(e);
    	        	$("#translationForModelPosX").val(e.x);
    	        	$("#translationForModelPosY").val(e.y);
    	        	$("#translationForModelPosZ").val(e.z-50);
    				dragControls=null;
    			}
    		 });
    	}

    	 return false;
    });
	
    
    
    
	  $('#translationForModelPosZ').on('input', function() {
		 var ops=form.val('modelBizSceneModelForm');  // 回显数据
    	 var primitive=VFG.Model.getById(id)
    	 if(primitive){
    		 var modelMatrix=VFG.Util.getPrimitiveModelMatrix(ops);
    		 primitive._root.transform=modelMatrix;
    	 }   
	  });
	  

	 $('#scaleForModel-input').on('input', function() {
		 var ops=form.val('modelBizSceneModelForm');  // 回显数据
    	 var primitive=VFG.Model.getById(id)
    	 if(primitive){
    		 var modelMatrix=VFG.Util.getPrimitiveModelMatrix(ops);
    		 primitive._root.transform=modelMatrix;
    	 } 
	 });
	  
	  
	
	  slider.render({
	     elem: '#rotateForModelRoll-slide'
	    ,max:360
	    ,min:0
	    ,step:0.1
	    ,input: true //输入框
	    ,value:$("#rotateForModelRoll-input").val()*1 || 0
	    ,change: function(value){
	    	 $("#rotateForModelRoll-input").val(value);
	         var ops=form.val('modelBizSceneModelForm');  // 回显数据
	         var ops=form.val('modelBizSceneModelForm');  // 回显数据
	    	 var primitive=VFG.Model.getById(id)
	    	 if(primitive){
	    		 var modelMatrix=VFG.Util.getPrimitiveModelMatrix(ops);
	    		 primitive._root.transform=modelMatrix;
	    	 } 
	       
	    }
	  });
	  
	  slider.render({
	     elem: '#rotateForModelHeading-slide'
	    ,max:360
	    ,min:0
	    ,step:0.1
	    ,input: true //输入框
	    ,value:$("#rotateForModelHeading-input").val()*1 || 0
	    ,change: function(value){
	         $("#rotateForModelHeading-input").val(value);
	         var ops=form.val('modelBizSceneModelForm');  // 回显数据
	         var ops=form.val('modelBizSceneModelForm');  // 回显数据
  	    	 var primitive=VFG.Model.getById(id)
  	    	 if(primitive){
  	    		 var modelMatrix=VFG.Util.getPrimitiveModelMatrix(ops);
  	    		 primitive._root.transform=modelMatrix;
  	    	 } 
	    }
	  });            	  
	  slider.render({
	     elem: '#rotateForModelPitch-slide'
	    ,max:360
	    ,min:0
	    ,step:0.1
	    ,input: true //输入框
	    ,value:$("#rotateForModelPitch-input").val()*1 || 0
	    ,change: function(value){
	        $("#rotateForModelPitch-input").val(value);
	        var ops=form.val('modelBizSceneModelForm');  // 回显数据
  	    	 var primitive=VFG.Model.getById(id)
  	    	 if(primitive){
  	    		 var modelMatrix=VFG.Util.getPrimitiveModelMatrix(ops);
  	    		 primitive._root.transform=modelMatrix;
  	    	 } 
	    }
	  });
	  
	form.on('submit(modelBizSceneModelSubmit)', function (data) {
	    layer.load(2);
	    var formData={}; 
	    for(var key in data.field){
			formData['bizSceneModel.'+key]=data.field[key];
	    }
	    _this.ajaxFunc(_this.ctx+"/scene/model/save",formData, "json", function(res){
	        layer.closeAll('loading');
	        if (res!=undefined && 200==res.code) {
	        	var data=res.data;
	            layer.msg(res.message, {icon: 1});
	            _this.reAsyncLayerByPId(data.layerId);
	        } else {
	        	 layer.msg(res.message, {icon: 1});
	        }
	    });
	    return false;
	});
    $('#setting-visualangle-btn').click(function () {
    	layer.load(2);
      	var visualAngle= _this.globe.getVisualAngle();
      	if(visualAngle){
            var formData={}; 
            formData['bizSceneModel.id']=id;
            formData['bizSceneModel.cameraX']=visualAngle.position.x;
            formData['bizSceneModel.cameraY']=visualAngle.position.y;
            formData['bizSceneModel.cameraZ']=visualAngle.position.z;
            formData['bizSceneModel.heading']=visualAngle.heading;
            formData['bizSceneModel.pitch']=visualAngle.pitch;
            formData['bizSceneModel.roll']=visualAngle.roll;
            _this.ajaxFunc(_this.ctx+"/scene/model/save",formData, "json", function(res){
            	layer.closeAll('loading');
                if (undefined!=res && 200==res.code) {
            		layer.msg('成功！！')
            		_this.reAsyncLayerByPId(res.data.layerId);
                }else{
            		layer.msg('失败！！')
            		_this.globe.removeModelById(id);
                }
            });
      	}else{
      		layer.closeAll('loading');
      	}
    	return false;
    }); 
	form.render();
}

SceneEditor.prototype.editModelGLTF=function(id){
	let _this=this;
	let isPick=false;
    $('#pick-position-btn').click(function () {
    	if(!isPick){
   		    $("#pick-position-btn").attr("disabled", true)
        	isPick=true;
        	_this.drawModel=new Cesium.DrawModel(_this.viewer,{
        		leftClick:function(e){
        			var position=VFG.Util.getC3ToLnLa(_this.viewer,e);
        			if(position){
        				$("#translationForModelPosX").val(position.x);
        				$("#translationForModelPosY").val(position.y);
        				$("#translationForModelPosZ").val(position.z);
        				_this.globe.changeModelPosition(id,position);
        			}
        		},
        		end:function(){
        			isPick=false;
        			 $("#pick-position-btn").attr("disabled", false)
        		}
        	});
    	}
    	return false;
    });
    $('#setting-visualangle-btn').click(function () {
    	layer.load(2);
      	var visualAngle= _this.globe.getVisualAngle();
      	if(visualAngle){
            var formData={}; 
            formData['bizSceneModel.id']=id;
            formData['bizSceneModel.cameraX']=visualAngle.position.x;
            formData['bizSceneModel.cameraY']=visualAngle.position.y;
            formData['bizSceneModel.cameraZ']=visualAngle.position.z;
            formData['bizSceneModel.heading']=visualAngle.heading;
            formData['bizSceneModel.pitch']=visualAngle.pitch;
            formData['bizSceneModel.roll']=visualAngle.roll;
            _this.ajaxFunc(_this.ctx+"/scene/model/save",formData, "json", function(res){
            	layer.closeAll('loading');
                if (undefined!=res && 200==res.code) {
            		layer.msg('成功！！')
            		_this.reAsyncLayerByPId(res.data.layerId);
                }else{
            		layer.msg('失败！！')
            		_this.globe.removeModelById(id);
                }
            });
      	}else{
      		layer.closeAll('loading');
      	}
    	return false;
    });    
    
    var dragControls;
    $('#setting-drag-btn').click(function () {
    	if(dragControls==null){
        	var x=$("#translationForModelPosX").val();
        	var y=$("#translationForModelPosY").val();
        	var z=$("#translationForModelPosZ").val()*1+5;
    		dragControls= new VFG.OrbitControls(_this.globe.viewer,{
    			id:id,
    			position:Cesium.Cartesian3.fromDegrees(x,y,z),
    			callback:function(e){
    				var e=_this.globe.getLnLaFormC3(e);
    	        	$("#translationForModelPosX").val(e.x);
    	        	$("#translationForModelPosY").val(e.y);
    	        	$("#translationForModelPosZ").val(e.z-5);
    	        	e.z-=5;
    	        	_this.globe.changeModelPosition(id,e);
    			},
    			end:function(e){
    				var e=_this.globe.getLnLaFormC3(e);
    	        	$("#translationForModelPosX").val(e.x);
    	        	$("#translationForModelPosY").val(e.y);
    	        	$("#translationForModelPosZ").val(e.z-5);
    				dragControls=null;
    			}
    		 });
    	}

    	 return false;
    });
	
	 $('#translationForModelPosZ').on('input', function() {
		 var value=$("#translationForModelPosZ").val();
		 _this.globe.changeModelHeight(id,value)   	
	 });
	
	 slider.render({
	     elem: '#rotateForModelRoll-slide'
	    ,max:360
	    ,min:0
	    ,step:0.1
	    ,input: true //输入框
	    ,value:$("#rotateForModelRoll-input").val()*1 || 0
	    ,change: function(value){
	    	$("#rotateForModelRoll-input").val(value);
	    	_this.globe.changeModelRoll(id,value)   	 
	    }
	  });
	  slider.render({
		     elem: '#rotateForModelHeading-slide'
		    ,max:360
		    ,min:0
		    ,step:0.1
		    ,input: true //输入框
		    ,value:$("#rotateForModelHeading-input").val()*1 || 0
	    ,change: function(value){
	        $("#rotateForModelHeading-input").val(value);
	        _this.globe.changeModelHeading(id,value)
	    }
	  });            	  
	  slider.render({
		     elem: '#rotateForModelPitch-slide'
		    ,max:360
		    ,min:0
		    ,step:0.1
		    ,input: true //输入框
		    ,value:$("#rotateForModelPitch-input").val()*1 || 0
	    ,change: function(value){
	        $("#rotateForModelPitch-input").val(value);
	        _this.globe.changeModelPitch(id,value)   	                
	    }
	  });
	  
	  

	 $('#scaleForModel-input').on('input', function() {
		  var value=$(this).val()*1;
		  _this.globe.changeModelScale(id,value)   
	 });
  
  	 colorpicker.render({//颜色
		elem: '#color',
		color: $("#color-input").val() || null,
		format: 'rgb',
		predefine: true,
		alpha: true,
		done: function(color) {
			$('#color-input').val(color); //向隐藏域赋值
			color || this.change(color); //清空时执行 change
			_this.globe.changeModelColor(id,color)   
			
		},
		change: function(color) {}
	 });
  	 
  	 colorpicker.render({//颜色
		elem: '#silhouetteColor',
		color:$("#silhouetteColor-input").val() || null,
		format: 'rgb',
		predefine: true,
		alpha: true,
		done: function(color) {
			$('#silhouetteColor-input').val(color); //向隐藏域赋值
			color || this.change(color); //清空时执行 change
			_this.globe.changeModelSilhouetteColor(id,color)   
		},
		change: function(color) {}
	 }); 
  	 
	  $('#silhouetteSize').on('input', function() {
		  var value=$(this).val()*1;
		  _this.globe.changeModelSilhouetteSize(id,value)   
	 });
	  
	 form.on('select(colorBlendMode)', function(data) {
		_this.globe.changeModelColorBlendMode(id,data.value)  
	 });
	 
	 form.on('select(oftenShow)', function(data) {
		_this.globe.changeModelHighlight(id,data.value)  
	 });
  
     form.on('submit(modelbizSceneModelSubmit)', function (data) {
    	layer.load(2);
	    var formData={}; 
	    for(var key in data.field){
			formData['bizSceneModel.'+key]=data.field[key];
	    }
	    _this.ajaxFunc(_this.ctx+"/scene/model/save",formData, "json", function(res){
	        layer.closeAll('loading');
	        if (res!=undefined && 200==res.code) {
	        	var data=res.data;
	            layer.msg(res.message, {icon: 1});
	            _this.reAsyncLayerByPId(data.layerId);
	        } else {
	        	 layer.msg(res.message, {icon: 1});
	        }
	    });
	    return false;
	});
	form.render();
}

