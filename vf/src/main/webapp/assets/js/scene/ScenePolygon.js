SceneEditor.prototype.showOrHidePolygon=function(layerId,state,type){
	var _this=this;
	if('Polygons'==type){
		layer.load(2);
	    _this.ajaxFunc(_this.ctx+"/scene/polygon/getList",{
	    	layerId:layerId
	    }, "json", function(res){
	    	layer.closeAll('loading');
			 if (undefined!=res && 200==res.code) {
				 var list=res.data;
				 if(state==true){
					 _this.globe.addPolygons(list);
				 }else{
					 _this.globe.removePolygons(list);
				 }
			 }
			 else{
			 }
	    });
	}
	else if ('Polygon'==type){
		layer.load(2);
	    _this.ajaxFunc(_this.ctx+"/scene/polygon/getById",{
	    	id:layerId
	    }, "json", function(res){
	    	layer.closeAll('loading');
			 if (undefined!=res && 200==res.code) {
				 var model=res.data; 
				 if(model){
					 if(state==true){
						 _this.globe.addPolygon(model);
						 _this.globe.flyToPolygonById(model.id);
					 }else{
						 _this.globe.removePolygon(model);
					 }
				 }
			 }
			 else{
			 }
	    });
	}else{
		layer.alert('未识别图层标识【"'+type+'"】！！', {icon: 1,skin: 'layer-ext-moon' })
	}
}

/************************************绘制多边形*********************************************/
SceneEditor.prototype.drawPolygon = function (node) {
	var _this=this;
	if(this.isDraw){
		console.log('已经处于绘制状态！！！');
		return;
	}
	_this.isDraw=true;
	_this.globe.drawPolygon({finish:function(positions){
		_this.isDraw=false;
		if(positions && positions.length>=3){
		    layer.load(2);
		    var formData={}; 
		    formData['bizPolygon.id']=VFG.Util.getUuid();
		    formData['bizPolygon.name']='多边形';
		    formData['bizPolygon.points']=JSON.stringify(positions);
		    formData['bizPolygon.layerId']=node.id;
		    _this.ajaxFunc(_this.ctx+"/biz/polygon/save",formData, "json", function(res){
		        layer.closeAll('loading');
		        if (res!=undefined && 200==res.code) {
		        	layer.msg(res.message);
		        	_this.globe.addPolygon(res.data);
		        } else{
		        	layer.msg(res?res.message:'保持失败！！');
		        }
		    });
		}
	}});
};

SceneEditor.prototype.operationPolygon = function (screenPos,polygonId,type) {
	var _this=this;
	 var polygon=_this.globe.getById(polygonId,type);
	if(polygon){
		layer.load(2);
		_this._Ajax('get',_this.ctx+"/biz/polygon/operation",{polygonId:polygonId},'html',function(e){
			layer.closeAll('loading');   
			var layerIndex= layer.open({
					id:polygonId,
				    type: 1,
				    title: false,
				    shade: 0,
				    content:e,
				    resize :false,
				    success: function (layero, dIndex) {
			            $(layero).children('.layui-layer-content').css('overflow', 'hidden');
			            var cartesian3=VFG.Util.getScreenToC3(_this.viewer, screenPos, null);
			        	VFGWin=new Cesium.VFGWin(_this.viewer,{
			            	position:cartesian3,
			            	layero:layero,
			            	index:dIndex,
			            });
			        	$('#toolForBizOperationDrag').click(function () {
			        		layer.close(dIndex);
			        		_this.globe.removePolygonById(polygonId);
			        		_this.dragPolygon(polygon.id,polygon.name,VFG.Polyline.getPositions(polygon.points));
			        	});	
			        	$('#toolForBizOperationEdit').click(function () {
			        		layer.close(dIndex);
			        		_this.editPolygon(polygonId,polygon.name);
			        	});	
			        	$('#toolForBizOperationDel').click(function () {
			        		layer.close(dIndex);
			        		_this.deleteLayer({
			        			id:polygonId,
			        			type:type,
			        			name:polygon.name,
			        		})
			        	});	
					},
					end:function(){
					}
		    	});  
		});
	}else{
		layer.alert('信息未知！！', {icon: 1,skin: 'layer-ext-moon' })
	}
}

/**
 * 编辑
 */
SceneEditor.prototype.editPolygon = function (polygonId,name) {
	var _this=this;
	_this._Ajax('get',_this.ctx+"/scene/polygon/edit",{polygonId:polygonId},'html',function(e){
		   var layerIndex= layer.open({
		    	id:'editPolygon',
		        title: name || '编辑【'+name+'】',
		        type: 1,
		        area: '300px',
		        shade:0,
				offset: ['0px', '224px'],
				fixed: true,
				move: false,
				skin: 'layui-layer  layui-layer-adminRight layui-layer-city',
			    content: e,
			    success: function (layero, dIndex) {
		            $(layero).children('.layui-layer-content').css('overflow', 'hidden');
		        	form.on('submit(submitBizPolygonForm)', function (data) {
		        	    layer.load(2);
		                var formData={}; 
		                for(var key in data.field){
		                	formData['bizPolygon.'+key]=data.field[key];
		                }
		                _this.ajaxFunc(_this.ctx+"/biz/polygon/save",formData, "json", function(res){
			                layer.closeAll('loading');
			                if (res!=undefined && 200==res.code) {
			                    layer.msg(res.message, {icon: 1});
			                    _this.reAsyncLayerByPId(res.data.layerId);
			                } else {
			                    layer.msg(res.message, {icon: 2});
			                }		                	
		                });
		        	    return false;
		        	});
		        	
		        	$('#settingBizPolygonVisualAngle').click(function () {
				        var visualPos=VFG.Util.getVisualAngle(_this.viewer);
				        if(visualPos){
				            var formData={}; 
				            formData['bizPolygon.id']=polygonId;
				            formData['bizPolygon.heading']=visualPos.heading;
				            formData['bizPolygon.pitch']=visualPos.pitch;
				            formData['bizPolygon.roll']=visualPos.roll;
				            formData['bizPolygon.cameraX']=visualPos.position.x;
				            formData['bizPolygon.cameraY']=visualPos.position.y;
				            formData['bizPolygon.cameraZ']=visualPos.position.z;
				            _this.ajaxFunc(_this.ctx+"/biz/polygon/save",formData, "json", function(res){
					            if (undefined!=res && 200==res.code) {
					            	 layer.msg(res.message);
					            }
				            });
				        }
				    });
		        	form.render(); 
		            var insTb = table.render({
		                elem: '#tablePolygonForSettingStyle',
		                url: _this.ctx+'/scene/polygon/getStyleList',
		                page: false,
		                where:{
		                	id:polygonId
		                },
		                cols: [[
		                    {field: 'name', sort: false, title: '名称'},
		                    {align: 'center', toolbar: '#tableBarPolygonForSettingStyle', title: '操作', minWidth: 80}
		                ]],
		            });
		        	$('#btnAddPolygonForStyle').click(function () {
		        		_this.editPolygonStyle(polygonId,null,function(){
		        			 insTb.reload({}, 'data');
		        		});
		        	});
		        	
		            // 工具条点击事件
		            table.on('tool(tablePolygonForSettingStyle)', function (obj) {
		                var data = obj.data;
		                var layEvent = obj.event;
		                if (layEvent === 'edit') { // 修改
			        		_this.editPolygonStyle(polygonId,data,function(){
			        			 insTb.reload({}, 'data');
			        		});
		                } else if (layEvent === 'del') { 
		                    layer.confirm('确定要删除“' + data.name + '”吗？', {
		                        skin: 'layui-layer-admin',
		                        shade: .1
		                    }, function (i) {
		                        layer.close(i);
		                        layer.load(2);
					            _this.ajaxFunc(_this.ctx+"/scene/polygon/deleteStyle",{id: data.id}, "json", function(res){
				                    layer.closeAll('loading');
				                    if (undefined!=res && res.code == 200) {
				                        layer.msg(res.message, {icon: 1});
				                        insTb.reload({}, 'data');
				                    } else {
				                        layer.msg(res.message, {icon: 2});
				                    }
					            });
		                    });
		                }
		            });
				},
				end:function(){
				}
	    	});  
	});
}

SceneEditor.prototype.editPolygonStyle=function (polygonId,data,callback){
	let _this=this;
	layer.open({
		id:'addOrEditPolygonStyle',
        type: 1,
        anim:5,
        title:data?'编辑':'新增',
        area: '350px',
        content:$('#modelBizStylePolygonScript').html(),
        shade:0,
        offset: ['0px', '224px'],	
        fixed:true,
        move:false,
		skin:'layui-layer  layui-layer-adminRight layui-layer-city',
        success: function (layero, dIndex) {
        	$(layero).children('.layui-layer-content').css('overflow', 'hidden');
           	form.val('modelBizStylePolygonForm', data);  // 回显数据
        	colorpicker.render({//颜色
        		elem: '#color',
        		color: (data&&data.color)?data.color:null,
        		format: 'rgb',
        		predefine: true,
        		alpha: true,
        		done: function(color) {
        			$('#color-input').val(color); //向隐藏域赋值
        			color || this.change(color); //清空时执行 change
        		},
        		change: function(color) {}
        	});
        	
        	colorpicker.render({//轮廓颜色
        		elem: '#outlineColor',
        		color: (data&&data.outlineColor)?data.outlineColor:null,
        		format: 'rgb',
        		predefine: true,
        		alpha: true,
        		done: function(color) {
        			$('#outlineColor-input').val(color); //向隐藏域赋值
        			color || this.change(color); //清空时执行 change
        		},
        		change: function(color) {}
        	});	
        	
        	//选择图标
			$('#mapUrl-select-btn').click(function () {
				_this.selectIcon('polygon',function(data){
		        	$("#mapUrl-img").attr("src",_this.ctx+"/"+data.url);
		        	$("#mapUrl-input").val(data.url);
				});
				return false;
			});
        	
			form.on('select(displayEffect)', function(data){
        		displayEffect(data.value);
        	})
        	
			form.on('select(perPositionHeight)', function(data){
				if("1"==data.value){
					setting3d("none")
				}else{
					setting3d("")
				}
        	})        	
			
        	form.on('select(material)', function(data){
        		if("color"==data.value){
        			showOrHideColorTr("")
        			showOrHideFillTr("");
        			showOrHideMapUrlTr("none");
        		}else{
        			showOrHideMapUrlTr("");
        			showOrHideColorTr("none");
        			showOrHideFillTr("none");
        		}
        	})
        	
        	$('#distanceDisplayConditionXBtn').click(function () {
        		 $("#distanceDisplayConditionX").val(_this.getVisualHeight());
        		 return false;
        	});	
        	$('#distanceDisplayConditionYBtn').click(function () {
	    		 $("#distanceDisplayConditionY").val(_this.getVisualHeight());
	    		 return false;
        	});
        	
        	if(data && data.displayEffect){
        		displayEffect(data.displayEffect);
        	}else{
        		face();
        	}
        	
	    	if(data&& data.mapUrl){
	    		$("#mapUrl-img").attr("src",_this.ctx+"/"+data.mapUrl);
	    	}
	    	
	    	if(data && data.displayEffect=="0"){
	    		if(data.perPositionHeight=="1"){
	    			setting3d("none")
	    		}else{
	    			setting3d("")
	    		}
	    	}
	    	
	    	if(data){
	    		if("color"==data.material){
	    			showOrHideColorTr("")
	    			showOrHideFillTr("");
	    			showOrHideMapUrlTr("none");
	    		}else{
	    			showOrHideMapUrlTr("");
	    			showOrHideColorTr("none");
	    			showOrHideFillTr("none");
		    	}
    		}else{
    			showOrHideColorTr("")
    			showOrHideFillTr("");
    			showOrHideMapUrlTr("none");
	    	}
        	
        	function displayEffect(effect){
        		if("0"==effect){
        			face();
        		}else if("1"==effect){
        			build();
        		}
        		else if("2"==effect){
        			water();
        		}
        	}
        	
        	function face(){
        		showOrHideMaterialTr("");
        		setting3d("");
        		showOrHidePerPositionHeightTr("");
        		if(data){
            		if("color"==data.material){
            			showOrHideColorTr("")
            			showOrHideFillTr("");
            			showOrHideOutlineTr("");
            			showOrHideMapUrlTr("none");
            		}else{
            			showOrHideMapUrlTr("");
            			showOrHideColorTr("none");
            			showOrHideFillTr("none");
            			showOrHideOutlineTr("none");
            		}
        		}else{
        			showOrHideColorTr("none");
        			showOrHideFillTr("none");
        		}
        		showOrHideAnimationTr("none");
        	}
        	
        	function build(){
        		showOrHideMaterialTr("");
        		setting3d("");
        		showOrHideAnimationTr("none");
        		showOrHidePerPositionHeightTr("none");
        		$('#closeTopTr').css('display',"none");
        		$('#closeBottomTr').css('display',"none");
        	}
        	
        	function water(){
        		showOrHideAnimationTr("");
    			showOrHideColorTr("")
    			showOrHideMapUrlTr("");
    			setting3d("none");
    			showOrHideFillTr("none");
    			showOrHideOutlineTr("none");
    			showOrHideMaterialTr("none");
    			showOrHidePerPositionHeightTr("none");
        	}
        	
        	function setting3d(state){
        		$('#extrudedHeightReferenceTr').css('display',state);
        		$('#extrudedHeightTr').css('display',state);
        		$('#closeTopTr').css('display',state);
        		$('#closeBottomTr').css('display',state);
        		$('#heightReferenceTr').css('display',state);
        		$('#heightTr').css('display',state);
        		$('#outlineTr').css('display',state);
        	}
        	
        	function showOrHideMaterialTr(state){
				$('#materialTr').css('display',state);
        	}
        	function showOrHideColorTr(state){
				$('#colorTr').css('display',state);
        	}
        	
        	function showOrHideMapUrlTr(state){
				$('#mapUrlTr').css('display',state);
        	}
        	function showOrHideFillTr(state){
				$('#fillTr').css('display',state);
        	}
        	function showOrHideOutlineTr(state){
				$('#outlineTr').css('display',state);
        	}
        	function showOrHideAnimationTr(state){
				$('#animationSpeedTr').css('display',state);
				$('#frequencyTr').css('display',state);
				$('#amplitudeTr').css('display',state);
        	}
        	function showOrHidePerPositionHeightTr(state){
				$('#perPositionHeightTr').css('display',state);
        	}
		    form.on('submit(modelSubmitBizStylePolygontForm)', function (data) {
		        layer.load(2);
		        var formData={}; 
		        for(var key in data.field){
		        	formData['bizStylePolygon.'+key]=data.field[key];
		        }
		        formData['polygonId']=polygonId;
			    _this.ajaxFunc(_this.ctx+"/scene/polygon/saveStyle",formData, "json", function(res){
			    	layer.closeAll('loading');
	                if (res.code == 200) {
	                    layer.msg(res.message, {icon: 1});
	                    $("#polygonStyleId").val(res.data.id);
	                    _this.globe.updateStyle(res.data);
	                } else {
	                    layer.msg(res.message, {icon: 2});
	                }
			    });
		        return false;
		    });
		    form.render();            
		},
		end:function(){
			if(callback){
				callback();
			}
		}
	});
	
	
}


SceneEditor.prototype.dragPolygon = function (uniqueId,name,points) {
	if(!uniqueId && !points){
		console.log('傻冒，错了！');
		return;
	}
	var _this=this;
	_this.isDrag=true;
	var map=new Map();
	var insertMap=new Map();
	var positions=points;
	_this.showPointForPolygon(positions,map,insertMap) ;
	var entityId=uniqueId;
	var entity=_this.createPolygon(uniqueId,name,positions);
	
	var cesiumDrag =new Cesium.Drag(_this.viewer,{
		type:'pointForPolygon',
		primitive:entity,
		LEFT_DOWN:function(e,pick){
        	if(pick && pick.id && pick.id.id && insertMap.has(pick.id.id)){
        		VFG.Util.inset(positions,pick.id.index,pick.id.position._value);
        	}else{ 
        		_this.clearCenterPointForLine(insertMap);
        	}
		},
		MIDDLE_CLICK:function(id,obj){
            if( map.has(id)){
				var point=map.get(id);
				VFG.Util.splice(positions,point.index, 1);
				_this.clearPointForPolygon(map);
				_this.clearCenterPointForPolygon(insertMap);
				_this.showPointForPolygon(positions,map,insertMap) ;
            }
		},		
		mouseMove:function(id,e){
			if(map.has(id)){
				var point=map.get(id);
				point.position = new Cesium.CallbackProperty(function () {
                    return e;
                }, false);
				positions[point.index]=e;
			}
			else if(insertMap.has(id)){
				var point=insertMap.get(id);
				point.position = new Cesium.CallbackProperty(function () {
                    return e;
                }, false);
				positions[point.index]=e;
			}
		},
		LEFT_UP:function(e){
			//清中间点位
			_this.clearPointForPolygon(map);
			_this.clearCenterPointForPolygon(insertMap);
			_this.showPointForPolygon(positions,map,insertMap) ;
		},
		end:function(){
			_this.isDrag=false;
			cesiumDrag=null;
		    layer.load(2);
		    var formData={}; 
		    formData['bizPolygon.id']=entityId;
		    formData['bizPolygon.points']=JSON.stringify(VFG.Util.c3sToLnLas(_this.viewer,positions));
		    _this.ajaxFunc(_this.ctx+"/biz/polygon/save",formData, "json", function(res){
		        layer.closeAll('loading');
		        if (res!=undefined && 200==res.code) {
					_this.clearPointForPolygon(map);
					_this.clearCenterPointForPolygon(insertMap);
					_this.viewer.entities.removeById(uniqueId);
		        	_this.globe.addPolygon(res.data);
		        	layer.msg(res.message)
		        } else{
		        	layer.msg(res?res.message:'保持失败！！！')
		        }
		    });
		}
	});
}

//辅助点
SceneEditor.prototype.showPointForPolygon = function (positions,map,insertMap) {
	var _this=this;
	for(var i=0;i<positions.length;i++){
		var uuid=VFG.Util.getUuid();
		var point=_this.addPointForPolygon(uuid,i,i,positions[i]);
		map.set(uuid,point);
		if(i+1<positions.length){
			var uuid=VFG.Util.getUuid();
			var centePoint=VFG.Util.getCenterPoint(positions[i],positions[i+1]);
			var point=_this.insertCenterPointForPolygon(uuid,i+1,i+1,centePoint);
			insertMap.set(uuid,point);
		}
	}
}

//描点
SceneEditor.prototype.addPointForPolygon = function (id,name,index,position,color) {
	var ops={
	        id: id,
	        name:name,
	        type:'pointForPolygon',
	        show:true,
	        index:index,
	        position:position,
	        point:new Cesium.PointGraphics ( {
	            show : true,
	            pixelSize : 5,
	            heightReference :Cesium.HeightReference.NONE,
	            color :color || Cesium.Color.YELLOW,
	            outlineColor :Cesium.Color.RED,
	            outlineWidth :1,
	            eyeOffset: new Cesium.Cartesian3(0, 0, -10)
	        } )
	    }
	return this.viewer.entities.add(new Cesium.Entity(ops));
}

SceneEditor.prototype.insertCenterPointForPolygon = function (id,name,index,position) {
	var ops={
	        id: id,
	        name:name,
	        type:'pointForPolygon',
	        show:true,
	        index:index,
	        position:position,
	        point:new Cesium.PointGraphics ( {
	            show : true,
	            pixelSize : 5,
	            heightReference :Cesium.HeightReference.NONE,
	            color :Cesium.Color.CORNFLOWERBLUE,
	            outlineColor :Cesium.Color.RED,
	            outlineWidth :1,
	            eyeOffset: new Cesium.Cartesian3(0, 0, -10)
	        } )
	    }
	return this.viewer.entities.add(new Cesium.Entity(ops));
}


/**
 * 清除辅助点
 */
SceneEditor.prototype.clearPointForPolygon = function (map) {
	var _this=this;
	map.forEach(function(value,key){
		VFG.Util.removeEntityById(_this.viewer,key)
	});
	map.clear();
}
/**
 * 清除插入的辅助点
 */
SceneEditor.prototype.clearCenterPointForPolygon = function (map) {
	var _this=this;
	map.forEach(function(value,key){
		VFG.Util.removeEntityById(_this.viewer,key)
	});
	map.clear();
}

SceneEditor.prototype.createPolygon= function (id,name,positions) {
	var _this=this;
	return _this.viewer.entities.add({
    	id:id,
    	name:name||'',
//        polygon:{
//            hierarchy :hierarchy,
//            perPositionHeight: false,
//            material: Cesium.Color.RED.withAlpha(0.7),
//        }
	      polyline: {
	        positions: new Cesium.CallbackProperty(function () {
	            return positions;
	        }, false),
	        width: 2.0,
	       // clampToGround: true,
	        material:  Cesium.Color.RED,
	    }
    });
}





SceneEditor.prototype.editPolygonLayer=function(node){
	var _this = this;
	layer.load(2);
	_this._Ajax('get', _this.ctx + "/scene/polygon/setting", {
		id: node.id
	}, 'html', function(e) {
		layer.closeAll();
		layer.open({
			id: 'editPolygonLayer',
			type: 1,
			anim: 5,
			title: '编辑【' + node.name + '】',
			area: '350px',
			content: e,
			shade: 0,
			offset: ['0px', '224px'],
			fixed: true,
			move: false,
			skin: 'layui-layer  layui-layer-adminRight layui-layer-city',
			success: function(layero, dIndex) {
				$(layero).children('.layui-layer-content').css('overflow', 'hidden');
				
	      	     treeSelect.render({
	     	        elem: '#parentId',
	     	        data: _this.ctx+'/scene/layer/getTree?sceneId='+_this.sceneId+'&type=Layers',
	     	        type: 'get',
	     	        placeholder: '请选择',
	     	        search: true,
	     	        click: function(d){
	     	        },
	     	        success: function (d) {
	     	        	if(node){
	     	        		treeSelect.checkNode('parentId', node.parentId);
	     	        	}
	     	        }
	     	     });
				
				element.on('tab(polygonStyleTabFilter)', function(data) {
					curStyleTabIndex = data.index;
				});
				var curStyleTabIndex = 0;
				form.on('select(polygonType)', function(data) {
					curStyleTabIndex=0;
					if ("0" == data.value) {
						_this.findPolygonStyle(curStyleTabIndex, node);
					}
				});
				form.on('select(polygonHoverType)', function(data) {
					curStyleTabIndex=1;
					if ("0" == data.value) {
						_this.findPolygonStyle(curStyleTabIndex, node);
					}
				});
				form.on('select(polygonSelectType)', function(data) {
					curStyleTabIndex=2;
					if ("0" == data.value) {
						_this.findPolygonStyle(curStyleTabIndex, node);
					}
				});
				
				form.on('submit(submitBizLayerForm)', function(data) {
					layer.load(2);
					var formData = {};
					for (var key in data.field) {
						formData['bizSceneLayer.' + key] = data.field[key];
					}
					_this.ajaxFunc(_this.ctx + "/scene/layer/save", formData, "json", function(res) {
						layer.closeAll('loading');
						if (200 == res.code) {
							layer.msg(res.message, {
								icon: 1
							});
						} else {
							layer.msg(res.message, {
								icon: 2
							});
						}
					});
					return false;
				});
				form.render();
			},
			end: function() {}
		});
	});
}

SceneEditor.prototype.findPolygonStyle=function(curTabIndex,node){
	var _this=this;
	 layer.load(2);
	var id;
 	if(curTabIndex==0) id=node.defaultStyleId;
	else if(curTabIndex==1) id=node.hoverStyleId;
	else if(curTabIndex==2) id=node.selectedStyleId;
    _this.ajaxFunc(_this.ctx+"/scene/polygon/findStyleById",{id:id}, "json", function(res){
    	layer.closeAll('loading');
        if (res.code == 200) {
        	_this.addOrEditPolygonStyle(curTabIndex,node,res.data);
        }else{
        	_this.addOrEditPolygonStyle(curTabIndex,node,null);
        }
    });	
}

SceneEditor.prototype.addOrEditPolygonStyle=function (curTabIndex,node,data){
	var _this=this;
	var text;
	if(curTabIndex==0) text="默认";
	else if(curTabIndex==1) text="悬浮";
	else if(curTabIndex==2) text="选中";
	layer.open({
		id:'addOrEditPolygonStyle',
        type: 1,
        anim:5,
        title:'【'+node.name+'】-->【'+text+'】样式设置',
        area: '350px',
        content:$('#modelBizStylePolygonScript').html(),
        shade:0,
        offset: ['0px', '224px'],	
        fixed:true,
        move:false,
		skin:'layui-layer  layui-layer-adminRight layui-layer-city',
        success: function (layero, dIndex) {
        	$(layero).children('.layui-layer-content').css('overflow', 'hidden');
           	form.val('modelBizStylePolygonForm', data);  // 回显数据
        	colorpicker.render({//颜色
        		elem: '#color',
        		color: (data&&data.color)?data.color:null,
        		format: 'rgb',
        		predefine: true,
        		alpha: true,
        		done: function(color) {
        			$('#color-input').val(color); //向隐藏域赋值
        			color || this.change(color); //清空时执行 change
        		},
        		change: function(color) {}
        	});
        	
        	colorpicker.render({//轮廓颜色
        		elem: '#outlineColor',
        		color: (data&&data.outlineColor)?data.outlineColor:null,
        		format: 'rgb',
        		predefine: true,
        		alpha: true,
        		done: function(color) {
        			$('#outlineColor-input').val(color); //向隐藏域赋值
        			color || this.change(color); //清空时执行 change
        		},
        		change: function(color) {}
        	});	
        	
        	//选择图标
			$('#mapUrl-select-btn').click(function () {
				_this.selectIcon('polygon',function(data){
		        	$("#mapUrl-img").attr("src",_this.ctx+"/"+data.url);
		        	$("#mapUrl-input").val(data.url);
				});
				return false;
			});
        	
			form.on('select(displayEffect)', function(data){
        		displayEffect(data.value);
        	})
        	
			form.on('select(perPositionHeight)', function(data){
				if("1"==data.value){
					setting3d("none")
				}else{
					setting3d("")
				}
        	})        	
        	
			
        	form.on('select(material)', function(data){
        		if("color"==data.value){
        			showOrHideColorTr("")
        			showOrHideFillTr("");
        			showOrHideMapUrlTr("none");
        		}else{
        			showOrHideMapUrlTr("");
        			showOrHideColorTr("none");
        			showOrHideFillTr("none");
        		}
        	})
        	
        	$('#distanceDisplayConditionXBtn').click(function () {
        		 $("#distanceDisplayConditionX").val(_this.getVisualHeight());
        		 return false;
        	});	
        	$('#distanceDisplayConditionYBtn').click(function () {
	    		 $("#distanceDisplayConditionY").val(_this.getVisualHeight());
	    		 return false;
        	});
        	
        	if(data && data.displayEffect){
        		displayEffect(data.displayEffect);
        	}else{
        		face();
        	}
        	
	    	if(data&& data.mapUrl){
	    		$("#mapUrl-img").attr("src",_this.ctx+"/"+data.mapUrl);
	    	}
	    	
	    	if(data && data.displayEffect=="0"){
	    		if(data.perPositionHeight=="1"){
	    			setting3d("none")
	    		}else{
	    			setting3d("")
	    		}
	    	}
        	
        	function displayEffect(effect){
        		if("0"==effect){
        			face();
        		}else if("1"==effect){
        			build();
        		}
        		else if("2"==effect){
        			water();
        		}
        	}
        	
        	function face(){
        		showOrHideMaterialTr("");
        		setting3d("");
        		showOrHidePerPositionHeightTr("");
        		if(data){
            		if("color"==data.material){
            			showOrHideColorTr("")
            			showOrHideFillTr("");
            			showOrHideOutlineTr("");
            			showOrHideMapUrlTr("none");
            		}else{
            			showOrHideMapUrlTr("");
            			showOrHideColorTr("none");
            			showOrHideFillTr("none");
            			showOrHideOutlineTr("none");
            		}
        		}else{
        			showOrHideColorTr("none");
        			showOrHideFillTr("none");
        		}
        		showOrHideAnimationTr("none");
        	}
        	
        	function build(){
        		showOrHideMaterialTr("");
        		setting3d("");
        		showOrHideAnimationTr("none");
        		showOrHidePerPositionHeightTr("none");
        		$('#closeTopTr').css('display',"none");
        		$('#closeBottomTr').css('display',"none");
        	}
        	
        	function water(){
        		showOrHideAnimationTr("");
    			showOrHideColorTr("")
    			showOrHideMapUrlTr("");
    			setting3d("none");
    			showOrHideFillTr("none");
    			showOrHideOutlineTr("none");
    			showOrHideMaterialTr("none");
    			showOrHidePerPositionHeightTr("none");
        	}
        	
        	function setting3d(state){
        		$('#extrudedHeightReferenceTr').css('display',state);
        		$('#extrudedHeightTr').css('display',state);
        		$('#closeTopTr').css('display',state);
        		$('#closeBottomTr').css('display',state);
        		$('#heightReferenceTr').css('display',state);
        		$('#heightTr').css('display',state);
        		$('#outlineTr').css('display',state);
        	}
        	
        	function showOrHideMaterialTr(state){
				$('#materialTr').css('display',state);
        	}
        	function showOrHideColorTr(state){
				$('#colorTr').css('display',state);
        	}
        	
        	function showOrHideMapUrlTr(state){
				$('#mapUrlTr').css('display',state);
        	}
        	function showOrHideFillTr(state){
				$('#fillTr').css('display',state);
        	}
        	function showOrHideOutlineTr(state){
				$('#outlineTr').css('display',state);
        	}
        	function showOrHideAnimationTr(state){
				$('#animationSpeedTr').css('display',state);
				$('#frequencyTr').css('display',state);
				$('#amplitudeTr').css('display',state);
        	}
        	function showOrHidePerPositionHeightTr(state){
				$('#perPositionHeightTr').css('display',state);
        	}
        	
        	
		    form.on('submit(modelSubmitBizStylePolygontForm)', function (data) {
		        layer.load(2);
		        var formData={}; 
		        for(var key in data.field){
		        	formData['bizStylePolygon.'+key]=data.field[key];
		        }
	        	if(curTabIndex==0)  formData['bizStylePolygon.name']=node.name+"【默认】";
	        	else if(curTabIndex==1)  formData['bizStylePolygon.name']=node.name+"【悬浮】";
	        	else if(curTabIndex==2)  formData['bizStylePolygon.name']=node.name+"【选中】";
		        formData['bizStylePolygon.code']=node.code;
	        	if(curTabIndex==0) formData['bizStylePolygon.id']=node.defaultStyleId;
	        	else if(curTabIndex==1) formData['bizStylePolygon.id']=node.hoverStyleId;
	        	else if(curTabIndex==2) formData['bizStylePolygon.id']=node.selectedStyleId;
			    _this.ajaxFunc(_this.ctx+"/scene/polygon/saveStyle",formData, "json", function(res){
			    	layer.closeAll('loading');
	                if (res.code == 200) {
	                    layer.msg(res.message, {icon: 1});
	                    _this.globe.updateStyle(res.data);
	                } else {
	                    layer.msg(res.message, {icon: 2});
	                }
			    });
		        return false;
		    });
		    form.render();            
		},
		end:function(){
		}
	});
}
