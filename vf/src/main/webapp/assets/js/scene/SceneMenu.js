SceneEditor.prototype.initMenuTree=function(){
	var _this=this;
	var setting = {
		async: {
			enable: true,
			url: _this.ctx+"/scene/menu/getAsyncTreeData?sceneId="+_this.sceneId,
			autoParam:["id", "name=n", "level=lv","type=type"],
			dataFilter: filter,
		},
		view: {
			showLine: false,
			showIcon: false,
			selectedMulti: false,
			dblClickExpand: false,
			addHoverDom: addHoverDom,
			removeHoverDom:removeHoverDom
		},
		check: {
			enable: true,
		},
		edit: {
		},
		callback: {
			onCheck: onCheck,
		}
	};
	
	function filter(treeId, parentNode, childNodes) {
		if (!childNodes) return null;
		return childNodes;
	}
	
	function onCheck(e, treeId, treeNode) {
	}
	
	function addHoverDom(treeId, treeNode) {
		var sObj = $("#" + treeNode.tId + "_span");
		if( '99'==treeNode.type &&  !$("#editMenusBtn_"+treeNode.tId).length>0){
			var addStr= "<span class='button edit' id='editMenusBtn_" + treeNode.tId+ "' title='编辑' onfocus='this.blur();'></span>";
			  addStr += "<span class='button into' id='importMenusBtn_" + treeNode.tId+ "' title='导入' onfocus='this.blur();'></span>";
			  addStr += "<span class='button remove' id='delMenusBtn_" + treeNode.tId+ "' title='删除' onfocus='this.blur();'></span>";
			  sObj.after(addStr);
			var editBtn = $("#editMenusBtn_"+treeNode.tId);
			if (editBtn) editBtn.bind("click", function(){
				return false;
			});
			
			var importBtn = $("#importMenusBtn_"+treeNode.tId);
			if (importBtn) importBtn.bind("click", function(){
				return false;
			});
			
			var delBtn = $("#delMenusBtn_"+treeNode.tId);
			if (delBtn) delBtn.bind("click", function(){
				return false;
			});
		}
	};
	function removeHoverDom(treeId, treeNode) {
		$("#editMenusBtn_"+treeNode.tId).unbind().remove();
		$("#importMenusBtn_"+treeNode.tId).unbind().remove();
		$("#delMenusBtn_"+treeNode.tId).unbind().remove();
	};
	$.fn.zTree.init($("#sceneMenu-"+_this.sceneId), setting);
	
    $('#sceneMenuAdd').click(function () {
    	_this.editSceneMenu();
    })
}

SceneEditor.prototype.editSceneMenu=function(id){
	var _this=this;
	 layer.load(2);
    _this.ajaxFunc(_this.ctx+"/scene/menu/findById",{id:id}, "json", function(res){
    	layer.closeAll('loading');
    	layer.open({
    		id:'editSceneMenu',
            type: 1,
            anim:5,
            title:id?"编辑":"新增",
            area: '300px',
            content:$('#modelBizSceneMenuScript').html(),
            shade:0,
            offset: ['0px', '224px'],	
            fixed:true,
            move:false,
    		skin:'layui-layer  layui-layer-adminRight layui-layer-city',
            success: function (layero, dIndex) {
            	$(layero).children('.layui-layer-content').css('overflow', 'hidden');
            	$(layero).children('.layui-layer-content').css('height', 'calc(100vh - 50px)');
               	form.val('modelBizSceneMenuForm',res.data);  // 回显数据
    		    form.on('submit(modelSubmitBizSceneMenutForm)', function (data) {
    		        layer.load(2);
    		        var formData={}; 
    		        for(var key in data.field){
    		        	formData['bizSceneMenu.'+key]=data.field[key];
    		        }
    		        if(formData['bizSceneMenu.parentId']==''){
    		        	formData['bizSceneMenu.parentId']=_this.sceneId;
    		        }
    			    _this.ajaxFunc(_this.ctx+"/scene/menu/save",formData, "json", function(res){
    			    	layer.closeAll('loading');
    	                if (res.code == 200) {
    	                    layer.msg(res.message, {icon: 1});
    	                } else {
    	                    layer.msg(res.message, {icon: 2});
    	                }
    			    });
    		        return false;
    		    });
    		    form.render();            
    		},
    		end:function(){
    		}
    	});
        
        
    });
}


SceneEditor.prototype.reAsyncMenuByPId=function(id){
	var _this=this;
	var treeObj =  $.fn.zTree.getZTreeObj("sceneMenu-"+_this.sceneId);
	if (treeObj) {
		var  node= treeObj.getNodeByParam("id",id, null);
		treeObj.reAsyncChildNodes(node||null, "refresh");
	}
}

SceneEditor.prototype.addOrUpdateMenuNode=function(data){
	var _this=this;
	var treeObj =  $.fn.zTree.getZTreeObj("sceneMenu-"+_this.sceneId);
	if (treeObj) {
		var node=treeObj.getNodeByParam("id",data.id, null);
		if(node){
			if(node.name){
				node.name=data.name
				node.code=data.code
				node.sort=data.sort
			}
			treeObj.updateNode(node);
		}else{
			var parentNode=treeObj.getNodeByParam("id",data.id, null);
			treeObj.addNodes(parentNode, {
				id:data.id, 
				pId:data.layerId, 
				layerId:data.layerId, 
				name:data.name||"",
				type:data.type||null,
				code:data.code||null,
				defaultStyleId:data.defaultStyleId||null,
				hoverStyleId:data.hoverStyleId||null,
				selectedStyleId:data.selectedStyleId||null,
			});
		}
	}
}

SceneEditor.prototype.getMenuNodeById=function(id){
	var _this=this;
	var treeObj =  $.fn.zTree.getZTreeObj("sceneMenu-"+_this.sceneId);
	if (treeObj) {
		return treeObj.getNodeByParam("id",id, null);
	}
}

SceneEditor.prototype.addMenuNodeById=function(id){
	var _this=this;
	var treeObj =  $.fn.zTree.getZTreeObj("sceneMenu-"+_this.sceneId);
	if (treeObj) {
		return treeObj.getNodeByParam("id",id, null);
	}
}




