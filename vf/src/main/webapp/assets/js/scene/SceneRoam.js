SceneEditor.prototype.editRoam = function (node) {
	var _this=this;
	layer.load(2);
	_this._Ajax('get',_this.ctx+"/scene/roam/setting",{roamId:node.id},'html',function(e){
		layer.closeAll('loading');   
		layer.open({
	    	id:'editRoam',
	        title: node ? '编辑【'+node.name+'】':'新增',
	        type: 1,
	        area: '300px',
	        content:e,
	        shade:0,
	        offset: ['0px', '224px'],	
	        fixed:true,
	        move:false,
			skin:'layui-layer  layui-layer-adminRight layui-layer-city',
		    success: function (layero, dIndex) {
	            $(layero).children('.layui-layer-content').css('overflow', 'hidden');
	        	
	            
	        	form.on('select(isPath)', function(data){
	        		if("1"==data.value){
	        		}else{
	        		}
	        	})
	            
	            _this.roamPoints(node,function(e){
	            	console.log(e);
	            	_this.globe.add({
	                	id:node.id,
	                	name:node.name,
	                	code:node.code,
	                	type:"Polyline",
	                	points:e,
                	});
	            	
	            });
	            
	            form.on('submit(submitRoamForm)', function (data) {
	        	    layer.load(2);
	                var formData={}; 
	                for(var key in data.field){
	                	formData['bizSceneRoam.'+key]=data.field[key];
	                }
	                _this.ajaxFunc(_this.ctx+"/scene/roam/save",formData, "json", function(res){
		                layer.closeAll('loading');
		                if (200==res.code) {
		        		    layer.msg(res.message);
		        		    _this.reAsyncLayerByPId(res.data.layerId);		                   
		                } else {
		                    layer.msg(res.message, {icon: 2});
		                }
	                });
	        	    return false;
	        	});
	        	form.render(); 
			},
			end:function(){
				_this.layerForEdit();
			}
    	});  
	});
};

SceneEditor.prototype.addRoam = function (treeNode) {
	console.log(treeNode);
	var _this=this;
	if(this.isDraw){
		return;
	}
	_this.isDraw=true;
	this.drawRoam=new Cesium.DrawRoam(_this.viewer,{
		END:function(c3s,lnlas){
			if(c3s&& c3s.length>=2){
				_this.saveRoamPoints({
					layerId:treeNode.id,
					points:lnlas
				});
			}
			_this.isDraw=false;
			_this.drawRoam=null;
		}
	});
};
SceneEditor.prototype.saveRoamPoints=function(option){
	var _this=this;
	var formData={}; 
	formData['layerId']=option.layerId;
	formData['points']=JSON.stringify(option.points);
	_this.ajaxFunc(_this.ctx+"/scene/roam/savePoints",formData, "json", function(res){
	    if (undefined!=res && 200==res.code) {
	    	layer.msg(res.message);
	    }else{
	    	layer.msg(res?res.message:'保持失败！！');
	    }
	});
}

SceneEditor.prototype.doingRoam = function (treeNode) {
	var _this=this;
	layer.load(2);
	layer.closeAll();
	_this._Ajax('get',_this.ctx+"/biz/roam/doingRoam",{sceneId:_this.sceneId,roamId:treeNode.id},'html',function(e){
		layer.closeAll('loading');  
		var paths;
		var roamObj;
		var layerIndex= layer.open({
		    	id:'point',
		        title: name || '【'+treeNode.name+'】路线',
		        type: 1,
		        area: ['300px','500px'],
		        content:e,
		        shade:0,
		        offset: 'l',
		        fixed:true,
		        move:false,
			    skin:'layui-layer',
			    content: e,
			    success: function (layero, dIndex) {
		            $(layero).children('.layui-layer-content').css('overflow', 'hidden');
		            var insTb = table.render({
		                elem: '#tableBizRoamPoint',
		                url: _this.ctx+'/biz/roam/listDataForPoint', 
		                page: false,
		                toolbar: false,
		                cellMinWidth: 100,
		                where:{
		                	roamId:treeNode.id
		                },
		                cols: [[
		                    {type: 'numbers', title: '序号'},
		                    {field: 'name', sort: false, title: '名称'},
		                ]],
		                done : function(res, curr, count){
		                	paths=res.data;
	                	}
		            });
		            $('#bizRoamStart').click(function () {
		            	if(roamObj){
		            		roamObj.PauseOrContinue(true);
		            		return;
		            	}
		            	layer.load(2);
		                _this.ajaxFunc(_this.ctx+"/biz/roam/getById",{roamId:paths[0].roamId}, "json", function(res){
			                layer.closeAll('loading');
			                if (res && 200==res.code) {
				            	roamObj=new VFG.Roam(_this.viewer,res.data);
			                } else {
			                    layer.msg(res.message, {icon: 2});
			                }
		                });
		            });
		            $('#bizRoamsuspend').click(function () {
		            	if(roamObj)roamObj.PauseOrContinue(false);
		            });
		            $('#bizRoamStop').click(function () {
		            	if(roamObj)roamObj.destroy();
		            	layer.close(dIndex);
		            });
				},
				end:function(){
					if(roamObj)roamObj.destroy();
					_this.layerForEdit();
				}
	    	});  
	});
};	
	

SceneEditor.prototype.roamPoints = function (node,callback){
    var insTb = table.render({
        elem: '#tableRoamPoint',
        url: this.ctx+'/scene/roam/listDataForPoint', 
        page: true,
        toolbar: false,
        skin:'nob',
        page: {
            layout: ['prev', 'page', 'next','count'] 
            ,groups:5 
            ,first: false
            ,last: false
        }, 
        where:{
        	roamId:node.id
        },
        cols: [[
            {field: 'sort', sort: false, title: '名称'},
        ]],
        done: function(res, curr, count){
            if(callback){
            	callback(res.data)
            }
          }
    });
}	

