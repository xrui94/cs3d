VFGEditor.prototype.drawPolygon1=function(){
	var _this=this;
	if(this.isDraw){
		console.log('已经处于绘制状态！！！');
		return;
	}
	var primitive;
	var positions;
	_this.isDraw=true;
	var uniqueId=VFG.Util.getUuid();
	var drawPolygon=new Cesium.DrawPolygon(_this.viewer,{
		leftClick:function(e){
		},
		mouseMove:function(e){
			positions=e;
			if (!Cesium.defined(primitive)) {
				primitive = _this.viewer.entities.add({
		            id:uniqueId,
		            polygon:{
						hierarchy:new Cesium.CallbackProperty(function() {
							return new Cesium.PolygonHierarchy(positions);
						}, false),
		                material: Cesium.Color.RED.withAlpha(0.7),
		                outline: true,
		                outlineColor: Cesium.Color.YELLOW.withAlpha(1)
		            }
		        })
		        drawPolygon.primitive=primitive;
            }
		},
		end:function(){
			_this.isDraw=false;
			drawPolygon=null;
			if(primitive && positions.length>=3){
				
				layer.load(2);
				var lnlas=[];
				for(var i=0;i<positions.length;i++){
					lnlas.push(VFG.Util.getLnLaFormC3(_this.viewer,positions[i]));
				}
				var polygon=VFG.Util.getTurfPolygon(lnlas);
				var inPoints=[];
				VFG.Point.Primitives.forEach(function(value,key){
					if(VFG.Util.booleanPointInPolygon(polygon,VFG.Util.getTurfPoint(value)) ){
						inPoints.push(value);
					}
				});
				
				var modelPoints=[];
				var terrainPoints=[];
				for(var i=0;i<inPoints.length;i++){
					var cartesian2=Cesium.SceneTransforms.wgs84ToWindowCoordinates(_this.viewer.scene,VFG.Util.getC3FormLnLa(inPoints[i]));
					if (viewer.scene.mode !== Cesium.SceneMode.MORPHING && cartesian2) {
						var pickedObject = viewer.scene.pick(cartesian2);
						if (viewer.scene.pickPositionSupported && Cesium.defined(pickedObject) && pickedObject.id!=inPoints[i].id) {
							modelPoints.push(inPoints[i]);
						}else{
							terrainPoints.push(inPoints[i]);
						}
					}else{
						terrainPoints.push(inPoints[i]);
					}
				}
				
				var ioOk=false;
				
				if(terrainPoints.length==0){
					ioOk=true;
					console.log('no-terrainPoints');
				}
				if(modelPoints.length==0){
					ioOk=true;
					console.log('no-modelPoints');
				}
				
				if(modelPoints.length>=0){
					var modelPositions=[];
					for(var i=0;i<modelPoints.length;i++){
						modelPositions.push(VFG.Util.getC3FormLnLa(modelPoints[i]));
					}
					
					_this.viewer.scene.clampToHeightMostDetailed(modelPositions).then(function(clampedCartesians) {
						var jsonArr=[];
						for(var i=0;i<clampedCartesians.length;i++){
				            var clampedPt = clampedCartesians[i];
				            if (Cesium.defined(clampedPt)) {
				                var heightTiles = Cesium.Cartographic.fromCartesian(clampedPt).height;
				                if (Cesium.defined(heightTiles) && heightTiles > -1000) {
				                	jsonArr.push({
				                		id:modelPoints[i].id,
				                		z:heightTiles+2
				                	});
				                }
				            }
						}
						console.log('-----------模型-------------'+jsonArr.length)
						if(jsonArr.length>0){
							console.log('-----------模型-------------'+jsonArr.length)
						    _this.ajaxFunc(_this.ctx+"/biz/marker/point/updateHeights",{'points':JSON.stringify(jsonArr)}, "json", function(res){
						    	if(ioOk){
						    		layer.closeAll('loading');
						    	}
						    	ioOk=true;
						    	if (undefined!=res && 200==res.code) {
						    		console.log('model-over');
						        }else{
						        }
						    });
						}else{
					    	if(ioOk){
					    		layer.closeAll('loading');
					    	}
					    	ioOk=true;
						}
					});
				}else{
			    	if(ioOk){
			    		layer.closeAll('loading');
			    	}
			    	ioOk=true;
				}

				
				if(terrainPoints.length>0){
					var terrainPositions=[];
					for(var i=0;i<terrainPoints.length;i++){
						terrainPositions.push(Cesium.Cartographic.fromCartesian(VFG.Util.getC3FormLnLa(terrainPoints[i])));
					}
				    Cesium.sampleTerrainMostDetailed(_this.viewer.scene.terrainProvider,terrainPositions).then(function(samples) {
						var jsonArr=[];
						for(var i=0;i<samples.length;i++){
						    var heightTerrain;
				            var clampedCart = samples[i];
				            
				            if (Cesium.defined(clampedCart) && Cesium.defined(clampedCart.height)) {
				                heightTerrain = clampedCart.height;
			                	jsonArr.push({
			                		id:terrainPoints[i].id,
			                		z:heightTerrain
			                	});
				            } else {
				                heightTerrain = _this.viewer.scene.globe.getHeight(terrainPositions[i]);
			                	jsonArr.push({
			                		id:terrainPoints[i].id,
			                		z:heightTerrain
			                	});
				            }
						}
						if(jsonArr.length>0){
							console.log('-----------地形-------------'+jsonArr.length)
						    _this.ajaxFunc(_this.ctx+"/biz/marker/point/updateHeights",{'points':JSON.stringify(jsonArr)}, "json", function(res){
						    	if(ioOk){
						    		layer.closeAll('loading');
						    	}
						    	ioOk=true;
						    	layer.closeAll('loading');
						    	if (undefined!=res && 200==res.code) {
						    		console.log('Terrain-over');
						        }else{
						        }
						    });
						}else{
					    	if(ioOk){
					    		layer.closeAll('loading');
					    	}
					    	ioOk=true;
						}
				    })
				}else{
			    	if(ioOk){
			    		layer.closeAll('loading');
			    	}
			    	ioOk=true;
				}

			}
		}
	});
}

/**
 * 获取高程
 */
VFGEditor.prototype.getClampToHeight=function(){
	var _this=this;
	layer.load(2);
	var points=[];
	var keys=[];
	VFG.Point.Primitives.forEach(function(value,key){
		points.push(Cesium.Cartesian3.fromDegrees(value.x*1,value.y*1,value.z*1));
		keys.push(key);
	});
	_this.viewer.scene.clampToHeightMostDetailed(points).then(function(clampedCartesians) {
		var jsonArr=[];
		for(var i=0;i<clampedCartesians.length;i++){
            var clampedPt = clampedCartesians[i];
            if (Cesium.defined(clampedPt)) {
                var heightTiles = Cesium.Cartographic.fromCartesian(clampedPt).height;
                if (Cesium.defined(heightTiles) && heightTiles > -1000) {
                	jsonArr.push({
                		id:keys[i],
                		z:heightTiles+2
                	});
                }
            }
		}
		if(jsonArr.length>0){
			console.log('-----------模型-------------'+jsonArr.length)
		    _this.ajaxFunc(_this.ctx+"/biz/marker/point/updateHeights",{'points':JSON.stringify(jsonArr)}, "json", function(res){
		    	layer.closeAll('loading');
		    	if (undefined!=res && 200==res.code) {
		    		console.log('model-over');
		        }else{
		        }
		    });
		}else{
			layer.closeAll('loading');
		}
	});
}
VFGEditor.prototype.getSampleTerrain=function(){
	var _this=this;
	layer.load(2);
	var points=[];
	var keys=[];
	VFG.Point.Primitives.forEach(function(value,key){
		points.push(Cesium.Cartographic.fromCartesian(Cesium.Cartesian3.fromDegrees(value.x*1,value.y*1,value.z*1)));
		keys.push(key);
	});
    Cesium.sampleTerrainMostDetailed(_this.viewer.scene.terrainProvider,points).then(function(samples) {
		var jsonArr=[];
		for(var i=0;i<samples.length;i++){
		    var heightTerrain;
            var clampedCart = samples[i];
            if (Cesium.defined(clampedCart) && Cesium.defined(clampedCart.height)) {
                heightTerrain = clampedCart.height;
            	jsonArr.push({
            		id:keys[i],
            		z:heightTerrain
            	});
            }
		}
		if(jsonArr.length>0){
		    _this.ajaxFunc(_this.ctx+"/biz/marker/point/updateHeights",{'points':JSON.stringify(jsonArr)}, "json", function(res){
		    	layer.closeAll('loading');
		    	if (undefined!=res && 200==res.code) {
		    		console.log('Terrain-over');
		        }
		    });
		}else{
			layer.closeAll('loading');
		}
    })
}
