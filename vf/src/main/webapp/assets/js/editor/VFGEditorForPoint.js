VFGEditor.prototype.showOrHidePoint=function(layerId,state,type){
	var _this=this;
	if('Points'==type){
		layer.load(2);
	    _this.ajaxFunc(_this.url+"/api/point/list",{
	    	sceneId:_this.sceneId,
	    	layerId:layerId
	    }, "json", function(res){
	    	layer.closeAll('loading');
			 if (undefined!=res && 200==res.code) {
				 var list=res.data;
				 if(true==state){
					 _this.globe.addPointPrimitives(list); 
//					 _this.globe.addCluster({
//						 layerId:layerId,
//					     points:list		 
//					 });
				 }else{
					 _this.globe.removePointPrimitives(list); 
//					 _this.globe.removeCluster({
//						 layerId:layerId,
//					     points:list		 
//					 });
				 }
			 }
			 else{
			 }
	    });
	}
	else if ('Point'==type){
		layer.load(2);
	    _this.ajaxFunc(_this.url+"/api/point/getByLayerId",{
	    	sceneId:_this.sceneId,
	    	layerId:layerId
	    }, "json", function(res){
	    	layer.closeAll('loading');
			 if (undefined!=res && 200==res.code) {
				 var point=res.data;
				 if(true==state){
					 _this.globe.addPointPrimitive(point); 
					 _this.globe.flyToPointById(point.id,function(e){
						 
					 });
				 }else{
					 _this.globe.setViewPointById(point.id,function(e){
						 _this.globe.removePointPrimitive(point); 
					 });
				 }
			 }
			 else{
			 }
	    });
	}else{
		layer.alert('未识别图层标识【"'+type+'"】！！', {icon: 1,skin: 'layer-ext-moon' })
	} 
} 

/**
 * 打点
 */
VFGEditor.prototype.markPoint = function (treeNode) {
	var _this=this;
	if(this.isDraw){
		console.log('已经处于绘制状态！！！');
		return;
	}
	layer.closeAll();
	_this.isDraw=true;
	this.drawPoint=new Cesium.DrawPoint(_this.viewer,{
		pick:function(e){
			var position=VFG.Util.getC3ToLnLa(_this.viewer,e);
			var option={
					id:VFG.Util.getUuid(),
					sceneId:_this.sceneId,
					layerId:treeNode.id,
					name:'',
					x:position.x,
					y:position.y,
					z:position.z,
					defaultStyleId:treeNode.defaultStyleId,
					hoverStyleId:treeNode.hoverStyleId,
					selectedStyleId:treeNode.selectedStyleId,
				};
			_this.globe.addPointPrimitive(option);
			_this.savePoint(option)
		},
		end:function(){
			_this.isDraw=false;
			_this.drawPoint=null;
			_this.layerForEdit();
		}
	});
};

/**
 * 保存点信息
 */
VFGEditor.prototype.savePoint=function(option){
	var _this=this;
    var formData={}; 
    formData['bizPoint.id']=option.id;
    formData['bizPoint.name']=option.name;
    formData['bizPoint.x']=option.x;
    formData['bizPoint.y']=option.y;
    formData['bizPoint.z']=option.z;
    formData['bizPoint.layerId']=option.layerId;
    formData['bizPoint.sceneId']=option.sceneId;
    formData['bizPoint.defaultStyleId']=option.defaultStyleId;
    formData['bizPoint.hoverStyleId']=option.hoverStyleId;
    formData['bizPoint.selectedStyleId']=option.selectedStyleId;
    _this.ajaxFunc(_this.ctx+"/biz/marker/point/save",formData, "json", function(res){
        if (undefined!=res && 200==res.code) {
        	layer.msg(res.message);
        	_this.globe.flyToPointById(option.id,function(e){
			 });
        }else{
        	layer.msg(res?res.message:'保持失败！！');
        }
    });
}

/**
 * 编辑点信息
 */
VFGEditor.prototype.editPoint = function (treeNode) {
	var _this=this;
	layer.load(2);
	_this._Ajax('get',_this.ctx+"/biz/marker/point/pointForEdit",{sceneId:_this.sceneId,pointId:treeNode.id},'html',function(e){
		layer.closeAll('loading');   
		var layerIndex= layer.open({
		    	id:'point',
		        title: name || '编辑【'+treeNode.name+'】点',
		        type: 1,
		        area: '300px',
		        content:e,
		        shade:0,
		        offset: 't',
		        fixed:true,
		        move:false,
			    skin:'layui-layer',
			    content: e,
			    success: function (layero, dIndex) {
		            $(layero).children('.layui-layer-content').css('overflow', 'hidden');
		        	form.on('submit(submitBizPointForm)', function (data) {
		        	    layer.load(2);
		                var formData={}; 
		                for(var key in data.field){
		                	formData['bizPoint.'+key]=data.field[key];
		                }
		                _this.ajaxFunc(_this.ctx+"/biz/marker/point/save",formData, "json", function(res){
			                layer.closeAll('loading');
			                if (200==res.code) {
			                	layer.close(dIndex);
			                    layer.msg(res.message, {icon: 1});
			                    var pointRender=res.data;
			                    if(pointRender){
			                    	 _this.pointForCreate(pointRender);
			                    	var data=pointRender.point;
			                    	if(data){
					                    //更新树
					                	var zTree = $.fn.zTree.getZTreeObj("bizLayerTree")||sceneTree;
					                	if(zTree){
					                		var editNode =zTree.getNodeByParam("id",data.id);
					                		if(editNode){
					                			editNode.name=data.name;
					                			zTree.updateNode(editNode);
					                		}else{
					                			var treeNode =zTree.getNodeByParam("id",data.layerId);
					                			zTree.addNodes(treeNode?treeNode:null, {id:data.id, pId:data.layerId, name:data.name,type:'point'});
					                		}
					                	}		                    		
			                    	}
			                    }
			                } else {
			                    layer.msg(res.message, {icon: 2});
			                }
		                });
		        	    return false;
		        	});
				    $('#settingBizPointVisualAngle').click(function () {
				        var visualPos=VFG.Util.getVisualAngle(_this.viewer);
				        if(visualPos){
				            var formData={}; 
				            formData['bizPoint.id']=treeNode.id;
				            formData['bizPoint.heading']=visualPos.heading;
				            formData['bizPoint.pitch']=visualPos.pitch;
				            formData['bizPoint.roll']=visualPos.roll;
				            formData['bizPoint.cameraX']=visualPos.position.x;
				            formData['bizPoint.cameraY']=visualPos.position.y;
				            formData['bizPoint.cameraZ']=visualPos.position.z;
				            _this.ajaxFunc(_this.ctx+"/biz/marker/point/save",formData, "json", function(res){
					            if (undefined!=res && 200==res.code) {
					            	 layer.msg(res.message);
					            }
				            });
				        }
				    });
		        	form.render(); 
				},
				end:function(){
				}
	    	});  
	});
};

/**
 * 点拖拽
 */
VFGEditor.prototype.dragPoint = function (pointId) {
	var _this=this;
    _this.isDrag=true;
    var position;
    _this.globe.togglePointPrimitiveStyle(pointId)
	var cesiumPick =new Cesium.Pick(_this.viewer,{
		LEFT_CLICK:function(movement){
			position=VFG.Util.getScreenToLnLa (_this.viewer, movement.position);
			if(position){
				_this.globe.setPointPrimitivePosition(pointId,position);
			}
		},
		RIGHT_CLICK:function(e){
			_this.isDrag=false;
			if(position){
			    var formData={}; 
			    formData['bizPoint.id']=pointId;
			    formData['bizPoint.x']=position.x;
			    formData['bizPoint.y']=position.y;
			    formData['bizPoint.z']=position.z;
			    _this.ajaxFunc(_this.ctx+"/biz/marker/point/save",formData, "json", function(res){
			        if (undefined!=res && 200==res.code) {
			        	layer.msg('坐标更新成功！');
			        }else{
			        	layer.msg('坐标更新失败！');
			        }
			    });
			}
			_this.globe.unselectedPointPrimitive(pointId);
		}
	});
}

/**
 * 操作点
 */
VFGEditor.prototype.operationPoint = function (screenPos,pointId,type) {
	var _this=this;
	 var point=_this.globe.getPrimitiveById(pointId,type);
	if(point){
		layer.load(2);
		var VFGWin;
		_this._Ajax('get',_this.ctx+"/biz/marker/point/operation",{sceneId:_this.sceneId,pointId:pointId},'html',function(e){
			layer.closeAll('loading');   
			var layerIndex= layer.open({
					id:pointId,
				    type: 1,
				    title: false,
				    shade: 0,
				    content:e,
				    resize :false,
				    success: function (layero, dIndex) {
			            $(layero).children('.layui-layer-content').css('overflow', 'hidden');
			        	VFGWin=new Cesium.VFGWin(_this.viewer,{
			            	position:Cesium.Cartesian3.fromDegrees(point.x*1,point.y*1,point.z*1),
			            	layero:layero,
			            	index:dIndex,
			            });
			        	$('#toolForBizOperationDrag').click(function () {
			        		layer.close(dIndex);
			        		_this.dragPoint(pointId);
			        	});	
			        	$('#toolForBizOperationEdit').click(function () {
			        		layer.close(dIndex);
			        		_this.editPoint({id:pointId,name:point.name});
			        	});	
			        	$('#toolForBizOperationStyle').click(function () {
			        	});	
			        	$('#toolForBizOperationDel').click(function () {
			        		layer.close(dIndex);
			        		_this.deleteLayer({
			        			id:pointId,
			        			type:type,
			        			name:point.name,
			        		})
			        	});	
					},
					end:function(){
						if(VFGWin){
							 VFGWin.destroy();
							 VFGWin=null;
						}
					}
		    	});  
		});
	}else{
		layer.alert('信息未知！！', {icon: 1,skin: 'layer-ext-moon' })
	}
}











