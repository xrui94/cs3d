/************************************事件管理*********************************************/
VFGEditor.prototype.clickEvent = function (objId,event) {
	var _this=this;
	_this._Ajax('get',ctx+"/event/mouse",{sceneId:this.sceneId,objId:objId,event:event},'html',function(e){
	    layer.open({
	        title:'鼠标事件',
	        type: 1,
	        area: '660px',
	        content:e,
	        shade:0,
	        offset: 't',
	        fixed:true,
	        move:false,
		    skin:'layui-layer',
		    content: e,
		    success: function (layero, dIndex) {
	            $(layero).children('.layui-layer-content').css('overflow', 'hidden');
	        	form.on('submit(submitFormForBizText)', function (data) {
	        	    layer.load(2);
	                var formData={}; 
	                for(var key in data.field){
	                	formData['bizText.'+key]=data.field[key];
	                }
	                var res = common_ajax.ajaxFunc(_this.ctx+"/biz/text/save",formData, "json", null);
	                layer.closeAll('loading');
	                if (200==res.code) {
	                	layer.close(dIndex);
	                    layer.msg(res.message, {icon: 1});
	                } else {
	                    layer.msg(res.message, {icon: 2});
	                }
	        	    return false;
	        	});
	        	
	        	//
	        	_this.selectForPointStlye(objId);
	        	_this.propertyForEventTb(objId,event);
	        	_this.pageForEventTb(objId,event)
	        	_this.panoramaForEventTb(objId,event);
	        	_this.pictureForEventTb(objId,event);
	        	_this.videoForEventTb(objId,event);
			}
    	});  
	});
};


//编辑
VFGEditor.prototype.eventForEdit = function (sceneId,objId,eventId,callBack) {
	var _this=this;
	_this._Ajax('get',ctx+"/biz/event/eventForEdit",{sceneId:this.sceneId,objId:objId,eventId:eventId},'html',function(e){
	    layer.open({
	        title:'鼠标事件',
	        type: 1,
	        area: '450px',
	        content:e,
	        shade:0,
	        offset: 't',
	        fixed:true,
	        move:false,
		    skin:'layui-layer',
		    content: e,
		    success: function (layero, dIndex) {
	            $(layero).children('.layui-layer-content').css('overflow', 'hidden');
	            form.render();
	            if(callBack){
	            	
	            }
			}
    	});  
	});
};

/**
 * 模拟鼠标左键 '
 */
VFGEditor.prototype.analogForMouseLeftClick = function (objId,event,position,callBack) {
	var _this=this;
	_this.tipsForLayer(objId,event,position);
};

