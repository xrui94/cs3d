VFGEditor.prototype.addRoam = function (treeNode) {
	var _this=this;
	layer.load(2);
	layer.closeAll();
	_this._Ajax('get',_this.ctx+"/biz/roam/edit",{sceneId:_this.sceneId,roamId:''},'html',function(e){
		layer.closeAll('loading');   
		var layerIndex= layer.open({
		    	id:'addRoam',
		        title:'新增漫游',
		        type: 1,
		        area: '400px',
		        content:e,
		        shade:0,
		        offset: 't',
		        fixed:true,
		        move:false,
			    skin:'layui-layer',
			    content: e,
			    success: function (layero, dIndex) {
		            $(layero).children('.layui-layer-content').css('overflow', 'hidden');
		        	form.on('submit(submitbizRoamForm)', function (data) {
		        	    layer.load(2);
		                var formData={}; 
		                for(var key in data.field){
		                	formData['bizRoam.'+key]=data.field[key];
		                }
		                formData['bizRoam.layerId']=treeNode.id;
		                formData['bizRoam.sceneId']=_this.sceneId;
		                _this.ajaxFunc(_this.ctx+"/biz/roam/save",formData, "json", function(res){
			                layer.closeAll('loading');
			                if (200==res.code) {
			                	layer.close(dIndex);
			                    layer.msg(res.message, {icon: 1});
			                } else {
			                    layer.msg(res.message, {icon: 2});
			                }
		                });
		        	    return false;
		        	});
		        	form.render(); 
				},
				end:function(){
					_this.layerForEdit();
				}
	    	});  
	});
};

VFGEditor.prototype.editRoam = function (treeNode) {
	var _this=this;
	layer.load(2);
	layer.closeAll();
	_this._Ajax('get',_this.ctx+"/biz/roam/edit",{sceneId:_this.sceneId,roamId:treeNode.id},'html',function(e){
		layer.closeAll('loading');   
		var layerIndex= layer.open({
		    	id:'point',
		        title: name || '编辑【'+treeNode.name+'】',
		        type: 1,
		        area: '400px',
		        content:e,
		        shade:0,
		        offset: 't',
		        fixed:true,
		        move:false,
			    skin:'layui-layer',
			    content: e,
			    success: function (layero, dIndex) {
		            $(layero).children('.layui-layer-content').css('overflow', 'hidden');
		        	form.on('submit(submitbizRoamForm)', function (data) {
		        	    layer.load(2);
		                var formData={}; 
		                for(var key in data.field){
		                	formData['bizRoam.'+key]=data.field[key];
		                }
		                _this.ajaxFunc(_this.ctx+"/biz/roam/save",formData, "json", function(res){
			                layer.closeAll('loading');
			                if (200==res.code) {
			                	layer.close(dIndex);
			                    layer.msg(res.message, {icon: 1});
			                } else {
			                    layer.msg(res.message, {icon: 2});
			                }
		                });
		        	    return false;
		        	});
				    $('#settingbizRoamVisualAngle').click(function () {
				        var visualPos=VFG.Util.getVisualAngle(_this.viewer);
				        if(visualPos){
				            var formData={}; 
				            formData['bizRoam.id']=treeNode.id;
				            formData['bizRoam.heading']=visualPos.heading;
				            formData['bizRoam.pitch']=visualPos.pitch;
				            formData['bizRoam.roll']=visualPos.roll;
				            formData['bizRoam.cameraX']=visualPos.position.x;
				            formData['bizRoam.cameraY']=visualPos.position.y;
				            formData['bizRoam.cameraZ']=visualPos.position.z;
				            _this.ajaxFunc(_this.ctx+"/biz/roam/save",formData, "json", function(res){
					            if (undefined!=res && 200==res.code) {
					            	 layer.msg(res.message);
					            }
				            });
				        }
				    });
		        	form.render(); 
				},
				end:function(){
					_this.layerForEdit();
				}
	    	});  
	});
};

VFGEditor.prototype.addRoamRoad = function (treeNode) {
	console.log(treeNode);
	var _this=this;
	if(this.isDraw){
		console.log('已经处于绘制状态！！！');
		return;
	}
	layer.closeAll();
	_this.isDraw=true;
	this.drawRoam=new Cesium.DrawRoam(_this.viewer,{
		END:function(c3s,lnlas){
			if(c3s&& c3s.length>=2){
				console.log(c3s);
				_this.saveRoamPoints({
					roamId:treeNode.id,
					points:lnlas
				});
			}
			_this.isDraw=false;
			_this.drawRoam=null;
			_this.layerForEdit();
		}
	});
	};
	VFGEditor.prototype.saveRoamPoints=function(option){
	var _this=this;
	var formData={}; 
	formData['roamId']=option.roamId;
	formData['points']=JSON.stringify(option.points);
	_this.ajaxFunc(_this.ctx+"/biz/roam/savePoints",formData, "json", function(res){
	    if (undefined!=res && 200==res.code) {
	    	layer.msg(res.message);
	    }else{
	    	layer.msg(res?res.message:'保持失败！！');
	    }
	});
}

VFGEditor.prototype.doingRoam = function (treeNode) {
	var _this=this;
	layer.load(2);
	layer.closeAll();
	_this._Ajax('get',_this.ctx+"/biz/roam/doingRoam",{sceneId:_this.sceneId,roamId:treeNode.id},'html',function(e){
		layer.closeAll('loading');  
		var paths;
		var roamObj;
		var layerIndex= layer.open({
		    	id:'point',
		        title: name || '【'+treeNode.name+'】路线',
		        type: 1,
		        area: ['300px','500px'],
		        content:e,
		        shade:0,
		        offset: 'l',
		        fixed:true,
		        move:false,
			    skin:'layui-layer',
			    content: e,
			    success: function (layero, dIndex) {
		            $(layero).children('.layui-layer-content').css('overflow', 'hidden');
		            var insTb = table.render({
		                elem: '#tableBizRoamPoint',
		                url: _this.ctx+'/biz/roam/listDataForPoint', 
		                page: false,
		                toolbar: false,
		                cellMinWidth: 100,
		                where:{
		                	roamId:treeNode.id
		                },
		                cols: [[
		                    {type: 'numbers', title: '序号'},
		                    {field: 'name', sort: false, title: '名称'},
		                ]],
		                done : function(res, curr, count){
		                	paths=res.data;
	                	}
		            });
		            $('#bizRoamStart').click(function () {
		            	if(roamObj){
		            		roamObj.PauseOrContinue(true);
		            		return;
		            	}
		            	layer.load(2);
		                _this.ajaxFunc(_this.ctx+"/biz/roam/getById",{roamId:paths[0].roamId}, "json", function(res){
			                layer.closeAll('loading');
			                if (res && 200==res.code) {
				            	roamObj=new VFG.Roam(_this.viewer,res.data);
			                } else {
			                    layer.msg(res.message, {icon: 2});
			                }
		                });
		            });
		            $('#bizRoamsuspend').click(function () {
		            	if(roamObj)roamObj.PauseOrContinue(false);
		            });
		            $('#bizRoamStop').click(function () {
		            	if(roamObj)roamObj.destroy();
		            	layer.close(dIndex);
		            });
				},
				end:function(){
					if(roamObj)roamObj.destroy();
					_this.layerForEdit();
				}
	    	});  
	});
};	
	


	

