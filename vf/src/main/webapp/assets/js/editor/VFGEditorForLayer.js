VFGEditor.prototype.layerForEdit=function(type){
	 var _this=this;
	 if(_this.sceneLayerMap.has('sceneLayer')){
		layer.msg('页面已经打开！！！', {icon: 3});
		return;
	 }	 
	 layer.load(2);
	 _this._Ajax('get',_this.ctx+"/biz/layer",{sceneId:_this.sceneId},'html',function(e){
		layer.closeAll();
		var layerIndex=layer.open({
			id:'sceneLayer',
            type: 1,
            title:'图层管理',
            area: '320px',
            content:e,
            shade:0,
            offset: ['50px', '0px'],	
            fixed:true,
            move:false,
		    skin:'layui-layer layui-layer-adminRight',
            success: function (layero, dIndex) {
               $(layero).children('.layui-layer-content').css('overflow', 'hidden');
               _this.sceneLayerMap.set('sceneLayer',dIndex);
				if('Point'==type){
					_this.pointForDraw();
				}
				else if('Line'==type){
					_this.polyLineForDraw();
				}
				else if('Polygon'==type){
					_this.polygonForDraw();
				}
				else if('Feature'==type){
					_this.featureForDraw();
				}
				else if('Map'==type){
					_this.addOnLineProvider();
				}
				else if('Model'==type){
					_this.addOnLineModel();
				}
			},
			end:function(){
				_this.sceneLayerMap.delete('sceneLayer');
			}
		});	
	});	
}

/**
 * 图层显示或隐藏
 */
VFGEditor.prototype.showOrHideLayer=function(treeNode){
	if(!treeNode){
		layer.alert('未选中图层节点！！', {icon: 1,skin: 'layer-ext-moon' })
		return;
	}
	var _this=this;
	if('Maps'==treeNode.type || 'Map'==treeNode.type ){
		_this.showOrHideProvider(treeNode.id,treeNode.checked,treeNode.type);
	}
	else if('Points'==treeNode.type || 'Point'==treeNode.type){
		_this.showOrHidePoint(treeNode.id,treeNode.checked,treeNode.type);
	}
	else if('Models'==treeNode.type || 'Model'==treeNode.type ){
		_this.showOrHideModel(treeNode.id,treeNode.checked,treeNode.type);
	}
	else if('Polylines'==treeNode.type || 'Polyline'==treeNode.type){
		_this.showOrHidePolyline(treeNode.id,treeNode.checked,treeNode.type);
	}
	else if('Polygons'==treeNode.type || 'Polygon'==treeNode.type){
		_this.showOrHidePolygon(treeNode.id,treeNode.checked,treeNode.type);
	}
	else if('VideoPlanes'==treeNode.type || 'VideoPlane'==treeNode.type){
		_this.showOrHideVideoPlane(treeNode.id,treeNode.checked,treeNode.type);
	}
	else if('VideoShed3ds'==treeNode.type || 'VideoShed3d'==treeNode.type){
		_this.showOrHideVideoShed3d(treeNode.id,treeNode.checked,treeNode.type);
	}
	else if('VideoModels'==treeNode.type || 'VideoModel'==treeNode.type){
		_this.showOrHideVModel(treeNode.id,treeNode.checked,treeNode.type);
	}
}

//VFGEditor.prototype.onClickLayer=function(treeNode){
//	if(!treeNode){
//		layer.alert('未选中图层节点！！', {icon: 1,skin: 'layer-ext-moon' })
//		return;
//	}
//	var _this=this;
//	if('Point'==treeNode.type){
//		_this.editPoint(treeNode) ;
//	}
//	else if('Polygons'==treeNode.type || 'polygon'==treeNode.type){
//		_this.showOrHidePolygon(treeNode.id,treeNode.checked,treeNode.type);
//		VFGEditor.prototype.editPolygon = function (polygonId,name) 
//	}
//	else if('Lines'==treeNode.type || 'line'==treeNode.type){
//		_this.showOrHidePolyline(treeNode.id,treeNode.checked,treeNode.type);
//	}
//	else if('Maps'==treeNode.type || 'map'==treeNode.type || 'terrain'==treeNode.type || 'division'==treeNode.type|| 'road'==treeNode.type){
//		_this.showOrHideProvider(treeNode.id,treeNode.checked,treeNode.type);
//	}
//	else if('GLTF'==treeNode.type){
//	}
//	else if('3DTILES'==treeNode.type){
//		var model=_this.globe.cacheModelMap.get(treeNode.id);
//		if(model && model.posX && model.posY){
//			_this.viewer.camera.flyTo({
//			    destination : Cesium.Cartesian3.fromDegrees(model.posX*1,model.posY*1, 3000.0)
//			});
//		}
//	}
//}

VFGEditor.prototype.layerForSwitch=function(treeNode){
	 var _this=this;
 	layer.load(2);
    common_ajax.ajaxAsyncFunc(_this.ctx+"/api/point/list",{
    	sceneId:_this.sceneId,
    	layerId:treeNode.id
    }, "json", function(res){
        layer.closeAll('loading');
        if (undefined!=res && 200==res.code) {
        	for(var i=0;i<res.data.length;i++){
        		if(treeNode.checked){
        			VFG.Point.addPrimitive(_this.viewer,res.data[i],_this.ctx);
        		}else{
        			VFG.Point.removePrimitiveById(res.data[i].point.id);
        			VFG.Point.removePointById(res.data[i].point.id);
        		}
        	}
        }
    });
    
    common_ajax.ajaxAsyncFunc(_this.ctx+"/api/model/list",{
    	sceneId:_this.sceneId,
    	layerId:treeNode.id
    }, "json", function(res){
        layer.closeAll('loading');
        if (undefined!=res && 200==res.code) {
        	for(var i=0;i<res.data.length;i++){
        		var model=res.data[i];
        		if(treeNode.checked){
        			if("3DTILES"==model.type){
        				VFG.Util.create3DTilePrimitive(_this.viewer,model)
        			}else{
        				VFG.Util.createGltfPrimitive(_this.viewer,model)
        			}
        		}else{
        			VFG.Util.removePrimitive(_this.viewer,model.id)
        		}
        	}
        }
    });
}

VFGEditor.prototype.deleteLayer=function(treeNode){
	var _this=this;
    layer.confirm('确定要删除“【<span style="color:red;">' + treeNode.name + '</span>】”吗？', {
        skin: 'layui-layer-admin',
        shade: .1
    }, function (index) {
        layer.close(index);
        layer.load(2);
        common_ajax.ajaxAsyncFunc(ctx+"/biz/layer/delete",{sceneId:_this.sceneId,id:treeNode.id,type:treeNode.type}, "json", function(res){
	        layer.closeAll('loading');
	        if (200==res.code) {
	            layer.msg(res.message, {icon: 1});
	    		var treeObj = $.fn.zTree.getZTreeObj("bizLayerTree");
	    		if(treeNode&&treeObj){
	    			var node = treeObj.getNodeByParam("id",treeNode.id, null);
	    			if(node){
	    				treeObj.removeNode(node);
	    			}
	    			_this.globe.removePrimitiveById(treeNode.id,treeNode.type);
	    		}
	            return true;
	        } else {
	            layer.msg(res.message, {icon: 2});
	            return false;
	        }
        });
    }, function (index) {
        layer.close(index);
        return false;
    }); 
}

VFGEditor.prototype.addOrUpdateLayer=function(node){
	var _this=this;
	var zTree = $.fn.zTree.getZTreeObj("bizLayerTree")||sceneTree;
	if(zTree){
		var editNode =zTree.getNodeByParam("id",node.id);
		if(editNode){
			editNode.name=data.name;
			zTree.updateNode(editNode);
		}else{
			var treeNode =zTree.getNodeByParam("id",node.layerId);
			zTree.addNodes(treeNode?treeNode:null, {id:node.id, pId:node.layerId, name:node.name,type:node.type});
		}
	}
}

