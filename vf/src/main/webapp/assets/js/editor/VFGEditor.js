function VFGEditor(viewer,option){
	this.viewer=viewer;
	this.sceneId=option.sceneId;
	if(!this.viewer){
		console.log('viewer参数必传参数！！！');
	}
	this.ctx=option.ctx||ctx;
	this.url=option.url||ctx;
	this.globe=option.globe||null;
	this.POINT_STYLE=option.globe.POINT_STYLE;
	this.LINE_STYLE=option.globe.LINE_STYLE;
	this.POLYGON_STYLE=option.globe.POLYGON_STYLE;
	this.sceneLayerMap=new Map();
	this.isDraw=false;
	this.isDrag=false;
	this.drawPoint;
	this.init();
}



/**
 * 初始化
 */
VFGEditor.prototype.init=function(){
	var _this=this;
	_this.globe.LEFT_CLICK_PRIMITIVE(function(screenPos,id,type){
		if(!_this.isDrag && !_this.isDraw){
			if("Point"==type){
				_this.operationPoint(screenPos,id,type);
			}
			else if("Polyline"==type){
				_this.operationPolyline(screenPos,id,type);
			}
			else if("Polygon"==type){
				_this.operationPolygon(screenPos,id,type);
			}
			else if("VideoPlane"==type){
				_this.operationVideoPlane(screenPos,id,type);
			}
		}
	});
}

VFGEditor.prototype._Ajax = function(reqType, url, data, dataType, callback) {
	$.ajax({
		type: reqType,
		url: encodeURI(encodeURI(url)),
		data: data,
		dataType: dataType ? dataType : 'html',
		contentType: "application/x-www-form-urlencoded; charset=UTF-8",
		async: true,
		cache: false,
		success: function(response) {
			if (callback != null) {
				callback(response);
			}
		}
	});
}

VFGEditor.prototype.ajaxFunc = function(url, data, dataType, callback){
	if(dataType == undefined || dataType == null){
		dataType = "html";
	}
	var result;
	$.ajax({
		type : "post",
		url : encodeURI(encodeURI(url)),
		data : data,
		dataType : dataType,
		contentType: "application/x-www-form-urlencoded; charset=UTF-8",
		async: true,
		cache: false,
		success:function(response){
			result = response;
			//扩展回调函数
			if( callback != null ){
				callback(result);
			}
		}
	});
	return result;
};

/**
 * 获取选中节点ID
 */
VFGEditor.prototype.getImgLayerTreeIdForPoint=function(){
	var zTree = $.fn.zTree.getZTreeObj("bizLayerTree")||sceneTree;
	if(zTree){
		var nodes = zTree.getSelectedNodes();
		if(nodes!=null && nodes.length>0){
			var node=nodes[0];
			if(node.isParent){
				return node.id;
			}else{
				if('folder'==node.type){
					return node.id;
				}else{
					return node.parentId;
				}
			}
		}
	}
	return '';
}

VFGEditor.prototype.getSelectNode=function(){
	var zTree = $.fn.zTree.getZTreeObj("bizLayerTree")||sceneTree;
	if(zTree){
		var nodes = zTree.getSelectedNodes();
		if(nodes!=null && nodes.length>0){
			var node=nodes[0];
			return node;
		}
	}
	return null;
}

/**
 * 新增图层树节点
 */
VFGEditor.prototype.addImgLayerTreeNode=function(data,type){
	var zTree = $.fn.zTree.getZTreeObj("bizLayerTree")||sceneTree;
	if(zTree){
		var editNode =zTree.getNodeByParam("id",data.id);
		if(editNode){
			editNode.name=data.name;
			zTree.updateNode(editNode);
		}else{
			var pId='';
			if(data.parentId){
				pId=data.parentId;
			}
			if(data.layerId){
				pId=data.layerId;
			}
			var treeNode =zTree.getNodeByParam("id",pId);
			zTree.addNodes(treeNode?treeNode:null, {id:data.id, pId:pId, name:data.name,type:type,isParent:false,parentId:pId,checked:true});
		}
	}
}

VFGEditor.prototype.delImgLayerTreeNode=function (param){
	var _this=this;
    layer.confirm('确定要删除“【<span style="color:red;">' + (param.name||'模型') + '</span>】”吗？', {
        shade: .1
    }, function (index) {
        layer.close(index);
        layer.load(2);
        _this.ajaxFunc(_this.ctx+"/biz/layer/delete",{id:param.id}, "json", function(res){
            layer.closeAll('loading');
            if (undefined!=res && 200==res.code) {
            	var data=res.data;
        		var zTree = $.fn.zTree.getZTreeObj("bizLayerTree")||sceneTree;
        		if(zTree){
        			var treeNode =zTree.getNodeByParam("id",data.id);
        			if(treeNode){
            			zTree.removeNode(treeNode);
            		}
        		}
        		VFG.Util.removeScenePrimitive(_this.viewer,{
        			id:data.id,
        			type:data.type,
        		});
                return true;
            } else {
                return false;
            }      	
        });
    }, function (index) {
        layer.close(index);
        return false;
    }); 
}

/**
 * 鼠标事件处理
 */
VFGEditor.prototype.handleSceneMouseEvent=function(entity){
	var _this=this;
	if(!_this.isDrag){
		if (Cesium.defined(obj) && obj.primitive && obj.id){
			if (obj.primitive instanceof Cesium.Label){
			}
			else if (obj.primitive instanceof Cesium.PointPrimitive){
			}
			else if (obj.primitive instanceof Cesium.Billboard){
			}
		}
	}
}

VFGEditor.prototype.clickLayerNode=function(node){
	var _this=this;
	if('point'==node.type){
		this.flyToPoint(node);
	}
	else if('view'==node.type){
		this.flyToView(node);
	}
}

/**
 * 飞行至点
 */
VFGEditor.prototype.flyToPoint=function(node){
	var _this=this;
    _this.ajaxFunc(_this.ctx+"/biz/marker/point/getById",{id:node.id}, "json", function(res){
        if (undefined!=res && 200==res.code) {
        	var data=res.data;
        	if(data.cameraX && data.cameraY && data.cameraZ){
        		_this.viewer.camera.flyTo({
        		    destination : new Cesium.Cartesian3(data.cameraX*1,data.cameraY*1, data.cameraZ*1),
        		    orientation : {
        		        heading : data.heading*1,
        		        pitch : data.pitch*1,
        		        roll : data.roll*1
        		    }
        		});
        	}else{
        		_this.viewer.camera.flyTo({
        		    destination : Cesium.Cartesian3.fromDegrees(data.x*1, data.y*1, data.z*1+100)
        		});
        	}
        }else{
        	if(undefined!=res) layer.msg(res.message); else layer.msg("未知错误！！！");
        }
    });
}

