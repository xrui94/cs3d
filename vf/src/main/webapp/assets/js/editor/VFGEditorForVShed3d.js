VFGEditor.prototype.showOrHideVideoShed3d=function(layerId,state,type){
	var _this=this;
	if('VideoShed3ds'==type){
		layer.load(2);
	    _this.ajaxFunc(_this.url+"/api/video/shed3d/list",{
	    	sceneId:_this.sceneId,
	    	layerId:layerId
	    }, "json", function(res){
	    	layer.closeAll('loading');
			 if (undefined!=res && 200==res.code) {
				 var list=res.data;
				 if(state==true){
					 _this.globe.addVideoShed3ds(list);
				 }else{
					 _this.globe.removeVideoShed3ds(list);
				 }
			 }
			 else{
			 }
	    });
	}
	else if ('VideoShed3d'==type){
		layer.load(2);
	    _this.ajaxFunc(_this.url+"/api/video/shed3d/getByLayerId",{
	    	sceneId:_this.sceneId,
	    	layerId:layerId
	    }, "json", function(res){
	    	layer.closeAll('loading');
			 if (undefined!=res && 200==res.code) {
				 var model=res.data; 
				 if(model){
					 if(state==true){
						 _this.globe.addVideoShed3d(model);
						 _this.globe.flyToVideoShed3dById(model.id);
					 }else{
						 _this.globe.removeVideoShed3d(model);
					 }
				 }
			 }
			 else{
			 }
	    });
	}else{
		layer.alert('未识别图层标识【"'+type+'"】！！', {icon: 1,skin: 'layer-ext-moon' })
	} 
} 

VFGEditor.prototype.addVideoShed3d = function (treeNode) {
	var _this=this;
	if(this.isDraw){
		console.log('已经处于绘制状态！！！');
		return;
	}
	layer.closeAll();
	_this.isDraw=true;
	this.drawVideoShed3d=new Cesium.DrawVideoShed3D(_this.viewer,{
		END:function(e){
			if(e&& e.length==2){
				var cPosition=VFG.Util.getLnLaFormC3(_this.viewer,e[0]); 
				var vPosition=VFG.Util.getLnLaFormC3(_this.viewer,e[1]);
				var option={
						id:VFG.Util.getUuid(),
						name:'',
						code:'',
						cx:cPosition.x,
						cy:cPosition.y,
						cz:cPosition.z,
						vx:vPosition.x,
						vy:vPosition.y,
						vz:vPosition.z,
						aspectRatio:0.75,
						debugFrustum:true,
						layerId:treeNode.id,
						sceneId:_this.sceneId,
					};
				
				_this.globe.addVideoShed3d(option);
				_this.saveVideoShed3d(option);
			}
			_this.isDraw=false;
			_this.drawVideoShed3D=null;
			_this.layerForEdit();
		}
	});
};

VFGEditor.prototype.saveVideoShed3d=function(option){
	var _this=this;
    var formData={}; 
    formData['bizVideoShed3d.id']=option.id;
    formData['bizVideoShed3d.name']=option.name;
    formData['bizVideoShed3d.cx']=option.cx;
    formData['bizVideoShed3d.cy']=option.cy;
    formData['bizVideoShed3d.cz']=option.cz;
    formData['bizVideoShed3d.vx']=option.vx;
    formData['bizVideoShed3d.vy']=option.vy;
    formData['bizVideoShed3d.vz']=option.vz;
    formData['bizVideoShed3d.aspectRatio']=option.aspectRatio;
    formData['bizVideoShed3d.layerId']=option.layerId;
    formData['bizVideoShed3d.sceneId']=option.sceneId;
    _this.ajaxFunc(_this.ctx+"/biz/video/shed3d/save",formData, "json", function(res){
        if (undefined!=res && 200==res.code) {
        	layer.msg(res.message);
        }else{
        	layer.msg(res?res.message:'保持失败！！');
        }
    });
}

VFGEditor.prototype.operationVideoShed3d = function (screenPos,planeId,type) {
	var _this=this;
	 var plane=_this.globe.getPrimitiveById(planeId,type);
	if(plane){
		layer.load(2);
		var VFGWin;
		_this._Ajax('get',_this.ctx+"/biz/video/shed3d/operation",{sceneId:_this.sceneId,planeId:planeId},'html',function(e){
			layer.closeAll('loading');   
			var layerIndex= layer.open({
					id:planeId,
				    type: 1,
				    title: false,
				    shade: 0,
				    content:e,
				    resize :false,
				    success: function (layero, dIndex) {
			            $(layero).children('.layui-layer-content').css('overflow', 'hidden');
			            var cartesian3=VFG.Util.getScreenToC3(_this.viewer, screenPos, null);
			        	VFGWin=new Cesium.VFGWin(_this.viewer,{
			            	position:cartesian3,
			            	layero:layero,
			            	index:dIndex,
			            });
			        	$('#toolForBizOperationDrag').click(function () {
			        		layer.close(dIndex);
			        		//_this.dragPoint(pointId);
			        	});	
			        	$('#toolForBizOperationEdit').click(function () {
			        		layer.close(dIndex);
			        		_this.editVideoShed3d({id:planeId,name:plane.option.name});
			        	});	
			        	$('#toolForBizOperationDel').click(function () {
			        		layer.close(dIndex);
			        		_this.deleteLayer({
			        			id:planeId,
			        			type:type,
			        			name:plane.option.name,
			        		})
			        	});	
					},
					end:function(){
						if(VFGWin){
							 VFGWin.destroy();
							 VFGWin=null;
						}
					}
		    	});  
		});
	}else{
		layer.alert('信息未知！！', {icon: 1,skin: 'layer-ext-moon' })
	}
}

VFGEditor.prototype.editVideoShed3d = function (treeNode) {
	var _this=this;
	if(this.isDraw){
		console.log('已经处于绘制状态！！！');
		return;
	}
	this.isDraw=true;
	layer.closeAll();
	layer.load(2);
	_this._Ajax('get',_this.ctx+"/biz/video/shed3d/edit",{sceneId:_this.sceneId,shed3dId:treeNode.id},'html',function(e){
		layer.closeAll('loading');   
		var layerIndex= layer.open({
		    	id:'point',
		        title: name || '编辑【'+treeNode.name+'】',
		        type: 1,
		        area: '400px',
		        content:e,
		        shade:0,
		        offset: 'r',
		        fixed:true,
		        move:false,
			    skin:'layui-layer',
			    content: e,
			    success: function (layero, dIndex) {
		            $(layero).children('.layui-layer-content').css('overflow', 'hidden');
		            
		            _this.globe.changeDebugFrustum(treeNode.id,true);
		            
		            slider.render({
		              elem: '#bizVideoShed3dVHeading'
		              ,input: true //输入框
		              ,min: 0 //最小值
		              ,max: 360 //最大值
		              ,value:$("#bizVideoShed3dVHeading-input").val()*1
		              ,change: function(value){
	            	    $("#bizVideoShed3dVHeading-input").val(value);
		            	  var videoShed3dId=$("#bizVideoShed3dId").val();
		            	  if(videoShed3dId){
			            	  _this.globe.changeHeading(videoShed3dId,value);
		            	  }
	            	  }
		            });
		            slider.render({
		              elem: '#bizVideoShed3dVPitch'
		              ,input: true //输入框
		              ,min: 0 //最小值
		              ,max: 360 //最大值
		              ,value:$("#bizVideoShed3dVPitchg-input").val()*1
		              ,change: function(value){
		            	  $("#bizVideoShed3dVPitchg-input").val(value);
		            	  var videoShed3dId=$("#bizVideoShed3dId").val();
		            	  if(videoShed3dId){
			            	 _this.globe.changePitch(videoShed3dId,value);
		            	  }
	            	  }
		            });		            
		            slider.render({
		              elem: '#bizVideoShed3dVRoll'
		              ,input: true //输入框
		              ,min: 0 //最小值
		              ,max: 360 //最大值
		              ,value:$("#bizVideoShed3dVRollg-input").val()*1
		              ,change: function(value){
		            	  $("#bizVideoShed3dVRollg-input").val(value);
		            	  var videoShed3dId=$("#bizVideoShed3dId").val();
		            	  if(videoShed3dId){
			            	  _this.globe.changeRoll(videoShed3dId,value);
		            	  }
	            	  }
		            });
		            
		            slider.render({
		              elem: '#bizVideoShed3dFov'
		              ,input: true //输入框
		              ,min: 30 //最小值
		              ,max: 120 //最大值
		              ,value:$("#bizVideoShed3dFov-input").val()*1
		              ,change: function(value){
		            	  $("#bizVideoShed3dFov-input").val(value);
		            	  var videoShed3dId=$("#bizVideoShed3dId").val();
		            	  if(videoShed3dId){
			            	 _this.globe.changeFov(videoShed3dId,value);
		            	  }
	            	  }
		            });
		            slider.render({
		              elem: '#bizVideoShed3dAspectRatio'
		              ,input: true //输入框
		              ,min: 5 //最小值
		              ,max: 30 //最大值
		              ,value:$("#bizVideoShed3dAspectRatio-input").val()*1*10
		              ,change: function(value){
		            	  $("#bizVideoShed3dAspectRatio-input").val(value/10);
		            	  var videoShed3dId=$("#bizVideoShed3dId").val();
		            	  if(videoShed3dId){
		            		  _this.globe.changeAspectRatio(videoShed3dId,value/10)
		            	  }
	            	  }
		            });

		            slider.render({
		              elem: '#bizVideoShed3dAlpha'
		              ,input: true //输入框
		              ,min: 1 //最小值
		              ,max: 10 //最大值
		              ,value:$("#bizVideoShed3dAlpha-input").val()*10
		              ,change: function(value){
		            	  $("#bizVideoShed3dAlpha-input").val(value/10);
		            	  var videoShed3dId=$("#bizVideoShed3dId").val();
		            	  if(videoShed3dId){
		            		  _this.globe.changeAlpha(videoShed3dId,value/10);
		            	  }
	            	  }
		            });
		            
		            
                	$('#bizVideoShed3dFar').on('input', function(e) {
                		var videoShed3dId=$("#bizVideoShed3dId").val();
                		if(videoShed3dId){
                			_this.globe.changeFar(videoShed3dId,$("#bizVideoShed3dFar").val()*1);
                		}
                	}); 
                	
                	$('#bizVideoShed3dNear').on('input', function(e) {
                		var videoShed3dId=$("#bizVideoShed3dId").val();
                		if(videoShed3dId){
                			_this.globe.changeNear(videoShed3dId,$("#bizVideoShed3dNear").val()*1);
                		}
                	});
                	$('#bizVideoShed3dZ').on('input', function(e) {
                		var videoShed3dId=$("#bizVideoShed3dId").val();
                		if(videoShed3dId){
                			_this.globe.changeHeight(videoShed3dId,$("#bizVideoShed3dZ").val()*1);
                		}
                	});
                	
				    $('#bizVideoShed3dGetBtnVideo').click(function () {
				    	_this.selectVideoForVideoShed3d(treeNode,function(e){
				    		$("#bizVideoShed3dName").val(e.name);
				    		$("#bizVideoShed3dCode").val(e.code);
				    		$("#bizVideoShed3dVideoId").val(e.id);
				    		$("#bizVideoShed3dAspectRatio").val(e.aspectRatio);
				    	});
				    });
				    
				    $('#bizVideoShed3dLoadBtnMaskUrl').click(function () {
                		var videoShed3dId=$("#bizVideoShed3dId").val();
                		var maskUrl=$("#bizVideoShed3dMaskUrl").val();
                		if(videoShed3dId&&maskUrl){
                			if(maskUrl){
                    			if(maskUrl.indexOf("http") != -1){
                    				_this.globe.changeMaskUrl(videoShed3dId,maskUrl);
                    			}else{
                    				_this.globe.changeMaskUrl(videoShed3dId,_this.ctx+maskUrl);
                    			}
                			}else{
                				_this.globe.changeMaskUrl(videoShed3dId,'');
                			}
                		}
				    });
				    var isPick=false;
				    $('#bizVideoShed3dGetBtnPosition').click(function () {
				    	if(!isPick){
							this.pickPosition=new Cesium.DrawPoint(_this.viewer,{
								pick:function(e){
									var position=VFG.Util.getC3ToLnLa(_this.viewer,e);
						    		$("#bizVideoShed3dX").val(position.x);
						    		$("#bizVideoShed3dY").val(position.y);
						    		$("#bizVideoShed3dZ").val(position.z);
						    		
					            	  var videoShed3dId=$("#bizVideoShed3dId").val();
					            	  if(videoShed3dId){
						            	  var videoShed3d=_this.globe.getVideoShed3dById(videoShed3dId);
						            	  if(videoShed3d){
						            		  videoShed3d.option.x=position.x;
						            		  videoShed3d.option.y=position.y;
						            		  videoShed3d.option.z=position.z;
						            		  var vPos=_this.globe.getVideoShed3dViewPosition(videoShed3d.option);
						            		  videoShed3d.cameraPosition=Cesium.Cartesian3.fromDegrees(position.x*1,position.y*1,position.z*1);
						            		  videoShed3d.position=Cesium.Cartesian3.fromDegrees(vPos.longitude*1,vPos.latitude*1,vPos.altitude*1)
						            	  }
					            	  }
						    		
								},
								end:function(){
									isPick=false;
								}
							});
				    	}
				    });
		            
		        	form.on('submit(submitbizVideoShed3dForm)', function (data) {
		        	    layer.load(2);
		                var formData={}; 
		                for(var key in data.field){
		                	formData['bizVideoShed3d.'+key]=data.field[key];
		                }
		                formData['bizVideoShed3d.layerId']=data.field['layerId']||treeNode.id;
		                formData['bizVideoShed3d.sceneId']=_this.sceneId;
		                _this.ajaxFunc(_this.ctx+"/biz/video/shed3d/save",formData, "json", function(res){
			                layer.closeAll('loading');
			                if (200==res.code) {
			                    layer.msg(res.message, {icon: 1});
			                    var data=res.data;
			                    $("#bizVideoShed3dId").val(data.id)
			                } else {
			                    layer.msg(res.message, {icon: 2});
			                }
		                });
		        	    return false;
		        	});
				    $('#settingbizVideoShed3dVisualAngle').click(function () {
				    	if($("#bizVideoShed3dId").val()){
					        var visualPos=VFG.Util.getVisualAngle(_this.viewer);
					        if(visualPos){
					            var formData={}; 
					            formData['bizVideoShed3d.id']=$("#bizVideoShed3dId").val();
					            formData['bizVideoShed3d.heading']=visualPos.heading;
					            formData['bizVideoShed3d.pitch']=visualPos.pitch;
					            formData['bizVideoShed3d.roll']=visualPos.roll;
					            formData['bizVideoShed3d.cameraX']=visualPos.position.x;
					            formData['bizVideoShed3d.cameraY']=visualPos.position.y;
					            formData['bizVideoShed3d.cameraZ']=visualPos.position.z;
					            _this.ajaxFunc(_this.ctx+"/biz/video/shed3d/save",formData, "json", function(res){
						            if (undefined!=res && 200==res.code) {
						            	 layer.msg(res.message);
						            }
					            });
					        }
				    	}
				    });
		        	form.render(); 
				},
				end:function(){
					_this.globe.changeDebugFrustum(treeNode.id,false);
					_this.layerForEdit();
					_this.isDraw=false;
				}
	    	});  
	});
};


VFGEditor.prototype.selectVideoForVideoShed3d = function (treeNode,callback) {
	var _this=this;
	layer.load(2);
	_this._Ajax('get',_this.ctx+"/biz/video/selectList",{sceneId:_this.sceneId,targetId:treeNode.id},'html',function(e){
		layer.closeAll('loading');   
		var layerIndex= layer.open({
		    	id:'selectVideoForVideoShed3d',
		        title: '编辑【'+treeNode.name+'】视频源',
		        type: 1,
		        area: '400px',
		        content:e,
		        shade:0,
		        offset: 'r',
		        fixed:true,
		        move:false,
			    skin:'layui-layer',
			    content: e,
			    success: function (layero, dIndex) {
		            $(layero).children('.layui-layer-content').css('overflow', 'hidden');
		            var insTb = table.render({
		                elem: '#tableSelectBizVideo',
		                url: _this.ctx+'/biz/video/listData', 
		                page: true,
		                toolbar: false,
		                cellMinWidth: 100,
		                where:{
		                	sceneId:_this.sceneId	
		                },
		                cols: [[
		                    {field: 'name', sort: false, title: '名称'},
		                ]],
		            });
		            //监听行单击事件（双击事件为：rowDouble）
		            table.on('row(tableSelectBizVideo)', function(obj){
		            	var data = obj.data;
		              	obj.tr.addClass('layui-table-click').siblings().removeClass('layui-table-click');
		              	if(callback){
		            	  callback(data);
		              	}
		            });
				},
				end:function(){
				}
	    	});  
	});
};








