VFGEditor.prototype.sceneMenuForEdit=function(){
	 var _this=this;
	 if(_this.sceneLayerMap.has('sceneMenu')){
		layer.msg('页面已经打开！！！', {icon: 3});
		return;
	 }	 
	 layer.load(2);
	 _this._Ajax('get',_this.ctx+"/biz/scene/menu",{sceneId:_this.sceneId},'html',function(e){
		layer.closeAll();
		var layerIndex=layer.open({
			id:'sceneMenu',
            type: 1,
            title:'场景菜单',
            area: '320px',
            content:e,
            shade:0,
            offset: ['50px', '0px'],	
            fixed:true,
            move:false,
		    skin:'layui-layer layui-layer-adminRight',
            success: function (layero, dIndex) {
               $(layero).children('.layui-layer-content').css('overflow', 'hidden');
               _this.sceneLayerMap.set('sceneMenu',dIndex);
			},
			end:function(){
				_this.sceneLayerMap.delete('sceneMenu');
			}
		});	
	});
}

/************************************菜单元素*********************************************/
VFGEditor.prototype.menuForItem = function (sceneId,menuId) {
	var _this=this;
	_this._Ajax('get',ctx+"/biz/scene/menu/itemIndex",{sceneId:this.sceneId,menuId:menuId},'html',function(e){
	    layer.open({
	        title:'菜单元素',
	        type: 1,
	        area: '660px',
	        content:e,
	        shade:0,
	        offset: 't',
	        fixed:true,
	        move:false,
		    skin:'layui-layer',
		    content: e,
		    success: function (layero, dIndex) {
	            $(layero).children('.layui-layer-content').css('overflow', 'hidden');
	        	_this.menuForItemLayer(sceneId,menuId);
	        	_this.menuForItemFlight(sceneId,menuId);
			}
    	});  
	});
}

//加载图层树
VFGEditor.prototype.menuForItemLayer=function(sceneId,menuId){
	var _this=this;
	var setting = {
		async: {
			enable: true,
			url: _this.ctx+"/biz/scene/menu/getAsyncTreeData?sceneId="+sceneId+"&menuId="+menuId,
			autoParam:["id", "name=n", "level=lv"],
		},
		check: {
			enable: true,
		},
		callback: {
			onCheck: function(event, treeId, treeNode){
				layer.load(2);
				var nodes = treeNode.children;var formData={};var curl;
				if(treeNode.checked){
					curl = '/biz/scene/menu/saveItem';
					if(nodes){
						for(node of nodes){
							formData['bizSceneMenuItem.type']='layer';
							formData['bizSceneMenuItem.objId']=node.id;
							formData['bizSceneMenuItem.menuId']=menuId;
							_this.ajaxFunc(ctx+curl,formData, "json", null);
						}
					}
					formData['bizSceneMenuItem.type']='layer';
					formData['bizSceneMenuItem.objId']=treeNode.id;
					formData['bizSceneMenuItem.menuId']=menuId;
					_this.ajaxFunc(ctx+curl,formData, "json", null);
					layer.closeAll('loading');
					layer.msg('添加成功！', {icon: 1});
				}else{
					curl = '/biz/scene/menu/deleteItem';
					if(nodes){
						for(node of nodes){
							formData.type='layer';
							formData.objId=node.id;
							formData.menuId=menuId;
							_this.ajaxFunc(ctx+curl,formData, "json", null);
						}
					}
					formData.type='layer';
					formData.objId=treeNode.id;
					formData.menuId=menuId;
					_this.ajaxFunc(ctx+curl,formData, "json", null);
					layer.closeAll('loading');
					layer.msg('取消成功！', {icon: 1});
				}
			}
		}
	};
	$.fn.zTree.init($("#bizSceneMenuTreeDetails"), setting);
}

//加载漫游列表
VFGEditor.prototype.menuForItemFlight=function(sceneId,menuId){
	var _this=this;
	table =  $.extend(table, {config: {checkName: 'checked'}});
	var menuBizFlightTb = table.render({
	   elem: '#menuTableBizFlight',
	   url: _this.ctx+'/biz/flight/listData',
	   page: true,
	   where:{
	   	 menuId:menuId,
	   	 sceneId:sceneId
	   },
	   height:300,
	   cols: [[
		   {type: 'numbers'},
		   {type:'checkbox'},
	       {field: 'name', sort: false, title: '名称', edit: 'text',width: 200}
	   ]]
	});
	
	table.on('checkbox(menuTableBizFlight)', function(obj){
		console.log(obj.checked); //选中行的相关数据
		layer.load(2);
		var curl;var formData={};
		if(obj.checked){
			formData['bizSceneMenuItem.type']='flight';
			formData['bizSceneMenuItem.objId']=obj.data.id;
			formData['bizSceneMenuItem.menuId']=menuId;
    		curl = '/biz/scene/menu/saveItem';
		}else{
			curl = '/biz/scene/menu/deleteItem';
			formData.type='flight';
			formData.objId=obj.data.id;
			formData.menuId=menuId;
		}
		var res = common_ajax.ajaxFunc(ctx+curl,formData, "json", null);
		layer.closeAll('loading');
        if (200==res.code) {
            layer.msg(res.message, {icon: 1});
        }else{
        	layer.msg(res.message, {icon: 2});
        }
	});
}




