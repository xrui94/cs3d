VFGEditor.prototype.viewForSave=function(){
	var _this=this;
    var visualPos=VFG.Util.getVisualAngle(_this.viewer);
    if(visualPos){
        var formData={}; 
        formData['bizView.id']='';
        formData['bizView.name']='未命名';
        formData['bizView.sceneId']=_this.sceneId;
        formData['bizView.heading']=visualPos.heading;
        formData['bizView.pitch']=visualPos.pitch;
        formData['bizView.roll']=visualPos.roll;
        formData['bizView.cameraX']=visualPos.position.x;
        formData['bizView.cameraY']=visualPos.position.y;
        formData['bizView.cameraZ']=visualPos.position.z;
        var res = common_ajax.ajaxFunc(_this.ctx+"/biz/view/save",formData, "json", null);
        if (undefined!=res && 200==res.code) {
        	 layer.msg(res.message);
             var data=res.data;
         	if(data){
                 //更新树
             	var zTree = $.fn.zTree.getZTreeObj("bizLayerTree")||sceneTree;
             	if(zTree){
             		var editNode =zTree.getNodeByParam("id",data.id);
             		if(editNode){
             			editNode.name=data.name;
             			zTree.updateNode(editNode);
             		}else{
             			var treeNode =zTree.getNodeByParam("id",data.id);
             			zTree.addNodes(treeNode?treeNode:null, {id:data.id, pId:'root', name:data.name,type:'view'});
             		}
             	}		                    		
         	}
        }
    }
}

/**
 * 编辑点信息
 */
VFGEditor.prototype.viewForEdit = function (viewId,name) {
	var _this=this;
	_Ajax('get',ctx+"/biz/view/viewForEdit",{sceneId:_this.sceneId,viewId:viewId},'html',function(e){
		   var layerIndex= layer.open({
		    	id:'viewForEdit',
		        title: name || '编辑【'+name+'】视角',
		        type: 1,
		        area: '300px',
		        content:e,
		        shade:0,
		        offset: 't',
		        fixed:true,
		        move:false,
			    skin:'layui-layer',
			    content: e,
			    success: function (layero, dIndex) {
		            $(layero).children('.layui-layer-content').css('overflow', 'hidden');
		        	form.on('submit(submitBizViewForm)', function (data) {
		        	    layer.load(2);
		                var formData={}; 
		                for(var key in data.field){
		                	formData['bizView.'+key]=data.field[key];
		                }
		                var res = common_ajax.ajaxFunc(ctx+"/biz/view/save",formData, "json", null);
		                layer.closeAll('loading');
		                if (200==res.code) {
		                	layer.close(dIndex);
		                    layer.msg(res.message, {icon: 1});
		                    var data=res.data;
	                    	if(data){
			                    //更新树
			                	var zTree = $.fn.zTree.getZTreeObj("bizLayerTree")||sceneTree;
			                	if(zTree){
			                		var editNode =zTree.getNodeByParam("id",data.id);
			                		if(editNode){
			                			editNode.name=data.name;
			                			zTree.updateNode(editNode);
			                		}else{
			                			var treeNode =zTree.getNodeByParam("id",data.id);
			                			zTree.addNodes(treeNode?treeNode:null, {id:data.id, pId:data.id, name:data.name,type:'view'});
			                		}
			                	}		                    		
	                    	}
		                } else {
		                    layer.msg(res.message, {icon: 2});
		                }
		        	    return false;
		        	});

				    $('#settingBizViewVisualAngle').click(function () {
				        var visualPos=VFG.Util.getVisualAngle(_this.viewer);
				        if(visualPos){
				            var formData={}; 
				            formData['bizView.id']=$("#bizViewId").val();
				            formData['bizView.heading']=visualPos.heading;
				            formData['bizView.pitch']=visualPos.pitch;
				            formData['bizView.roll']=visualPos.roll;
				            formData['bizView.cameraX']=visualPos.position.x;
				            formData['bizView.cameraY']=visualPos.position.y;
				            formData['bizView.cameraZ']=visualPos.position.z;
				            var res = common_ajax.ajaxFunc(_this.ctx+"/biz/view/save",formData, "json", null);
				            if (undefined!=res && 200==res.code) {
				            	 layer.msg(res.message);
				            }
				        }
				    });
		        	form.render(); 
				},
				end:function(){
				}
	    	});  
	});
};

/**
 * 事件
 */
VFGEditor.prototype.viewForEvent = function (viewId,name) {
	var _this=this;
	_this.operateForMenu(viewId,'view')
};

/**
 * 定位
 */
VFGEditor.prototype.flyToView=function(node){
	var _this=this;
    var res = common_ajax.ajaxFunc(_this.ctx+"/biz/view/getRenderById",{id:node.id,event:'LEFTCLICK'}, "json", null);
    if (undefined!=res && 200==res.code) {
    	var render=res.data;
    	var view=render.view;
    	if(view.cameraX && view.cameraY && view.cameraZ){
    		_this.viewer.camera.flyTo({
    		    destination : new Cesium.Cartesian3(view.cameraX*1,view.cameraY*1, view.cameraZ*1),
    		    orientation : {
    		        heading : view.heading*1,
    		        pitch : view.pitch*1,
    		        roll : view.roll*1
    		    }
    		});
    	}
/*    	var pages=render.pages;
    	if(pages){
    		for(var i=0;i<pages.length;i++){
    			$("#themeDivId").load(pages[i].url);
    		}
    	}*/
    }else{
    	if(undefined!=res) layer.msg(res.message); else layer.msg("未知错误！！！");
    }
}

