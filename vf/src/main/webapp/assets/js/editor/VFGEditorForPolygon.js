VFGEditor.prototype.showOrHidePolygon=function(layerId,state,type){
	var _this=this;
	if('Polygons'==type){
		layer.load(2);
	    _this.ajaxFunc(_this.url+"/api/polygon/list",{
	    	sceneId:_this.sceneId,
	    	layerId:layerId
	    }, "json", function(res){
	    	layer.closeAll('loading');
			 if (undefined!=res && 200==res.code) {
				 var list=res.data;
				 if(state==true){
					 _this.globe.addPolygons(list);
				 }else{
					 _this.globe.removePolygons(list);
				 }
			 }
			 else{
			 }
	    });
	}
	else if ('Polygon'==type){
		layer.load(2);
	    _this.ajaxFunc(_this.url+"/api/polygon/getByLayerId",{
	    	sceneId:_this.sceneId,
	    	layerId:layerId
	    }, "json", function(res){
	    	layer.closeAll('loading');
			 if (undefined!=res && 200==res.code) {
				 var model=res.data; 
				 if(model){
					 if(state==true){
						 _this.globe.addPolygon(model);
						 _this.globe.flyToPolygonById(model.id);
					 }else{
						 _this.globe.removePolygon(model);
					 }
				 }
			 }
			 else{
			 }
	    });
	}else{
		layer.alert('未识别图层标识【"'+type+'"】！！', {icon: 1,skin: 'layer-ext-moon' })
	}
}

/************************************绘制多边形*********************************************/
VFGEditor.prototype.drawPolygon = function (node) {
	var _this=this;
	if(this.isDraw){
		console.log('已经处于绘制状态！！！');
		return;
	}
	layer.closeAll();
	var primitive;
	var positions;
	_this.isDraw=true;
	var uniqueId=VFG.Util.getUuid();
	var drawPolygon=new Cesium.DrawPolygon(_this.viewer,{
		leftClick:function(e){
		},
		mouseMove:function(e){
			positions=e;
			if (!Cesium.defined(primitive)) {
				primitive=_this.createPolygon(uniqueId,"",new Cesium.CallbackProperty(function() {
					return new Cesium.PolygonHierarchy(positions);
				}, false));
				drawPolygon.primitive=primitive;
            }
		},
		end:function(){
			_this.isDraw=false;
			drawPolygon=null;
			if(primitive && positions.length>=3){
			    layer.load(2);
			    var formData={}; 
			    formData['bizPolygon.id']=uniqueId;
			    formData['bizPolygon.name']='多边形';
			    formData['bizPolygon.points']=JSON.stringify(VFG.Util.c3sToLnLas(_this.viewer,positions));
			    formData['bizPolygon.layerId']=node.id;
			    formData['bizPolygon.sceneId']=_this.sceneId;
			    formData['bizPolygon.defaultStyleId']=node.defaultStyleId;
			    formData['bizPolygon.hoverStyleId']=node.hoverStyleId;
			    formData['bizPolygon.selectedStyleId']=node.selectedStyleId;
			    _this.ajaxFunc(_this.ctx+"/biz/marker/polygon/save",formData, "json", function(res){
			        layer.closeAll('loading');
			        if (res!=undefined && 200==res.code) {
			        	layer.msg(res.message);
			        	_this.viewer.entities.removeById(uniqueId);
			        	_this.globe.addPolygon(res.data);
			        } else{
			        	layer.msg(res?res.message:'保持失败！！');
			        }
			    });
				
			}
			_this.layerForEdit();
		}
	});
};

VFGEditor.prototype.operationPolygon = function (screenPos,polygonId,type) {
	var _this=this;
	 var polygon=_this.globe.getPrimitiveById(polygonId,type);
	if(polygon){
		layer.load(2);
		_this._Ajax('get',_this.ctx+"/biz/marker/polygon/operation",{sceneId:_this.sceneId,polygonId:polygonId},'html',function(e){
			layer.closeAll('loading');   
			var layerIndex= layer.open({
					id:polygonId,
				    type: 1,
				    title: false,
				    shade: 0.5,
				    content:e,
				    resize :false,
				    success: function (layero, dIndex) {
			            $(layero).children('.layui-layer-content').css('overflow', 'hidden');
			            var cartesian3=VFG.Util.getScreenToC3(_this.viewer, screenPos, null);
			        	VFGWin=new Cesium.VFGWin(_this.viewer,{
			            	position:cartesian3,
			            	layero:layero,
			            	index:dIndex,
			            });
			        	$('#toolForBizOperationDrag').click(function () {
			        		layer.close(dIndex);
			        		_this.globe.removePolygonById(polygonId);
			        		_this.dragPolygon(polygon.id,polygon.name,VFG.Polygon.getPositions(polygon.points));
			        	});	
			        	$('#toolForBizOperationEdit').click(function () {
			        		layer.close(dIndex);
			        		_this.editPolygon(polygonId,polygon.name);
			        	});	
			        	$('#toolForBizOperationStyle').click(function () {
			        	});	
			        	$('#toolForBizOperationDel').click(function () {
			        		layer.close(dIndex);
			        		_this.deleteLayer({
			        			id:polygonId,
			        			type:type,
			        			name:polygon.name,
			        		})
			        	});	
					},
					end:function(){
					}
		    	});  
		});
	}else{
		layer.alert('信息未知！！', {icon: 1,skin: 'layer-ext-moon' })
	}
}

/**
 * 编辑
 */
VFGEditor.prototype.editPolygon = function (polygonId,name) {
	var _this=this;
	_this._Ajax('get',_this.ctx+"/biz/marker/polygon/polygonForEdit",{sceneId:_this.sceneId,polygonId:polygonId},'html',function(e){
		   var layerIndex= layer.open({
		    	id:'polygonForEdit',
		        title: name || '编辑【'+name+'】点',
		        type: 1,
		        area: '300px',
		        content:e,
		        shade:0.3,
		        offset: 't',
		        fixed:true,
		        move:false,
			    skin:'layui-layer',
			    content: e,
			    success: function (layero, dIndex) {
		            $(layero).children('.layui-layer-content').css('overflow', 'hidden');
		        	form.on('submit(submitBizPolygonForm)', function (data) {
		        	    layer.load(2);
		                var formData={}; 
		                for(var key in data.field){
		                	formData['bizPolygon.'+key]=data.field[key];
		                }
		                _this.ajaxFunc(ctx+"/biz/marker/polygon/save",formData, "json", function(res){
			                layer.closeAll('loading');
			                if (res!=undefined && 200==res.code) {
			                	layer.close(dIndex);
			                    layer.msg(res.message, {icon: 1});
			                } else {
			                    layer.msg(res.message, {icon: 2});
			                }		                	
		                });
		        	    return false;
		        	});
		        	
		        	$('#settingBizPolygonVisualAngle').click(function () {
				        var visualPos=VFG.Util.getVisualAngle(_this.viewer);
				        if(visualPos){
				            var formData={}; 
				            formData['bizPolygon.id']=polygonId;
				            formData['bizPolygon.heading']=visualPos.heading;
				            formData['bizPolygon.pitch']=visualPos.pitch;
				            formData['bizPolygon.roll']=visualPos.roll;
				            formData['bizPolygon.cameraX']=visualPos.position.x;
				            formData['bizPolygon.cameraY']=visualPos.position.y;
				            formData['bizPolygon.cameraZ']=visualPos.position.z;
				            _this.ajaxFunc(_this.ctx+"/biz/marker/polygon/save",formData, "json", function(res){
					            if (undefined!=res && 200==res.code) {
					            	 layer.msg(res.message);
					            }
				            });

				        }
				    });

		        	$('#settingBizPolygonHeight').click(function () {
		        		layer.load(2);
		        	    common_ajax.ajaxAsyncFunc(_this.ctx+"/biz/marker/polygon/getById",{
		        	    	id:polygonId,
		        	    }, "json", function(res){
		        	       
		        	        if (undefined!=res && 200==res.code) {
		        	        	var data=res.data;
		        	        	var points = jQuery.parseJSON(data.points);
		        	        	var newPoints=[];
		        	        	for(var i=0;i<points.length;i++){
		        	        		var point=points[i];
		        	        		var height=VFG.Util.getSurfaceHeight(_this.viewer, VFG.Util.getC3FormLnLa(points[i]), {asyn:false})
	        	        			newPoints.push({
	        	        				x:points[i].x,
	        	        				y:points[i].y,
	        	        				z:height?100:0,
	        	        			});		        	        		
		        	        	}
		        	        	
		        	        	var formData={}; 
					            formData['bizPolygon.id']=polygonId;
					            formData['bizPolygon.points']=JSON.stringify(newPoints);
				        	    common_ajax.ajaxAsyncFunc(_this.ctx+"/biz/marker/polygon/save",formData, "json", function(res){
				        	    	 layer.closeAll('loading');
				        	        if (undefined!=res && 200==res.code) {
				        	        	layer.msg(res.message, {icon: 1, anim: 6});
				        	        }else{
				        	        	layer.msg(res.message, {icon: 5, anim: 6});
				        	        }
				        	    });
		        	        }else{
		        	        	 layer.closeAll('loading');
		        	        	layer.msg(res.message, {icon: 5, anim: 6});
		        	        }
		        	    });
				    });
		        	form.render(); 
				},
				end:function(){
				}
	    	});  
	});
}

/**
 * 样式
 */
VFGEditor.prototype.polygonForStyle=function(id){
	var _this=this;
	_this._Ajax('get',_this.ctx+"/biz/marker/polygon/polygonForStyle",{sceneId:this.sceneId,polygonId:id},'html',function(e){
	    layer.open({
	        title:'样式列表',
	        type: 1,
	        area: '450px',
	        content:e,
	        shade:0,
	        offset: 't',
	        fixed:true,
	        move:false,
		    skin:'layui-layer',
		    content: e,
		    success: function (layero, dIndex) {
	            $(layero).children('.layui-layer-content').css('overflow', 'hidden');
                var insTbForBizPolygonIcon = table.render({
                    elem: '#tableStyleBizPolygon',
                    url: _this.ctx+'/biz/style/polygon/listData', 
                    page: true,
                    cellMinWidth: 100,
                    height:700,
                    cols: [[
                        {type:'radio'},
                        {field: 'name', sort: false, title: '名称'},
                    ]]
                });
                
                form.on('submit(formSubSearchBizPolygonStyle)', function (data) {
                	insTbForBizPolygonIcon.reload({where: data.field}, 'data');
                });
	            
	    		$('#selectBizPolygonStyleBtn').click(function () {
	    			var checkStatus = table.checkStatus("tableStyleBizPolygon"); //获取选中行状态
	    	        var polygonStyles = checkStatus.data;  //获取选中行数据
	    	        if(polygonStyles && polygonStyles.length>0){
	    	        	layer.load(2);
	    	            var formData={}; 
	    	            formData['bizPolygon.id']=id;
	    	            formData['bizPolygon.defaultStyleId']=polygonStyles[0].id;
	    	            _this.ajaxFunc(ctx+"/biz/marker/polygon/saveForStyle",formData, "json", function(res){
		    	            layer.closeAll('loading');
		    	            if (undefined!=res && 200==res.code) {
		    	                layer.msg(res.message, {icon: 1});
		    	                _this.polygonForCreate(res.data);
		    	            } else {
		    	                layer.msg(res.message, {icon: 2});
		    	            }
		    	        	layer.close(dIndex);
	    	            });
	    	        }else{
	    	        	layer.msg("请选择样式!!!", {icon: 3});
	    	        }
	    	        return false;
    			});
	    		
	    		$('#selectBizPointHoverStyleBtn').click(function () {
	    			var checkStatus = table.checkStatus("tableStyleBizPolygon"); //获取选中行状态
	    	        var polygonStyles = checkStatus.data;  //获取选中行数据
	    	        if(polygonStyles && polygonStyles.length>0){
	    	        	layer.load(2);
	    	            var formData={}; 
	    	            formData['bizPolygon.id']=id;
	    	            formData['bizPolygon.hoverStyleId']=polygonStyles[0].id;
	    	            _this.ajaxFunc(ctx+"/biz/marker/polygon/saveForStyle",formData, "json", function(res){
		    	            layer.closeAll('loading');
		    	            if (undefined!=res && 200==res.code) {
		    	                layer.msg(res.message, {icon: 1});
		    	                _this.polygonForCreate(res.data);
		    	            } else {
		    	                layer.msg(res.message, {icon: 2});
		    	            }
		    	        	layer.close(dIndex);
	    	            });
	    	        }else{
	    	        	layer.msg("请选择样式!!!", {icon: 3});
	    	        }
	    	        return false;
    			});
	    		
	    		$('#selectBizPolygonStyleSelectedStyleBtn').click(function () {
	    			var checkStatus = table.checkStatus("tableStyleBizPolygon"); //获取选中行状态
	    	        var polygonStyles = checkStatus.data;  //获取选中行数据
	    	        if(polygonStyles && polygonStyles.length>0){
	    	        	layer.load(2);
	    	            var formData={}; 
	    	            formData['bizPolygon.id']=id;
	    	            formData['bizPolygon.selectedStyleId']=polygonStyles[0].id;
	    	            _this.ajaxFunc(ctx+"/biz/marker/polygon/saveForStyle",formData, "json", function(res){
		    	            layer.closeAll('loading');
		    	            if (undefined!=res && 200==res.code) {
		    	                layer.msg(res.message, {icon: 1});
		    	                _this.polygonForCreate(res.data);
		    	            } else {
		    	                layer.msg(res.message, {icon: 2});
		    	            }
		    	        	layer.close(dIndex);
	    	            });
	    	        }else{
	    	        	layer.msg("请选择样式!!!", {icon: 3});
	    	        }
	    	        return false;
    			});
			}
    	});  
	});
}




VFGEditor.prototype.dragPolygon = function (uniqueId,name,points) {
	if(!uniqueId && !points){
		console.log('傻冒，错了！');
		return;
	}
	var _this=this;
	_this.isDrag=true;
	var map=new Map();
	var insertMap=new Map();
	var positions=points;
	_this.showPointForPolygon(positions,map,insertMap) ;
	var entityId=uniqueId;
	var entity=_this.createPolygon(uniqueId,name,new Cesium.CallbackProperty(function() {
		return new Cesium.PolygonHierarchy(positions);
	}, false));
	
	
	var cesiumDrag =new Cesium.Drag(_this.viewer,{
		type:'pointForPolygon',
		primitive:entity,
		LEFT_DOWN:function(e,pick){
        	if(pick && pick.id && pick.id.id && insertMap.has(pick.id.id)){
        		VFG.Util.inset(positions,pick.id.index,pick.id.position._value);
        	}else{ 
        		_this.clearCenterPointForLine(insertMap);
        	}
		},
		MIDDLE_CLICK:function(id,obj){
            if( map.has(id)){
				var point=map.get(id);
				VFG.Util.splice(positions,point.index, 1);
				_this.clearPointForPolygon(map);
				_this.clearCenterPointForPolygon(insertMap);
				_this.showPointForPolygon(positions,map,insertMap) ;
            }
		},		
		mouseMove:function(id,e){
			if(map.has(id)){
				var point=map.get(id);
				point.position = new Cesium.CallbackProperty(function () {
                    return e;
                }, false);
				positions[point.index]=e;
			}
			else if(insertMap.has(id)){
				var point=insertMap.get(id);
				point.position = new Cesium.CallbackProperty(function () {
                    return e;
                }, false);
				positions[point.index]=e;
			}
		},
		LEFT_UP:function(e){
			//清中间点位
			_this.clearPointForPolygon(map);
			_this.clearCenterPointForPolygon(insertMap);
			_this.showPointForPolygon(positions,map,insertMap) ;
		},
		end:function(){
			_this.isDrag=false;
			cesiumDrag=null;
		    layer.load(2);
		    var formData={}; 
		    formData['bizPolygon.id']=entityId;
		    formData['bizPolygon.points']=JSON.stringify(VFG.Util.c3sToLnLas(_this.viewer,positions));
		    _this.ajaxFunc(_this.ctx+"/biz/marker/polygon/save",formData, "json", function(res){
		        layer.closeAll('loading');
		        if (res!=undefined && 200==res.code) {
					_this.clearPointForPolygon(map);
					_this.clearCenterPointForPolygon(insertMap);
					_this.viewer.entities.removeById(uniqueId);
		        	_this.globe.addPolygon(res.data);
		        	layer.msg(res.message)
		        } else{
		        	layer.msg(res?res.message:'保持失败！！！')
		        }
		    });
		}
	});
}

//辅助点
VFGEditor.prototype.showPointForPolygon = function (positions,map,insertMap) {
	var _this=this;
	for(var i=0;i<positions.length;i++){
		var uuid=VFG.Util.getUuid();
		var point=_this.addPointForPolygon(uuid,i,i,positions[i]);
		map.set(uuid,point);
		if(i+1<positions.length){
			var uuid=VFG.Util.getUuid();
			var centePoint=VFG.Util.getCenterPoint(positions[i],positions[i+1]);
			var point=_this.insertCenterPointForPolygon(uuid,i+1,i+1,centePoint);
			insertMap.set(uuid,point);
		}
	}
}

//描点
VFGEditor.prototype.addPointForPolygon = function (id,name,index,position,color) {
	var ops={
	        id: id,
	        name:name,
	        type:'pointForPolygon',
	        show:true,
	        index:index,
	        position:position,
	        point:new Cesium.PointGraphics ( {
	            show : true,
	            pixelSize : 5,
	            heightReference :Cesium.HeightReference.NONE,
	            color :color || Cesium.Color.YELLOW,
	            outlineColor :Cesium.Color.RED,
	            outlineWidth :1,
	            eyeOffset: new Cesium.Cartesian3(0, 0, -10)
	        } )
	    }
	return this.viewer.entities.add(new Cesium.Entity(ops));
}

VFGEditor.prototype.insertCenterPointForPolygon = function (id,name,index,position) {
	var ops={
	        id: id,
	        name:name,
	        type:'pointForPolygon',
	        show:true,
	        index:index,
	        position:position,
	        point:new Cesium.PointGraphics ( {
	            show : true,
	            pixelSize : 5,
	            heightReference :Cesium.HeightReference.NONE,
	            color :Cesium.Color.CORNFLOWERBLUE,
	            outlineColor :Cesium.Color.RED,
	            outlineWidth :1,
	            eyeOffset: new Cesium.Cartesian3(0, 0, -10)
	        } )
	    }
	return this.viewer.entities.add(new Cesium.Entity(ops));
}


/**
 * 清除辅助点
 */
VFGEditor.prototype.clearPointForPolygon = function (map) {
	var _this=this;
	map.forEach(function(value,key){
		VFG.Util.removeEntityById(_this.viewer,key)
	});
	map.clear();
}
/**
 * 清除插入的辅助点
 */
VFGEditor.prototype.clearCenterPointForPolygon = function (map) {
	var _this=this;
	map.forEach(function(value,key){
		VFG.Util.removeEntityById(_this.viewer,key)
	});
	map.clear();
}

VFGEditor.prototype.createPolygon= function (id,name,hierarchy) {
	var _this=this;
	return _this.viewer.entities.add({
    	id:id,
    	name:name||'',
    	show: true,
        polygon:{
            hierarchy :hierarchy,
            perPositionHeight: false,
            material: Cesium.Color.RED.withAlpha(0.7),
            outline: true,
            outlineColor: Cesium.Color.YELLOW.withAlpha(1)
        }
    });
}


