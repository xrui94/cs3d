VFGEditor.prototype.showOrHidePolyline=function(layerId,state,type){
	var _this=this;
	if('Polylines'==type){
		layer.load(2);
	    _this.ajaxFunc(_this.url+"/api/polyline/list",{
	    	sceneId:_this.sceneId,
	    	layerId:layerId
	    }, "json", function(res){
	    	layer.closeAll('loading');
			 if (undefined!=res && 200==res.code) {
				 var list=res.data;
				 if(state==true){
					 _this.globe.addPolylines(list);
				 }else{
					 _this.globe.removePolylines(list);
				 }
			 }
			 else{
			 }
	    });
	}
	else if ('Polyline'==type){
		layer.load(2);
	    _this.ajaxFunc(_this.url+"/api/polyline/getByLayerId",{
	    	sceneId:_this.sceneId,
	    	layerId:layerId
	    }, "json", function(res){
	    	layer.closeAll('loading');
			 if (undefined!=res && 200==res.code) {
				 var model=res.data; 
				 if(model){
					 if(state==true){
						 _this.globe.addPolyline(model);
					 }else{
						 _this.globe.removePolyline(model);
					 }
				 }
			 }
			 else{
			 }
	    });
	}else{
		layer.alert('未识别图层标识【"'+type+'"】！！', {icon: 1,skin: 'layer-ext-moon' })
	}
} 


/************************************绘制线*********************************************/
/**
 * 开始绘制
 */
VFGEditor.prototype.drawPolyline = function (node) {
	var _this=this;
	if(this.isDraw){
		layer.msg('已经处于绘制状态！！！');
		return;
	}
	layer.closeAll();
	var primitive;
	var positions;
	_this.isDraw=true;
	var uniqueId=VFG.Util.getUuid();
	var drawLine=new Cesium.DrawLine(_this.viewer,{
		leftClick:function(e){
		},
		mouseMove:function(e){
			positions=e;
			if (!Cesium.defined(primitive)) {
				primitive=_this.createLine(uniqueId,"",positions);
				drawLine.primitive=primitive;
            }
		},
		end:function(){
			_this.isDraw=false;
			drawLine=null;
			if(primitive && positions.length>=2){
			    var formData={}; 
			    formData['bizLine.id']=uniqueId;
			    formData['bizLine.name']='线';
			    formData['bizLine.sceneId']=_this.sceneId;
			    formData['bizLine.layerId']=node.id;
			    formData['bizLine.defaultStyleId']=node.defaultStyleId;
			    formData['bizLine.hoverStyleId']=node.hoverStyleId;
			    formData['bizLine.selectedStyleId']=node.selectedStyleId;
			    formData['bizLine.points']=JSON.stringify(VFG.Util.c3sToLnLas(_this.viewer,positions));
			    _this.savePolyLine(formData,function(res){
			        if (res!=undefined && 200==res.code) {
			        	layer.msg(res.message);
			        	_this.viewer.entities.removeById(uniqueId);
			        	_this.globe.addPolyline(res.data);
			        }else{
			        	layer.msg(res?res.message:'保持失败！！');
			        }
			    });
			}
			_this.layerForEdit();
		}
	});
};

/**
 * 保存
 */
VFGEditor.prototype.savePolyLine=function(formData,callback){
	var _this=this;
    layer.load(2);
    _this.ajaxFunc(_this.ctx+"/biz/marker/line/save",formData, "json", function(res){
    	 layer.closeAll('loading');
    	if(callback){
    		callback(res);
    	}
    });
}

/**
 * 操作点
 */
VFGEditor.prototype.operationPolyline = function (screenPos,polylineId,type) {
	var _this=this;
	 var polyline=_this.globe.getPrimitiveById(polylineId,type);
	if(polyline){
		layer.load(2);
		_this._Ajax('get',_this.ctx+"/biz/marker/line/operation",{sceneId:_this.sceneId,polylineId:polylineId},'html',function(e){
			layer.closeAll('loading');   
			var layerIndex= layer.open({
					id:polylineId,
				    type: 1,
				    title: false,
				    shade: 0.5,
				    content:e,
				    resize :false,
				    success: function (layero, dIndex) {
			            $(layero).children('.layui-layer-content').css('overflow', 'hidden');
			            var cartesian3=VFG.Util.getScreenToC3(_this.viewer, screenPos, null);
			        	VFGWin=new Cesium.VFGWin(_this.viewer,{
			            	position:cartesian3,
			            	layero:layero,
			            	index:dIndex,
			            });
			        	$('#toolForBizOperationDrag').click(function () {
			        		layer.close(dIndex);
			        		_this.globe.removePolylineById(polyline.id);
			        		_this.dragPolyline(polyline.id,polyline.name,VFG.Polyline.getPositions(polyline.points));
			        	});	
			        	$('#toolForBizOperationEdit').click(function () {
			        		layer.close(dIndex);
			        		_this.editPolyLine(polylineId,polyline.name);
			        	});	
			        	$('#toolForBizOperationStyle').click(function () {
			        	});	
			        	$('#toolForBizOperationDel').click(function () {
			        		layer.close(dIndex);
			        		_this.deleteLayer({
			        			id:polylineId,
			        			type:type,
			        			name:polyline.name,
			        		})
			        	});	
					},
					end:function(){
					}
		    	});  
		});
	}else{
		layer.alert('信息未知！！', {icon: 1,skin: 'layer-ext-moon' })
	}
}

/**
 * 编辑
 */
VFGEditor.prototype.editPolyLine = function (lineId,name) {
	var _this=this;
	_this._Ajax('get',_this.ctx+"/biz/marker/line/polyLineForEdit",{sceneId:_this.sceneId,lineId:lineId},'html',function(e){
		   var layerIndex= layer.open({
		    	id:'line',
		        title: name || '编辑【'+name+'】点',
		        type: 1,
		        area: '300px',
		        content:e,
		        shade:0,
		        offset: 't',
		        fixed:true,
		        move:false,
			    skin:'layui-layer',
			    content: e,
			    success: function (layero, dIndex) {
		            $(layero).children('.layui-layer-content').css('overflow', 'hidden');
		        	form.on('submit(submitBizLineForm)', function (data) {
		                var formData={}; 
		                for(var key in data.field){
		                	formData['bizLine.'+key]=data.field[key];
		                }
		                _this.savePolyLine(formData,function(res){
			                if (res!=undefined && 200==res.code) {
			                	 layer.close(dIndex);
			                    layer.msg(res.message);
			                } else {
			                    layer.msg(res.message);
			                }
		                })
		        	    return false;
		        	});
		        	form.render(); 
				},
				end:function(){
				}
	    	});  
	});
};

/**
 * 编辑菜单
 */
VFGEditor.prototype.polyLineForMenu=function(entity,type){
	var _this=this;
	_this.operateForMenu(entity,type);
}

//拖住
VFGEditor.prototype.dragPolyline = function (uniqueId,name,points) {
	var _this=this;
	_this.isDrag=true;
	if(!uniqueId && !points){
		console.log('傻冒，错了！');
		return;
	}
	var map=new Map();
	var insertMap=new Map();
	var positions=points;
	var distanceDisplayCondition;
	var entityId=uniqueId;
	var entity=_this.createLine(uniqueId,name,points)
	_this.showPointForLine(positions,map,insertMap) ;
	var cesiumDrag =new Cesium.Drag(_this.viewer,{
		type:'pointForLine',
		primitive:entity,
		LEFT_DOWN:function(e,pick){
        	if(pick && pick.id && pick.id.id && insertMap.has(pick.id.id)){
        		VFG.Util.inset(positions,pick.id.index,pick.id.position._value);
        	}else{ 
        		_this.clearCenterPointForLine(insertMap);
        	}
		},
		MIDDLE_CLICK:function(id,obj){
            if( map.has(id)){
				var point=map.get(id);
				VFG.Util.splice(positions,point.index, 1);
				_this.clearPointForLine(map);
				_this.clearCenterPointForLine(insertMap);
				_this.showPointForLine(positions,map,insertMap) ;
            }
		},
		mouseMove:function(id,e){
			if(map.has(id)){
				var point=map.get(id);
				point.position = new Cesium.CallbackProperty(function () {
                    return e;
                }, false);
				positions[point.index]=e;
			}
			else if(insertMap.has(id)){
				var point=insertMap.get(id);
				point.position = new Cesium.CallbackProperty(function () {
                    return e;
                }, false);
				positions[point.index]=e;
			}
		},
		LEFT_UP:function(e){
			//清中间点位
			_this.clearPointForLine(map);
			_this.clearCenterPointForLine(insertMap);
			_this.showPointForLine(positions,map,insertMap) ;
		},
		end:function(){
			_this.isDrag=false;
			cesiumDrag=null;
		    var formData={}; 
		    formData['bizLine.id']=entityId;
		    formData['bizLine.points']=JSON.stringify(VFG.Util.c3sToLnLas(_this.viewer,positions));
		    _this.savePolyLine(formData,function(res){
		        if (res!=undefined && 200==res.code) {
		        	layer.msg(res.message)
					_this.clearPointForLine(map);
					_this.clearCenterPointForLine(insertMap);
					_this.viewer.entities.removeById(uniqueId);
		        	_this.globe.addPolyline(res.data);
		        }else{
		        	layer.msg(res?res.message:'保持失败！！！')
		        }
		    });
		}
	});
}
VFGEditor.prototype.showPointForLine = function (positions,map,insertMap) {
	var _this=this;
	for(var i=0;i<positions.length;i++){
		var uuid=VFG.Util.getUuid();
		var point=_this.addPointForLine(uuid,i,i,positions[i]);
		map.set(uuid,point);
		if(i+1<positions.length){
			var uuid=VFG.Util.getUuid();
			var centePoint=VFG.Util.getCenterPoint(positions[i],positions[i+1]);
			var point=_this.insertCenterPointForLine(uuid,i+1,i+1,centePoint);
			insertMap.set(uuid,point);
		}
	}
}
/**
 * 清除辅助点
 */
VFGEditor.prototype.clearPointForLine = function (map) {
	var _this=this;
	map.forEach(function(value,key){
		VFG.Util.removeEntityById(_this.viewer,key)
	});
	map.clear();
}
/**
 * 清除插入的辅助点
 */
VFGEditor.prototype.clearCenterPointForLine = function (map) {
	var _this=this;
	map.forEach(function(value,key){
		VFG.Util.removeEntityById(_this.viewer,key)
	});
	map.clear();
}

VFGEditor.prototype.addPointForLine = function (id,name,index,position) {
	var ops={
	        id: id,
	        name:name,
	        type:'pointForLine',
	        show:true,
	        index:index,
	        position:position,
	        point:new Cesium.PointGraphics ( {
	            show : true,
	            pixelSize : 5,
	            heightReference :Cesium.HeightReference.NONE,
	            color :Cesium.Color.YELLOW,
	            outlineColor :Cesium.Color.RED,
	            outlineWidth :1,
	            eyeOffset: new Cesium.Cartesian3(0, 0, -10)
	        } )
	    }
	return this.viewer.entities.add(new Cesium.Entity(ops));
}
VFGEditor.prototype.insertCenterPointForLine = function (id,name,index,position) {
	var ops={
	        id: id,
	        name:name,
	        type:'pointForLine',
	        show:true,
	        index:index,
	        position:position,
	        point:new Cesium.PointGraphics ( {
	            show : true,
	            pixelSize : 5,
	            heightReference :Cesium.HeightReference.NONE,
	            color :Cesium.Color.CORNFLOWERBLUE,
	            outlineColor :Cesium.Color.RED,
	            outlineWidth :1,
	            eyeOffset: new Cesium.Cartesian3(0, 0, -10)
	        } )
	    }
	return this.viewer.entities.add(new Cesium.Entity(ops));
}

VFGEditor.prototype.createLine = function (id,name,positions) {
	var _this=this;
	_this.viewer.entities.removeById(id);
	return _this.viewer.entities.add({
		id:id,
        name: name?name:"",
        polyline: {
            positions: new Cesium.CallbackProperty(function () {
	            return positions;
	        }, false),
            width: 3.0,
            clampToGround: true,
            material:  Cesium.Color.RED,
        }
    });
}
