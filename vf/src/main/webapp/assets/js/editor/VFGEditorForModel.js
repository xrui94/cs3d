/************************************模型*********************************************/
VFGEditor.prototype.showOrHideModel=function(layerId,state,type){
	var _this=this;
	if('Models'==type){
		layer.load(2);
	    _this.ajaxFunc(_this.url+"/api/model/list",{
	    	sceneId:_this.sceneId,
	    	layerId:layerId
	    }, "json", function(res){
	    	layer.closeAll('loading');
			 if (undefined!=res && 200==res.code) {
				 var list=res.data;
				 if(state==true){
					 _this.globe.addModels(list);
				 }else{
					 _this.globe.removeModels(list);
				 }
			 }
			 else{
			 }
	    });
	}else if('Model'==type){
		layer.load(2);
	    _this.ajaxFunc(_this.url+"/api/model/getByLayerId",{
	    	sceneId:_this.sceneId,
	    	layerId:layerId
	    }, "json", function(res){
	    	layer.closeAll('loading');
			 if (undefined!=res && 200==res.code) {
				 var model=res.data; 
				 if(model){
					 if(state==true){
						 _this.globe.addModel(model);
						// _this.globe.flyToModelById(model.id);
					 }else{
						 _this.globe.removeModel(model);
					 }
				 }
			 }
			 else{
			 }
	    });
	}else{
		layer.alert('未识别图层标识【"'+type+'"】！！', {icon: 1,skin: 'layer-ext-moon' })
	}
} 

/**
 * 添加在线模型
 */
VFGEditor.prototype.addOnLineModel=function(node){
	 var _this=this;
	 if(_this.sceneLayerMap.has('bizModelLayer')){
		layer.msg('页面已经打开！！！', {icon: 3});
		return;
	 }	 
	 layer.load(2);
	 _this._Ajax('get',ctx+"/biz/model/modelForEdit",{sceneId:id},'html',function(e){
		layer.closeAll('loading');
	    layer.open({
	    	id:'bizModelLayer',
	        title:'在线模型',
	        type: 1,
	        area: '320px',
	        content:e,
	        shade:0,
            offset: 'r',
            fixed:true,
            move:false,
		    skin:'layui-layer layui-layer-adminRight',
		    content: e,
		    success: function (layero, dIndex) {
	            $(layero).children('.layui-layer-content').css('overflow', 'hidden');
	            _this.sceneLayerMap.set('bizModelLayer',dIndex);
	            
	            //人工模型
	            var bizModelForGLTFTb = table.render({
	                elem: '#tableBizModelForGLTFEditor',
	                url: _this.ctx+'/biz/model/listData', 
	                toolbar: false,
	                cellMinWidth: 100,
	                page: {
	                    layout: ['prev', 'page', 'next','count'] //自定义分页布局
	                    //,curr: 5 //设定初始在第 5 页
	                    ,groups:5 //只显示 1 个连续页码
	                    ,first: false //不显示首页
	                    ,last: false //不显示尾页
	                },
	                where:{
	                	type:'GLTF',
	                	sceneId:'#(sceneId)',
	                },
	                cols: [[
	                    {field: 'name', sort: false, title: '名称', align: 'left'},
	                ]],
	                done: function (res, curr, count) {
	                }
	            });
	            
	            //实景模型
	            var bizModel3dtilesForTb = table.render({
	                elem: '#tableBizModelFor3DTILESEditor',
	                url: _this.ctx+'/biz/model/listData', 
	                toolbar: false,
	                cellMinWidth: 100,
	                page: {
	                    layout: ['prev', 'page', 'next','count'] //自定义分页布局
	                    //,curr: 5 //设定初始在第 5 页
	                    ,groups:5 //只显示 1 个连续页码
	                    ,first: false //不显示首页
	                    ,last: false //不显示尾页
	                },
	                where:{
	                	type:'3DTILES',
	                	sceneId:'#(sceneId)',
	                },
	                cols: [[
	                    {type:'checkbox'},
	                    {field: 'name', sort: false, title: '名称', align: 'left'},
	                ]],
	            }); 
	            
	            form.on('submit(formSubSearchGLTF)', function (data) {
	            	bizModelForGLTFTb.reload({where: data.field}, 'data');
                });
	            form.on('submit(formSubSearch3DTILES)', function (data) {
	            	bizModel3dtilesForTb.reload({where: data.field}, 'data');
                });	  
	            table.on('checkbox(tableBizModelFor3DTILESEditor)', function (obj){
	            	_this.addModelToScene(node,obj.data);
	            });
			},
			end:function(){
				_this.sceneLayerMap.delete('bizModelLayer');
				_this.destroyForDrawModel();
			}
    	});  
	});
}

/**
 * 添加模型至场景
 */
VFGEditor.prototype.addModelToScene = function (node,model) {
	var _this=this;
	layer.load(2);
	var id=VFG.Util.getUuid();
	_this.globe.addModel({
		id:id,
		uri:model.url,
		name:model.name,
		type:model.type,
		callBack:function(e){
			layer.closeAll('loading');
			if(e.state==true){
		        var formData={}; 
		        formData['bizSceneModel.id']=id;
		        formData['bizSceneModel.sceneId']=_this.sceneId;
		        formData['bizSceneModel.modelId']=model.id;
		        formData['bizSceneModel.layerId']=node.id;
		        formData['bizSceneModel.name']=model.name;
		        formData['bizSceneModel.type']=model.type;
		        formData['bizSceneModel.posX']=e.param.x;
		        formData['bizSceneModel.posY']=e.param.y;
		        formData['bizSceneModel.posZ']=e.param.z;
		        formData['bizSceneModel.pitch']=e.param.rotation_x;
		        formData['bizSceneModel.roll']=e.param.rotation_z;
		        formData['bizSceneModel.heading']=e.param.rotation_y;
		        _this.ajaxFunc(_this.ctx+"/biz/model/saveModelToScene",formData, "json", function(res){
		            if (undefined!=res && 200==res.code) {
		        		layer.alert('成功！！', {icon: 1,skin: 'layer-ext-moon' })
		            }else{
		        		layer.alert('失败！！', {icon: 3,skin: 'layer-ext-moon' })
		            }
		        });
			}else{
				layer.alert(e.message, {icon: 3,skin: 'layer-ext-moon' })
			}
		}
	})
}

/**
 *编辑
 */
VFGEditor.prototype.editModel=function(node){
	var _this=this;
	_this._Ajax('get',_this.ctx+"/biz/model/edit",{sceneId:this.sceneId,id:node.id},'html',function(e){
	    layer.open({
	        title:"模型控制",
	        type: 1,
	        content:e,
	        shade:0,
	        fixed:true,
	        area: '300px',
	        move:true,
	        offset: 'r',
	        resize:false,
		    skin:'layui-layer',
		    content: e,
		    success: function (layero, dIndex) {
		    	var type=$("#modelBizSceneModelType").val();
		    	if('3DTILES'==type){
		    		_this.editModel3DTILES(node.id);
		    	}
		    	console.log(type);
			},
			end:function(){
			}
    	});  
	});
}

VFGEditor.prototype.editModel3DTILES=function(id){
	var _this=this;
	  $('#translationForModelPosZ').on('input', function() {
		 var ops=form.val('modelBizModelParamForm');  // 回显数据
    	 var primitive=VFG.Model.getById(id)
    	 if(primitive){
    		 var modelMatrix=VFG.Util.getPrimitiveModelMatrix(ops);
    		 primitive._root.transform=modelMatrix;
    	 }   
	  });
	
	  slider.render({
	     elem: '#rotateForModelRoll-slide'
	    ,max:360
	    ,min:0
	    ,step:0.1
	    ,input: true //输入框
	    ,value:$("#rotateForModelRoll-input").val()*1 || 0
	    ,change: function(value){
	    	 $("#rotateForModelRoll-input").val(value);
	         var ops=form.val('modelBizSceneModelForm');  // 回显数据
	    	 var primitive=VFG.Model.getById(id)
	    	 if(primitive){
	    		 var modelMatrix=VFG.Util.getPrimitiveModelMatrix(ops);
	    		 primitive._root.transform=modelMatrix;
	    	 } 
	       
	    }
	  });
	  
	  slider.render({
	     elem: '#rotateForModelHeading-slide'
	    ,max:360
	    ,min:0
	    ,step:0.1
	    ,input: true //输入框
	    ,value:$("#rotateForModelHeading-input").val()*1 || 0
	    ,change: function(value){
	         $("#rotateForModelHeading-input").val(value);
	         var ops=form.val('modelBizSceneModelForm');  // 回显数据
  	    	 var primitive=VFG.Model.getById(id)
  	    	 if(primitive){
  	    		 var modelMatrix=VFG.Util.getPrimitiveModelMatrix(ops);
  	    		 primitive._root.transform=modelMatrix;
  	    	 } 
	    }
	  });            	  
	  slider.render({
	     elem: '#rotateForModelPitch-slide'
	    ,max:360
	    ,min:0
	    ,step:0.1
	    ,input: true //输入框
	    ,value:$("#rotateForModelPitch-input").val()*1 || 0
	    ,change: function(value){
	        $("#rotateForModelPitch-input").val(value);
	        var ops=form.val('modelBizSceneModelForm');  // 回显数据
  	    	 var primitive=VFG.Model.getById(id)
  	    	 if(primitive){
  	    		 var modelMatrix=VFG.Util.getPrimitiveModelMatrix(ops);
  	    		 primitive._root.transform=modelMatrix;
  	    	 } 
	    }
	  });
	  
	form.on('submit(modelBizSceneModelSubmit)', function (data) {
	    layer.load(2);
	    var formData={}; 
	    for(var key in data.field){
			formData['bizSceneModel.'+key]=data.field[key];
	    }
	    _this.ajaxFunc(_this.ctx+"/biz/model/saveModelToScene",formData, "json", function(res){
	        layer.closeAll('loading');
	        if (res!=undefined && 200==res.code) {
	        	var data=res.data;
	            layer.msg(res.message, {icon: 1});
	        } else {
	        	 layer.msg(res.message, {icon: 1});
	        }
	    });
	
	    return false;
	});
	form.render();
}

/**
 * editModelGLTF
 */
VFGEditor.prototype.editModelGLTF=function(id,entity){
	var _this=this;
	 $('#translationForModelPosZ').on('input', function() {
	    var ops=form.val('modelBizModelParamForm');  // 回显数据
		    var modelMatrix=VFG.Util.getPrimitiveModelMatrix(ops)
	        entity.primitive.modelMatrix = modelMatrix;
	 });
	
	 slider.render({
	     elem: '#rotateForModelRoll-slide'
	    ,max:360
	    ,min:0
	    ,step:0.1
	    ,input: true //输入框
	    ,value:$("#rotateForModelRoll-input").val()*1 || 0
	    ,change: function(value){
	    	 $("#rotateForModelRoll-input").val(value);
	          var ops=form.val('modelBizModelParamForm');  // 回显数据
	    	 var modelMatrix=VFG.Util.getPrimitiveModelMatrix(ops)
	         entity.primitive.modelMatrix = modelMatrix;
	       
	    }
	  });
	  slider.render({
		     elem: '#rotateForModelHeading-slide'
		    ,max:360
		    ,min:0
		    ,step:0.1
		    ,input: true //输入框
		    ,value:$("#rotateForModelHeading-input").val()*1 || 0
	    ,change: function(value){
	        $("#rotateForModelHeading-input").val(value);
	         var ops=form.val('modelBizModelParamForm');  // 回显数据
	   	    var modelMatrix=VFG.Util.getPrimitiveModelMatrix(ops)
	           entity.primitive.modelMatrix = modelMatrix;
	    }
	  });            	  
	  slider.render({
		     elem: '#rotateForModelPitch-slide'
		    ,max:360
		    ,min:0
		    ,step:0.1
		    ,input: true //输入框
		    ,value:$("#rotateForModelPitch-input").val()*1 || 0
	    ,change: function(value){
	        $("#rotateForModelPitch-input").val(value);
	         var ops=form.val('modelBizModelParamForm');  // 回显数据
	   	    var modelMatrix=VFG.Util.getPrimitiveModelMatrix(ops)
	           entity.primitive.modelMatrix = modelMatrix;    	                
	    }
	  });
	  slider.render({
		     elem: '#rotateForModelScale-slide'
		    ,max:16
		    ,min:0
		    ,step:0.1
		    ,input: true //输入框
		    ,value:$("#rotateForModelScale-input").val()*1 || 1
	    ,change: function(value){
	    	entity.primitive.scale = value;
	    	$("#rotateForModelScale-input").val(value);
	    }
	  });
	  
	  $('#scaleForModel-input').on('input', function() {
		  var value=$(this).val()*1;
		  entity.primitive.scale = value;
	 });
  
  
    form.on('submit(modelBizModelParamSubmit)', function (data) {
    	layer.load(2);
	    var formData={}; 
	    for(var key in data.field){
			formData['bizModelParam.'+key]=data.field[key];
	    }
	    _this.ajaxFunc(ctx+"/biz/model/saveModelToScene",formData, "json", function(res){
	        layer.closeAll('loading');
	        if (res!=undefined && 200==res.code) {
	        	var data=res.data;
	            layer.msg(res.message, {icon: 1});
	        } else {
	        	layer.msg(res.message, {icon: 1});
	        }
       });
	    return false;
	});
	form.render();
}



VFGEditor.prototype.drawForModel = function (data,callBack) {
	var _this=this;
	if(this.isDraw){
		console.log('已经处于绘制状态！！！');
		return;
	}
	var primitive=null;
	_this.isDraw=true;
	var position;
	_this.drawModel=new Cesium.DrawModel(_this.viewer,{
		leftClick:function(e){
			var position=VFG.Util.getC3ToLnLa(_this.viewer,e);
			if(position){
				var id=VFG.Util.getUuid();
				primitive=VFG.Util.createGltfPrimitive(_this.viewer,{
					id:id,
					uri:data.url,
					name:data.name,
					posX:position.x,
					posY:position.y,
					posZ:position.z,
					type:data.type
				});
				_this.saveForModel(id,data,position);
				if(callBack){
					callBack(primitive);
				}
			}
		},
		end:function(){
			_this.isDraw=false;
			_this.drawModel=null;
			if(callBack)callBack(null);
		}
	});
};
VFGEditor.prototype.destroyForDrawModel = function () {
	var _this=this;
	if(_this.drawModel){
		_this.drawModel.destroy();
		_this.drawModel=null;
	}
	_this.isDraw=false;
};

VFGEditor.prototype.saveForModel = function (id,data,position) {
	var _this=this;
    var formData={}; 
    formData['bizModelParam.id']=id;
    formData['bizModelParam.name']=data.name||'未命名';
    formData['bizModelParam.uri'] =data.url||'';
    formData['bizModelParam.posX']=position.x;
    formData['bizModelParam.posY']=position.y;
    formData['bizModelParam.posZ']=position.z;
    formData['bizModelParam.type']=data.type||'';
    formData['bizModelParam.modelId']=data.id||'';
	var layerId=this.getImgLayerTreeIdForPoint();
    formData['bizModelParam.layerId']=layerId?layerId:'root';
    formData['bizModelParam.sceneId']=this.sceneId;
    _this.ajaxFunc(_this.ctx+"/biz/model/saveForParam",formData, "json", function(res){
        if (res!=undefined && 200==res.code) {
        	var data=res.data;
        	_this.addImgLayerTreeNode(data,'GLTF');
        }
    });
};

VFGEditor.prototype.saveForModelPosition = function (id,position) {
	var _this=this;
    var formData={}; 
    formData['bizModelParam.id']=id;
    formData['bizModelParam.posX']=position.x;
    formData['bizModelParam.posY']=position.y;
    formData['bizModelParam.posZ']=position.z;
    _this.ajaxFunc(ctx+"/biz/model/saveForParam",formData, "json",function(res){
    	if (res!=undefined && 200==res.code) {
        	var data=res.data;
        	this.addImgLayerTreeNode(data,'GLTF');
        }
    });
};

VFGEditor.prototype.menuForModel=function(id,entity){
	var _this=this;
	_this._Ajax('get',ctx+"/biz/model/menu",{sceneId:this.sceneId,modelId:id},'html',function(e){
	    layer.open({
	        title:false,
	        type: 1,
	        content:e,
	        shade:0.3,
	        fixed:true,
	        move:false,
	        resize:false,
		    skin:'layui-layer',
		    content: e,
		    success: function (layero, dIndex) {
	            $(layero).children('.layui-layer-content').css('overflow', 'hidden');
	        	$('#toolForBizModelInfo').click(function () {
	        		 console.log('明白了');
	        	});
	        	$('#toolForBizModelDrag').click(function () {
	        		layer.close(dIndex);
	        		_this.dragForModel(entity); 
	        	});	        	
	        	
	        	$('#toolForBizModelEdit').click(function () {
	        		 layer.close(dIndex);
		  	    	 _this.controlForModel(entity.id,entity);
	        	});	 
	        	$('#toolForBizModelDel').click(function () {
	        		 layer.close(dIndex);
			    	 if(entity.primitive){
		    		    _this.delImgLayerTreeNode({
			    		   id:entity.id,
			    		   name: entity.primitive.name,
			    	    });
			    	 }
	        	});
			}
    	});  
	});
}

VFGEditor.prototype.controlForModel=function(id,entity){
	var _this=this;
	_this._Ajax('get',ctx+"/biz/model/editForGLTF",{sceneId:this.sceneId,modelId:id},'html',function(e){
	    layer.open({
	        title:"模型控制",
	        type: 1,
	        content:e,
	        shade:0,
	        fixed:true,
	        area: '300px',
	        move:true,
	        offset: 'r',
	        resize:false,
		    skin:'layui-layer',
		    content: e,
		    success: function (layero, dIndex) {
		    	 _this.isDrag=true;
	            $(layero).children('.layui-layer-content').css('overflow', 'hidden');
	        	
            	$('#translationForModelPosZ').on('input', function() {
                    var ops=form.val('modelBizModelParamForm');  // 回显数据
       	    	    var modelMatrix=VFG.Util.getPrimitiveModelMatrix(ops)
	                    entity.primitive.modelMatrix = modelMatrix;
            	});
            	
            	  slider.render({
            	     elem: '#rotateForModelRoll-slide'
            	    ,max:360
            	    ,min:0
            	    ,step:0.1
            	    ,input: true //输入框
            	    ,value:$("#rotateForModelRoll-input").val()*1 || 0
            	    ,change: function(value){
            	    	 $("#rotateForModelRoll-input").val(value);
                          var ops=form.val('modelBizModelParamForm');  // 回显数据
            	    	 var modelMatrix=VFG.Util.getPrimitiveModelMatrix(ops)
    	                 entity.primitive.modelMatrix = modelMatrix;
    	               
            	    }
            	  });
            	  slider.render({
             	     elem: '#rotateForModelHeading-slide'
             	    ,max:360
             	    ,min:0
             	    ,step:0.1
             	    ,input: true //输入框
             	    ,value:$("#rotateForModelHeading-input").val()*1 || 0
            	    ,change: function(value){
    	                $("#rotateForModelHeading-input").val(value);
                         var ops=form.val('modelBizModelParamForm');  // 回显数据
           	    	    var modelMatrix=VFG.Util.getPrimitiveModelMatrix(ops)
   	                    entity.primitive.modelMatrix = modelMatrix;
            	    }
             	  });            	  
            	  slider.render({
              	     elem: '#rotateForModelPitch-slide'
              	    ,max:360
              	    ,min:0
              	    ,step:0.1
              	    ,input: true //输入框
              	    ,value:$("#rotateForModelPitch-input").val()*1 || 0
            	    ,change: function(value){
    	                $("#rotateForModelPitch-input").val(value);
                         var ops=form.val('modelBizModelParamForm');  // 回显数据
           	    	    var modelMatrix=VFG.Util.getPrimitiveModelMatrix(ops)
   	                    entity.primitive.modelMatrix = modelMatrix;    	                
            	    }
              	  });
            	  slider.render({
               	     elem: '#rotateForModelScale-slide'
               	    ,max:16
               	    ,min:0
               	    ,step:0.1
               	    ,input: true //输入框
               	    ,value:$("#rotateForModelScale-input").val()*1 || 1
            	    ,change: function(value){
            	    	entity.primitive.scale = value;
            	    	$("#rotateForModelScale-input").val(value);
            	    }
               	  });
            	  
              	$('#scaleForModel-input').on('input', function() {
                    var value=$(this).val()*1;
                    entity.primitive.scale = value;
            	});
            	  
            	  
  			    form.on('submit(modelBizModelParamSubmit)', function (data) {
			        layer.load(2);
			        var formData={}; 
			        for(var key in data.field){
		        		formData['bizModelParam.'+key]=data.field[key];
			        }
			        _this.ajaxFunc(ctx+"/biz/model/saveForParam",formData, "json", function(res){
				        layer.closeAll('loading');
				        if (res!=undefined && 200==res.code) {
				        	var data=res.data;
				        	_this.addImgLayerTreeNode(data,'GLTF');
	                        layer.close(dIndex);
	                        layer.msg(res.message, {icon: 1});
				        } else {
				        	 layer.msg(res.message, {icon: 1});
				        }
			        });

			        return false;
			    });
            	form.render();
			},
			end:function(){
				 _this.isDrag=false;
			}
    	});  
	});
}

/**
 * 平移
 */
VFGEditor.prototype.translationForModel=function(entity,cartesian3){
	var translation=Cesium.Matrix4.fromTranslation(cartesian3);
    Cesium.Matrix4.multiply(entity.primitive.modelMatrix,translation,entity.primitive.modelMatrix);
}

/**
 * 点拖拽
 */
VFGEditor.prototype.dragForModel = function (pointEntity) {
	var _this=this;
    _this.isDrag=true;
    var selectEntityId=pointEntity.id;
    var selectEntity=pointEntity.id;
    var isChange=false;
    var endPosition;
	var cesiumDrag =new Cesium.Drag(_this.viewer,{
		primitive:selectEntity,
		mouseMove:function(id,e){
			if(selectEntityId==id){
				endPosition=e;
		        pointEntity.primitive.modelMatrix =Cesium.Transforms.headingPitchRollToFixedFrame(e, VFG.Util.modelMatrixToHeadingPitchRoll(pointEntity.primitive.modelMatrix), Cesium.Ellipsoid.WGS84, Cesium.Transforms.eastNorthUpToFixedFrame, new Cesium.Matrix4());
		        isChange=true;
			 }
		},
		end:function(){
			_this.isDrag=false;
			if(isChange){
				var position=VFG.Util.getC3ToLnLa(_this.viewer,endPosition);
				_this.saveForModelPosition(selectEntityId,position)
			}
		    selectEntityId=null;
		    selectEntity=null;
		    isChange=null;
		    endPosition=null;
    	    endPosition=null;
		}
	});
}

VFGEditor.prototype.editForModel=function(id){
	var _this=this;
	_this._Ajax('get',_this.ctx+"/biz/model/editForModel",{sceneId:this.sceneId,modelId:id},'html',function(e){
	    layer.open({
	        title:"模型控制",
	        type: 1,
	        content:e,
	        shade:0,
	        fixed:true,
	        area: '300px',
	        move:true,
	        offset: 'r',
	        resize:false,
		    skin:'layui-layer',
		    content: e,
		    success: function (layero, dIndex) {
		    	 _this.isDrag=true;
	            $(layero).children('.layui-layer-content').css('overflow', 'hidden');
            	$('#translationForModelPosZ').on('input', function() {
                    var ops=form.val('modelBizModelParamForm');  // 回显数据
	       	    	 var primitive=VFG.Model.getById(id)
	       	    	 if(primitive){
	       	    		 var modelMatrix=VFG.Util.getPrimitiveModelMatrix(ops);
	       	    		 primitive._root.transform=modelMatrix;
	       	    	 }   
            	});
            	
            	  slider.render({
            	     elem: '#rotateForModelRoll-slide'
            	    ,max:360
            	    ,min:0
            	    ,step:0.1
            	    ,input: true //输入框
            	    ,value:$("#rotateForModelRoll-input").val()*1 || 0
            	    ,change: function(value){
            	    	 $("#rotateForModelRoll-input").val(value);
                         var ops=form.val('modelBizModelParamForm');  // 回显数据
    	       	    	 var primitive=VFG.Model.getById(id)
    	       	    	 if(primitive){
    	       	    		 var modelMatrix=VFG.Util.getPrimitiveModelMatrix(ops);
    	       	    		 primitive._root.transform=modelMatrix;
    	       	    	 } 
    	               
            	    }
            	  });
            	  slider.render({
             	     elem: '#rotateForModelHeading-slide'
             	    ,max:360
             	    ,min:0
             	    ,step:0.1
             	    ,input: true //输入框
             	    ,value:$("#rotateForModelHeading-input").val()*1 || 0
            	    ,change: function(value){
    	                $("#rotateForModelHeading-input").val(value);
                         var ops=form.val('modelBizModelParamForm');  // 回显数据
	   	       	    	 var primitive=VFG.Model.getById(id)
	   	       	    	 if(primitive){
	   	       	    		 var modelMatrix=VFG.Util.getPrimitiveModelMatrix(ops);
	   	       	    		 primitive._root.transform=modelMatrix;
	   	       	    	 } 
            	    }
             	  });            	  
            	  slider.render({
              	     elem: '#rotateForModelPitch-slide'
              	    ,max:360
              	    ,min:0
              	    ,step:0.1
              	    ,input: true //输入框
              	    ,value:$("#rotateForModelPitch-input").val()*1 || 0
            	    ,change: function(value){
    	                $("#rotateForModelPitch-input").val(value);
                        var ops=form.val('modelBizModelParamForm');  // 回显数据
	   	       	    	 var primitive=VFG.Model.getById(id)
	   	       	    	 if(primitive){
	   	       	    		 var modelMatrix=VFG.Util.getPrimitiveModelMatrix(ops);
	   	       	    		 primitive._root.transform=modelMatrix;
	   	       	    	 } 
            	    }
              	  });
            	  
  			    form.on('submit(modelBizModelParamSubmit)', function (data) {
			        layer.load(2);
			        var formData={}; 
			        for(var key in data.field){
		        		formData['bizModelParam.'+key]=data.field[key];
			        }
			        _this.ajaxFunc(_this.ctx+"/biz/model/saveForParam",formData, "json", function(res){
				        layer.closeAll('loading');
				        if (res!=undefined && 200==res.code) {
				        	var data=res.data;
				        	_this.addImgLayerTreeNode(data,'GLTF');
	                        layer.close(dIndex);
	                        layer.msg(res.message, {icon: 1});
				        } else {
				        	 layer.msg(res.message, {icon: 1});
				        }
			        });

			        return false;
			    });
            	form.render();
			},
			end:function(){
				 _this.isDrag=false;
			}
    	});  
	});
}

