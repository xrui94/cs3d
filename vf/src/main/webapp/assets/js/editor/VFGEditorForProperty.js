
/**
 * 选择点样式
 */
VFGEditor.prototype.selectForPointStlye = function(id) {
	var _this=this;
	var insTbForBizPointIcon = table.render({
	    elem: '#tableStyleBizPoint',
	    url: _this.ctx+'/biz/style/point/listData', 
	    page: true,
	    cols: [[
	        {type:'radio'},
	        {
                align: 'center', templet: function (d) {
                	
                	if(d.imageForBillboard)
                    	return '<img src="'+_this.ctx+d.imageForBillboard+'" style="width: 35px;height:35px;max-width: none;cursor: pointer;" />';
                    else
                    	return '';
                }, title: '缩略图', width: 260, unresize: true
            },
	        {field: 'name', sort: false, title: '名称',width: 250}
	    ]]
	});
	form.on('submit(formSubSearchBizPointStyle)', function (data) {
		insTbForBizPointIcon.reload({where: data.field}, 'data');
	});
	$('#selectBizPointStyleBtn').click(function () {
		var checkStatus = table.checkStatus("tableStyleBizPoint"); //获取选中行状态
	    var pointStyles = checkStatus.data;  //获取选中行数据
	    if(pointStyles && pointStyles.length>0){
	    	layer.load(2);
	        var formData={}; 
	        formData['bizRelation.objId']=id;
	        formData['bizRelation.srcId']=pointStyles[0].id;
	        formData['bizRelation.sceneId']=_this.sceneId;
	        formData['bizRelation.type']='STYLE';
	        formData['bizRelation.event']='LEFTCLICK';
	        var res = common_ajax.ajaxFunc(_this.ctx+"/biz/relation/save",formData, "json", null);
	        layer.closeAll('loading');
	        if (undefined!=res && 200==res.code) {
	            layer.msg(res.message, {icon: 1});
	        } else {
	            layer.msg(res.message, {icon: 2});
	        }
	    }else{
	    	layer.msg("请选择样式!!!", {icon: 3});
	    }
	    return false;
	});
	
	$('#selectBizPointHoverStyleBtn').click(function () {
		var checkStatus = table.checkStatus("tableStyleBizPoint"); //获取选中行状态
	    var pointStyles = checkStatus.data;  //获取选中行数据
	    if(pointStyles && pointStyles.length>0){
	    	layer.load(2);
	        var formData={}; 
	        formData['bizRelation.objId']=id;
	        formData['bizRelation.srcId']=pointStyles[0].id;
	        formData['bizRelation.sceneId']=_this.sceneId;
	        formData['bizRelation.type']='STYLE';
	        formData['bizRelation.event']='LEFTCLICK';
	        var res = common_ajax.ajaxFunc(_this.ctx+"/biz/relation/save",formData, "json", null);
	        layer.closeAll('loading');
	        if (undefined!=res && 200==res.code) {
	            layer.msg(res.message, {icon: 1});
	        } else {
	            layer.msg(res.message, {icon: 2});
	        }
	    }else{
	    	layer.msg("请选择样式!!!", {icon: 3});
	    }
	    return false;
	});
	
	$('#selectBizPointSelectedStyleBtn').click(function () {
		var checkStatus = table.checkStatus("tableStyleBizPoint"); //获取选中行状态
	    var pointStyles = checkStatus.data;  //获取选中行数据
	    if(pointStyles && pointStyles.length>0){
	    	layer.load(2);
	        var formData={}; 
	        formData['bizRelation.objId']=id;
	        formData['bizRelation.srcId']=pointStyles[0].id;
	        formData['bizRelation.sceneId']=_this.sceneId;
	        formData['bizRelation.type']='STYLE';
	        formData['bizRelation.event']='LEFTCLICK';
	        var res = common_ajax.ajaxFunc(_this.ctx+"/biz/relation/save",formData, "json", null);
	        layer.closeAll('loading');
	        if (undefined!=res && 200==res.code) {
	            layer.msg(res.message, {icon: 1});
	        } else {
	            layer.msg(res.message, {icon: 2});
	        }
	    }else{
	    	layer.msg("请选择样式!!!", {icon: 3});
	    }
	    return false;
	});
}

/**
 * 属性列表
 */
VFGEditor.prototype.propertyForEventTb=function(objId,event,callBack){
	 var _this=this;
	 var bizPropertyTb = table.render({
        elem: '#tableBizProperty',
        url: _this.ctx+'/biz/property/listData',
        page: true,
        where:{
        	objId:objId,
        	event:event,
        },
        height:300,
        cols: [[
            {type: 'numbers'},
            {field: 'key', sort: false, title: '字段', edit: 'text',width: 150},
            {field: 'value', sort: false, title:'值', edit: 'text',width: 250},
            {field: 'sort', align: 'center',sort: false, title:'序号', edit: 'text',width: 60},
            {align: 'center', toolbar: '#tableBarBizProperty', title: '操作',width: 80}
        ]]
    });
    //工具条点击事件
    table.on('tool(tableBizProperty)', function (obj) {
        var data = obj.data;
        var layEvent = obj.event;
        if (layEvent === 'del') { 
            layer.confirm('确定要删除“【<span style="color:red;">' + data.key + '</span>】”吗？', {
                skin: 'layui-layer-admin',
                shade: .1
            }, function (index) {
                layer.close(index);
                layer.load(2);
                var res = common_ajax.ajaxFunc(_this.ctx+"/biz/property/delete",{
                    id: data.id
                }, "json", null);
                layer.closeAll('loading');
                if (undefined!=res && 200==res.code) {
                    layer.msg(res.message, {icon: 1});
                    bizPropertyTb.reload({}, 'data');
                } else {
                    layer.msg(res.message, {icon: 2});
                }
            });
        }
    });
    //监听单元格编辑
    table.on('edit(tableBizProperty)', function(obj){
	      var value = obj.value //得到修改后的值
	      ,data = obj.data //得到所在行所有键值
	      ,field = obj.field; //得到字段
	      var formData={}; 
	      formData['bizProperty.id']=data.id;
	      formData['bizProperty.'+field]=value;
		  _this.propertyForSave(formData,function(){
				bizPropertyTb.reload({}, 'data');
		  });
    });
	$('#btnAddBizProperty').click(function () {
		console.log(event);
		_this.propertyForSave({
			'bizProperty.objId':objId,
			'bizProperty.key':'未定义',
			'bizProperty.event':event,
		},function(){
			bizPropertyTb.reload({}, 'data');
		});
		return false;
	});
}
//
VFGEditor.prototype.propertyForSave=function(formData,callBack){
	var _this=this;
    layer.load(2);
    var res = common_ajax.ajaxFunc(_this.ctx+"/biz/property/save",formData, "json", null);
    layer.closeAll('loading');
    if (undefined!=res && 200==res.code) {
        layer.msg(res.message, {icon: 1});
        if(callBack){
        	callBack();
        }
    } else {
        layer.msg(res.message, {icon: 2});
    }
}
//全景
VFGEditor.prototype.panoramaForEventTb=function(objId,event,callBack){
	var _this=this;
	var bizPanoramaTb = table.render({
	   elem: '#tableBizPanorama',
	   url: _this.ctx+'/biz/panorama/listData',
	   page: true,
	   where:{
	   	objId:objId,
	   	event:event,
	   },
	   height:300,
	   cols: [[
	       {type: 'numbers'},
	       {field: 'name', sort: false, title: '名称', edit: 'text',width: 150},
	       {field: 'url', sort: false, title:'地址', edit: 'text',width:300},
	       {align: 'center', toolbar: '#tableBarBizPanorama', title: '操作',width: 80}
	   ]]
	});
	//工具条点击事件
	table.on('tool(tableBizPanorama)', function (obj) {
	   var data = obj.data;
	   var layEvent = obj.event;
	   if (layEvent === 'del') { 
	       layer.confirm('确定要删除“【<span style="color:red;">' + data.name + '</span>】”吗？', {
	           skin: 'layui-layer-admin',
	           shade: .1
	       }, function (index) {
	           layer.close(index);
	           layer.load(2);
	           var res = common_ajax.ajaxFunc(_this.ctx+"/biz/panorama/delete",{
	               id: data.id
	           }, "json", null);
	           layer.closeAll('loading');
	           if (undefined!=res && 200==res.code) {
	               layer.msg(res.message, {icon: 1});
	               bizPanoramaTb.reload({}, 'data');
	           } else {
	               layer.msg(res.message, {icon: 2});
	           }
	       });
	   }
	});
	//监听单元格编辑
	table.on('edit(tableBizPanorama)', function(obj){
	     var value = obj.value //得到修改后的值
	     ,data = obj.data //得到所在行所有键值
	     ,field = obj.field; //得到字段
	     var formData={}; 
	     formData['bizPanorama.id']=data.id;
	     formData['bizPanorama.'+field]=value;
		  _this.panoramaForSave(formData,function(){
				bizPanoramaTb.reload({}, 'data');
		  });
	});
	$('#btnAddBizPanorama').click(function () {
		_this.panoramaForSave({
			'bizPanorama.objId':objId,
			'bizPanorama.name':'未定义',
			'bizPanorama.sceneId':_this.sceneId,
			'bizPanorama.event':event,
		},function(){
			bizPanoramaTb.reload({}, 'data');
		});
		return false;
	});
}
//
VFGEditor.prototype.panoramaForSave=function(formData,callBack){
	var _this=this;
	layer.load(2);
	var res = common_ajax.ajaxFunc(_this.ctx+"/biz/panorama/save",formData, "json", null);
	layer.closeAll('loading');
	if (undefined!=res && 200==res.code) {
	   layer.msg(res.message, {icon: 1});
	   if(callBack){
	   	callBack();
	   }
	} else {
	   layer.msg(res.message, {icon: 2});
	}
}

//图片
VFGEditor.prototype.pictureForEventTb=function(objId,event,callBack){
	var _this=this;
	var bizPictureTb = table.render({
		   elem: '#tableBizPicture',
		   url: _this.ctx+'/biz/picture/listData',
		   page: true,
		   where:{
		   	objId:objId,
		   	event:event,
		   },
		   height:300,
		   cols: [[
		       {type: 'numbers'},
		       {field: 'name', sort: false, title: '名称', edit: 'text',width: 150},
		       {field: 'url', sort: false, title:'地址', edit: 'text',width:300},
		       {align: 'center', toolbar: '#tableBarBizPicture', title: '操作',width: 80}
		   ]]
		});
	
		table.on('tool(tableBizPicture)', function (obj) {
		   var data = obj.data;
		   var layEvent = obj.event;
		   if (layEvent === 'del') { 
		       layer.confirm('确定要删除“【<span style="color:red;">' + data.name + '</span>】”吗？', {
		           skin: 'layui-layer-admin',
		           shade: .1
		       }, function (index) {
		           layer.close(index);
		           layer.load(2);
		           var res = common_ajax.ajaxFunc(_this.ctx+"/biz/picture/delete",{
		               id: data.id
		           }, "json", null);
		           layer.closeAll('loading');
		           if (undefined!=res && 200==res.code) {
		               layer.msg(res.message, {icon: 1});
		               bizPictureTb.reload({}, 'data');
		           } else {
		               layer.msg(res.message, {icon: 2});
		           }
		       });
		   }
		});
	
    upload.render({
         elem: '#uploadBtnForBizPicture'
         ,url:_this.ctx+'/biz/picture/upload?objId='+objId+'&sceneId='+this.sceneId+'&event='+event
         ,accept: 'images'
     	 ,before: function(obj){ 
     		layer.load(); 
     	 }
	  	 ,done: function(res, index, upload){
	  		layer.closeAll('loading'); //关闭loading
	          if (200==res.code) {
	              layer.msg(res.message, {icon: 1});
	              bizPictureTb.reload({}, 'data');
	          } else {
	              layer.msg(res.message, {icon: 2});
	          }
	  	  }
	  	  ,error: function(index, upload){
	  	    layer.closeAll('loading');
	  	    layer.msg("上传失败！", {icon: 2});
	  	  }       
      });
}

/**
 * 视频资源
 */
VFGEditor.prototype.videoForEventTb=function(objId,event,callBack){
	 var _this=this;
	 var bizVideoTb = table.render({
        elem: '#tableBizVideo',
        url: _this.ctx+'/biz/video/listData',
        page: true,
        where:{
        	objId:objId,
        	event:event,
        },
        height:300,
        cols: [[
            {type: 'numbers'},
            {field: 'name', sort: false, title: '名称',width: 250},
            {align: 'left', toolbar: '#tableBarBizVideo', title: '操作',width: 120}
        ]]
    });
    //工具条点击事件
    table.on('tool(tableBizVideo)', function (obj) {
        var data = obj.data;
        var layEvent = obj.event;
        if (layEvent === 'del') { 
            layer.confirm('确定要删除“【<span style="color:red;">' + data.name + '</span>】”吗？', {
                skin: 'layui-layer-admin',
                shade: .1
            }, function (index) {
                layer.close(index);
                layer.load(2);
                var res = common_ajax.ajaxFunc(_this.ctx+"/biz/video/delete",{
                    id: data.id
                }, "json", null);
                layer.closeAll('loading');
                if (undefined!=res && 200==res.code) {
                    layer.msg(res.message, {icon: 1});
                    bizVideoTb.reload({}, 'data');
                } else {
                    layer.msg(res.message, {icon: 2});
                }
            });
        }
        else if(layEvent === 'edit'){
    		_this.videoForEdit(data.id,objId,event,function(){
    			bizVideoTb.reload({}, 'data');
    		});
        }
    });
	$('#btnAddBizVideo').click(function () {
		_this.videoForEdit(null,objId,event,function(){
			bizVideoTb.reload({}, 'data');
		});
		return false;
	});
}

/**
 * 页面编辑
 */
VFGEditor.prototype.videoForEdit=function(id,objId,event,callBack){
	var _this=this;
	_this._Ajax('get',_this.ctx+"/biz/video/edit",{id:id,sceneId:_this.sceneId,objId:objId,event:event},'html',function(e){
	    layer.open({
	        type: 1,
	        area: '450px',
	        content:e,
	        shade:0.1,
	        offset: 't',
		    success: function (layero, dIndex) {
	            $(layero).children('.layui-layer-content').css('overflow', 'hidden');
	        	form.on('submit(submitBizPageForm)', function (data) {
	        	    layer.load(2);
	                var formData={}; 
	                for(var key in data.field){
	                	formData['bizPage.'+key]=data.field[key];
	                }
	                var res = common_ajax.ajaxFunc(_this.ctx+"/biz/video/save",formData, "json", null);
	                layer.closeAll('loading');
	                if (200==res.code) {
	                	layer.close(dIndex);
	                    layer.msg(res.message, {icon: 1});
	                    if(callBack){
	                    	callBack();
	                    }
	                } else {
	                    layer.msg(res.message, {icon: 2});
	                }
	        	    return false;
	        	});
	            form.render();
			},
    	});  
	});
}
