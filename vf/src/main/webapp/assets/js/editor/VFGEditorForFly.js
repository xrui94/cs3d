/************************************飞行定义*********************************************/
VFGEditor.prototype.flightForEdit=function(){
	 var _this=this;
	 if(_this.sceneLayerMap.has('bizFlightLayer')){
		layer.msg('页面已经打开！！！', {icon: 3});
		return;
	 }	 
	 layer.load(2);
	 _this._Ajax('get',_this.ctx+"/biz/flight",{sceneId:_this.sceneId},'html',function(e){
		layer.closeAll();
		layer.open({
			id:'bizFlightLayer',
		    title:'飞行设置',
		    type: 1,
		    area: '300px',
		    content:e,
		    shade:0,
		    offset: ['50px', '0px'],
		    fixed:true,
		    move:false,
		    skin:'layui-layer layui-layer-adminRight',
		    content: e,
		    success: function (layero, dIndex) {
		        $(layero).children('.layui-layer-content').css('overflow', 'hidden');
		        _this.sceneLayerMap.set('bizFlightLayer',dIndex);
			},
			end:function(){
				_this.sceneLayerMap.delete('bizFlightLayer');
				}
			});  
	});
}

VFGEditor.prototype.startForFly = function (id) {
	var _this=this;
	_this.ajaxFunc(_this.ctx+"/biz/flight/getFlightRender",{flightId:id}, "json", function(res){
        if (undefined!=res && 200==res.code) {
       	 layer.msg(res.message);
       	 var flightPrimitive=new VFG.VFGFlightPrimitive(_this.viewer,res.data);
         //flightPrimitive.start();
         //flightPrimitive.aircraftView();
       	 
       }else{
       	 layer.msg(res.message);
       }
    });
	
};

VFGEditor.prototype.handleForFly = function (points,index,length) {
	var _this=this;
	var flight=points[index].flight;
	var flightPoint=points[index].point;
	var point=flightPoint.point;
	if(point){
		if(point.cameraX && point.cameraY && point.cameraZ){
			_this.viewer.camera.flyTo({
			    destination : new Cesium.Cartesian3(point.cameraX*1,point.cameraY*1, point.cameraZ*1),
			    orientation : {
			        heading : point.heading*1,
			        pitch : point.pitch*1,
			        roll : point.roll*1
			    }
			});
		}else{
			_this.viewer.camera.flyTo({
			    destination : Cesium.Cartesian3.fromDegrees(point.x*1, point.y*1, point.z*1+100)
			});
		}
		
		setTimeout(function () {
			 index++;
			 if(index<length){
				 _this.handleForFly(points,index,length);
			 }else{
				 layer.msg("飞行结束！！！ "); 
			 }
		}, 1000*flight.time);
	}
};

