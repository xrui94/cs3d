VFGEditor.prototype.showOrHideVModel=function(layerId,state,type){
	var _this=this;
	if('VideoModels'==type){
		layer.load(2);
	    _this.ajaxFunc(_this.url+"/api/video/model/list",{
	    	sceneId:_this.sceneId,
	    	layerId:layerId
	    }, "json", function(res){
	    	layer.closeAll('loading');
			 if (undefined!=res && 200==res.code) {
				 var list=res.data;
				 if(state==true){
					 _this.globe.addVModels(list,_this.url);
				 }else{
					 _this.globe.removeVModels(list);
				 }
			 }
			 else{
			 }
	    });
	}
	else if ('VideoModel'==type){
		layer.load(2);
	    _this.ajaxFunc(_this.url+"/api/video/model/getByLayerId",{
	    	sceneId:_this.sceneId,
	    	layerId:layerId
	    }, "json", function(res){
	    	layer.closeAll('loading');
			 if (undefined!=res && 200==res.code) {
				 var model=res.data; 
				 if(model){
					 if(state==true){
						 _this.globe.addVModel(model,_this.url);
						 _this.globe.flyToVModelById(model.id);
					 }else{
						 _this.globe.removeVModel(model);
					 }
				 }
			 }
			 else{
			 }
	    });
	}else{
		layer.alert('未识别图层标识【"'+type+'"】！！', {icon: 1,skin: 'layer-ext-moon' })
	} 
} 

VFGEditor.prototype.operationVModel = function (screenPos,planeId,type) {
	var _this=this;
	 var plane=_this.globe.getPrimitiveById(planeId,type);
	if(plane){
		layer.load(2);
		var VFGWin;
		_this._Ajax('get',_this.ctx+"/biz/video/model/operation",{sceneId:_this.sceneId,planeId:planeId},'html',function(e){
			layer.closeAll('loading');   
			var layerIndex= layer.open({
					id:planeId,
				    type: 1,
				    title: false,
				    shade: 0,
				    content:e,
				    resize :false,
				    success: function (layero, dIndex) {
			            $(layero).children('.layui-layer-content').css('overflow', 'hidden');
			            var cartesian3=VFG.Util.getScreenToC3(_this.viewer, screenPos, null);
			        	VFGWin=new Cesium.VFGWin(_this.viewer,{
			            	position:cartesian3,
			            	layero:layero,
			            	index:dIndex,
			            });
			        	$('#toolForBizOperationDrag').click(function () {
			        		layer.close(dIndex);
			        		//_this.dragPoint(pointId);
			        	});	
			        	$('#toolForBizOperationEdit').click(function () {
			        		layer.close(dIndex);
			        		_this.editVModel({id:planeId,name:plane.option.name});
			        	});	
			        	$('#toolForBizOperationDel').click(function () {
			        		layer.close(dIndex);
			        		_this.deleteLayer({
			        			id:planeId,
			        			type:type,
			        			name:plane.option.name,
			        		})
			        	});	
					},
					end:function(){
						if(VFGWin){
							 VFGWin.destroy();
							 VFGWin=null;
						}
					}
		    	});  
		});
	}else{
		layer.alert('信息未知！！', {icon: 1,skin: 'layer-ext-moon' })
	}
}

VFGEditor.prototype.editVModel = function (treeNode) {
	var _this=this;
	layer.load(2);
	_this._Ajax('get',_this.ctx+"/biz/video/model/edit",{sceneId:_this.sceneId,modelId:treeNode.id},'html',function(e){
		layer.closeAll('loading');   
		var layerIndex= layer.open({
		    	id:'editVModel',
		        title: name || '编辑【'+treeNode.name+'】',
		        type: 1,
		        area: '400px',
		        content:e,
		        shade:0,
		        offset: 'r',
		        fixed:true,
		        move:false,
			    skin:'layui-layer',
			    content: e,
			    success: function (layero, dIndex) {
		            $(layero).children('.layui-layer-content').css('overflow', 'hidden');
		            
				    $('#bizVideoModelGetBtnVideo').click(function () {
				    	_this.selectVideoForVModel(treeNode,function(e){
				    		$("#bizVideoModelName-input").val(e.name);
				    		$("#bizVideoModelCode").val(e.code);
				    		$("#bizVideoModelVideoId").val(e.id);
				    	});
				    });
				    $('#bizVideoModelGetBtnModel').click(function () {
				    	_this.selectModelForVModel(treeNode,function(e){
				    		$("#bizVideoModelModelName-input").val(e.name);
				    		$("#bizVideoModelModelId").val(e.id);
				    	});
				    });
				    
				    slider.render({
		              elem: '#bizVideoModelRotationX'
		              ,input: true //输入框
		              ,min: 0 //最小值
		              ,max: 360 //最大值
		              ,value:$("#bizVideoModelRotationX-input").val()*1
		              ,change: function(value){
		            	  $("#bizVideoModelRotationX-input").val(value);
		            	  var bizVideoModelId=$("#bizVideoModelId").val();
		            	  _this.globe.changeVModelRX(bizVideoModelId,value*1);
	            	  }
		            });
		            slider.render({
		              elem: '#bizVideoModelRotationY'
		              ,input: true //输入框
		              ,min: 0 //最小值
		              ,max: 360 //最大值
		              ,value:$("#bizVideoModelRotationY-input").val()*1
		              ,change: function(value){
		            	  $("#bizVideoModelRotationY-input").val(value);
		            	  var bizVideoModelId=$("#bizVideoModelId").val();
		            	  _this.globe.changeVModelRY(bizVideoModelId,value*1);
	            	  }
		            });
			            
		            slider.render({
		              elem: '#bizVideoModelRotationZ'
		              ,input: true //输入框
		              ,min: 0 //最小值
		              ,max: 360 //最大值
		              ,value:$("#bizVideoModelRotationZ-input").val()*1
		              ,change: function(value){
		            	  $("#bizVideoModelRotationZ-input").val(value);
		            	  var bizVideoModelId=$("#bizVideoModelId").val();
		            	  _this.globe.changeVModelRZ(bizVideoModelId,value*1);
	            	  }
		            });
		            
                	$('#bizVideoModelZ').on('input', function(e) {
                		var bizVideoModelId=$("#bizVideoModelId").val();
                		_this.globe.changeVModelHeight(bizVideoModelId,$("#bizVideoModelZ").val()*1);
                	});
		            
				    var isPick=false;
				    $('#bizVideoModelGetBtnPosition').click(function () {
				    	if(!isPick){
							this.pickPosition=new Cesium.DrawPoint(_this.viewer,{
								pick:function(e){
									var position=VFG.Util.getC3ToLnLa(_this.viewer,e);
						    		$("#bizVideoModelX").val(position.x);
						    		$("#bizVideoModelY").val(position.y);
						    		$("#bizVideoModelZ").val(position.z);
					            	  var bizVideoModelId=$("#bizVideoModelId").val();
					            	  _this.globe.changeVModelPosition(bizVideoModelId,{
					            		  x:position.x,
					            		  y:position.y,
					            		  z:position.z,
					            	  });
								},
								end:function(){
									isPick=false;
								}
							});
				    	}
				    });
		        	form.on('submit(submitbizVModelForm)', function (data) {
		        	    layer.load(2);
		                var formData={}; 
		                for(var key in data.field){
		                	formData['bizVideoModel.'+key]=data.field[key];
		                }
		                formData['bizVideoModel.layerId']=data.field['layerId']||treeNode.id;
		                formData['bizVideoModel.sceneId']=_this.sceneId;
		                _this.ajaxFunc(_this.ctx+"/biz/video/model/save",formData, "json", function(res){
			                layer.closeAll('loading');
			                if (200==res.code) {
			                    var data=res.data;
			                    $("#bizVideoModelId").val(data.id)
			                    layer.msg(res.message, {icon: 1});
			                } else {
			                    layer.msg(res.message, {icon: 2});
			                }
		                });
		        	    return false;
		        	});
				    $('#settingbizVModelVisualAngle').click(function () {
				        var visualPos=VFG.Util.getVisualAngle(_this.viewer);
				        if(visualPos){
				            var formData={}; 
				            formData['bizVideoModel.id']=treeNode.id;
				            formData['bizVideoModel.heading']=visualPos.heading;
				            formData['bizVideoModel.pitch']=visualPos.pitch;
				            formData['bizVideoModel.roll']=visualPos.roll;
				            formData['bizVideoModel.cameraX']=visualPos.position.x;
				            formData['bizVideoModel.cameraY']=visualPos.position.y;
				            formData['bizVideoModel.cameraZ']=visualPos.position.z;
				            _this.ajaxFunc(_this.ctx+"/biz/video/model/save",formData, "json", function(res){
					            if (undefined!=res && 200==res.code) {
					            	 layer.msg(res.message);
					            }
				            });
				        }
				    });
		        	form.render(); 
				},
				end:function(){
				}
	    	});  
	});
};

VFGEditor.prototype.selectVideoForVModel = function (treeNode,callback) {
	var _this=this;
	layer.load(2);
	_this._Ajax('get',_this.ctx+"/biz/video/selectList",{sceneId:_this.sceneId,targetId:treeNode.id},'html',function(e){
		layer.closeAll('loading');   
		var layerIndex= layer.open({
		    	id:'selectVideoForVModel',
		        title: '编辑【'+treeNode.name+'】视频资源',
		        type: 1,
		        area: '400px',
		        content:e,
		        shade:0,
		        offset: 'r',
		        fixed:true,
		        move:false,
			    skin:'layui-layer',
			    content: e,
			    success: function (layero, dIndex) {
		            $(layero).children('.layui-layer-content').css('overflow', 'hidden');
		            var insTb = table.render({
		                elem: '#tableSelectBizVideo',
		                url: _this.ctx+'/biz/video/listData', 
		                page: true,
		                toolbar: false,
		                cellMinWidth: 100,
		                where:{
		                	sceneId:_this.sceneId	
		                },
		                cols: [[
		                    {field: 'name', sort: false, title: '名称'},
		                ]],
		            });
		            
		            // 搜索
		            form.on('submit(formSubSearchSelectBizModel)', function (data) {
		                insTb.reload({where: data.field}, 'data');
		            });
		            
		            //监听行单击事件（双击事件为：rowDouble）
		            table.on('row(tableSelectBizVideo)', function(obj){
		            	var data = obj.data;
		              	obj.tr.addClass('layui-table-click').siblings().removeClass('layui-table-click');
		              	if(callback){
		            	  callback(data);
		              	}
		            });
				},
				end:function(){
				}
	    	});  
	});
};

VFGEditor.prototype.selectModelForVModel = function (treeNode,callback) {
	var _this=this;
	layer.load(2);
	_this._Ajax('get',_this.ctx+"/biz/model/selectList",{sceneId:_this.sceneId,targetId:treeNode.id},'html',function(e){
		layer.closeAll('loading');   
		var layerIndex= layer.open({
		    	id:'selectModelForVModel',
		        title: '编辑【'+treeNode.name+'】模型资源',
		        type: 1,
		        area: '400px',
		        content:e,
		        shade:0,
		        offset: 'r',
		        fixed:true,
		        move:false,
			    skin:'layui-layer',
			    content: e,
			    success: function (layero, dIndex) {
		            $(layero).children('.layui-layer-content').css('overflow', 'hidden');
		            var insTb = table.render({
		                elem: '#tableSelectBizModel',
		                url: _this.ctx+'/biz/model/listData', 
		                page: true,
		                toolbar: false,
		                cellMinWidth: 100,
		                where:{
		                	sceneId:_this.sceneId,
		                	type:'OBJ'
		                },
		                cols: [[
		                    {field: 'name', sort: false, title: '名称'},
		                ]],
		            });
		            //监听行单击事件（双击事件为：rowDouble）
		            table.on('row(tableSelectBizModel)', function(obj){
		            	var data = obj.data;
		              	obj.tr.addClass('layui-table-click').siblings().removeClass('layui-table-click');
		              	if(callback){
		            	  callback(data);
		              	}
		            });
				},
				end:function(){
				}
	    	});  
	});
};






