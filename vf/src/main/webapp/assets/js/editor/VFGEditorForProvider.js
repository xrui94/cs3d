VFGEditor.prototype.showOrHideProvider=function(layerId,state,type){
	var _this=this;
	if('Maps'==type){
		
		layer.load(2);
	    _this.ajaxFunc(_this.url+"/api/map/list",{
	    	sceneId:_this.sceneId,
	    	layerId:layerId
	    }, "json", function(res){
	    	layer.closeAll('loading');
			 if (undefined!=res && 200==res.code) {
				 var list=res.data;
				 if(state==true){
					 _this.globe.addProviders(list);
				 }else{
					 _this.globe.removeProviders(list);
				 }
			 }
			 else{
				 layer.alert(res.message, {icon: 3,skin: 'layer-ext-moon' })
			 }
	    });
	}else if('Map'==type){
		layer.load(2);
	    _this.ajaxFunc(_this.url+"/api/map/getByLayerId",{
	    	sceneId:_this.sceneId,
	    	layerId:layerId
	    }, "json", function(res){
	    	layer.closeAll('loading');
			 if (undefined!=res && 200==res.code) {
				 var model=res.data; 
				 if(model){
					 if(state==true){
						 _this.globe.addProvider(model);
					 }else{
						 _this.globe.removeProvider(model.id);
					 }
				 }
			 }
			 else{
				 layer.alert(res.message, {icon: 3,skin: 'layer-ext-moon' })
			 }
	    });
	}else{
		layer.alert('未识别图层标识【"'+type+'"】！！', {icon: 1,skin: 'layer-ext-moon' })
	}
} 

/**
 * 在线底图
 */
VFGEditor.prototype.addOnLineProvider=function(node){
	 var _this=this;
	 if(_this.sceneLayerMap.has('bizMapLayer')){
		layer.msg('页面已经打开！！！', {icon: 3});
		return;
	 }	 
	 layer.closeAll();
	 layer.load(2);
	 _this._Ajax('get',_this.ctx+"/biz/map/mapForEdit",{sceneId:_this.sceneId},'html',function(e){
		layer.closeAll('loading');
	    layer.open({
	    	id:'bizMapLayer',
	        title:'在线底图',
	        type: 1,
	        area: '300px',
	        content:e,
	        shade:0,
	        offset: 'r',
	        fixed:true,
	        move:false,
	        skin:'layui-layer layui-layer-adminRight',
		    content: e,
		    success: function (layero, dIndex) {
	            $(layero).children('.layui-layer-content').css('overflow', 'hidden');
              _this.sceneLayerMap.set('bizMapLayer',dIndex);
              _this.mapForTable(node);
			},
			end:function(){
              _this.sceneLayerMap.delete('bizMapLayer');
              _this.layerForEdit();
			}
   	});  
	});
}

/**
* 初始化表格
*/
VFGEditor.prototype.mapForTable=function(node){
	 var _this=this;
    // 底图
    var bizMapForEditorTb = table.render({
        elem: '#tableBizMapForEditor',
        url: _this.ctx+'/biz/map/listData', 
        page: true,
        toolbar: false,
        page: {
            layout: ['prev', 'page', 'next','count'] //自定义分页布局
            ,groups:5 //只显示 1 个连续页码
            ,first: false //不显示首页
            ,last: false //不显示尾页
        },
        where:{
        	sceneId:_this.sceneId,
        },
        cols: [[
            {type:'checkbox', event: 'checkbox'},
            {field: 'name', sort: false, title: '名称'},
        ]],
        done: function (res, curr, count) {
        }
    });
    
    table.on('checkbox(tableBizMapForEditor)', function (obj){
   	 if(node && node.type=='Maps'){
       	 _this.mapToLayer('map',{
             	id:obj.data.id,
             	status:obj.checked?'1':'0',
             	sceneId:_this.sceneId,
             	layerId:node.id,
             },function(){
             	bizMapForEditorTb.reload({}, 'data');
             });
   	 }else{
   		 layer.msg('请选择图层目录！！！', {icon: 3});
   	 }
    });
    
    form.on('submit(formSubSearchBizMap)', function (data) {
   	 bizMapForEditorTb.reload({where: data.field}, 'data');
    });
    form.on('submit(formSubClearBizMap)', function (data) {
   	 $('#edtSearchBizMap').val('');
   	 bizMapForEditorTb.reload({where: data.field}, 'data');
    });
}

VFGEditor.prototype.mapToLayer = function (type,param,callBack) {
	var _this=this;
	layer.load(2);
	param.type=type;
   _this.ajaxFunc(_this.ctx+"/biz/map/addMapToLayer",param, "json", function(res){
       layer.closeAll('loading');
       if (res.code == 200) {
           layer.msg(res.message, {icon: 1});
           if('map'==type){
           	if(callBack){
           		callBack();
           	}
           }
        	if('1'==param.status){
       		var data=res.data;
       		var maps=[data];
       		VFG.Provider.addImageryProvidersFromServ(_this.viewer,maps)
        	}else{
        		var data=res.data;
        		VFG.Util.removeScenePrimitive(_this.viewer,{
        			id:data.id,
        			type:data.type
        		})
        	}
       } else {
           layer.msg(res.message, {icon: 2});
       }
   	
   });
};

VFGEditor.prototype.addForImageryProvider = function () {
	var _this=this;

};

VFGEditor.prototype.addImageryProvidersFromServ = function (maps) {
	var _this=this;
	VFG.Provider.addImageryProvidersFromServ(_this.viewer,maps);
};

/**
 * 编辑图层
 */
VFGEditor.prototype.editProvider = function (treeNode) {
	var _this=this;
	 var _this=this;
	 if(_this.sceneLayerMap.has('bizMapLayer')){
		layer.msg('页面已经打开！！！', {icon: 3});
		return;
	 }	 
	 layer.load(2);
	 _this._Ajax('get',_this.ctx+"/biz/map/edit",{sceneId:_this.sceneId,layerId:treeNode.id},'html',function(e){
	 	layer.closeAll('loading');
	    layer.open({
	    	id:'bizMapLayer',
	        title:'编辑图层',
	        type: 1,
	        area: '400px',
	        content:e,
	        shade:0,
	        offset: 't',
	        fixed:true,
		    content: e,
		    success: function (layero, dIndex) {
	            $(layero).children('.layui-layer-content').css('overflow', 'hidden');
			},
			end:function(){
			}
	    });  
	});	
	
};





