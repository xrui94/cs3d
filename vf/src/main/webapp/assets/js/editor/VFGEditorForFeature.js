VFGEditor.prototype.drawFeature = function (node) {
	var _this=this;
	if(this.isDraw){
		console.log('已经处于绘制状态！！！');
		return;
	}
	_this.isDraw=true;
	this.drawPoint=new Cesium.DrawPoint(_this.viewer,{
		pick:function(e){
			var position=VFG.Util.getC3ToLnLa(_this.viewer,e);
			var primitive=VFG.Model.Feature.addFeature({
				id:VFG.Util.getUuid(),
				position:position,
				dimensions:{
					x:10,
					y:10,
					z:10,
				},
				type:'Feature',
			},null);
			if(primitive){
			    var formData={}; 
			    formData['bizModelFeature.id']=primitive?primitive.id:'';
			    formData['bizModelFeature.name']=primitive?primitive.name:'未定义';
			    formData['bizModelFeature.positionX']=position.x;
			    formData['bizModelFeature.positionY']=position.y;
			    formData['bizModelFeature.positionZ']=position.z;
			    formData['bizModelFeature.dimensionsX']=10;
			    formData['bizModelFeature.dimensionsY']=10;
			    formData['bizModelFeature.dimensionsZ']=10;
			    formData['bizModelFeature.layerId']=node.id;
			    formData['bizModelFeature.sceneId']=_this.sceneId;
			    var res = common_ajax.ajaxFunc(_this.ctx+"/biz/feature/save",formData, "json", null);
			    if (undefined!=res && 200==res.code) {
			    	var data=res.data;
			    	if(data){
			    		this.addImgLayerTreeNode(data,'Feature');
			    	}
			    }
			}
		},
		end:function(){
			_this.isDraw=false;
			_this.drawPoint=null;
		}
	});
};

/**
 * 保存点信息
 */
VFGEditor.prototype.featureForSave=function(primitive,position){
	var layerId=this.getImgLayerTreeIdForPoint();
    var formData={}; 
    formData['bizModelFeature.id']=primitive?primitive.id:'';
    formData['bizModelFeature.name']=primitive?primitive.name:'未定义';
    formData['bizModelFeature.positionX']=position.x;
    formData['bizModelFeature.positionY']=position.y;
    formData['bizModelFeature.positionZ']=position.z;
    formData['bizModelFeature.dimensionsX']=10;
    formData['bizModelFeature.dimensionsY']=10;
    formData['bizModelFeature.dimensionsZ']=10;
    formData['bizModelFeature.layerId']=layerId|| 'root';
    formData['bizModelFeature.sceneId']=this.sceneId;
    var res = common_ajax.ajaxFunc(ctx+"/biz/feature/save",formData, "json", null);
    if (undefined!=res && 200==res.code) {
    	var data=res.data;
    	if(data){
    		this.addImgLayerTreeNode(data,'Feature');
    	}
    }
}

/**
 * 编辑点信息
 */
VFGEditor.prototype.editFeature = function (featureId,name) {
	var _this=this;
	_this._Ajax('get',ctx+"/biz/feature/edit",{sceneId:_this.sceneId,featureId:featureId},'html',function(e){
		   var layerIndex= layer.open({
		    	id:'feature',
		        title: name || '编辑【'+name+'】点',
		        type: 1,
		        area: '300px',
		        content:e,
		        shade:0,
		        offset: 'r',
		        fixed:true,
		        move:false,
			    skin:'layui-layer',
			    content: e,
			    success: function (layero, dIndex) {
		            $(layero).children('.layui-layer-content').css('overflow', 'hidden');
                    colorpicker.render({
                        elem: '#modelFeatureNormalColorForDiv'
                        ,color:$("#modelFeatureNormalColor").val()
                        ,format: 'rgb'
                        ,predefine: true
                        ,alpha: true
                        ,done: function(color){
                          $('#modelFeatureNormalColor').val(color);
                          VFG.Model.Feature.setNormalColor(featureId,color)
                  	 	}   
                        ,change: function(color){
                            $('#modelFeatureNormalColor').val(color);
                            VFG.Model.Feature.setNormalColor(featureId,color)
                        }
                      });		            
		            
                    colorpicker.render({
                        elem: '#modelFeatureHoverColorForDiv'
                        ,color:$("#modelFeatureHoverColor").val()
                        ,format: 'rgb'
                        ,predefine: true
                        ,alpha: true
                        ,done: function(color){
                          $('#modelFeatureHoverColor').val(color);
                          VFG.Model.Feature.setHoverColor(featureId,color)
                  	 	}   
                        ,change: function(color){
                            $('#modelFeatureHoverColor').val(color);
                            VFG.Model.Feature.setHoverColor(featureId,color)
                        }
                      });
		           
                    
                    
		            
	            	slider.render({
	              	     elem: '#rotateForModelFeatureX-slide'
	              	    ,max:360
	              	    ,min:0
	              	    ,step:0.1
	              	    ,input: true //输入框
	              	    ,value:$("#rotateForModelFeatureX-input").val()*1 || 0
	             	    ,change: function(value){
	     	                $("#rotateForModelFeatureX-input").val(value);
	     	                var ops=form.val('modelBizModelFeatureForm');
	     	       	    	VFG.Model.Feature.rotateFeature(featureId,{
     	       	    		  x:ops.rotationX,
     	       	    		  y:ops.rotationY,
     	       	    	      z:ops.rotationZ
	     	       	    	});
	     	                
	             	    }
	              	});
	            	  
	            	slider.render({
	              	     elem: '#rotateForModelFeatureY-slide'
	              	    ,max:360
	              	    ,min:0
	              	    ,step:0.1
	              	    ,input: true //输入框
	              	    ,value:$("#rotateForModelFeatureY-input").val()*1 || 0
	             	    ,change: function(value){
	     	                $("#rotateForModelFeatureY-input").val(value);
	     	                var ops=form.val('modelBizModelFeatureForm');
	     	       	    	VFG.Model.Feature.rotateFeature(featureId,{
     	       	    		  x:ops.rotationX,
     	       	    		  y:ops.rotationY,
     	       	    	      z:ops.rotationZ
	     	       	    	});
	     	                
	             	    }
	              	});
	            	  
	            	slider.render({
	              	     elem: '#rotateForModelFeatureZ-slide'
	              	    ,max:360
	              	    ,min:0
	              	    ,step:0.1
	              	    ,input: true //输入框
	              	    ,value:$("#rotateForModelHeading-input").val()*1 || 0
	             	    ,change: function(value){
	     	                $("#rotateForModelFeatureZ-input").val(value);
	     	                var ops=form.val('modelBizModelFeatureForm');
	     	       	    	VFG.Model.Feature.rotateFeature(featureId,{
     	       	    		  x:ops.rotationX,
     	       	    		  y:ops.rotationY,
     	       	    	      z:ops.rotationZ
	     	       	    	});
	             	    }
	              	});
	            	  
	              	$('#positionForModelFeatureZ').on('input', function() {
     	                var ops=form.val('modelBizModelFeatureForm');
     	       	    	VFG.Model.Feature.translateFeature(featureId,{
 	       	    		  x:ops.positionX,
 	       	    		  y:ops.positionY,
 	       	    	      z:ops.positionZ
     	       	    	});

	            	}); 
	              	$('#dimensionsForModelFeatureX').on('input', function() {
     	                var ops=form.val('modelBizModelFeatureForm');
     	       	    	VFG.Model.Feature.setDimensions(featureId,{
 	       	    		  x:ops.dimensionsX,
 	       	    		  y:ops.dimensionsY,
 	       	    	      z:ops.dimensionsZ
     	       	    	});
	            	}); 
	              	$('#dimensionsForModelFeatureY').on('input', function() {
     	                var ops=form.val('modelBizModelFeatureForm');
     	       	    	VFG.Model.Feature.setDimensions(featureId,{
 	       	    		  x:ops.dimensionsX,
 	       	    		  y:ops.dimensionsY,
 	       	    	      z:ops.dimensionsZ
     	       	    	});
	            	}); 
	              	$('#dimensionsForModelFeatureZ').on('input', function() {
     	                var ops=form.val('modelBizModelFeatureForm');
     	       	    	VFG.Model.Feature.setDimensions(featureId,{
 	       	    		  x:ops.dimensionsX,
 	       	    		  y:ops.dimensionsY,
 	       	    	      z:ops.dimensionsZ
     	       	    	});
	            	}); 
	              	
	              	form.on('select(isView)', function (data) {
	              		VFG.Model.Feature.setDisplay(featureId,data.value)
	                });
	              	
		        	form.on('submit(modelBizModelFeatureSubmit)', function (data) {
		        	    layer.load(2);
		                var formData={}; 
		                for(var key in data.field){
		                	formData['bizModelFeature.'+key]=data.field[key];
		                }
		                var res = common_ajax.ajaxFunc(ctx+"/biz/feature/save",formData, "json", null);
		                layer.closeAll('loading');
		                if (200==res.code) {
		                	layer.close(dIndex);
		                    layer.msg(res.message, {icon: 1});
		                    var data=res.data;
		                    if(data){
			                	var zTree = $.fn.zTree.getZTreeObj("bizLayerTree")||sceneTree;
			                	if(zTree){
			                		var editNode =zTree.getNodeByParam("id",data.id);
			                		if(editNode){
			                			editNode.name=data.name;
			                			zTree.updateNode(editNode);
			                		}else{
			                			var treeNode =zTree.getNodeByParam("id",data.layerId);
			                			zTree.addNodes(treeNode?treeNode:null, {id:data.id, pId:data.layerId, name:data.name,type:'Feature'});
			                		}
			                	}
		                    }
		                } else {
		                    layer.msg(res.message, {icon: 2});
		                }
		        	    return false;
		        	});

				    $('#settingBizPointVisualAngle').click(function () {
				        var visualPos=VFG.Util.getVisualAngle(_this.viewer);
				        if(visualPos){
				            var formData={}; 
				            formData['bizPoint.id']=pointId;
				            formData['bizPoint.heading']=visualPos.heading;
				            formData['bizPoint.pitch']=visualPos.pitch;
				            formData['bizPoint.roll']=visualPos.roll;
				            formData['bizPoint.cameraX']=visualPos.position.x;
				            formData['bizPoint.cameraY']=visualPos.position.y;
				            formData['bizPoint.cameraZ']=visualPos.position.z;
				            var res = common_ajax.ajaxFunc(_this.ctx+"/biz/feature/save",formData, "json", null);
				            if (undefined!=res && 200==res.code) {
				            	 layer.msg(res.message);
				            }
				        }
				    });
		        	form.render(); 
				},
				end:function(){
				}
	    	});  
	});
};
