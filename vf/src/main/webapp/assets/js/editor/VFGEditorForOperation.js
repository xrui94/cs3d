/**
 * 编辑菜单
 */
VFGEditor.prototype.operateForMenu=function(entity,type){
	var _this=this;
	if(!_this.isDrag){
		_this.isDrag=true;
		_this._Ajax('get',_this.ctx+"/biz/operate/menu",{sceneId:this.sceneId,type:type},'html',function(e){
			var VFGWin; 
			layer.open({
			    type: 1,
			    title: false,
			    shade: 0,
			    content:e,
			    resize :false,
			    success: function (layero, dIndex) {
			        layui.$(layero).children('.layui-layer-content').css('overflow', 'visible'); 
			        
			        if('view'!=type){
			        	$('#toolForBizPointInfo').click(function () {
			        		layer.close(dIndex);
			        		_this.tipsForLayer(entity.id.id,entity.id.position.getValue());
			        		var height=VFG.Util.getSurfaceTerrainHeight(_this.viewer.scene, entity.id.position.getValue(), {}) ;
			        		 console.log(VFG.Provider.hasTerrain,height);
			        	});
			        	$('#toolForBizOperationDrag').click(function () {
			        		layer.close(dIndex);
			        		if('point'==type){
			        			_this.pointForDrag(entity); 
			        		}
			        		else if('line'==type){
				        		_this.polyLineForDrag(entity,entity.id.polyline.positions.getValue()); 
			        		}
			        		else if('polygon'==type){
				        		_this.polygonForDrag(entity,entity.id.polygon.hierarchy.getValue().positions); 
			        		}
			        		
			        		
			        	});	        	
			        	
			        	$('#toolForBizOperationEdit').click(function () {
			        		 layer.close(dIndex);
			        		 if('point'==type){
				        		 var curEntity=entity.id;
						    	 if(curEntity){
					    		    _this.pointForEdit(curEntity.id,curEntity.name);
						    	 }
			        		 }
			        		 else if('line'==type){
					  	    	 var curEntity=entity.id;
						    	 if(curEntity){
					    		    _this.polyLineForEdit(curEntity.id,curEntity.name);
						    	 }
			        		 }else if('polygon'==type){
					  	    	 var curEntity=entity.id;
						    	 if(curEntity){
					    		    _this.polygonForEdit(curEntity.id,curEntity.name);
						    	 }
			        		 }
			        	});	 
			        	
			        	$('#toolForBizOperationStyle').click(function () {
			        		 layer.close(dIndex);
			        		 if('point'==type){
					  	    	 var curEntity=entity.id;
						    	 if(curEntity){
					    		    _this.pointForStyle(curEntity.id);
						    	 }
			        		 }
			        		 else if('line'==type){
					  	    	 var curEntity=entity.id;
						    	 if(curEntity){
					    		    _this.polyLineForStyle(curEntity.id);
						    	 }
			        		 }else if('polygon'==type){
					  	    	 var curEntity=entity.id;
						    	 if(curEntity){
					    		    _this.polygonForStyle(curEntity.id);
						    	 }
			        		 }

			        	});	
			        	
			        	$('#toolForBizOperationDel').click(function () {
			        		 layer.close(dIndex);
			        		 if('point'==type || 'line'==type || 'polygon'==type){
					  	    	 var curEntity=entity.id;
						    	 if(curEntity){
					    		    _this.delImgLayerTreeNode({
						    		   id:curEntity.id,
						    		   name:curEntity.name,
						    	    });
						    	 }
			        		 }

			        	});
			        	
			        }
			        

		        	//单击
		        	$('#mouseLeftClickEvent').click(function () {
		        		 layer.close(dIndex);
		        		 if('view'==type){
		        			 _this.clickEvent(entity,'LEFTCLICK')
		        		 }else{
			        		 var curEntity=entity.id;
					    	 if(curEntity){
					    		 _this.clickEvent(curEntity.id,'LEFTCLICK')
					    	 }
		        		 }
		        	});
		        	
		        	//鼠标悬浮
		        	$('#mouseHoverEvent').click(function () {
		        		 layer.close(dIndex);
		        		 if('view'!=type){
			        		 var curEntity=entity.id;
					    	 if(curEntity){
					    		 _this.clickEvent(curEntity.id,'MOUSEHOVER')
					    	 }
		        		 }
		        	});
		        	
		        	
		        	
		        	//模拟鼠标单击
		        	$('#analogMouseLeftClickEvent').click(function () {
		        		 layer.close(dIndex);
		        		 if('point'==type){
		        			 var curEntity=entity.id;
				  	    	_this.analogForMouseLeftClick(curEntity.id,'LEFTCLICK',entity.id.position.getValue());
		        		 }
		        		 else if('line'==type){
				  	    	 var curEntity=entity.id;
					    	 if(curEntity){
				    		    _this.polyLineForStyle(curEntity.id);
					    	 }
		        		 }else if('polygon'==type){
		        			 var curEntity=entity.id;
				        	var position=VFG.Util.getCenterForPolygon(_this.viewer,entity.id.polygon.hierarchy.getValue().positions, 0);
		        			 _this.analogForMouseLeftClick(entity.id.id,'LEFTCLICK',position);
		        		 }
		        	});		        	
		        	
		        	
		        	if('point'==type){
		        		VFGWin=new Cesium.VFGWin(_this.viewer,{
				        	position:entity.id.position.getValue(),
				        	layero:layero,
				        	index:dIndex,
				        });
		        	}
		        	else if('polygon'==type){
		        		var position=VFG.Util.getCenterForPolygon(_this.viewer,entity.id.polygon.hierarchy.getValue().positions, 0);
		        		VFGWin=new Cesium.VFGWin(_this.viewer,{
				        	position:position,
				        	layero:layero,
				        	index:dIndex,
				        });
		        	}
			    },
			    end:function(){
			    	if(VFGWin){
			    		VFGWin.destroy();
			    		VFGWin=null;
			    	}
			    	_this.isDrag=false;
			    }
			});  
		});
	}
}

