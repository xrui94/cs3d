!function(e, t) {
	"object" == typeof exports && "object" == typeof module ? module.exports = t(require("Cesium"))
			: "function" == typeof define && define.amd ? define([ "Cesium" ],
					t)
					: "object" == typeof exports ? exports.CesiumVFGWin = t(require("Cesium"))
							: e.CesiumVFGWin = t(e.Cesium)
}("undefined" != typeof self ? self : this, function(cesium) {
	function _(viewer,option) {
		this.viewer = viewer;
		this.option = Cesium.defaultValue(option, Cesium.defaultValue.EMPTY_OBJECT);
		this.position=option.position;
		this.layerIndex=option.index;
		this.layero=option.layero;
		this.width;
		this.height;
		this.init();
	}
	_.prototype.init= function() {
		var _this=this;
		var cartesian2=Cesium.SceneTransforms.wgs84ToWindowCoordinates(_this.viewer.scene, _this.position);
		if (Cesium.defined(cartesian2)){
			_this.width=(layui.$(_this.layero).css('width').replace('px','')*1)/2;
			_this.height=(layui.$(_this.layero).css('height').replace('px','')*1)+30;
			layer.style(_this.layerIndex, {
				left:(cartesian2.x+200-_this.width)+'px',
				top:(cartesian2.y-_this.height)+'px'
			});
		}
    	_this.event=function(){
    		var cartesian2=Cesium.SceneTransforms.wgs84ToWindowCoordinates(_this.viewer.scene, _this.position);
    		if (Cesium.defined(cartesian2)){
        		layer.style(_this.layerIndex, {
    				left:(cartesian2.x+200-_this.width)+'px',
    				top:(cartesian2.y-_this.height)+'px'
        		});
    		}
    	}
    	_this.viewer.scene.postRender.addEventListener(_this.event);
	};
	_.prototype.destroy = function() {
		var _this = this;
		if(_this.event){
			_this.viewer.scene.postRender.removeEventListener(_this.event);
		}
		delete this.viewer,
		delete this.option,
		delete this.position,
		delete this.layerIndex;
		delete this.layero;
		delete this.width;
		delete this.height;
		return Cesium.destroyObject(this);
	};
	cesium.VFGWin= _;
})