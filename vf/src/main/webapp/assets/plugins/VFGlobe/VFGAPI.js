﻿///<jscompress sourcefile="VFGMap.js" />
VFG.Viewer=function(container, options){
	if(!container || !options){
		console.log('参数必填!!!');
		return;
	}
	
	this.POINT_STYLE=options.POINT_STYLE;//点样式集合
	this.LINE_STYLE=options.LINE_STYLE;//线样式集合
	this.POLYGON_STYLE=options.POLYGON_STYLE;//面样式集合
	this.EVENT_CONFIG=options.EVENT_CONFIG;//面样式集合
	
	this.container=container||''
	this.url=options.url;
	this.SCENE=options.scene||null;
	this.sceneId=this.SCENE.id||''
	this.geocoder=options.geocoder||false;
	this.complete=options.complete||null;
	this.progress=options.progress||null;
	this.providers=options.providers||null;
	this.viewer=null;
	this.scene=null;
	this.tip=null;
	this.ECEF =new Cesium.ECEF();
	return this.init();
}

VFG.Viewer.prototype.init=function(){
	var _this=this;
	_this.ellipsoid = Cesium.Ellipsoid.WGS84;
	var imageryProvider;
	if(this.providers){
		var p=this.getFirstProvider(this.providers);
		if(p){
			imageryProvider=VFG.Provider.packProvider(p);
			VFG.Provider.Map.set(p.id,imageryProvider);
			_this.cacheProviderMap.set(p.id,p);
		}
	}
	
	_this.globe = new VFG.Globe({
		domId: _this.container,
		geocoder: _this.geocoder,
		imageryProvider:imageryProvider
	});
	_this.viewer=_this.globe.viewer;
	_this.scene=_this.globe.viewer.scene;
	_this.tip=new VFG.Tip(_this.viewer);
	_this.handler = new Cesium.ScreenSpaceEventHandler(_this.viewer.scene.canvas);
	
	var CesiumViewerSceneController = _this.viewer.scene.screenSpaceCameraController;
    CesiumViewerSceneController.inertiaSpin = 0.1;
    CesiumViewerSceneController.inertiaTranslate = 0.1;
    CesiumViewerSceneController.inertiaZoom = 0.1;

    
    _this.addProviders(this.providers);
	
	if(this.complete){
		_this.flyToHome({
			complete:this.complete
		});
	}else{
		_this.flyToHome({
			complete:this.complete
		});	
	}
	return this;
}

VFG.Viewer.prototype.getFirstProvider=function(providers){
	var _this=this;
	var provider;
	if(providers){
		for(var i=0;i<providers.length;i++){
			if(providers[i].dataType=='map'){
				return providers[i];
			}
		}
	}
	return provider;
}

VFG.Viewer.prototype.flyToHome=function(option){
	var _this=this;
	if(_this.SCENE.cameraX && _this.SCENE.cameraX && _this.SCENE.cameraZ && _this.SCENE.heading && _this.SCENE.pitch && _this.SCENE.roll){
		var lnla=VFG.Util.getC3ToLnLa(_this.viewer, new Cesium.Cartesian3(_this.SCENE.cameraX*1,_this.SCENE.cameraY*1,_this.SCENE.cameraZ*1));
		this.viewer.camera.flyTo({
			destination:new Cesium.Cartesian3(_this.SCENE.cameraX*1,_this.SCENE.cameraY*1,_this.SCENE.cameraZ*1),
			orientation:{
		        heading : _this.SCENE.heading*1,
		        pitch : _this.SCENE.pitch*1,
		        roll : _this.SCENE.roll*1,
		    },
	        complete: function () {
	        	if(option.complete){
	        		option.complete(_this);
	        	}
	        },
	        cancle: function () {
	        	if(option.cancle){
	        		option.cancle(_this);
	        	}
	        },
		});
	}else{
    	if(option.complete){
    		option.complete(_this);
    	}
	}
}

VFG.Viewer.prototype.setViewToHome=function(){
	var _this=this;
	if(_this.SCENE.cameraX && _this.SCENE.cameraX && _this.SCENE.cameraZ && _this.SCENE.heading && _this.SCENE.pitch && _this.SCENE.roll){
		this.viewer.scene.camera.setView({
			destination:new Cesium.Cartesian3(_this.SCENE.cameraX*1,_this.SCENE.cameraY*1,_this.SCENE.cameraZ*1),
			orientation:{
		        heading : _this.SCENE.heading*1,
		        pitch : _this.SCENE.pitch*1,
		        roll : _this.SCENE.roll*1,
		    },
	        complete: function () {
	        	if(option.complete){
	        		option.complete(_this);
	        	}
	        },
	        cancle: function () {
	        	if(option.cancle){
	        		option.cancle(_this);
	        	}
	        },
		});
	}else{
    	if(option.complete){
    		option.complete(_this);
    	}
	}
}

VFG.Viewer.prototype.flyTo=function(option){
	var position=option.position;
	var orientation=option.orientation;
	var param={};
	if(position){
		param.destination=Cesium.Cartesian3.fromDegrees(position.x,position.y, position.z);
	}
	if(orientation){
		param.orientation={
	        heading : Cesium.Math.toRadians(orientation.heading*1),
	        pitch : Cesium.Math.toRadians(orientation.pitch*1),
	        roll : Cesium.Math.toRadians(orientation.roll*1),
	    };
	}
	this.viewer.camera.flyTo(param);
}

VFG.Viewer.prototype.setView=function(option){
	var position=option.position;
	var orientation=option.orientation;
	var param={};
	if(position){
		param.destination=Cesium.Cartesian3.fromDegrees(position.x,position.y, position.z);
	}
	if(orientation){
		param.orientation={
	        heading : Cesium.Math.toRadians(orientation.heading*1),
	        pitch : Cesium.Math.toRadians(orientation.pitch*1),
	        roll : Cesium.Math.toRadians(orientation.roll*1),
	    };
	}
	this.viewer.scene.camera.setView(param);
}

VFG.Viewer.prototype.setView=function(option){
	var position=option.position;
	var orientation=option.orientation;
	var param={};
	if(position){
		param.destination=Cesium.Cartesian3.fromDegrees(position.x,position.y, position.z);
	}
	if(orientation){
		param.orientation={
	        heading : Cesium.Math.toRadians(orientation.heading*1),
	        pitch : Cesium.Math.toRadians(orientation.pitch*1),
	        roll : Cesium.Math.toRadians(orientation.roll*1),
	    };
	}
	this.viewer.scene.camera.setView(param);
}

//漫游
VFG.Viewer.prototype.creatRoam=function(option){
	return new VFG.Roam(this.viewer,option);
}

//绕点旋转
VFG.Viewer.prototype.creatAroundPoint=function(option){
	return new VFG.AroundPoint(this.viewer,option);
}

VFG.Viewer.prototype.enableRotate=function(enabled){
	this.viewer.scene.screenSpaceCameraController.enableRotate = enabled;
}

/**
 * 平移
 */
VFG.Viewer.prototype.enableTranslate=function(enabled){
	this.viewer.scene.screenSpaceCameraController.enableTranslate = enabled;
}

/**
 * 放大和缩小
 */
VFG.Viewer.prototype.enableZoom=function(enabled){
	this.viewer.scene.screenSpaceCameraController.enableZoom = enabled;
}
/**
 * 倾斜相机
 */
VFG.Viewer.prototype.enableTilt=function(enabled){
	this.viewer.scene.screenSpaceCameraController.enableZoom = enabled;
}

VFG.Viewer.prototype.destroy=function(){
	var _this = this;
	try{
		_this.closeWebSocket();
		_this.handler.destroy();
		if(_this.handler3D){
			_this.handler3D.destroy();
		}
		_this.globe.destroy();
	}catch (e) {
	}
}
;
///<jscompress sourcefile="VFGMapCluster.js" />
VFG.Viewer.prototype.cacheClusterMap=new Map();

VFG.Viewer.prototype.addCluster=function(option){
	var _this=this;
	if(!_this.cacheClusterMap.has(option.layerId)){
		var points=option.points;
		var Layers=new Cesium.CustomDataSource(option.layerId);
		for(var i=0;i<points.length;i++){
			var point=points[i];
			var entity=VFG.Point.addEntity(_this.viewer,{
				point:point,
				style:point.defaultStyleId?_this.POINT_STYLE[point.defaultStyleId]:null
			},_this.url);
			Layers.entities.add(entity);
		}
		var cluster=new VFG.Layer.Cluster(_this.viewer,{
			Layers:Layers
		});
		_this.cacheClusterMap.set(option.layerId,cluster);
	}
}

VFG.Viewer.prototype.removeCluster=function(option){
	var _this=this;
	if(_this.cacheClusterMap.has(option.layerId)){
		_this.cacheClusterMap.get(option.layerId).destroy();
		_this.cacheClusterMap.delete(option.layerId);
	}
}
;
///<jscompress sourcefile="VFGMapEvent.js" />
/**
 * 鼠标左键单击拾取实体
 * complete(c2,data)：回调函数
 */
VFG.Viewer.prototype.LEFT_CLICK_PRIMITIVE=function(callback){
	var _this=this;
	_this.handler.setInputAction(function(movement) {
		var obj = _this.viewer.scene.pick(movement.position);
		if (Cesium.defined(obj) && obj.primitive && obj.id){
			 if (obj.primitive instanceof Cesium.ClassificationPrimitive){
		    	if(callback){
		    		callback(movement.position,obj.id.id);
		    	}
			 }
			 else if (obj.primitive instanceof Cesium.Label){
		    	if(callback){
		    		callback(movement.position,obj.primitive.id,'Point');
		    	}
			 }
			 else if (obj.primitive instanceof Cesium.PointPrimitive){
		    	if(callback){
		    		callback(movement.position,obj.primitive.id,'Point');
		    	}
			 }
			 else if (obj.primitive instanceof Cesium.Billboard){
		    	if(callback){
		    		callback(movement.position,obj.primitive.id,'Point');
		    	}
			 }
			 else if (obj.primitive instanceof Cesium.Primitive && obj.id && obj.id.PrimitiveType){
		    	if(callback){
		    		var primitive=obj.id;
		    		callback(movement.position,primitive.id,primitive.PrimitiveType);
		    		console.log(primitive);
		    	}
			 }
			 else if(obj.id && (obj.id.ellipse) ){
		    	if(callback){
		    		callback(movement.position,obj.id.id);
		    	}
			 }
			 else if(obj.id.polygon){
		    	if(callback){
		    		callback(movement.position,obj.id.id,'Polygon');
		    	}
			 }else if(obj.id.polyline){
		    	if(callback){
		    		callback(movement.position,obj.id.id,'Polyline');
		    	}
			 }
			 else if(obj.id.point ||obj.id.billboard || obj.id.label){
		    	if(callback){
			    	if(callback){
			    		callback(movement.position,obj.id.id,'Point',obj.id.name);
			    	}
		    	}
			 }			 
			 else if(obj.id.polyline){
		    	if(callback){
		    		callback(movement.position,obj.id.id,'Polyline');
		    	}
			 }
		}
	}, Cesium.ScreenSpaceEventType.LEFT_CLICK);
}

/**
 * 鼠标双击事件
 */
VFG.Viewer.prototype.LEFT_DOUBLE_CLICK_PRIMITIVE=function(callback){
	var _this=this;
	this.handler.setInputAction(function(movement) {
		var obj = _this.viewer.scene.pick(movement.position);
		if (Cesium.defined(obj) && obj.primitive && obj.id){
			 if (obj.primitive instanceof Cesium.ClassificationPrimitive){
		    	if(callback){
		    		callback(movement.position,obj.id.id);
		    	}
			 }
			 else if (obj.primitive instanceof Cesium.Label){
		    	if(callback){
		    		callback(movement.position,obj.primitive.id,'Point');
		    	}
			 }
			 else if (obj.primitive instanceof Cesium.PointPrimitive){
		    	if(callback){
		    		callback(movement.position,obj.primitive.id,'Point');
		    	}
			 }
			 else if (obj.primitive instanceof Cesium.Billboard){
		    	if(callback){
		    		callback(movement.position,obj.primitive.id,'Point');
		    	}
			 }
			 else if(obj.id && (obj.id.ellipse) ){
		    	if(callback){
		    		callback(movement.position,obj.id.id);
		    	}
			 }
			 else if(obj.id.polygon){
		    	if(callback){
		    		callback(movement.position,obj.id.id,'Polygon');
		    	}
			 }else if(obj.id.polyline){
		    	if(callback){
		    		callback(movement.position,obj.id.id,'Line');
		    	}
			 }
		}
	}, Cesium.ScreenSpaceEventType.LEFT_DOUBLE_CLICK);
}

/**
 * 鼠标右键
 */
VFG.Viewer.prototype.RIGHT_CLICK_PRIMITIVE=function(callback){
	var _this=this;
	this.handler.setInputAction(function(movement) {
		var obj = _this.viewer.scene.pick(movement.position);
		if (Cesium.defined(obj) && obj.primitive && obj.id){
			 if (obj.primitive instanceof Cesium.ClassificationPrimitive){
		    	if(callback){
		    		callback(movement.position,obj.id.id);
		    	}
			 }
			 else if (obj.primitive instanceof Cesium.Label){
		    	if(callback){
		    		callback(movement.position,obj.primitive.id,'Point');
		    	}
			 }
			 else if (obj.primitive instanceof Cesium.PointPrimitive){
		    	if(callback){
		    		callback(movement.position,obj.primitive.id,'Point');
		    	}
			 }
			 else if (obj.primitive instanceof Cesium.Billboard){
		    	if(callback){
		    		callback(movement.position,obj.primitive.id,'Point');
		    	}
			 }
			 else if(obj.id && (obj.id.ellipse) ){
		    	if(callback){
		    		callback(movement.position,obj.id.id);
		    	}
			 }
			 else if(obj.id.polygon){
		    	if(callback){
		    		callback(movement.position,obj.id.id,'Polygon');
		    	}
			 }else if(obj.id.polyline){
		    	if(callback){
		    		callback(movement.position,obj.id.id,'Line');
		    	}
			 }
		}
	}, Cesium.ScreenSpaceEventType.RIGHT_CLICK);
}


/**
 * 鼠标滑过效果
 */
VFG.Viewer.prototype.MOUSE_MOVE_PRIMITIVE=function(callback){
	var _this=this;
	var currentEntity = null;
	var lastEntity = null;
	this.handler.setInputAction(function(movement) {
		var obj = _this.viewer.scene.pick(movement.endPosition);
		if (Cesium.defined(obj) && obj.primitive && obj.id){
			 if ((obj.primitive instanceof Cesium.Label) || (obj.primitive instanceof Cesium.PointPrimitive)|| (obj.primitive instanceof Cesium.Billboard)){
				 if (Cesium.defined(lastEntity) && lastEntity.primitive && lastEntity.id){
					 if((lastEntity.primitive instanceof Cesium.Label) || (lastEntity.primitive instanceof Cesium.PointPrimitive)|| (lastEntity.primitive instanceof Cesium.Billboard)){
						 if(lastEntity.id==obj.id){
							 if(VFG.Point.Primitives.has(obj.id)){
								 var point=VFG.Point.getPrimitiveById(obj.id);
								 _this.tip.showAt(movement.endPosition, `<div class="con" style="color: white;">`+point.name+`</div>`);
							 }
						 }else{
							 if(VFG.Point.Primitives.has(obj.id)){
								 var point=VFG.Point.getPrimitiveById(obj.id);
								 if(_this.POINT_STYLE && _this.POINT_STYLE[point.hoverStyleId]){
									 if(!_this.selectPointMap.has(obj.primitive.id)){
										 _this.changePointPrimitiveStyle(obj.primitive.id,'HOVER');
									 }
								 }
								 _this.tip.showAt(movement.endPosition, `<div class="con" style="color: white;">`+point.name+`</div>`);
							 }
							 _this.MOUSE_OUT_PRIMITIVE(lastEntity);
						 }
					 }else{
						 if(VFG.Point.Primitives.has(obj.id)){
							 var point=VFG.Point.getPrimitiveById(obj.id);
							 if(_this.POINT_STYLE && _this.POINT_STYLE[point.hoverStyleId]){
								 _this.changePointPrimitiveStyle(obj.primitive.id,'HOVER');
							 }
							 _this.tip.showAt(movement.endPosition, `<div class="con" style="color: white;">`+point.name+`</div>`);
						 }
						 _this.MOUSE_OUT_PRIMITIVE(lastEntity);
					 }
				 }else{
					 if(VFG.Point.Primitives.has(obj.id)){
						 var point=VFG.Point.getPrimitiveById(obj.id);
						 if(_this.POINT_STYLE && _this.POINT_STYLE[point.hoverStyleId]){
							 _this.changePointPrimitiveStyle(obj.primitive.id,'HOVER');
						 }
						 _this.tip.showAt(movement.endPosition, `<div class="con" style="color: white;">`+point.name+`</div>`);
					 }
					 _this.MOUSE_OUT_PRIMITIVE(lastEntity);
				 }
			 }
			 //面
			 else if (obj.id.polygon){
				 if (Cesium.defined(lastEntity) && lastEntity.primitive && lastEntity.id){
					 if(lastEntity.id.polygon){
						 if(lastEntity.id.id==obj.id.id){
						 }else{
							 //_this.changePolygonEntityStyle(obj.id.id,'HOVER');
							 _this.MOUSE_OUT_PRIMITIVE(lastEntity);
						 }
					 }else{
						 //_this.changePolygonEntityStyle(obj.id.id,'HOVER');
						 _this.MOUSE_OUT_PRIMITIVE(lastEntity);
					 }
				 }else{
					 //_this.changePolygonEntityStyle(obj.id.id,'HOVER');
					 _this.MOUSE_OUT_PRIMITIVE(lastEntity);
				 }
				 if(obj.id.name){
					 _this.tip.showAt(movement.endPosition, `<div class="con" style="color: white;">`+obj.id.name+`</div>`);
				 }
			 }
			 else if (obj.id.polyline){
				 if(obj.id.name){
					 _this.tip.showAt(movement.endPosition, `<div class="con" style="color: white;">`+obj.id.name+`</div>`);
				 }
			 }
			 else{
				 _this.MOUSE_OUT_PRIMITIVE(lastEntity);
			 }
		}else{
			 _this.MOUSE_OUT_PRIMITIVE(lastEntity);
		}
		 lastEntity = obj;
	}, Cesium.ScreenSpaceEventType.MOUSE_MOVE);
}

VFG.Viewer.prototype.MOUSE_OUT_PRIMITIVE=function(lastEntity){
	var _this=this;
	if (lastEntity!=null && lastEntity.primitive && lastEntity.id) {
		 if (lastEntity.primitive instanceof Cesium.ClassificationPrimitive){
			 var feature=VFG.Model.Feature.getFeatureById(lastEntity.id);
			 if(feature){
				 feature.leave();
			 }
		 }
		 else if ((lastEntity.primitive instanceof Cesium.Label) || (lastEntity.primitive instanceof Cesium.PointPrimitive)|| (lastEntity.primitive instanceof Cesium.Billboard)){
			 if(!_this.selectPointMap.has(lastEntity.primitive.id)){
				 _this.changePointPrimitiveStyle(lastEntity.primitive.id,'DEFAULT');
			 }
			 _this.tip.setVisible(false);
		 }
		 else if(lastEntity.id.polygon){
			 _this.tip.setVisible(false);
			 if(!_this.selectPolygonMap.has(lastEntity.id.id)){
				 _this.changePolygonEntityStyle(lastEntity.id.id,'DEFAULT');
			 }
		 }
		 else if(lastEntity.id.polyline){
			 if(!_this.selectPolylineMap.has(lastEntity.id.id)){
				 _this.changePolylineEntityStyle(lastEntity.id.id,'DEFAULT');
			 }
			 _this.tip.setVisible(false);
		 }
		 lastEntity=null;
	}
}

;
///<jscompress sourcefile="VFGMapLayer.js" />
VFG.Viewer.prototype.showOrHideLayer=function(option){
	var _this=this;
	var type=option.type;
	var state=option.state;
	if('Point'==type || 'point'==type || 'Points'==type){
		_this.showOrHidePointBySceneMenu(option);
	}
	else if('Polyline'==type ||  'Polylines'==type){
		_this.showOrHidePolylineBySceneMenu(option);
	}
	else if('Polygon'==type || 'polygon'==type || 'Polygons'==type){
		_this.showOrHidePolygonBySceneMenu(option);
	}
	else if('Models'==type || 'GLTF'==type || '3DTILES'==type || 'Model'==type){
		_this.showOrHideModelBySceneMenu(option);
	}
	else if('Maps'==type || 'terrain'==type || 'division'==type || 'Map'==type || 'map'==type || 'road'==type ){
		_this.showOrHideProviderBySceneMenu(option);
	}
}

VFG.Viewer.prototype.showOrHideLayerByCode=function(option){
	var _this=this;
	var menu=_this.getSceneMenuByCode(option.code);
	if(menu){
		option.sceneMenuId=menu.id;
		option.type=menu.type;
		_this.showOrHideLayer(option)
	}
}

/**
 * 根据页面code获取menu
 */
VFG.Viewer.prototype.getSceneMenuByCode=function(code){
	var _this=this;
	//console.log(_this.getRelationshipByPrimitiveId(code,"5732d9fcc8934f7ba1833539ab17b016",'point'));
	return _this.SCENE_MENU_MAP_CODE[code];
}

VFG.Viewer.prototype.getSceneMenuById=function(id){
	var _this=this;
	return _this.SCENE_MENU_MAP_ID[id];
}

VFG.Viewer.prototype.getSceneMenuByRelationship=function(code){
	var _this=this;
	return _this.SCENE_MENU_MAP_RELATIONSHIP[code];
}

VFG.Viewer.prototype.changePrimitiveStyle=function(id,event,type){
	var _this=this;
	if('Point'==type){
		_this.changePointPrimitiveStyle(id,event)
	}
	else if('Polyline'==type){
		_this.changePolylineEntityStyle(id,event)
	}
	else if('Polygon'==type){
		_this.changePolygonEntityStyle(id,event)
	}
	else if('Model'==type){
	}
	else if('Map'==type){
	}
	else if('VideoPlane'==type){
	}
}

VFG.Viewer.prototype.getPrimitiveById=function(id,type){
	var _this=this;
	if('Point'==type){
		if(_this.cacheMessageMap.has(id)){
			return _this.cacheMessageMap.get(id);
		}else{
			return VFG.Point.getPrimitiveById(id);
		}
	}
	else if('Polyline'==type){
		return VFG.Polyline.getPrimitiveById(id);
	}
	else if('Polygon'==type){
		return VFG.Polygon.getPrimitiveById(id);
	}
	else if('Model'==type){
		return VFG.Model.getPrimitiveById(id);
	}
	else if('Map'==type){
		return VFG.Provider.getPrimitiveById(id);
	}
	else if('VideoPlane'==type){
		return _this.getVPlaneById(id);
	}
	else if('VideoModel'==type){
		return _this.getVModelById(id);
	}
}

/**
 * 获取管理资源
 */
VFG.Viewer.prototype.getPrimitiveRelationshipByCode=function(code,relationshipCode,type){
	var _this=this;
	//获取页面
	var page=_this.SCENE_MENU_MAP_CODE[code];
	if(page){
		var relationshipMap=_this.SCENE_MENU_MAP_RELATIONSHIP[page.id];
		if(relationshipMap){
			var list=relationshipMap[relationshipCode];
			if(type){
				if(list){
					for(var i=0;i<list.length;i++){
						if(list[i].type && type.toUpperCase()== list[i].type.toUpperCase()){
							if(list[i].type && type.toUpperCase()=='POINT'){
								return VFG.Point.getPrimitiveById(list[i].layerId);
							}
							else if(list[i].type && type.toUpperCase()=='POLYGON'){
								return VFG.Polygon.getPrimitiveById(list[i].layerId);
							}
							else if(list[i].type && type.toUpperCase()=='LINE'){
								return VFG.Polyline.getPrimitiveById(list[i].layerId);
							}
							else if((list[i].type && type.toUpperCase()=='MODEL') ||  (list[i].type && type.toUpperCase()=='GLTF') || (list[i].type && type.toUpperCase()=='3DTILES')){
								return VFG.Model.getPrimitiveById(list[i].layerId);
							}
							else{
								return list[i];
							}
						}
					}
				}
			}else{
				return list;
			}
		}
	}
}

VFG.Viewer.prototype.getRelationshipByPrimitiveId=function(code,id,type){
	var _this=this;
	var page=_this.SCENE_MENU_MAP_CODE[code];
	if(page){
		var layers=_this.SCENE_MENU_MAP_LAYER[page.id];
		if(layers){
			var menu=layers[id];
			if(menu){
				return _this.getPrimitiveRelationshipByCode(code,menu.relationship,type)
			}
		}
	}
}

VFG.Viewer.prototype.getRelationshipByPrimitiveId=function(code,id,type){
	var _this=this;
	var page=_this.SCENE_MENU_MAP_CODE[code];
	if(page){
		var layers=_this.SCENE_MENU_MAP_LAYER[page.id];
		if(layers){
			var menu=layers[id];
			if(menu){
				return _this.getPrimitiveRelationshipByCode(code,menu.relationship,type)
			}
		}
	}
}

VFG.Viewer.prototype.getRelationshipByPrimitiveId=function(code,id,type){
	var _this=this;
	var page=_this.SCENE_MENU_MAP_CODE[code];
	if(page){
		var layers=_this.SCENE_MENU_MAP_LAYER[page.id];
		if(layers){
			var menu=layers[id];
			if(menu){
				return _this.getPrimitiveRelationshipByCode(code,menu.relationship,type)
			}
		}
	}
}

/**
 * 移除对象
 * id:图层id
 * type：类型
 */
VFG.Viewer.prototype.removePrimitiveById=function(id,type){
	if('Point'==type){
		this.removePointPrimitiveById(id);
	}
	else if('Model'==type){
		this.removeModelById(id);
	}
	else if('Polyline'==type){
		this.removePolylineById(id);
	}
	else if('Polygon'==type){
		this.removePolygonById(id);
	}
	else if('Feature'==type){
		this.removeFeatureById(id);
	}
	else if('VideoPlane'==type){
		return this.removeVPlaneById(id);
	}
	else if('VideoShed3d'==type){
		return this.removeVideoShed3dById(id);
	}
	else if('VideoModel'==type){
		return this.removeVModelById(id);
	}
}




;
///<jscompress sourcefile="VFGMapListener.js" />
/**
 * 监听集合
 */
VFG.Viewer.prototype.ListenerMap=new Map();
/**
 * 监听屏幕坐标
 * 	bizPoint
 *  callBack:fun(c2)回调函数
 */
VFG.Viewer.prototype.addListenerScreenPosition  = function(bizPoint,callBack) {
	if(!bizPoint)return;
	if(!this.ListenerMap.has(bizPoint.id)){
		var option = {
			position:{
		  		x:bizPoint.x*1,
		  		y:bizPoint.y*1,
		  		z:bizPoint.z*1
			},
			id:bizPoint.id,
			callBack:callBack
		}
		var listener=new VFG.Listener(this.viewer,option);
		this.ListenerMap.set(bizPoint.id,listener);
	}
}

/**
 * 移出
 */
VFG.Viewer.prototype.removeListenerScreenPosition= function(id) {
	if(this.ListenerMap.has(id)){
		var listener=this.ListenerMap.get(id).destroy();
		this.ListenerMap.delete(id)
	}
}

VFG.Viewer.prototype.addListenerCamera=function(callback){
	var _this=this;
	_this.removeListenerCamera();
	_this.handler3D = new Cesium.ScreenSpaceEventHandler(_this.viewer.scene.canvas);
	var viewer=this.viewer;
	_this.handler3D.setInputAction(function(movement) {
	    var pick= new Cesium.Cartesian2(movement.endPosition.x,movement.endPosition.y);
	    if(pick){
	        var cartesian = _this.viewer.scene.globe.pick(_this.viewer.camera.getPickRay(pick), viewer.scene);
	        if(cartesian){
	            // 世界坐标转地理坐标（弧度）
	            var cartographic = _this.viewer.scene.globe.ellipsoid.cartesianToCartographic(cartesian);
	            if(cartographic){
	                // 海拔
	                var height = _this.viewer.scene.globe.getHeight(cartographic);
	                // 视角海拔高度
	                var he = Math.sqrt(_this.viewer.scene.camera.positionWC.x * _this.viewer.scene.camera.positionWC.x + _this.viewer.scene.camera.positionWC.y * _this.viewer.scene.camera.positionWC.y + viewer.scene.camera.positionWC.z * _this.viewer.scene.camera.positionWC.z);
	                var he2 = Math.sqrt(cartesian.x * cartesian.x + cartesian.y * cartesian.y + cartesian.z * cartesian.z);
	                // 地理坐标（弧度）转经纬度坐标
	                var point=[ cartographic.longitude / Math.PI * 180, cartographic.latitude / Math.PI * 180];
	                if(!height){
	                    height = 0;
	                }
	                if(!he){
	                    he = 0;
	                }
	                if(!he2){
	                    he2 = 0;
	                }
	                if(!point){
	                    point = [0,0];
	                }
	                var level=0;
                    var tilesToRender = _this.viewer.scene.globe._surface._tilesToRender;
                    if(tilesToRender.length>0){
                    	level= tilesToRender[0].level;
                    }else{
                    	level=0;
                    }
                    
                    var heading = Cesium.Math.toDegrees(_this.viewer.camera.heading).toFixed(2)*1;;
                    var pitch = Cesium.Math.toDegrees(_this.viewer.camera.pitch).toFixed(2)*1;;
                    var roll = Cesium.Math.toDegrees(_this.viewer.camera.roll).toFixed(2)*1;;
                    
                    if(callback){
                    	callback({
                    		position:{
                    			x:point[0].toFixed(6),
                    			y:point[1].toFixed(6),
                    			z:height.toFixed(2)
                    		},
                    		zoom:level,
                    		camera:{
                    			heading:heading,
                    			pitch:pitch,
                    			roll:roll,
                    		}
                    	});
                    }
	            }
	        }
	    }
	    
	}, Cesium.ScreenSpaceEventType.MOUSE_MOVE);
}
VFG.Viewer.prototype.removeListenerCamera=function(){
	var _this=this;
	if(_this.handler3D){
		_this.handler3D.destroy();
	}
}
;
///<jscompress sourcefile="VFGMapMessage.js" />
VFG.Viewer.prototype.cacheEventMap=new Map();
VFG.Viewer.prototype.cacheMessageMap=new Map();
VFG.Viewer.prototype.filterMessageMap=new Map();
VFG.Viewer.prototype.layerMessageMap=new Map();
/**
 * 添加控制图层Key
 */
VFG.Viewer.prototype.addLayerMessage=function(key,value){
	var _this=this;
	if(!this.layerMessageMap.has(key)){
		this.layerMessageMap.set(key,value);
		_this.newMessageMap().forEach(function(value,key){
			_this.addMessageEventPoint(value);
		});
	}
}


VFG.Viewer.prototype.newMessageMap=function(){
	var _this=this;
	var newMap = new Map();
	_this.cacheEventMap.forEach(function(value,key){
		newMap.set(key,value);
	});
	return newMap;
}


/**
 * 移除控制图层Key
 */
VFG.Viewer.prototype.removeLayerMessage=function(key){
	var _this=this;
	if(this.layerMessageMap.has(key)){
		this.layerMessageMap.delete(key);
		_this.newMessageMap().forEach(function(value,key){
			_this.addMessageEventPoint(value);
		});
	}
}

/**
 * 批量移除控制图层Key
 */
VFG.Viewer.prototype.removeAllLayerMessage=function(){
	var _this=this;
	_this.layerMessageMap.clear();
	_this.cacheMessageMap.forEach(function(value,key){
		VFG.Point.removePrimitiveById(key);
		_this.viewer.entities.removeById(key);
	});
	_this.cacheMessageMap.clear();
}

/**
 * 添加过滤条件
 */
VFG.Viewer.prototype.addFilterMessage=function(key,value){
	var _this=this;
	if(!this.filterMessageMap.has(key)){
		this.filterMessageMap.set(key,value);
		_this.newMessageMap().forEach(function(value,key){
			_this.addMessageEventPoint(value);
		});
	}
}

/**
 * 移除过滤条件
 */
VFG.Viewer.prototype.removeFilterMessage=function(key){
	var _this=this;
	if(this.filterMessageMap.has(key)){
		this.filterMessageMap.delete(key);
		_this.newMessageMap().forEach(function(value,key){
			_this.addMessageEventPoint(value);
		});
	}
}

/**
 * 批量移除控制图层Key
 */
VFG.Viewer.prototype.removeAllFilterMessage=function(){
	var _this=this;
	this.filterMessageMap.clear();
	_this.newMessageMap().forEach(function(value,key){
		_this.addMessageEventPoint(value);
	});
}


/**
 * 处理消息
 */
VFG.Viewer.prototype.handleMessage=function(event){
	var _this=this;
	var result=JSON.parse(event.data);
	if(result && result.code=='event'){
		var data=result.data;
		if(data){
			_this.addMessageEventPoint(data);
		}
	}
}

VFG.Viewer.prototype.addMessageEventPoint=function(data){
	var _this=this;
	if(data){
		var id=data.id||null;
		var level=data.level||null;
		var show=data.show||false;
		var code=data.code||null;
		var state=data.state||null;
		var type=data.type||null;
		var event=data.event||null;
		_this.cacheEventMap.delete(data.id);
		_this.cacheEventMap.set(data.id,data);
		if('Point'==type && event){
			if(event.x && event.x.length>0 && event.y && event.y.length>0 && event.z && event.z.length>0 ){
				VFG.Point.removePrimitiveById(event.id);
				_this.cacheMessageMap.delete(event.id) ;
				_this.viewer.entities.removeById(event.id)
				
				if(!show){
					return;
				}
				
				var filters=event.filters || null;
				var layerCode=event.layerCode || null;
				if(_this.filterMessageMap.size>0){
					if(filters && filters.length>0){
						_this.filterMessageMap.forEach(function(value,key){
							if(filters.indexOf(key)!=-1){
								if(_this.layerMessageMap.size>0 && _this.layerMessageMap.has(layerCode)){
									var key=code+state;
									var defaultStyleId='';
									var hoverStyleId='';
									var selectedStyleId='';
									var color=Cesium.Color.RED;
									if(_this.EVENT_CONFIG[key]){
										defaultStyleId=_this.EVENT_CONFIG[key].defaultStyleId;
										hoverStyleId=_this.EVENT_CONFIG[key].hoverStyleId;
										selectedStyleId=_this.EVENT_CONFIG[key].selectedStyleId;
										color=_this.EVENT_CONFIG[key].color? Cesium.Color.fromCssColorString(_this.EVENT_CONFIG[key].color) :Cesium.Color.RED;
									}
									var point={
										id:event.id||id,
										name:event.name||'',
										code:event.layerCode|| '',
										state:state,
										x:event.x*1,
										y:event.y*1,
										z:event.z*1,
										defaultStyleId:defaultStyleId,
										hoverStyleId:hoverStyleId,
										selectedStyleId:selectedStyleId,
										code:code
									};
									
									if('1'==level){
										var position= Cesium.Cartesian3.fromDegrees(event.x*1,event.y*1,event.z*1);
										_this.getEventMessagePoint(event.id,event.name,position,color);
										if(defaultStyleId){
											VFG.Point.addPrimitive(_this.viewer,{
												point:point,
												style:_this.POINT_STYLE[defaultStyleId]
											},_this.url);
										}
									}else{
										VFG.Point.addPrimitive(_this.viewer,{
											point:point,
											style:_this.POINT_STYLE[defaultStyleId]
										},_this.url);
									}
									_this.cacheMessageMap.set(event.id,point);
								}
							}
						});
					}
				}else{
					if(_this.layerMessageMap.size>0 && _this.layerMessageMap.has(layerCode)){
						var key=code+state;
						var defaultStyleId='';
						var hoverStyleId='';
						var selectedStyleId='';
						var color=Cesium.Color.RED;
						if(_this.EVENT_CONFIG[key]){
							defaultStyleId=_this.EVENT_CONFIG[key].defaultStyleId;
							hoverStyleId=_this.EVENT_CONFIG[key].hoverStyleId;
							selectedStyleId=_this.EVENT_CONFIG[key].selectedStyleId;
							color=Cesium.Color.fromCssColorString(_this.EVENT_CONFIG[key].color);
						}
						var point={
							id:event.id||id,
							name:event.name||'',
							code:event.layerCode|| '',
							state:state,
							x:event.x*1,
							y:event.y*1,
							z:event.z*1,
							defaultStyleId:defaultStyleId,
							hoverStyleId:hoverStyleId,
							selectedStyleId:selectedStyleId,
							code:code
						};
						if('1'==level){
							var position= Cesium.Cartesian3.fromDegrees(event.x*1,event.y*1,event.z*1);
							_this.getEventMessagePoint(event.id,event.name,position,color);
							if(defaultStyleId){
								VFG.Point.addPrimitive(_this.viewer,{
									point:point,
									style:_this.POINT_STYLE[defaultStyleId]
								},_this.url);
							}
						}else{
							VFG.Point.addPrimitive(_this.viewer,{
								point:point,
								style:_this.POINT_STYLE[defaultStyleId]
							},_this.url);
						}
						_this.cacheMessageMap.set(event.id,point);
					}

				}
			}
		}
	}
}

VFG.Viewer.prototype.removeAllEventMessage=function(){
	var _this=this;
	_this.cacheMessageMap.forEach(function(value,key){
		VFG.Point.removePrimitiveById(key);
		_this.viewer.entities.removeById(key)
	});
	_this.cacheMessageMap.clear();
	_this.cacheEventMap.clear();
	_this.filterMessageMap.clear();
	_this.layerMessageMap.clear();
}

VFG.Viewer.prototype.getEventMessagePoint=function(id,name,position,color){
	var _this=this;
	var x=1;
	var flog=true;
	_this.viewer.entities.removeById(id);
	_this.viewer.entities.add({
		id:id,
		name:name,
		position:position,
		point : {
			show : true,
			color :new Cesium.CallbackProperty(function () {
				if(flog){
					x=x-0.05;
					if(x<=0){
						flog=false;
					}
					}else{
						x=x+0.05;
						if(x>=1){
						flog=true;
						}
					}
					return color.withAlpha(x);
				},false),
			pixelSize : 20, // default: 1
			outlineWidth :0
		}
	});
}




;
///<jscompress sourcefile="VFGMapModel.js" />
VFG.Viewer.prototype.cacheModelMap=new Map();

/**
 * 移除所有
 */
VFG.Viewer.prototype.removeAllModels=function(){
	var _this=this;
	_this.cacheModelMap.forEach(function(value,key){
		VFG.Model.removeById(key);
	});
	_this.cacheModelMap.clear();
}

/**
 * 添加模型
 */
VFG.Viewer.prototype.addModel=function(model){
	var _this=this;
	VFG.Model.addModel(_this.viewer,model,_this.LocalModelUrl,_this.aliveLocalService);
	_this.cacheModelMap.set(model.id,model);
} 

/**
 * 批量添加
 */
VFG.Viewer.prototype.addModels=function(list){
	var _this=this;
	 for(var i=0;i<list.length;i++){
		 var model=list[i]; 
		 _this.addModel(model);
	 }
} 

VFG.Viewer.prototype.removeModels=function(list){
	var _this=this;
	for(var i=0;i<list.length;i++){
		var model=list[i]; 
		_this.removeModelById(model.id);
	}
}

VFG.Viewer.prototype.removeModel=function(model){
	var _this=this;
	_this.removeModelById(model.id);
}

/**
 * 移除模型
 * id：模型id
 */
VFG.Viewer.prototype.removeModelById=function(id){
	var _this=this;
	VFG.Model.removeById(id);
	_this.cacheModelMap.delete(id);
} 

VFG.Viewer.prototype.removeAllModels=function(){
	var _this=this;
	_this.cacheModelMap.forEach(function(value,key){
		VFG.Model.removeById(key);
	});
	_this.cacheModelMap.clear();
}

VFG.Viewer.prototype.flyToModelById=function(id,complete){
	if(this.cacheModelMap.has(id)){
		this.flyToModel(this.cacheModelMap.get(id))
	}
	if(complete){
		complete(this);
	}
}

VFG.Viewer.prototype.setViewModelById=function(id,complete){
	if(this.cacheModelMap.has(id)){
		this.setViewModel(this.cacheModelMap.get(id))
	}
	if(complete){
		complete(this);
	}
}

VFG.Viewer.prototype.flyToModel=function(model,complete){
//	this.viewer.camera.flyTo({
//		destination:Cesium.Cartesian3.fromDegrees(model.posX*1,model.posY*1,model.posZ*1+10000||10000)
//	});
//	if(complete){
//		complete(this);
//	}
}

VFG.Viewer.prototype.setViewModel=function(model,complete){
	if(model.cameraX && model.cameraX && model.cameraZ && model.heading && model.pitch && model.roll){
		this.viewer.scene.camera.setView({
			destination:new Cesium.Cartesian3(model.cameraX*1,model.cameraY*1,model.cameraZ*1),
			orientation:{
		        heading : model.heading*1,
		        pitch : model.pitch*1,
		        roll : model.roll*1,
		    }
		});
		if(complete){
			complete(this);
		}
	}else{
		this.viewer.scene.camera.setView({
			destination:Cesium.Cartesian3.fromDegrees(model.posX*1,model.posY*1,model.posZ*1+10000||10000)
		});
		if(complete){
			complete(this);
		}
	}
}
;
///<jscompress sourcefile="VFGMapPoint.js" />

/*****************************************************************控制页面**************************************************************************/
VFG.Viewer.prototype.cachePointMap=new Map();
VFG.Viewer.prototype.selectPointMap=new Map();


/**
 * 添加点
 */
VFG.Viewer.prototype.addPointPrimitive=function(option){
	var _this=this;
	VFG.Point.addPrimitive(_this.viewer,{
		point:option,
		style:option.defaultStyleId?_this.POINT_STYLE[option.defaultStyleId]:null
	},_this.url);
	_this.cachePointMap.set(option.id,option);
}

VFG.Viewer.prototype.addPointPrimitives=function(list){
	var _this=this;
	 for(var i=0;i<list.length;i++){
		 var point=list[i];
		 _this.addPointPrimitive(point);
	 }
}

VFG.Viewer.prototype.removePointPrimitives=function(list){
	var _this=this;
	 for(var i=0;i<list.length;i++){
		 var point=list[i];
		 _this.removePointPrimitiveById(point.id);
	 }
}

VFG.Viewer.prototype.showOrHidePoints=function(option){
	var _this=this;
} 

VFG.Viewer.prototype.showOrHidePoint=function(option){
	var _this=this;
}

/**
 * 移除全部
 */
VFG.Viewer.prototype.removeAllPoint=function(layerId){
	if(this.cachePointMap.has(layerId)){
		var points=this.cachePointMap.get(layerId);
		for(var i=0;i<points.length;i++){
			VFG.Point.removePrimitiveById(points[i].id);
		}
		this.cachePointMap.delete(layerId);
	}
}

/**
 * 获取渲染点
 */
VFG.Viewer.prototype.getScenePrimitiveById=function(id){
	if(VFG.Point.LabelMap.has(id)){
		return VFG.Point.LabelMap.get(id);
	}
	if(VFG.Point.PointMap.has(id)){
		return VFG.Point.PointMap.get(id)
	}
	if(VFG.Point.BillboardMap.has(id)){
		return VFG.Point.BillboardMap.get(id)
	}
}

/**
 * 移除点
 */
VFG.Viewer.prototype.removePointPrimitiveById=function(id){
	VFG.Point.removePrimitiveById(id);
	this.cachePointMap.delete(id);
	this.selectPointMap.delete(id);
}

/**
 * 移除点
 */
VFG.Viewer.prototype.removePointPrimitive=function(point){
	VFG.Point.removePrimitiveById(point.id);
	this.cachePointMap.delete(point.id);
	this.selectPointMap.delete(point.id);
}

/**
 * 更改样式
 * id:点id
 * event: DEFAULT(默认) HOVER(悬浮) SELECTED（选中）
 */
VFG.Viewer.prototype.changePointPrimitiveStyle=function(id,event){
	var _this=this;
	 var point=VFG.Point.getPrimitiveById(id);
	 if(point!=null){
		 if('DEFAULT'==event){
			 VFG.Point.removePrimitiveById(point.id);
			 VFG.Point.addPrimitive(_this.viewer,{
				point:point,
				style:_this.POINT_STYLE[point.defaultStyleId]
			 },_this.url);
			 _this.selectPointMap.delete(id);
		 }
		 else if('HOVER'==event && point.hoverStyleId){
			 VFG.Point.removePrimitiveById(point.id);
			 VFG.Point.addPrimitive(_this.viewer,{
				point:point,
				style:_this.POINT_STYLE[point.hoverStyleId]
			 },_this.url);
		 }
		 else if('SELECTED'==event && point.selectedStyleId){
			 VFG.Point.removePrimitiveById(point.id);
				VFG.Point.addPrimitive(_this.viewer,{
					point:point,
					style:_this.POINT_STYLE[point.selectedStyleId]
				},_this.url);
				_this.selectPointMap.set(id,point);
		 }else{
			 VFG.Point.removePrimitiveById(point.id);
			 VFG.Point.addPrimitive(_this.viewer,{
				point:point,
				style:_this.POINT_STYLE[point.defaultStyleId]
			 },_this.url);
			 _this.selectPointMap.delete(id);
		 }
	 }
}

/**
 * 切换
 */
VFG.Viewer.prototype.togglePointPrimitiveStyle=function(id){
	var _this=this;
	_this.selectPointMap.forEach(function(point,key){
		 VFG.Point.removePrimitiveById(key);
		 VFG.Point.addPrimitive(_this.viewer,{
			point:point,
			style:_this.POINT_STYLE[point.defaultStyleId]
		 },_this.url);
	});
	_this.selectedPointPrimitive(id);
}

/**
 * 设置点坐标
 * id：点位id
 * position：{
 * 	x:维度
 * 	y:经度
 * 	z:高度
 * }
 */
VFG.Viewer.prototype.setPointPrimitivePosition=function(id,position){
	var _this=this;
	var point=VFG.Point.getPrimitiveById(id);
	if(point){
		 VFG.Point.removePrimitiveById(id);
		 point.x=position.x;
		 point.y=position.y;
		 point.z=position.z;
		 if(_this.selectPointMap.has(id)){
			 VFG.Point.addPrimitive(_this.viewer,{
					point:point,
					style:_this.POINT_STYLE[point.selectedStyleId]
				 },_this.url);
		 }else{
			 VFG.Point.addPrimitive(_this.viewer,{
				point:point,
				style:_this.POINT_STYLE[point.defaultStyleId]
			 },_this.url);
		 }
		 _this.cachePointMap.set(id,point);
	}
}

/**
 *  选中
 *  id:点id
 */
VFG.Viewer.prototype.selectedPointPrimitive=function(id){
	var _this=this;
	var point=VFG.Point.getPrimitiveById(id);
	if('SELECTED'==event && point.selectedStyleId){
		VFG.Point.removePrimitiveById(point.id);
		VFG.Point.addPrimitive(_this.viewer,{
			point:point,
			style:_this.POINT_STYLE[point.selectedStyleId]
		},_this.url);
		_this.selectPointMap.set(id,point);
	 }
}

/**
 *  取消选中
 *  id:点id
 */
VFG.Viewer.prototype.unselectedPointPrimitive=function(id){
	var _this=this;
	var point=VFG.Point.getPrimitiveById(id);
	if(point){
		 VFG.Point.removePrimitiveById(point.id);
		 VFG.Point.addPrimitive(_this.viewer,{
			point:point,
			style:_this.POINT_STYLE[point.defaultStyleId]
		 },_this.url);
		 _this.selectPointMap.delete(id);
	}
}

VFG.Viewer.prototype.flyToPointById=function(id,complete){
	if(this.cachePointMap.has(id)){
		this.flyToPoint(this.cachePointMap.get(id))
	}
	if(complete){
		complete(this);
	}
}

VFG.Viewer.prototype.setViewPointById=function(id,complete){
	if(this.cachePointMap.has(id)){
		this.setViewPoint(this.cachePointMap.get(id))
	}
	if(complete){
		complete(this);
	}
}

VFG.Viewer.prototype.flyToPoint=function(point,complete){
	if(point.cameraX && point.cameraX && point.cameraZ && point.heading && point.pitch && point.roll){
		this.viewer.camera.flyTo({
			destination:new Cesium.Cartesian3(point.cameraX*1,point.cameraY*1,point.cameraZ*1),
			orientation:{
		        heading : point.heading*1,
		        pitch : point.pitch*1,
		        roll : point.roll*1,
		    }
		});
		if(complete){
			complete(this);
		}
	}else{
		this.viewer.camera.flyTo({
			destination:Cesium.Cartesian3.fromDegrees(point.x*1,point.y*1,point.z*1+100||10000)
		});
		if(complete){
			complete(this);
		}
	}
}

VFG.Viewer.prototype.setViewPoint=function(point,complete){
	if(point.cameraX && point.cameraX && point.cameraZ && point.heading && point.pitch && point.roll){
		this.viewer.scene.camera.setView({
			destination:new Cesium.Cartesian3(point.cameraX*1,point.cameraY*1,point.cameraZ*1),
			orientation:{
		        heading : point.heading*1,
		        pitch : point.pitch*1,
		        roll : point.roll*1,
		    }
		});
		if(complete){
			complete(this);
		}
	}else{
		this.viewer.scene.camera.setView({
			destination:Cesium.Cartesian3.fromDegrees(point.x*1,point.y*1,point.z*1+100||10000)
		});
		if(complete){
			complete(this);
		}
	}
}
;
///<jscompress sourcefile="VFGMapPolygon.js" />
/**********************************************************************************************************/
VFG.Viewer.prototype.cachePolygonMap=new Map();
VFG.Viewer.prototype.selectPolygonMap=new Map();

VFG.Viewer.prototype.addPolygon=function(polygon){
	var _this=this;
	VFG.Polygon.addEntityForAPI({
		polygon:polygon,
		style:_this.POLYGON_STYLE[polygon.defaultStyleId]
	},_this.url);
	_this.cachePolygonMap.set(polygon.id,polygon);
}

VFG.Viewer.prototype.addPolygons=function(list){
	var _this=this;
	 for(var i=0;i<list.length;i++){
		 var polygon=list[i];
		 _this.addPolygon(polygon);
	 }
}

VFG.Viewer.prototype.removePolygon=function(polygon){
	var _this=this;
	if(this.cachePolygonMap.has(polygon.id)){
		_this.removePolygonById(polygon.id);
	}
}

VFG.Viewer.prototype.removePolygons=function(list){
	var _this=this;
	 for(var i=0;i<list.length;i++){
		 var polygon=list[i];
		 _this.removePolygon(polygon);
	 }
}

VFG.Viewer.prototype.removePolygonById=function(id){
	var _this=this;
	 VFG.Polygon.removeEntityById(id);
	 _this.cachePolygonMap.delete(id);
}

/**
 * 移除所有
 */
VFG.Viewer.prototype.removeAllPolygons=function(){
	var _this=this;
	_this.cachePolygonMap.forEach(function(value,key){
		VFG.Polygon.removeEntityById(key);
	});
	_this.cachePolygonMap.clear();
}

VFG.Viewer.prototype.changePolygonEntityStyle=function(id,event){
	var _this=this;
	 var polygon=VFG.Polygon.getPrimitiveById(id);
	 if(polygon!=null && polygon.hoverStyleId){
		 if('DEFAULT'==event){
			 VFG.Polygon.removeEntityById(polygon.id);
			 VFG.Polygon.addEntityForAPI({
				polygon:polygon,
				style:_this.POLYGON_STYLE[polygon.defaultStyleId]
			 },_this.url);
		 }
		 else if('HOVER'==event && polygon.hoverStyleId){
			 VFG.Polygon.removeEntityById(polygon.id);
				VFG.Polygon.addEntityForAPI({
					polygon:polygon,
					style:_this.POLYGON_STYLE[polygon.hoverStyleId]
				},_this.url);
		 }
		 else if('SELECTED'==event && polygon.selectedStyleId){
			 VFG.Polygon.removeEntityById(polygon.id);
				VFG.Polygon.addEntityForAPI({
					polygon:polygon,
					style:_this.POLYGON_STYLE[polygon.selectedStyleId]
				},_this.url);
		 }else{
			 VFG.Polygon.removeEntityById(polygon.id);
			 VFG.Polygon.addEntityForAPI({
				polygon:polygon,
				style:_this.POLYGON_STYLE[polygon.defaultStyleId]
			 },_this.url);
		 }
	 }
}

VFG.Viewer.prototype.flyToPolygonById=function(id){
	if(this.cachePolygonMap.has(id)){
		var e=this.cachePolygonMap.get(id);
		if(e.cameraX && e.cameraY && e.cameraZ){
			this.viewer.camera.flyTo({
			    destination : new Cesium.Cartesian3(e.cameraX*1,e.cameraY*1, e.cameraZ*1),
			    orientation : {
			        heading : e.heading*1,
			        pitch : e.pitch*1,
			        roll : e.roll*1
			    }
			});
		}
	}
};
///<jscompress sourcefile="VFGMapPolyline.js" />
VFG.Viewer.prototype.cachePolylineMap=new Map();
VFG.Viewer.prototype.selectPolylineMap=new Map();
VFG.Viewer.prototype.batchAddPolylineForMap=function(map){
	var _this=this;
	var mapPolyline=new Map();
	for(var key in map){
		if(!_this.cachePolylineMap.has(key)){
			for(var i=0;i<map[key].length;i++){
				var point=map[key][i];
				var polyline=map[key][i];
				VFG.Polyline.addEntity({
					polyline:polyline,
					style:_this.LINE_STYLE[polyline.defaultStyleId]
				},_this.url);
			}
			if(map[key] && map[key].length>0){
				mapPolyline.set(key,map[key]);
			}
		}else{
			mapPolyline.set(key,map[key]);
		}
	}
	_this.cachePolylineMap.forEach(function(value,key){
		if(!mapPolyline.has(key)){
			for(var i=0;i<value.length;i++){
				VFG.Polyline.removeEntityById(value[i].id);
			}
		}
	});
	_this.cachePolylineMap=mapPolyline;
}


VFG.Viewer.prototype.batchAddPolylines=function(list,reload){
	var _this=this;
	var mapPolyline=new Map();
	if(list){
		for(var i=0;i<list.length;i++){
			var polyline=list[i];
			if(!_this.cachePolylineMap.has(polyline.id)){
				VFG.Polyline.addEntity({
					polyline:polyline,
					style:_this.LINE_STYLE[polyline.defaultStyleId]
				},_this.url);
				mapPolyline.set(polyline.id,polyline);
			}else{
				mapPolyline.set(polyline.id,polyline);
			}
		}
	}

	if(mapPolyline && mapPolyline.size>0){
		_this.cachePolylineMap.forEach(function(provider,key){
			if(!mapPolyline.has(key)){
				VFG.Polyline.removeEntityById(key);
			}
		});
		_this.cachePolylineMap=mapPolyline;
	}else{
		_this.removeAllPolylines();
	}
}

/**
 * 移除所有
 */
VFG.Viewer.prototype.removeAllPolylines=function(){
	var _this=this;
	_this.cachePolylineMap.forEach(function(value,key){
		VFG.Polyline.removeEntityById(key);
	});
	_this.cachePolylineMap.clear();
}

/**
 * 添加线
 */
VFG.Viewer.prototype.addPolyline=function(param){
	var _this=this;
	_this.cachePolylineMap.set(param.id,param);
	return VFG.Polyline.addEntity({
		polyline:param,
		style:_this.LINE_STYLE[param.defaultStyleId]
	});
}

VFG.Viewer.prototype.addPolylines=function(list){
	var _this=this;
	for(var i=0;i<list.length;i++){
		var polyline=list[i]; 
		_this.addPolyline(polyline);
	}
}

VFG.Viewer.prototype.removePolyline=function(polyline){
	var _this=this;
	if(_this.cachePolylineMap.has(polyline.id)){
		_this.removePolylineById(polyline.id);
	}
}

VFG.Viewer.prototype.removePolylines=function(list){
	var _this=this;
	for(var i=0;i<list.length;i++){
		var polyline=list[i]; 
		_this.removePolyline(polyline);
	}
}

VFG.Viewer.prototype.removePolylineById=function(id){
	var _this=this;
	 VFG.Polyline.removeEntityById(id);
	 _this.cachePolylineMap.delete(id);
}

VFG.Viewer.prototype.changePolylineEntityStyle=function(id,event){
	var _this=this;
	 var polyline=VFG.Polyline.getPrimitiveById(id);
	 if(polyline!=null && polyline.hoverStyleId){
		 if('DEFAULT'==event){
			 VFG.Polyline.removeEntityById(polyline.id);
			 VFG.Polyline.addEntity({
				polyline:polyline,
				style:_this.LINE_STYLE[polyline.defaultStyleId]
			 },_this.url);
		 }
		 else if('HOVER'==event && polyline.hoverStyleId){
			 VFG.Polyline.removeEntityById(polyline.id);
			 VFG.Polyline.addEntity({
				polyline:polyline,
				style:_this.LINE_STYLE[polyline.hoverStyleId]
			 },_this.url);
		 }
		 else if('SELECTED'==event && polyline.selectedStyleId){
			 VFG.Polyline.removeEntityById(polyline.id);
			 VFG.Polyline.addEntity({
				polyline:polyline,
				style:_this.LINE_STYLE[polyline.selectedStyleId]
			 },_this.url);
		 }else{
			 VFG.Polyline.removeEntityById(polyline.id);
			 VFG.Polyline.addEntity({
				polyline:polyline,
				style:_this.LINE_STYLE[polyline.defaultStyleId]
			 },_this.url);
		 }
	 }
};
///<jscompress sourcefile="VFGMapProvider.js" />
VFG.Viewer.prototype.cacheProviderMap=new Map();

/**
 * 添加底图
 */
VFG.Viewer.prototype.addProvider=function(provider){
	var _this=this;
	if(!VFG.Provider.Map.has(provider.id)){
		if('terrain'==provider.dataType){
			var terrainProvider=VFG.Provider.getTerrainProviderFromServ(provider);
			if(terrainProvider){
				VFG.Provider.hasTerrain=true;
				_this.viewer.terrainProvider=terrainProvider;
				VFG.Provider.Map.set(provider.id,terrainProvider);
				_this.cacheProviderMap.set(provider.id,provider);
			}
		}else{
			var imageryProvider=VFG.Provider.getImageryProviderFromServ(provider);
			if(imageryProvider){
				var sImgPro=_this.viewer.imageryLayers.addImageryProvider(imageryProvider);
				VFG.Provider.Map.set(provider.id,sImgPro);
				_this.cacheProviderMap.set(provider.id,provider);
			}
		}
	}
} 

/**
 * 添加底图
 */
VFG.Viewer.prototype.addProviders=function(providers){
	var _this=this;
	if(providers){
		 for(var i=0;i<providers.length;i++){
			 var provider=providers[i];
			 _this.addProvider(provider);
		 }
	}
}

/**
 * 移除所有
 */
VFG.Viewer.prototype.removeAllProviders=function(){
	var _this=this;
	_this.cacheProviderMap.forEach(function(provider,key){
		if('terrain'==provider.dataType){
			VFG.Provider.removeTerrainProviderById(_this.viewer,provider.id);
		}else{
			VFG.Provider.removeById(_this.viewer,provider.id);
		}
	});
	_this.cacheProviderMap.clear();
}

/**
 * 移除
 */
VFG.Viewer.prototype.removeProvider=function(id){
	var _this=this;
	if(_this.cacheProviderMap.has(id)){
		var provider=_this.cacheProviderMap.get(id);
		if('terrain'==provider.dataType){
			VFG.Provider.removeTerrainProviderById(_this.viewer,provider.id);
		}else{
			VFG.Provider.removeById(_this.viewer,provider.id);
		}
	}
	_this.cacheProviderMap.delete(id);
}


/**
 * 根据图层目录显示或隐藏
 * option:{
 * 	sceneI:场景id
 * 	layerId:图层目录id
 * 	progress:function(state,message){} --->
 * }
 */
VFG.Viewer.prototype.showOrHideProviders=function(option){
	var _this=this;
	if(option.progress){
		option.progress(false,'查询资源中！');
	}
    _this.ajaxFunc(_this.url+"/api/map/list",{
    	sceneId:_this.sceneId,
    	layerId:option.layerId
    }, "json", function(res){
		 if (undefined!=res && 200==res.code) {
			 var list=res.data;
			 for(var i=0;i<list.length;i++){
				 var provider=list[i];
				 if(option.state==true){
					if(!VFG.Provider.Map.has(provider.id) ){
						if('terrain'==provider.dataType){
							var terrainProvider=VFG.Provider.getTerrainProviderFromServ(provider);
							if(terrainProvider){
								VFG.Provider.hasTerrain=true;
								_this.viewer.terrainProvider=terrainProvider;
								VFG.Provider.Map.set(provider.id,terrainProvider);
								_this.cacheProviderMap.set(provider.id,provider);
							}
						}else{
							var imageryProvider=VFG.Provider.getImageryProviderFromServ(provider);
							if(imageryProvider){
								var sImgPro=_this.viewer.imageryLayers.addImageryProvider(imageryProvider);
								VFG.Provider.Map.set(provider.id,sImgPro);
								_this.cacheProviderMap.set(provider.id,provider);
							}
						}
					}
				 }else{
					if('terrain'==provider.dataType){
						VFG.Provider.removeTerrainProviderById(_this.viewer,provider.id);
						_this.cacheProviderMap.delete(provider.id);
					}else{
						VFG.Provider.removeById(_this.viewer,provider.id);
						_this.cacheProviderMap.delete(provider.id);
					}
				 }
			 }
			 if(option.progress){
				option.progress(true,res.message);
			 } 
		 }
		 else{
			 if(res==null){
				if(option.progress){
					option.progress(true,'资源服务异常！');
				} 
			 }else{
				if(option.progress){
					option.progress(true,res.message);
				} 
			 }

		 }
    });
} 

/**
 * 图层显示或隐藏
 * option:{
 * 	sceneI:场景id
 * 	layerId:图层id
 * 	progress:function(state,message){} 
 * }
 */
VFG.Viewer.prototype.showOrHideProvider=function(option){
	var _this=this;
	if(option.progress){
		option.progress(false,'查询资源中！');
	}
    _this.ajaxFunc(_this.url+"/api/map/getByLayerId",{
    	sceneId:_this.sceneId,
    	layerId:option.layerId
    }, "json", function(res){
		 if (undefined!=res && 200==res.code) {
			 var provider=res.data;
			 if(provider){
				 if(option.state==true){
					if(!VFG.Provider.Map.has(provider.id)){
						if('terrain'==provider.dataType){
							var terrainProvider=VFG.Provider.getTerrainProviderFromServ(provider);
							if(terrainProvider){
								VFG.Provider.hasTerrain=true;
								_this.viewer.terrainProvider=terrainProvider;
								VFG.Provider.Map.set(provider.id,terrainProvider);
								_this.cacheProviderMap.set(provider.id,provider);
							}
						}else{
							var imageryProvider=VFG.Provider.getImageryProviderFromServ(provider);
							if(imageryProvider){
								var sImgPro=_this.viewer.imageryLayers.addImageryProvider(imageryProvider);
								VFG.Provider.Map.set(provider.id,sImgPro);
								_this.cacheProviderMap.set(provider.id,provider);
							}
						}
					}
				 }else{
					if('terrain'==provider.dataType){
						VFG.Provider.removeTerrainProviderById(_this.viewer,provider.id);
						_this.cacheProviderMap.delete(provider.id);
					}else{
						VFG.Provider.removeById(_this.viewer,provider.id);
						_this.cacheProviderMap.delete(provider.id);
					}
				 }
			 }
			 if(option.progress){
				option.progress(true,res.message);
			 } 
		 }
		 else{
			 if(res==null){
				if(option.progress){
					option.progress(true,'资源服务异常！');
				} 
			 }else{
				if(option.progress){
					option.progress(true,res.message);
				} 
			 }

		 }
    });
} 

VFG.Viewer.prototype.loadProvidersBySceneMenu=function(option){
	var _this=this;
	if(option.progress){
		option.progress(false,'加载点资源中！');
	}
	 _this.ajaxFunc(_this.url+"/api/scene/getProviders",{
		 sceneId:_this.sceneId,
		 auth:_this.auth,
		 id:option.id || '',
		 code:option.code || ''
	  }, "json", function(res){
		 if (undefined!=res && 200==res.code) {
			 var list=res.data;
			 for(var i=0;i<list.length;i++){
				var provider=list[i];
				if(!VFG.Provider.Map.has(provider.id) && provider.show=='1'){
					if('terrain'==provider.dataType){
						var terrainProvider=VFG.Provider.getTerrainProviderFromServ(provider);
						if(terrainProvider){
							VFG.Provider.hasTerrain=true;
							_this.viewer.terrainProvider=terrainProvider;
							VFG.Provider.Map.set(provider.id,terrainProvider);
							_this.cacheProviderMap.set(provider.id,provider);
						}
					}else{
						var imageryProvider=VFG.Provider.getImageryProviderFromServ(provider);
						if(imageryProvider){
							var sImgPro=_this.viewer.imageryLayers.addImageryProvider(imageryProvider);
							VFG.Provider.Map.set(provider.id,sImgPro);
							_this.cacheProviderMap.set(provider.id,provider);
						}
					}
				}
			 }
			if(option.progress){
				option.progress(true,res.message);
			} 
		 }
		 else{
			 if(res==null){
				if(option.progress){
					option.progress(true,'资源服务异常！');
				} 
			 }else{
				if(option.progress){
					option.progress(true,res.message);
				} 
			 }

		 }
	 });
}

;
///<jscompress sourcefile="VFGMapUtil.js" />
/**
 * 获取UUID
 */
VFG.Viewer.prototype.getUuid = function() {
	var s = [];
	var hexDigits = "0123456789abcdef";
	for (var i = 0; i < 36; i++) {
		s[i] = hexDigits.substr(Math.floor(Math.random() * 0x10), 1);
	}
	s[14] = "4"; 
	s[19] = hexDigits.substr((s[19] & 0x3) | 0x8, 1); 
	s[8] = s[13] = s[18] = s[23] = "-";
	var uuid = s.join("");
	return uuid;
};

/**
 * 笛卡尔坐标转经纬度
 */
VFG.Viewer.prototype.getLnLaFormC3  = function(cartesian3) {
	return VFG.Util.getC3ToLnLa(this.viewer, cartesian3)
}

/**
 * 经纬度装屏幕坐标
 * position:{
 * 	x:经度
 *  y:维度
 *  z:高度
 * }
 */
VFG.Viewer.prototype.getC2FormLnLa = function(position) {
	return Cesium.SceneTransforms.wgs84ToWindowCoordinates(this.viewer.scene,this.getC3FormLnLa(position))
}

/**
 * 经纬度转笛卡尔坐标
 * position:{
 * 	x:经度
 *  y:维度
 *  z:高度
 * }
 */
VFG.Viewer.prototype.getC3FormLnLa = function(position) {
	return Cesium.Cartesian3.fromDegrees(position.x*1,position.y*1,position.z*1);
}


/**
 * Turf多边形
 */
VFG.Viewer.prototype.getTurfPolygon = function (points) {
	return VFG.Util.getTurfPolygon(points);
}

/**
 * Turf点
 */
VFG.Viewer.prototype.getTurfPoint = function (point) {
	return VFG.Util.getTurfPoint(point);
}

/**
 * 点是否在多边形内部
 */
VFG.Viewer.prototype.booleanPointInPolygon = function (polygon,point) {
	return VFG.Util.booleanPointInPolygon(polygon, point);
}

/**
 * 多边形内的点
 */
VFG.Viewer.prototype.getPointInPolygon = function (polygonPoints,points) {
	var polygon =VFG.Util.getTurfPolygon(polygonPoints);
	var inPoints=[];
	for(var i=0;i<points.length;i++){
		if(VFG.Util.booleanPointInPolygon(polygon, VFG.Util.getTurfPoint(points[i]))){
			inPoints.push(points[i]);
		}
	}
	return inPoints;
}
;
///<jscompress sourcefile="VFGMapVModel.js" />
VFG.Viewer.prototype.cacheVModelMap=new Map();

VFG.Viewer.prototype.addVModel=function(option,url){
	var _this=this;
	if(_this.cacheVModelMap.has(option.id)) return;
	option.PrimitiveType='VideoModel';
	option.localUrl=url;
	var vmodel=new VFG.VModel(_this.viewer,option);
	_this.cacheVModelMap.set(option.id,vmodel);
} 

VFG.Viewer.prototype.addVModels=function(list,url){
	var _this=this;
	 for(var i=0;i<list.length;i++){
		 var vmodel=list[i]; 
		 _this.addVModel(vmodel,url);
	 }
} 

VFG.Viewer.prototype.removeVModel=function(vmodel){
	var _this=this;
	if(_this.cacheVModelMap.has(vmodel.id)){
		_this.removeVModelById(vmodel.id);
	}
}

VFG.Viewer.prototype.removeVModels=function(list){
	var _this=this;
	 for(var i=0;i<list.length;i++){
		 var vmodel=list[i]; 
		 _this.removeVModel(vmodel);
	 }
}

VFG.Viewer.prototype.removeVModelById=function(id){
	var _this=this;
	if(_this.cacheVModelMap.has(id)){
		var vmodel=_this.cacheVModelMap.get(id);
		vmodel.destroy();
		_this.cacheVModelMap.delete(id);
		vmodel=null;
	}
}

VFG.Viewer.prototype.getVModelById=function(id){
	var _this=this;
	if(_this.cacheVModelMap.has(id)){
		return _this.cacheVModelMap.get(id);
	}
} 

VFG.Viewer.prototype.removeAllVModel=function(layerId){
	var _this=this;
	_this.cacheVModelMap.forEach(function(vmodel,key){
		vmodel.destroy();
	});
	_this.cacheVModelMap.clear();
}

VFG.Viewer.prototype.flyToVModelById=function(id){
	if(this.cacheVModelMap.has(id)){
		var e=this.cacheVModelMap.get(id).option;
		if(e.cameraX && e.cameraX!='0'  && e.cameraY && e.cameraY!='0' && e.cameraZ){
			this.viewer.camera.flyTo({
			    destination : new Cesium.Cartesian3(e.cameraX*1,e.cameraY*1, e.cameraZ*1 ||10000),
			    orientation : {
			        heading : e.heading*1,
			        pitch : e.pitch*1,
			        roll : e.roll*1
			    }
			});
		}
	}
}

VFG.Viewer.prototype.changeVModelRX=function(id,rx){
	var _this=this;
	var vModel=_this.getVModelById(id);
	if(vModel){
		vModel.option.rotationX=rx;
		vModel.update();
	}
}

VFG.Viewer.prototype.changeVModelRY=function(id,ry){
	var _this=this;
	var vModel=_this.getVModelById(id);
	if(vModel){
		vModel.option.rotationY=ry;
		vModel.update();
	}
}

VFG.Viewer.prototype.changeVModelRZ=function(id,rz){
	var _this=this;
	var vModel=_this.getVModelById(id);
	if(vModel){
		vModel.option.rotationZ=rz;
		vModel.update();
	}
}

VFG.Viewer.prototype.changeVModelPosition=function(id,position){
	var _this=this;
	var vModel=_this.getVModelById(id);
	if(vModel){
		vModel.option.x=position.x;
		vModel.option.y=position.y;
		vModel.option.z=position.z;
		vModel.update();
	}
}

VFG.Viewer.prototype.changeVModelHeight=function(id,height){
	var _this=this;
	var vModel=_this.getVModelById(id);
	if(vModel){
		vModel.option.z=height;
		vModel.update();
	}
}



;
///<jscompress sourcefile="VFGMapVPlane.js" />
VFG.Viewer.prototype.cacheVPlaneMap=new Map();

VFG.Viewer.prototype.addVPlane=function(option){
	var _this=this;
	if(!_this.cacheVPlaneMap.has(option.id)){
		option.PrimitiveType='VideoPlane';
		var vplane=new VFG.VPlane(_this.viewer,option);
		_this.cacheVPlaneMap.set(option.id,vplane);
	}

} 

VFG.Viewer.prototype.addVPlanes=function(list){
	var _this=this;
	 for(var i=0;i<list.length;i++){
		 var vplane=list[i]; 
		 _this.addVPlane(vplane);
	 }
} 

VFG.Viewer.prototype.removeVPlane=function(vplane){
	var _this=this;
	if(_this.cacheVPlaneMap.has(vplane.id)){
		_this.removeVPlaneById(vplane.id);
	}
}

VFG.Viewer.prototype.removeVPlanes=function(list){
	var _this=this;
	 for(var i=0;i<list.length;i++){
		 var vplane=list[i]; 
		 _this.removeVPlane(vplane);
	 }
}

VFG.Viewer.prototype.removeVPlaneById=function(id){
	var _this=this;
	if(_this.cacheVPlaneMap.has(id)){
		var vplane=_this.cacheVPlaneMap.get(id);
		vplane.destroy();
		_this.cacheVPlaneMap.delete(id);
		vplane=null;
	}
}

VFG.Viewer.prototype.getVPlaneById=function(id){
	var _this=this;
	if(_this.cacheVPlaneMap.has(id)){
		return _this.cacheVPlaneMap.get(id);
	}
} 

VFG.Viewer.prototype.removeAllVPlane=function(layerId){
	var _this=this;
	_this.cacheVPlaneMap.forEach(function(vplane,key){
		vplane.destroy();
	});
	_this.cacheVPlaneMap.clear();
}

VFG.Viewer.prototype.flyToVPlaneById=function(id){
	if(this.cacheVPlaneMap.has(id)){
		var e=this.cacheVPlaneMap.get(id).option;
		if(e.cameraX && e.cameraX!='0'  && e.cameraY && e.cameraY!='0' && e.cameraZ){
			this.viewer.camera.flyTo({
			    destination : new Cesium.Cartesian3(e.cameraX*1,e.cameraY*1, e.cameraZ*1 ||10000),
			    orientation : {
			        heading : e.heading*1,
			        pitch : e.pitch*1,
			        roll : e.roll*1
			    }
			});
		}
	}
}

VFG.Viewer.prototype.changeVPlaneDX=function(id,dx){
	var _this=this;
	var VPlane=_this.getVPlaneById(id);
	if(VPlane){
		VPlane.option.dimensionX=dx;
		VPlane.update()
	}
}
VFG.Viewer.prototype.changeVPlaneDY=function(id,dy){
	var _this=this;
	var VPlane=_this.getVPlaneById(id);
	if(VPlane){
		VPlane.option.dimensionY=dy;
		VPlane.update()
	}
}
VFG.Viewer.prototype.changeVPlaneDZ=function(id,dz){
	var _this=this;
	var VPlane=_this.getVPlaneById(id);
	if(VPlane){
		VPlane.option.dimensionZ=dz;
		VPlane.update()
	}
}
VFG.Viewer.prototype.changeVPlaneRX=function(id,rx){
	var _this=this;
	var VPlane=_this.getVPlaneById(id);
	if(VPlane){
		VPlane.option.rotationX=rx;
		VPlane.update()
	}
}
VFG.Viewer.prototype.changeVPlaneRY=function(id,ry){
	var _this=this;
	var VPlane=_this.getVPlaneById(id);
	if(VPlane){
		VPlane.option.rotationY=ry;
		VPlane.update()
	}
}
VFG.Viewer.prototype.changeVPlaneRZ=function(id,rz){
	var _this=this;
	var VPlane=_this.getVPlaneById(id);
	if(VPlane){
		VPlane.option.rotationZ=rz;
		VPlane.update()
	}
}

VFG.Viewer.prototype.changeVPlanePosition=function(id,position){
	var _this=this;
	var VPlane=_this.getVPlaneById(id);
	if(VPlane){
		VPlane.option.x=position.x;
		VPlane.option.y=position.y;
		VPlane.option.z=position.z;
		VPlane.update()
	}
}

VFG.Viewer.prototype.changeVPlaneHeight=function(id,height){
	var _this=this;
	var VPlane=_this.getVPlaneById(id);
	if(VPlane){
		VPlane.option.z=height;
		VPlane.update()
	}
}


;
///<jscompress sourcefile="VFGMapVShed3d.js" />
VFG.Viewer.prototype.cacheVideoShed3dMap=new Map();

VFG.Viewer.prototype.addVideoShed3d=function(option){
	var _this=this;
	
	if(_this.cacheVideoShed3dMap.has(option.id)) return;
	
	option.PrimitiveType='VideoShed3d';
	var vPos=_this.getVideoShed3dViewPosition(option);
	
	var param={
		id:option.id||'', 
		name:option.name||'', 
		code:option.code||'', 
		option:option||{}, 
		cameraPosition:Cesium.Cartesian3.fromDegrees(option.x*1,option.y*1,option.z*1),
		position:Cesium.Cartesian3.fromDegrees(vPos.longitude*1,vPos.latitude*1,vPos.altitude*1), 
		alpha:option.alpha||1, 
		debugFrustum:false, 
		fov:option.fov||null, 
		heading:option.vHeading||null, 
		pitch:option.vPitch||null, 
		roll:option.vRoll||null,
		near:option.near||null,
		aspectRatio:option.aspectRatio||null, 
		maskUrl:option.maskUrl||null, 
	};
	
	var vShed3d=new VFG.VShed3d(_this.viewer,param);
	_this.cacheVideoShed3dMap.set(option.id,vShed3d);
} 

VFG.Viewer.prototype.getVideoShed3dViewPosition=function(option){
	return this.ECEF.enu_to_ecef({ 
		longitude:option.x*1, 
		latitude:option.y*1, 
		altitude:option.z*1
	},
	{ 
		distance: option.far*1, 
		azimuth:option.vHeading*1, 
		elevation:option.vPitch*1
	});
}
VFG.Viewer.prototype.addVideoShed3ds=function(list){
	var _this=this;
	 for(var i=0;i<list.length;i++){
		 var vShed3d=list[i]; 
		 _this.addVideoShed3d(vShed3d);
	 }
}

VFG.Viewer.prototype.removeVideoShed3d=function(vShed3d){
	var _this=this;
	if(_this.cacheVideoShed3dMap.has(vShed3d.id)){
		_this.removeVideoShed3dById(vShed3d.id);
	}
}

VFG.Viewer.prototype.removeVideoShed3ds=function(list){
	var _this=this;
	 for(var i=0;i<list.length;i++){
		 var vShed3d=list[i]; 
		 _this.removeVideoShed3d(vShed3d);
	 }
}

VFG.Viewer.prototype.removeVideoShed3dById=function(id){
	var _this=this;
	if(_this.cacheVideoShed3dMap.has(id)){
		var vShed3d=_this.cacheVideoShed3dMap.get(id);
		vShed3d.destroy();
		_this.cacheVideoShed3dMap.delete(id);
		vShed3d=null;
	}
}

VFG.Viewer.prototype.getVideoShed3dById=function(id){
	var _this=this;
	if(_this.cacheVideoShed3dMap.has(id)){
		return _this.cacheVideoShed3dMap.get(id);
	}
} 

VFG.Viewer.prototype.removeAllVideoShed3d=function(layerId){
	var _this=this;
	_this.cacheVideoShed3dMap.forEach(function(vShed3d,key){
		vShed3d.destroy();
	});
	_this.cacheVideoShed3dMap.clear();
}

VFG.Viewer.prototype.flyToVideoShed3dById=function(id){
	if(this.cacheVideoShed3dMap.has(id)){
		var e=this.cacheVideoShed3dMap.get(id).option;
		if(e.cameraX && e.cameraX!='0' && e.cameraY && e.cameraY!='0' && e.cameraZ){
			this.viewer.camera.flyTo({
			    destination : new Cesium.Cartesian3(e.cameraX*1,e.cameraY*1, e.cameraZ  || 2000 ),
			    orientation : {
			        heading : e.heading*1,
			        pitch : e.pitch*1,
			        roll : e.roll*1
			    }
			});
		}
	}
}

VFG.Viewer.prototype.changeFar=function(id,far){
  var videoShed3d=this.getVideoShed3dById(id);
  if(videoShed3d){
	  videoShed3d.option.far=far;
	  var vPos=this.getVideoShed3dViewPosition(videoShed3d.option);
	  if(vPos){
		  var position=Cesium.Cartesian3.fromDegrees(vPos.longitude*1,vPos.latitude*1,vPos.altitude*1);
		  videoShed3d.option.position=position;
		  videoShed3d.position=position;
	  }
  }
}

VFG.Viewer.prototype.changeNear=function(id,near){
  var videoShed3d=this.getVideoShed3dById(id);
  if(videoShed3d){
	  videoShed3d.option.near=near;
	  videoShed3d.near=near;
  }
}

VFG.Viewer.prototype.changeAlpha=function(id,alpha){
  var videoShed3d=this.getVideoShed3dById(id);
  if(videoShed3d){
	  videoShed3d.option.alpha=alpha;
	  videoShed3d.alpha=alpha;
  }
}
VFG.Viewer.prototype.changeAspectRatio=function(id,aspectRatio){
  var videoShed3d=this.getVideoShed3dById(id);
  if(videoShed3d){
	  videoShed3d.option.aspectRatio=aspectRatio;
	  videoShed3d.aspectRatio=aspectRatio
  }
}

VFG.Viewer.prototype.changeFov=function(id,fov){
  var videoShed3d=this.getVideoShed3dById(id);
  if(videoShed3d){
	  videoShed3d.option.fov=fov;
	  videoShed3d.fov=fov
  }
}

VFG.Viewer.prototype.changePitch=function(id,pitch){
  var videoShed3d=this.getVideoShed3dById(id);
  if(videoShed3d){
	  videoShed3d.option.vPitch=pitch;
	  videoShed3d._pitch=pitch;
	  var vPos=this.getVideoShed3dViewPosition(videoShed3d.option);
	  videoShed3d.position=Cesium.Cartesian3.fromDegrees(vPos.longitude*1,vPos.latitude*1,vPos.altitude*1)
  }
}

VFG.Viewer.prototype.changeHeading=function(id,heading){
  var videoShed3d=this.getVideoShed3dById(id);
  if(videoShed3d){
	  videoShed3d.option.vHeading=heading;
	  videoShed3d._heading=heading;
	  var vPos=this.getVideoShed3dViewPosition(videoShed3d.option);
	  videoShed3d.position=Cesium.Cartesian3.fromDegrees(vPos.longitude*1,vPos.latitude*1,vPos.altitude*1)
  }
}

VFG.Viewer.prototype.changeRoll=function(id,roll){
  var videoShed3d=this.getVideoShed3dById(id);
  if(videoShed3d){
	  videoShed3d.option.vRollg=roll;
	  videoShed3d.roll=roll
  }
}

VFG.Viewer.prototype.changeHeight=function(id,height){
  var videoShed3d=this.getVideoShed3dById(id);
  if(videoShed3d){
	  videoShed3d.option.z=height;
	  videoShed3d.cameraPosition=Cesium.Cartesian3.fromDegrees(videoShed3d.option.x*1,videoShed3d.option.y*1,videoShed3d.option.z*1)
  }
}

VFG.Viewer.prototype.changeMaskUrl=function(id,maskUrl){
  var videoShed3d=this.getVideoShed3dById(id);
  if(videoShed3d){
	  videoShed3d.option.maskUrl=maskUrl;
	  videoShed3d.maskUrl=maskUrl
  }
}

VFG.Viewer.prototype.changeDebugFrustum=function(id,debugFrustum){
  var videoShed3d=this.getVideoShed3dById(id);
  if(videoShed3d){
	  videoShed3d.option.debugFrustum=debugFrustum;
	  videoShed3d.debugFrustum=debugFrustum
  }
}

;
