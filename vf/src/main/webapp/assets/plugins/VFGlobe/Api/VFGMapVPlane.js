VFG.Viewer.prototype.cacheVPlaneMap=new Map();

VFG.Viewer.prototype.addVPlane=function(option){
	var _this=this;
	if(!_this.cacheVPlaneMap.has(option.id)){
		option.PrimitiveType='VideoPlane';
		var vplane=new VFG.VPlane(_this.viewer,option);
		_this.cacheVPlaneMap.set(option.id,vplane);
	}

} 

VFG.Viewer.prototype.addVPlanes=function(list){
	var _this=this;
	 for(var i=0;i<list.length;i++){
		 var vplane=list[i]; 
		 _this.addVPlane(vplane);
	 }
} 

VFG.Viewer.prototype.removeVPlane=function(vplane){
	var _this=this;
	if(_this.cacheVPlaneMap.has(vplane.id)){
		_this.removeVPlaneById(vplane.id);
	}
}

VFG.Viewer.prototype.removeVPlanes=function(list){
	var _this=this;
	 for(var i=0;i<list.length;i++){
		 var vplane=list[i]; 
		 _this.removeVPlane(vplane);
	 }
}

VFG.Viewer.prototype.removeVPlaneById=function(id){
	var _this=this;
	if(_this.cacheVPlaneMap.has(id)){
		var vplane=_this.cacheVPlaneMap.get(id);
		vplane.destroy();
		_this.cacheVPlaneMap.delete(id);
		vplane=null;
	}
}

VFG.Viewer.prototype.getVPlaneById=function(id){
	var _this=this;
	if(_this.cacheVPlaneMap.has(id)){
		return _this.cacheVPlaneMap.get(id);
	}
} 

VFG.Viewer.prototype.removeAllVPlane=function(layerId){
	var _this=this;
	_this.cacheVPlaneMap.forEach(function(vplane,key){
		vplane.destroy();
	});
	_this.cacheVPlaneMap.clear();
}

VFG.Viewer.prototype.flyToVPlaneById=function(id){
	if(this.cacheVPlaneMap.has(id)){
		var e=this.cacheVPlaneMap.get(id).option;
		if(e.cameraX && e.cameraX!='0'  && e.cameraY && e.cameraY!='0' && e.cameraZ){
			this.viewer.camera.flyTo({
			    destination : new Cesium.Cartesian3(e.cameraX*1,e.cameraY*1, e.cameraZ*1 ||10000),
			    orientation : {
			        heading : e.heading*1,
			        pitch : e.pitch*1,
			        roll : e.roll*1
			    }
			});
		}
	}
}

VFG.Viewer.prototype.changeVPlaneDX=function(id,dx){
	var _this=this;
	var VPlane=_this.getVPlaneById(id);
	if(VPlane){
		VPlane.option.dimensionX=dx;
		VPlane.update()
	}
}
VFG.Viewer.prototype.changeVPlaneDY=function(id,dy){
	var _this=this;
	var VPlane=_this.getVPlaneById(id);
	if(VPlane){
		VPlane.option.dimensionY=dy;
		VPlane.update()
	}
}
VFG.Viewer.prototype.changeVPlaneDZ=function(id,dz){
	var _this=this;
	var VPlane=_this.getVPlaneById(id);
	if(VPlane){
		VPlane.option.dimensionZ=dz;
		VPlane.update()
	}
}
VFG.Viewer.prototype.changeVPlaneRX=function(id,rx){
	var _this=this;
	var VPlane=_this.getVPlaneById(id);
	if(VPlane){
		VPlane.option.rotationX=rx;
		VPlane.update()
	}
}
VFG.Viewer.prototype.changeVPlaneRY=function(id,ry){
	var _this=this;
	var VPlane=_this.getVPlaneById(id);
	if(VPlane){
		VPlane.option.rotationY=ry;
		VPlane.update()
	}
}
VFG.Viewer.prototype.changeVPlaneRZ=function(id,rz){
	var _this=this;
	var VPlane=_this.getVPlaneById(id);
	if(VPlane){
		VPlane.option.rotationZ=rz;
		VPlane.update()
	}
}

VFG.Viewer.prototype.changeVPlanePosition=function(id,position){
	var _this=this;
	var VPlane=_this.getVPlaneById(id);
	if(VPlane){
		VPlane.option.x=position.x;
		VPlane.option.y=position.y;
		VPlane.option.z=position.z;
		VPlane.update()
	}
}

VFG.Viewer.prototype.changeVPlaneHeight=function(id,height){
	var _this=this;
	var VPlane=_this.getVPlaneById(id);
	if(VPlane){
		VPlane.option.z=height;
		VPlane.update()
	}
}


