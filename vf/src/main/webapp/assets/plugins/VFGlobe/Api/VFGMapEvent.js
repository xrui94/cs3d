/**
 * 鼠标左键单击拾取实体
 * complete(c2,data)：回调函数
 */
VFG.Viewer.prototype.LEFT_CLICK_PRIMITIVE=function(callback){
	var _this=this;
	_this.handler.setInputAction(function(movement) {
		var obj=_this.checkPickType(_this.viewer.scene.pick(movement.position),movement.position);
		if (Cesium.defined(obj)){
			if(callback){
				callback(obj);
			}
		}
	}, Cesium.ScreenSpaceEventType.LEFT_CLICK);
}

/**
 * 鼠标双击事件
 */
VFG.Viewer.prototype.LEFT_DOUBLE_CLICK_PRIMITIVE=function(callback){
	var _this=this;
	this.handler.setInputAction(function(movement) {
		var obj=_this.checkPickType(_this.viewer.scene.pick(movement.position),movement.position);
		if (Cesium.defined(obj)){
			if(callback){
				callback(obj);
			}
		}
	}, Cesium.ScreenSpaceEventType.LEFT_DOUBLE_CLICK);
}

/**
 * 鼠标右键
 */
VFG.Viewer.prototype.RIGHT_CLICK_PRIMITIVE=function(callback){
	var _this=this;
	this.handler.setInputAction(function(movement) {
		var obj=_this.checkPickType(_this.viewer.scene.pick(movement.position),movement.position);
		if(callback){
			callback(obj);
		}
	}, Cesium.ScreenSpaceEventType.RIGHT_CLICK);
}

/**
 * 滚轮事件
 */
VFG.Viewer.prototype.WHEEL=function(callback){
	var _this=this;
	this.handler.setInputAction(function(movement) {
	}, Cesium.ScreenSpaceEventType.WHEEL);
}

/**
 * 相机移动停止事件
 */
VFG.Viewer.prototype.CAMERA_MOVE_END=function(callback){
	var _this=this;
	_this.cameraMoveEnd=function(){
		if(callback){
			callback({
				level:_this.getLevel(),
				height: Math.ceil(_this.viewer.camera.positionCartographic.height)
			})
		}
	}
	_this.viewer.scene.camera.moveEnd.addEventListener(_this.cameraMoveEnd)
}

/**
 * 移除相机移动停止事件
 */
VFG.Viewer.prototype.CAMERA_MOVE_END=function(callback){
	var _this=this;
	if(_this.cameraMoveEnd){
		_this.viewer.scene.camera.moveEnd.removeEventListener(_this.cameraMoveEnd)
	}
}

/**
 * 鼠标滑过效果
 */
VFG.Viewer.prototype.MOUSE_MOVE_PRIMITIVE=function(callback){
	var _this=this;
	var preObj = null;
	this.handler.setInputAction(function(movement) {
		var obj=_this.checkPickType(_this.viewer.scene.pick(movement.endPosition),movement.endPosition);
		if (Cesium.defined(obj)){
			_this.tip.showAt(movement.endPosition, `<div class="con" style="color: white;">`+obj.name+`</div>`);
			 if(Cesium.defined(preObj) && preObj.id==obj.id){
				 return;
			 }else{
				if(Cesium.defined(preObj)){
					_this.changeStyle(preObj.id,'DEFAULT',preObj.type);
				}
				_this.changeStyle(obj.id,'HOVER',obj.type);
				preObj=obj;
			 }
		}else{
			if(Cesium.defined(preObj)){
				_this.changeStyle(preObj.id,'DEFAULT',preObj.type);
			}
			_this.tip.setVisible(false);
			preObj=obj;
		}
	}, Cesium.ScreenSpaceEventType.MOUSE_MOVE);
}


VFG.Viewer.prototype.checkPickType=function(obj,position){
	var _this=this;
	if (Cesium.defined(obj) && obj.primitive && obj.id){ 
		if ((obj.primitive instanceof Cesium.Primitive 
				|| obj.primitive instanceof Cesium.GroundPrimitive ) &&!(obj.id instanceof Cesium.Entity)
				){
			if(obj.id && obj.id.PrimitiveType){
				var primitive=obj.id;
				return {
					id:primitive.id,
					layerId:primitive.layerId,
					type:primitive.PrimitiveType,
					name:primitive.name,
					code:primitive.code,
					position:position
				}
			 }
		}
		else if (obj.primitive instanceof Cesium.ClassificationPrimitive && obj.id){
			var primitive=_this.getFeatureById(obj.id);
			if(primitive){
				return {
					id:primitive.id,
					layerId:primitive.layerId,
					type:"Feature",
					name:primitive.name,
					code:primitive.code,
					position:position
				}
			}
		}
		else if (obj.primitive instanceof Cesium.Cesium3DTileset){
//			console.log(obj.id);
//			var primitive=_this.getModelById(obj.id);
//			if(primitive){
//				return {
//					id:primitive.id,
//					layerId:primitive.layerId,
//					type:"Feature",
//					name:primitive.name,
//					code:primitive.code,
//					position:position
//				}
//			}
		}
		else if (obj.primitive instanceof Cesium.Model){
			var primitive=_this.getModelById(obj.id);
			if(primitive){
				return {
					id:primitive.id,
					layerId:primitive.layerId,
					type:"Model",
					name:primitive.name,
					code:primitive.code,
					attribute:primitive.attribute,
					position:position,
				}
			}
		}
		else if ((obj.primitive instanceof Cesium.Label) || (obj.primitive instanceof Cesium.PointPrimitive)|| (obj.primitive instanceof Cesium.Billboard)){
			 var point=_this.getPointById(obj.id);
			 if(Cesium.defined(point)){
				 return {
					id:point.id,
					layerId:point.layerId,
					type:"Point",
					name:point.name,
					code:point.code,
					position:position
				}
			 }
		}
		else if(obj.id instanceof Cesium.Entity){
			if(Cesium.defined(obj.id.point) || Cesium.defined(obj.id.billboard) || Cesium.defined(obj.id.label)){
				var point=_this.getPointById(obj.id.id); 
				if(point){
					return {
						id:point.id,
						layerId:point.layerId|| null,
						type:"Point",
						name:point.name|| null,
						code:point.code|| null,
						position:position
					}
				}else{
					return {
						id:obj.id.id,
						layerId:null,
						type:"Point",
						name:obj.id.name|| null,
						code:obj.id.code|| null,
						position:position
					}
				}
			}
			else if( Cesium.defined(obj.id.polygon)){
				var polygon=_this.getPolygonById(obj.id.id); 
				if(polygon){
					return {
						id:polygon.id,
						layerId:polygon.layerId,
						type:"Polygon",
						name:polygon.name,
						code:polygon.code,
						position:position
					}
				}else{
					return {
						id:obj.id.id,
						layerId:null,
						type:"Polygon",
						name:obj.id.name|| null,
						code:obj.id.code|| null,
						position:position
					}
				}
			}
			else if( Cesium.defined(obj.id.polyline)){
				var polyline=_this.getPolylineById(obj.id.id); 
				if(polyline){
					return {
						id:polyline.id,
						layerId:polyline.layerId,
						type:"Polyline",
						name:polyline.name,
						code:polyline.code,
						position:position
					}
				}else{
					return {
						id:obj.id.id,
						layerId:null,
						type:"Polyline",
						name:obj.id.name|| null,
						code:obj.id.code|| null,
						position:position
					}
				}

			}
			else if( Cesium.defined(obj.id.ellipse)){
				return {
					id:obj.id.id,
					layerId:null,
					type:"Ellipse",
					name:obj.id.name|| null,
					code:obj.id.code|| null,
					position:position
				}
			}
		}
	}
}

