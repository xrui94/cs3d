/**
 * container:容器Id
 * options： 调用接口：http://localhost:8080/vf/i/scene/get(sceneId) 
 * options.url=http://localhost:8080/vf
 */
VFG.Viewer=function(container, options){
	if(!container || !options){
		console.log('参数必填!!!');
		return;
	}
	this.MAP_STYLE=options.MAP_STYLE;
	this.MAP_LAYER=options.MAP_LAYER;
	this.container=container||''
	this.url=options.url;
	this.SCENE=options||null;
	this.sceneId=this.SCENE.id||''
	this.geocoder=options.geocoder||false;
	this.complete=options.complete||null;
	this.providers=options.maps||null;
	this.viewer=null;
	this.scene=null;
	this.tip=null;
	this.ECEF =new Cesium.ECEF();
	return this.init();
}

VFG.Viewer.prototype.init=function(){
	let _this=this;
	_this.ellipsoid = Cesium.Ellipsoid.WGS84;
	var imageryProvider;
	if(this.providers){
		var p=this.getFirstProvider(this.providers);
		if(p){
			imageryProvider=VFG.Provider.packProvider(p);
			VFG.Provider.Map.set(p.id,imageryProvider);
			_this.cacheProviderMap.set(p.id,p);
		}
	}
	
	_this.globe = new VFG.Globe({
		domId: _this.container,
		geocoder: _this.geocoder,
		imageryProvider:imageryProvider
	});
	
	
	
	_this.viewer=_this.globe.viewer;
	_this.scene=_this.globe.viewer.scene;
	_this.tip=new VFG.Tip(_this.viewer);
	_this.handler = new Cesium.ScreenSpaceEventHandler(_this.viewer.scene.canvas);
	
	
	_this.scene.skyBox = new Cesium.SkyBox({
	  sources : {
          positiveX: _this.url+'/assets/img/sky/posx.jpg',
          negativeX: _this.url+'/assets/img/sky/negx.jpg',
          positiveY: _this.url+'/assets/img/sky/negy.jpg',
          negativeY: _this.url+'/assets/img/sky/posy.jpg',
          positiveZ: _this.url+'/assets/img/sky/posz.jpg',
          negativeZ: _this.url+'/assets/img/sky/negz.jpg'

	  }
	});

	
	var CesiumViewerSceneController = _this.viewer.scene.screenSpaceCameraController;
    CesiumViewerSceneController.inertiaSpin = 0.1;
    CesiumViewerSceneController.inertiaTranslate = 0.1;
    CesiumViewerSceneController.inertiaZoom = 0.1;
    
	if(_this.SCENE.enableLighting && _this.SCENE.enableLighting=='1'){
		 _this.enableLighting(true);
	}else{
		 _this.enableLighting(false);
	}
	if(_this.SCENE.shadows && _this.SCENE.shadows=='1'){
		 _this.showShadows(true);
	}else{
		 _this.showShadows(false);
	}
	if(_this.SCENE.globeShow && _this.SCENE.globeShow=='1'){
		 _this.showGlobe(true);
	}else{
		_this.showGlobe(false);
	}
	if(_this.SCENE.skyBoxShow && _this.SCENE.skyBoxShow=='1'){
	    _this.showSkyBox(true);
	}else{
	    _this.showSkyBox(false);
	}
    
    _this.addProviders(this.providers);
	
    if(_this.SCENE.models){
    	 _this.addModels(_this.SCENE.models);
    }
    _this.MOUSE_MOVE_PRIMITIVE();
	if(this.complete){
		_this.flyToHome({
			complete:this.complete
		});
	}else{
		_this.flyToHome({
			complete:this.complete
		});	
	}
	return this;
}

VFG.Viewer.prototype.getFirstProvider=function(providers){
	var _this=this;
	var provider;
	if(providers){
		for(var i=0;i<providers.length;i++){
			if(providers[i].dataType=='map'){
				return providers[i];
			}
		}
	}
	return provider;
}

VFG.Viewer.prototype.flyToHome=function(option){
	var _this=this;
	if(_this.SCENE.x && _this.SCENE.y && _this.SCENE.z && _this.SCENE.heading && _this.SCENE.pitch && _this.SCENE.roll){
		this.viewer.camera.flyTo({
			destination:Cesium.Cartesian3.fromDegrees(_this.SCENE.x,_this.SCENE.y, _this.SCENE.z),
			orientation:{
		        heading : Cesium.Math.toRadians(_this.SCENE.heading*1),
		        pitch : Cesium.Math.toRadians(_this.SCENE.pitch*1),
		        roll : Cesium.Math.toRadians(_this.SCENE.roll*1),
		    },
	        complete: function () {
	        	if(option.complete){
	        		option.complete(_this);
	        	}
	        },
	        cancle: function () {
	        	if(option.cancle){
	        		option.cancle(_this);
	        	}
	        },
		});
	}else{
    	if(option.complete){
    		option.complete(_this);
    	}
	}
}

VFG.Viewer.prototype.setViewToHome=function(){
	var _this=this;
	if(_this.SCENE.x && _this.SCENE.y && _this.SCENE.z && _this.SCENE.heading && _this.SCENE.pitch && _this.SCENE.roll){
		this.viewer.scene.camera.setView({
			destination:Cesium.Cartesian3.fromDegrees(_this.SCENE.x,_this.SCENE.y, _this.SCENE.z),
			orientation:{
		        heading : Cesium.Math.toRadians(_this.SCENE.heading*1),
		        pitch : Cesium.Math.toRadians(_this.SCENE.pitch*1),
		        roll : Cesium.Math.toRadians(_this.SCENE.roll*1),
		    },
	        complete: function () {
	        	if(option.complete){
	        		option.complete(_this);
	        	}
	        },
	        cancle: function () {
	        	if(option.cancle){
	        		option.cancle(_this);
	        	}
	        },
		});
	}else{
    	if(option.complete){
    		option.complete(_this);
    	}
	}
}

VFG.Viewer.prototype.flyTo=function(option){
	var position=option.position;
	var orientation=option.orientation;
	var param={};
	if(position){
		param.destination=Cesium.Cartesian3.fromDegrees(position.x,position.y, position.z);
	}
	if(orientation){
		param.orientation={
	        heading : Cesium.Math.toRadians(orientation.heading*1),
	        pitch : Cesium.Math.toRadians(orientation.pitch*1),
	        roll : Cesium.Math.toRadians(orientation.roll*1),
	    };
	}
	this.viewer.camera.flyTo(param);
}

VFG.Viewer.prototype.setView=function(option){
	var position=option.position;
	var orientation=option.orientation;
	var param={};
	if(position){
		param.destination=Cesium.Cartesian3.fromDegrees(position.x,position.y, position.z);
	}
	if(orientation){
		param.orientation={
	        heading : Cesium.Math.toRadians(orientation.heading*1),
	        pitch : Cesium.Math.toRadians(orientation.pitch*1),
	        roll : Cesium.Math.toRadians(orientation.roll*1),
	    };
	}
	this.viewer.scene.camera.setView(param);
}

VFG.Viewer.prototype.setView=function(option){
	var position=option.position;
	var orientation=option.orientation;
	var param={};
	if(position){
		param.destination=Cesium.Cartesian3.fromDegrees(position.x,position.y, position.z);
	}
	if(orientation){
		param.orientation={
	        heading : Cesium.Math.toRadians(orientation.heading*1),
	        pitch : Cesium.Math.toRadians(orientation.pitch*1),
	        roll : Cesium.Math.toRadians(orientation.roll*1),
	    };
	}
	this.viewer.scene.camera.setView(param);
}


VFG.Viewer.prototype.hideGlobe=function() {
	this.viewer.scene.sun.show = false; 
	this.viewer.scene.moon.show = false;
	this.viewer.scene.skyBox.show = false;//关闭天空盒，否则会显示天空颜色
	//this.viewer.scene.undergroundMode = true; //重要，开启地下模式，设置基色透明，这样就看不见黑色地球了
	//this.viewer.scene.underGlobe.show = true;
	//this.viewer.scene.underGlobe.baseColor = new Cesium.Color(0, 0, 0, 0);
	this.viewer.scene.globe.show = false; //不显示地球，这条和地球透明度选一个就可以
	this.viewer.scene.globe.baseColor = new Cesium.Color(0, 0, 0, 0);
	this.viewer.scene.backgroundcolor = new Cesium.Color(0, 0, 0, 0)
};

VFG.Viewer.prototype.updateStyle=function(style) {
	if(!this.MAP_STYLE){
		this.MAP_STYLE={};
	}
	this.MAP_STYLE[style.id]=style;
};

VFG.Viewer.prototype.removeStyle=function(styleId) {
	if(this.MAP_STYLE && this.MAP_STYLE[styleId]){
		delete this.MAP_STYLE[styleId]
	}
};
VFG.Viewer.prototype.getStyle=function(styleId) {
	if(this.MAP_STYLE && this.MAP_STYLE[styleId]){
		return this.MAP_STYLE[styleId];
	}
};

VFG.Viewer.prototype.updateLayer=function(layer) {
	if(!this.MAP_LAYER){
		this.MAP_LAYER={};
	}
	this.MAP_LAYER[layer.id]=layer;
};

VFG.Viewer.prototype.removeLayer=function(layerId) {
	if(this.MAP_LAYER && this.MAP_LAYER[layerId]){
		delete this.MAP_LAYER[layerId]
	}
};

VFG.Viewer.prototype.getLayerStyle=function(layerId,eventType) {
	if(this.MAP_LAYER && this.MAP_LAYER[layerId]){
		var layer=this.MAP_LAYER[layerId];
		if('DEFAULT'==eventType){
			return this.getStyle(layer.defaultStyleId);
		}
		else if('HOVER'==eventType){
			return this.getStyle(layer.hoverStyleId);
		}
		else if('SELECTED'==eventType){
			return this.getStyle(layer.selectedStyleId);
		}
	}
};


VFG.Viewer.prototype.createTranslationAxis=function(option) {
	return new VFG.Controls.Drag(this.viewer,{
		id:option.id,
		position:Cesium.Cartesian3.fromDegrees(option.position.x*1,option.position.y*1,option.position.z*1),
		callback:option.callback
	});
};

VFG.Viewer.prototype.destroy=function(){
	var _this = this;
	try{
		_this.closeWebSocket();
		_this.handler.destroy();
		if(_this.handler3D){
			_this.handler3D.destroy();
		}
		if(_this.cameraMoveEnd){
			_this.viewer.scene.camera.moveEnd.removeEventListener(_this.cameraMoveEnd)
		}
		
		_this.globe.destroy();
	}catch (e) {
	}
}
