VFG.Viewer.prototype.cacheVModelMap=new Map();

VFG.Viewer.prototype.addVModel=function(option,url){
	var _this=this;
	if(_this.cacheVModelMap.has(option.id)) return;
	option.PrimitiveType='VideoModel';
	option.localUrl=url;
	var vmodel=new VFG.VModel(_this.viewer,option);
	_this.cacheVModelMap.set(option.id,vmodel);
} 

VFG.Viewer.prototype.addVModels=function(list,url){
	var _this=this;
	 for(var i=0;i<list.length;i++){
		 var vmodel=list[i]; 
		 _this.addVModel(vmodel,url);
	 }
} 

VFG.Viewer.prototype.removeVModel=function(vmodel){
	var _this=this;
	if(_this.cacheVModelMap.has(vmodel.id)){
		_this.removeVModelById(vmodel.id);
	}
}

VFG.Viewer.prototype.removeVModels=function(list){
	var _this=this;
	 for(var i=0;i<list.length;i++){
		 var vmodel=list[i]; 
		 _this.removeVModel(vmodel);
	 }
}

VFG.Viewer.prototype.removeVModelById=function(id){
	var _this=this;
	if(_this.cacheVModelMap.has(id)){
		var vmodel=_this.cacheVModelMap.get(id);
		vmodel.destroy();
		_this.cacheVModelMap.delete(id);
		vmodel=null;
	}
}

VFG.Viewer.prototype.getVModelById=function(id){
	var _this=this;
	if(_this.cacheVModelMap.has(id)){
		return _this.cacheVModelMap.get(id);
	}
} 

VFG.Viewer.prototype.removeAllVModel=function(layerId){
	var _this=this;
	_this.cacheVModelMap.forEach(function(vmodel,key){
		vmodel.destroy();
	});
	_this.cacheVModelMap.clear();
}

VFG.Viewer.prototype.flyToVModelById=function(id){
	if(this.cacheVModelMap.has(id)){
		var e=this.cacheVModelMap.get(id).option;
		if(e.cameraX && e.cameraX!='0'  && e.cameraY && e.cameraY!='0' && e.cameraZ){
			this.viewer.camera.flyTo({
			    destination : new Cesium.Cartesian3(e.cameraX*1,e.cameraY*1, e.cameraZ*1 ||10000),
			    orientation : {
			        heading : e.heading*1,
			        pitch : e.pitch*1,
			        roll : e.roll*1
			    }
			});
		}
	}
}

VFG.Viewer.prototype.changeVModelRX=function(id,rx){
	var _this=this;
	var vModel=_this.getVModelById(id);
	if(vModel){
		vModel.option.rotationX=rx;
		vModel.update();
	}
}

VFG.Viewer.prototype.changeVModelRY=function(id,ry){
	var _this=this;
	var vModel=_this.getVModelById(id);
	if(vModel){
		vModel.option.rotationY=ry;
		vModel.update();
	}
}

VFG.Viewer.prototype.changeVModelRZ=function(id,rz){
	var _this=this;
	var vModel=_this.getVModelById(id);
	if(vModel){
		vModel.option.rotationZ=rz;
		vModel.update();
	}
}

VFG.Viewer.prototype.changeVModelPosition=function(id,position){
	var _this=this;
	var vModel=_this.getVModelById(id);
	if(vModel){
		vModel.option.x=position.x;
		vModel.option.y=position.y;
		vModel.option.z=position.z;
		vModel.update();
	}
}

VFG.Viewer.prototype.changeVModelHeight=function(id,height){
	var _this=this;
	var vModel=_this.getVModelById(id);
	if(vModel){
		vModel.option.z=height;
		vModel.update();
	}
}

VFG.Viewer.prototype.changeVModelScale=function(id,scale){
	var _this=this;
	var vModel=_this.getVModelById(id);
	if(vModel){
		vModel.option.scaleX=scale.scaleX;
		vModel.option.scaleY=scale.scaleY;
		vModel.option.scaleZ=scale.scaleZ;
		vModel.update();
	}
}



