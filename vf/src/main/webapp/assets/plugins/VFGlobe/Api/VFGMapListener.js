/**
 * 监听集合
 */
VFG.Viewer.prototype.ListenerMap=new Map();
/**
 * 监听屏幕坐标
 * 	bizPoint
 *  callBack:fun(c2)回调函数
 */
VFG.Viewer.prototype.addListenerScreenPosition  = function(bizPoint,callBack) {
	if(!bizPoint)return;
	if(!this.ListenerMap.has(bizPoint.id)){
		var option = {
			position:{
		  		x:bizPoint.x*1,
		  		y:bizPoint.y*1,
		  		z:bizPoint.z*1
			},
			id:bizPoint.id,
			callBack:callBack
		}
		var listener=new VFG.Listener(this.viewer,option);
		this.ListenerMap.set(bizPoint.id,listener);
	}
}

/**
 * 移出
 */
VFG.Viewer.prototype.removeListenerScreenPosition= function(id) {
	if(this.ListenerMap.has(id)){
		var listener=this.ListenerMap.get(id).destroy();
		this.ListenerMap.delete(id)
	}
}

VFG.Viewer.prototype.addListenerCamera=function(callback){
	var _this=this;
	_this.removeListenerCamera();
	_this.handler3D = new Cesium.ScreenSpaceEventHandler(_this.viewer.scene.canvas);
	var viewer=this.viewer;
	_this.handler3D.setInputAction(function(movement) {
	    var pick= new Cesium.Cartesian2(movement.endPosition.x,movement.endPosition.y);
	    if(pick){
	        var cartesian = _this.viewer.scene.globe.pick(_this.viewer.camera.getPickRay(pick), viewer.scene);
	        if(cartesian){
	            // 世界坐标转地理坐标（弧度）
	            var cartographic = _this.viewer.scene.globe.ellipsoid.cartesianToCartographic(cartesian);
	            if(cartographic){
	                // 海拔
	                var height = _this.viewer.scene.globe.getHeight(cartographic);
	                // 视角海拔高度
	                var he = Math.sqrt(_this.viewer.scene.camera.positionWC.x * _this.viewer.scene.camera.positionWC.x + _this.viewer.scene.camera.positionWC.y * _this.viewer.scene.camera.positionWC.y + viewer.scene.camera.positionWC.z * _this.viewer.scene.camera.positionWC.z);
	                var he2 = Math.sqrt(cartesian.x * cartesian.x + cartesian.y * cartesian.y + cartesian.z * cartesian.z);
	                // 地理坐标（弧度）转经纬度坐标
	                var point=[ cartographic.longitude / Math.PI * 180, cartographic.latitude / Math.PI * 180];
	                if(!height){
	                    height = 0;
	                }
	                if(!he){
	                    he = 0;
	                }
	                if(!he2){
	                    he2 = 0;
	                }
	                if(!point){
	                    point = [0,0];
	                }
	                var level=0;
                    var tilesToRender = _this.viewer.scene.globe._surface._tilesToRender;
                    if(tilesToRender.length>0){
                    	level= tilesToRender[0].level;
                    }else{
                    	level=0;
                    }
                    
                    var heading = Cesium.Math.toDegrees(_this.viewer.camera.heading).toFixed(2)*1;;
                    var pitch = Cesium.Math.toDegrees(_this.viewer.camera.pitch).toFixed(2)*1;;
                    var roll = Cesium.Math.toDegrees(_this.viewer.camera.roll).toFixed(2)*1;;
                    
                    if(callback){
                    	callback({
                    		position:{
                    			x:point[0].toFixed(6),
                    			y:point[1].toFixed(6),
                    			z:height.toFixed(2)
                    		},
                    		zoom:level,
                    		camera:{
                    			heading:heading,
                    			pitch:pitch,
                    			roll:roll,
                    		}
                    	});
                    }
	            }
	        }
	    }
	    
	}, Cesium.ScreenSpaceEventType.MOUSE_MOVE);
}
VFG.Viewer.prototype.removeListenerCamera=function(){
	var _this=this;
	if(_this.handler3D){
		_this.handler3D.destroy();
	}
}
