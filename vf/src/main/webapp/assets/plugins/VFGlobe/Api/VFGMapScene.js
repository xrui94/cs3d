VFG.Viewer.prototype.showGlobe=function(value){
	this.viewer.scene.globe.show = value;
}
VFG.Viewer.prototype.showSkyBox=function(value){
	this.viewer.scene.skyBox.show = value;
}
VFG.Viewer.prototype.enableLighting=function(value){
	this.viewer.scene.globe.enableLighting = value;
}
VFG.Viewer.prototype.showShadows=function(value){
	this.viewer.shadows = value
}

VFG.Viewer.prototype.zoomIn=function(value){
	let cameraPos = this.viewer.camera.position;
	let ellipsoid = this.viewer.scene.globe.ellipsoid;
	let cartographic = ellipsoid.cartesianToCartographic(cameraPos);
	let height = cartographic.height;
	this.viewer.camera.zoomIn(height / value);
	
}

VFG.Viewer.prototype.zoomOut=function(value){
	let cameraPos = this.viewer.camera.position;
	let ellipsoid = this.viewer.scene.globe.ellipsoid;
	let cartographic = ellipsoid.cartesianToCartographic(cameraPos);
	let height = cartographic.height;
	this.viewer.camera.zoomOut(height * value);
}


