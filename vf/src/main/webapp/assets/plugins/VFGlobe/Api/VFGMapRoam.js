VFG.Viewer.prototype.cacheRoamMap=new Map();
VFG.Viewer.prototype.selectRoamMap=new Map();

VFG.Viewer.prototype.addRoam=function(option){
	var _this=this;
	var roam=new VFG.Roam(option);
	_this.cacheRoamMap.set(option.id,option);
}

VFG.Viewer.prototype.addRoams=function(list){
	var _this=this;
	 for(var i=0;i<list.length;i++){
		 var roam=list[i];
		 _this.addRoam(roam);
	 }
}

VFG.Viewer.prototype.removeRoams=function(list){
	var _this=this;
	for(var i=0;i<list.length;i++){
		var roam=list[i];
		_this.removeRoamById(roam.id);
	}
}

VFG.Viewer.prototype.showPath=function(list,show){
	var _this=this;
} 


VFG.Viewer.prototype.removeAllRoam=function(){
	var _this=this;
	_this.cacheRoamMap.forEach(function(value,key){
		value.destroy();
	});
	_this.cacheRoamMap.clear();
	_this.selectRoamMap.clear();
}

VFG.Viewer.prototype.removeRoamById=function(id){
	var _this=this;
	var roam=_this.getRoamById(id);
	if(roam){
		roam.destroy();
	}
	this.cacheRoamMap.delete(id);
	this.selectRoamMap.delete(id);
}

VFG.Viewer.prototype.removeRoam=function(roam){
	var _this=this;
	_this.removeRoamById(roam.id);
}

VFG.Viewer.prototype.getRoamById=function(id){
	var _this=this;
	if(_this.containRoam(id)){
		 return _this.cacheRoamMap.get(id);
	}
}

VFG.Viewer.prototype.containRoam=function(id){
	return this.cacheRoamMap.has(id)
}


VFG.Viewer.prototype.updateRoam=function(option){
	var _this=this;
	if(option.id){
		if(_this.containRoam(option.id)){
			var roam=_this.getRoamById(option.id);
			if(roam){
			}else{
				throw new Error("未找到对象!");
			}
		}else{
			 throw new Error("未找到对象!");
		}
	}else{
	   throw new Error("参数有误!");
	}
}


