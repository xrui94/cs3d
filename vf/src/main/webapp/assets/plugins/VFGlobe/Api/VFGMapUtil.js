/**
 * 获取UUID
 */
VFG.Viewer.prototype.getUuid = function() {
	var s = [];
	var hexDigits = "0123456789abcdef";
	for (var i = 0; i < 36; i++) {
		s[i] = hexDigits.substr(Math.floor(Math.random() * 0x10), 1);
	}
	s[14] = "4"; 
	s[19] = hexDigits.substr((s[19] & 0x3) | 0x8, 1); 
	s[8] = s[13] = s[18] = s[23] = "-";
	var uuid = s.join("");
	return uuid;
};

/**
 * 笛卡尔坐标转经纬度
 */
VFG.Viewer.prototype.getLnLaFormC3  = function(cartesian3) {
	return VFG.Util.getC3ToLnLa(this.viewer, cartesian3)
}

/**
 * 经纬度装屏幕坐标
 * position:{
 * 	x:经度
 *  y:维度
 *  z:高度
 * }
 */
VFG.Viewer.prototype.getC2FormLnLa = function(position) {
	return Cesium.SceneTransforms.wgs84ToWindowCoordinates(this.viewer.scene,this.getC3FormLnLa(position))
}

/**
 * 经纬度转笛卡尔坐标
 * position:{
 * 	x:经度
 *  y:维度
 *  z:高度
 * }
 */
VFG.Viewer.prototype.getC3FormLnLa = function(position) {
	return Cesium.Cartesian3.fromDegrees(position.x*1,position.y*1,position.z*1);
}


/**
 * Turf多边形
 */
VFG.Viewer.prototype.getTurfPolygon = function (points) {
	return VFG.Util.getTurfPolygon(points);
}

/**
 * Turf点
 */
VFG.Viewer.prototype.getTurfPoint = function (point) {
	return VFG.Util.getTurfPoint(point);
}

/**
 * 点是否在多边形内部
 */
VFG.Viewer.prototype.booleanPointInPolygon = function (polygon,point) {
	return VFG.Util.booleanPointInPolygon(polygon, point);
}

//unit:degrees, radians, miles, or kilometers
VFG.Viewer.prototype.distance = function (point1,point2,unit) {
	var from = turf.point([point1.x*1,point1.y*1]);
	var to = turf.point([point2.x*1,point2.y*1]);
	var options = {units: unit||'miles'};
	var distance = turf.distance(from, to, options);
	return turf.distance(from, to, options);
}
/**
 * 多边形内的点
 */
VFG.Viewer.prototype.getPointInPolygon = function (polygonPoints,points) {
	var polygon =VFG.Util.getTurfPolygon(polygonPoints);
	var inPoints=[];
	for(var i=0;i<points.length;i++){
		if(VFG.Util.booleanPointInPolygon(polygon, VFG.Util.getTurfPoint(points[i]))){
			inPoints.push(points[i]);
		}
	}
	return inPoints;
}
