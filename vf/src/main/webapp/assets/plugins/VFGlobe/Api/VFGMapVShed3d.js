VFG.Viewer.prototype.cacheVideoShed3dMap=new Map();

VFG.Viewer.prototype.addVideoShed3d=function(option){
	var _this=this;
	
	if(_this.cacheVideoShed3dMap.has(option.id)) return;
	
	option.PrimitiveType='VideoShed3d';
	var vPos=_this.getVideoShed3dViewPosition(option);
	
	var param={
		id:option.id||'', 
		name:option.name||'', 
		code:option.code||'', 
		option:option||{}, 
		cameraPosition:Cesium.Cartesian3.fromDegrees(option.x*1,option.y*1,option.z*1),
		position:Cesium.Cartesian3.fromDegrees(vPos.longitude*1,vPos.latitude*1,vPos.altitude*1), 
		alpha:option.alpha||1, 
		debugFrustum:false, 
		fov:option.fov||null, 
		heading:option.vHeading||null, 
		pitch:option.vPitch||null, 
		roll:option.vRoll||null,
		near:option.near||null,
		aspectRatio:option.aspectRatio||null, 
		maskUrl:option.maskUrl||null, 
	};
	
	var vShed3d=new VFG.VShed3d(_this.viewer,param);
	_this.cacheVideoShed3dMap.set(option.id,vShed3d);
} 

VFG.Viewer.prototype.getVideoShed3dViewPosition=function(option){
	return this.ECEF.enu_to_ecef({ 
		longitude:option.x*1, 
		latitude:option.y*1, 
		altitude:option.z*1
	},
	{ 
		distance: option.far*1, 
		azimuth:option.vHeading*1, 
		elevation:option.vPitch*1
	});
}
VFG.Viewer.prototype.addVideoShed3ds=function(list){
	var _this=this;
	 for(var i=0;i<list.length;i++){
		 var vShed3d=list[i]; 
		 _this.addVideoShed3d(vShed3d);
	 }
}

VFG.Viewer.prototype.removeVideoShed3d=function(vShed3d){
	var _this=this;
	if(_this.cacheVideoShed3dMap.has(vShed3d.id)){
		_this.removeVideoShed3dById(vShed3d.id);
	}
}

VFG.Viewer.prototype.removeVideoShed3ds=function(list){
	var _this=this;
	 for(var i=0;i<list.length;i++){
		 var vShed3d=list[i]; 
		 _this.removeVideoShed3d(vShed3d);
	 }
}

VFG.Viewer.prototype.removeVideoShed3dById=function(id){
	var _this=this;
	if(_this.cacheVideoShed3dMap.has(id)){
		var vShed3d=_this.cacheVideoShed3dMap.get(id);
		vShed3d.destroy();
		_this.cacheVideoShed3dMap.delete(id);
		vShed3d=null;
	}
}

VFG.Viewer.prototype.getVideoShed3dById=function(id){
	var _this=this;
	if(_this.cacheVideoShed3dMap.has(id)){
		return _this.cacheVideoShed3dMap.get(id);
	}
} 

VFG.Viewer.prototype.removeAllVideoShed3d=function(layerId){
	var _this=this;
	_this.cacheVideoShed3dMap.forEach(function(vShed3d,key){
		vShed3d.destroy();
	});
	_this.cacheVideoShed3dMap.clear();
}

VFG.Viewer.prototype.flyToVideoShed3dById=function(id){
	if(this.cacheVideoShed3dMap.has(id)){
		var e=this.cacheVideoShed3dMap.get(id).option;
		if(e.cameraX && e.cameraX!='0' && e.cameraY && e.cameraY!='0' && e.cameraZ){
			this.viewer.camera.flyTo({
			    destination : new Cesium.Cartesian3(e.cameraX*1,e.cameraY*1, e.cameraZ  || 2000 ),
			    orientation : {
			        heading : e.heading*1,
			        pitch : e.pitch*1,
			        roll : e.roll*1
			    }
			});
		}
	}
}

VFG.Viewer.prototype.changeFar=function(id,far){
  var videoShed3d=this.getVideoShed3dById(id);
  if(videoShed3d){
	  videoShed3d.option.far=far;
	  var vPos=this.getVideoShed3dViewPosition(videoShed3d.option);
	  if(vPos){
		  var position=Cesium.Cartesian3.fromDegrees(vPos.longitude*1,vPos.latitude*1,vPos.altitude*1);
		  videoShed3d.option.position=position;
		  videoShed3d.position=position;
	  }
  }
}

VFG.Viewer.prototype.changeNear=function(id,near){
  var videoShed3d=this.getVideoShed3dById(id);
  if(videoShed3d){
	  videoShed3d.option.near=near;
	  videoShed3d.near=near;
  }
}

VFG.Viewer.prototype.changeAlpha=function(id,alpha){
  var videoShed3d=this.getVideoShed3dById(id);
  if(videoShed3d){
	  videoShed3d.option.alpha=alpha;
	  videoShed3d.alpha=alpha;
  }
}
VFG.Viewer.prototype.changeAspectRatio=function(id,aspectRatio){
  var videoShed3d=this.getVideoShed3dById(id);
  if(videoShed3d){
	  videoShed3d.option.aspectRatio=aspectRatio;
	  videoShed3d.aspectRatio=aspectRatio
  }
}

VFG.Viewer.prototype.changeFov=function(id,fov){
  var videoShed3d=this.getVideoShed3dById(id);
  if(videoShed3d){
	  videoShed3d.option.fov=fov;
	  videoShed3d.fov=fov
  }
}

VFG.Viewer.prototype.changePitch=function(id,pitch){
  var videoShed3d=this.getVideoShed3dById(id);
  if(videoShed3d){
	  videoShed3d.option.vPitch=pitch;
	  videoShed3d._pitch=pitch;
	  var vPos=this.getVideoShed3dViewPosition(videoShed3d.option);
	  videoShed3d.position=Cesium.Cartesian3.fromDegrees(vPos.longitude*1,vPos.latitude*1,vPos.altitude*1)
  }
}

VFG.Viewer.prototype.changeHeading=function(id,heading){
  var videoShed3d=this.getVideoShed3dById(id);
  if(videoShed3d){
	  videoShed3d.option.vHeading=heading;
	  videoShed3d._heading=heading;
	  var vPos=this.getVideoShed3dViewPosition(videoShed3d.option);
	  videoShed3d.position=Cesium.Cartesian3.fromDegrees(vPos.longitude*1,vPos.latitude*1,vPos.altitude*1)
  }
}

VFG.Viewer.prototype.changeRoll=function(id,roll){
  var videoShed3d=this.getVideoShed3dById(id);
  if(videoShed3d){
	  videoShed3d.option.vRollg=roll;
	  videoShed3d.roll=roll
  }
}

VFG.Viewer.prototype.changeHeight=function(id,height){
  var videoShed3d=this.getVideoShed3dById(id);
  if(videoShed3d){
	  videoShed3d.option.z=height;
	  videoShed3d.cameraPosition=Cesium.Cartesian3.fromDegrees(videoShed3d.option.x*1,videoShed3d.option.y*1,videoShed3d.option.z*1)
  }
}

VFG.Viewer.prototype.changeMaskUrl=function(id,maskUrl){
  var videoShed3d=this.getVideoShed3dById(id);
  if(videoShed3d){
	  videoShed3d.option.maskUrl=maskUrl;
	  videoShed3d.maskUrl=maskUrl
  }
}

VFG.Viewer.prototype.changeDebugFrustum=function(id,debugFrustum){
  var videoShed3d=this.getVideoShed3dById(id);
  if(videoShed3d){
	  videoShed3d.option.debugFrustum=debugFrustum;
	  videoShed3d.debugFrustum=debugFrustum
  }
}

