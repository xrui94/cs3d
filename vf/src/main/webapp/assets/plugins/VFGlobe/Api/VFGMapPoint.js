VFG.Viewer.prototype.cachePointMap=new Map();
VFG.Viewer.prototype.selectPointMap=new Map();

VFG.Viewer.prototype.addPoint=function(option){
	var _this=this;
	VFG.Point.add(_this.viewer,{
		point:option,
		style:option.layerId?_this.getLayerStyle(option.layerId,'DEFAULT'):null
	},_this.url);
	_this.cachePointMap.set(option.id,option);
}

VFG.Viewer.prototype.addPoints=function(list){
	var _this=this;
	 for(var i=0;i<list.length;i++){
		 var point=list[i];
		 _this.addPoint(point);
	 }
}

VFG.Viewer.prototype.removePoints=function(list){
	var _this=this;
	for(var i=0;i<list.length;i++){
		var point=list[i];
		_this.removePointById(point.id);
	}
}

VFG.Viewer.prototype.showPoints=function(list,show){
	var _this=this;
	for(var i=0;i<list.length;i++){
		var point=list[i];
		VFG.Point.show(_this.viewer,{
			id:point.id,
			show:show,
		});
	}
} 

VFG.Viewer.prototype.showPoint=function(option){
	var _this=this;
	VFG.Point.show(_this.viewer,option);
}

VFG.Viewer.prototype.removeAllPoint=function(){
	var _this=this;
	_this.cachePointMap.forEach(function(value,key){
		VFG.Point.removeById(_this.viewer,key);
	});
	_this.cachePointMap.clear();
	_this.selectPointMap.clear();
}

VFG.Viewer.prototype.removePointById=function(id){
	var _this=this;
	VFG.Point.removeById(_this.viewer,id);
	this.cachePointMap.delete(id);
	this.selectPointMap.delete(id);
}

VFG.Viewer.prototype.removePoint=function(point){
	var _this=this;
	VFG.Point.removeById(_this.viewer,point.id);
	this.cachePointMap.delete(point.id);
	this.selectPointMap.delete(point.id);
}

VFG.Viewer.prototype.changePointStyle=function(id,event){
	var _this=this;
	 var point=_this.getPointById(id);
	 if(point!=null){
		 if('DEFAULT'==event && point.layerId){
			 var style=_this.getLayerStyle(point.layerId,'DEFAULT');
			 if(style && "1"==style.enabled){
				 _this.setPointStyle({
					 id:point.id,
					 style:style,
					 cache:false,
				 });
			 }
		 }
		 else if('HOVER'==event && point.layerId){
			 if(!_this.selectPointMap.has(id)){
				 var style=_this.getLayerStyle(point.layerId,'HOVER');
				 if(style && "1"==style.enabled){
					 _this.setPointStyle({
						 id:point.id,
						 style:style,
						 cache:false,
					 });
				 }
			 }
		 }
		 else if('SELECTED'==event && point.layerId){
			 var style=_this.getLayerStyle(point.layerId,'SELECTED');
			 if(style && "1"==style.enabled){
				 _this.setPointStyle({
					 id:point.id,
					 style:style,
					 cache:true,
				 });
			 }
		 }else{
			 if(point.layerId){
				 var style=_this.getLayerStyle(point.layerId,'DEFAULT');
				 if(style && "1"==style.enabled){
					 _this.setPointStyle({
						 id:point.id,
						 style:style,
						 cache:true,
					 });
				 }
			 }
		 }
	 }
}

VFG.Viewer.prototype.flyToPointById=function(id,complete){
	if(this.containPoint(id)){
		this.flyToPoint(this.cachePointMap.get(id))
	}
	if(complete){
		complete(this);
	}
}

VFG.Viewer.prototype.setViewPointById=function(id,complete){
	if(this.containPoint(id)){
		this.setViewPoint(this.cachePointMap.get(id))
	}
	if(complete){
		complete(this);
	}
}

VFG.Viewer.prototype.flyToPoint=function(point,complete){
	var _this=this;
	if(point.cameraX && point.cameraX && point.cameraZ && point.heading && point.pitch && point.roll){
		this.viewer.camera.flyTo({
			destination:new Cesium.Cartesian3(point.cameraX*1,point.cameraY*1,point.cameraZ*1),
			orientation:{
		        heading : point.heading*1,
		        pitch : point.pitch*1,
		        roll : point.roll*1,
		    }
		});
		if(complete){
			complete(this);
		}
	}else{
		this.viewer.camera.flyTo({
			destination:Cesium.Cartesian3.fromDegrees(point.x*1,point.y*1,point.z*1+100||10000)
		});
		if(complete){
			complete(this);
		}
	}
}

VFG.Viewer.prototype.setViewPoint=function(point,complete){
	var _this=this;
	if(point.cameraX && point.cameraX && point.cameraZ && point.heading && point.pitch && point.roll){
		this.viewer.scene.camera.setView({
			destination:new Cesium.Cartesian3(point.cameraX*1,point.cameraY*1,point.cameraZ*1),
			orientation:{
		        heading : point.heading*1,
		        pitch : point.pitch*1,
		        roll : point.roll*1,
		    }
		});
		if(complete){
			complete(this);
		}
	}else{
		this.viewer.scene.camera.setView({
			destination:Cesium.Cartesian3.fromDegrees(point.x*1,point.y*1,point.z*1+100||10000)
		});
		if(complete){
			complete(this);
		}
	}
}

VFG.Viewer.prototype.getPointById=function(id){
	var _this=this;
	if(_this.containPoint(id)){
		 return _this.cachePointMap.get(id);
	}
}

VFG.Viewer.prototype.containPoint=function(id){
	return this.cachePointMap.has(id)
}

VFG.Viewer.prototype.setPointStyle=function(param){
	var _this=this;
	var point=_this.getPointById(param.id);
	if(point && param.style){
	   VFG.Point.update(_this.viewer,{
		point:point,
	 	style:param.style
	   },_this.url);
	   if(param.cache==true){
		   _this.selectPointMap.set(point.id,point);
	   }else{
		   _this.selectPointMap.delete(point.id);
	   }
	}
}

VFG.Viewer.prototype.updatePoint=function(option){
	var _this=this;
	if(option.id){
		if(_this.containPoint(option.id)){
			var point=_this.getPointById(option.id);
			if(point){
				point.layerId=option.layerId||point.layerId
				point.name=option.name||point.name
				point.code=option.code||point.code
				point.telephone=option.telephone||point.telephone
				point.adress=option.adress||point.adress
				point.x=option.x||point.x
				point.y=option.y||point.y
				point.z=option.z||point.z
				point.heading=option.heading||point.heading
				point.pitch=option.pitch||point.pitch
				point.roll=option.roll||point.roll
				point.cameraX=option.cameraX||point.cameraX
				point.cameraY=option.cameraY||point.cameraY
				point.cameraZ=option.cameraZ||point.cameraZ
				point.terrainHeight=option.terrainHeight||point.terrainHeight
				point.modelHeight=option.modelHeight||point.modelHeight
				 var style=_this.getLayerStyle(point.layerId,'DEFAULT');
				 if(style && "1"==style.enabled){
					 _this.setPointStyle({
						 id:point.id,
						 style:style,
						 cache:true,
					 });
				 }else{
				   VFG.Point.update(_this.viewer,{
						point:point,
					 	style:null
					   },_this.url);
					_this.cachePointMap.set(option.id,point);
				 }
			}else{
				throw new Error("未找到对象!");
			}
		}else{
			 throw new Error("未找到对象!");
		}
	}else{
	   throw new Error("参数有误!");
	}
}


