VFG.Draw = function() {
}

//绘制点
VFG.Draw.Point=function(viewer,option){
	this.option=option;
	this.viewer=viewer;
	this.entity;
	this.cesiumTips=new Cesium.Tip(this.viewer);
	this.handler = new Cesium.ScreenSpaceEventHandler(this.viewer.scene.canvas);
	this.init();
	return this;
}

VFG.Draw.Point.prototype.init=function(){
	var _this=this;
	var position;
	_this.handler.setInputAction(function (movement) {
        var cartesian3 =VFG.Util.getScreenToC3(_this.viewer,movement.position,_this.entity);
        if (Cesium.defined(cartesian3)){
        	position=cartesian3;
        	 if(!Cesium.defined(_this.entity)){
             	_this.entity =_this.viewer.entities.add({
            		id:VFG.Util.getUuid(),
            		position: new Cesium.CallbackProperty(function () {
        	            return position;
        	        }, false),
                    point: {
                        color: Cesium.Color.RED,
                        pixelSize: 5,
                        outline: true,
                        outlineColor: Cesium.Color.YELLOW.withAlpha(1),
                    	outlineWidth:1,
                    }
                });
        	 }
        }
    }, Cesium.ScreenSpaceEventType.LEFT_CLICK);
	
	_this.handler.setInputAction(function (movement) {
        if(Cesium.defined(_this.entity)){
        	_this.cesiumTips.showAt(movement.endPosition, `<div class="con" style="color: white;">单击更新位置，右键结束！</div>`);
        }else{
    		_this.cesiumTips.showAt(movement.endPosition, `<div class="con" style="color: white;">单击添加点，右键结束！</div>`);
    	}
    }, Cesium.ScreenSpaceEventType.MOUSE_MOVE);
	
    //右键点击操作
	_this.handler.setInputAction(function (click) {
    	if(_this.option && _this.option.finish){
    		 _this.option.finish(VFG.Util.getLnLaFormC3(_this.viewer,position));
    	}
    	_this.destroy();
    }, Cesium.ScreenSpaceEventType.RIGHT_CLICK);
}

VFG.Draw.Point.prototype.destroy=function(){
    if( this.handler){
    	this.handler.destroy();
    	this.handler = null;
    };
    if(Cesium.defined(this.entity)){
    	this.viewer.entities.remove(this.entity);
    }
    if( this.cesiumTips){
    	this.cesiumTips.destroy();
    	this.cesiumTips=null;
    };
    delete this.viewer,
    delete this.entity,
	delete this.cesiumTips,
	delete this.handler;
	return Cesium.destroyObject(this);
}


//绘制折线
VFG.Draw.Polyline=function(viewer,option){
	this.option=option;
	this.viewer=viewer;
	this.entity;
	this.cesiumTips=new Cesium.Tip(this.viewer);
	this.handler = new Cesium.ScreenSpaceEventHandler(this.viewer.scene.canvas);
	this.init();
	return this;
}

VFG.Draw.Polyline.prototype.init=function(){
	var _this=this;
	var positions= [];
	_this.handler.setInputAction(function (movement) {
        var cartesian3 =VFG.Util.getScreenToC3(_this.viewer,movement.position,_this.entity);
        if (Cesium.defined(cartesian3)){
            if (positions.length == 0) {
            	positions.push(cartesian3.clone());
            }
            positions.push(cartesian3);
        }
    }, Cesium.ScreenSpaceEventType.LEFT_CLICK);
	
	_this.handler.setInputAction(function (movement) {
    	var cartesian3 =VFG.Util.getScreenToC3(_this.viewer,movement.endPosition,_this.entity);
        if(Cesium.defined(cartesian3)){
        	positions.pop();
        	positions.push(cartesian3);
        }
        if (positions.length >= 2 && !Cesium.defined(_this.entity)){
        	_this.entity =_this.viewer.entities.add({
        		id:VFG.Util.getUuid(),
                polyline: {
                    positions: new Cesium.CallbackProperty(function () {
        	            return positions;
        	        }, false),
                    width: 3.0,
                    clampToGround: true,
                    material:  Cesium.Color.RED,
                }
            });
        }
    	if (positions.length > 1){
    		_this.cesiumTips.showAt(movement.endPosition, `<div class="con" style="color: white;">请绘制下一个点，右键结束！</div>`);
    	}else{
    		_this.cesiumTips.showAt(movement.endPosition, `<div class="con" style="color: white;">左键单击绘制，右键结束绘制！</div>`);
    	}
    }, Cesium.ScreenSpaceEventType.MOUSE_MOVE);
	
    //右键点击操作
	_this.handler.setInputAction(function (click) {
    	if(positions){
		  positions.pop();
    	}
    	if(_this.option && _this.option.finish){
    		 _this.option.finish(VFG.Util.c3sToLnLas(_this.viewer,positions));
    	}
    	_this.destroy();
    }, Cesium.ScreenSpaceEventType.RIGHT_CLICK);
}

VFG.Draw.Polyline.prototype.destroy=function(){
    if( this.handler){
    	this.handler.destroy();
    	this.handler = null;
    };
    if(Cesium.defined(this.entity)){
    	this.viewer.entities.remove(this.entity);
    }
    if( this.cesiumTips){
    	this.cesiumTips.destroy();
    	this.cesiumTips=null;
    };
    delete this.viewer,
    delete this.entity,
	delete this.cesiumTips,
	delete this.handler;
	return Cesium.destroyObject(this);
}

//绘制多边形
VFG.Draw.Polygon=function(viewer,option){
	this.option=option;
	this.viewer=viewer;
	this.entity;
	this.cesiumTips=new Cesium.Tip(this.viewer);
	this.handler = new Cesium.ScreenSpaceEventHandler(this.viewer.scene.canvas);
	this.init();
	return this;
}

VFG.Draw.Polygon.prototype.init=function(){
	var _this=this;
	var positions= [];
	_this.handler.setInputAction(function (movement) {
        var cartesian3 =VFG.Util.getScreenToC3(_this.viewer,movement.position,_this.entity);
        if (Cesium.defined(cartesian3)){
            if (positions.length == 0) {
            	positions.push(cartesian3.clone());
            }
            positions.push(cartesian3);
        }
    }, Cesium.ScreenSpaceEventType.LEFT_CLICK);
	
	_this.handler.setInputAction(function (movement) {
    	var cartesian3 =VFG.Util.getScreenToC3(_this.viewer,movement.endPosition,_this.entity);
        if(Cesium.defined(cartesian3)){
        	positions.pop();
        	positions.push(cartesian3);
        }
        if (positions.length >= 2 && !Cesium.defined(_this.entity)){
        	_this.entity =_this.viewer.entities.add({
        		id:VFG.Util.getUuid(),
//                polygon:{
//                    hierarchy : new Cesium.CallbackProperty(function () {
//        	            return new Cesium.PolygonHierarchy(positions);
//        	        }, false),
//                    perPositionHeight: false,
//                    material: Cesium.Color.RED.withAlpha(0.7),
//                }
                polyline: {
                    positions: new Cesium.CallbackProperty(function () {
        	            return positions;
        	        }, false),
                    width: 2.0,
                   /* clampToGround: false,*/
                    material:  Cesium.Color.RED,
                }
            });
        }
    	if (positions.length > 1){
    		_this.cesiumTips.showAt(movement.endPosition, `<div class="con" style="color: white;">请绘制下一个点，右键结束！</div>`);
    	}else{
    		_this.cesiumTips.showAt(movement.endPosition, `<div class="con" style="color: white;">左键单击绘制，右键结束绘制！</div>`);
    	}
    }, Cesium.ScreenSpaceEventType.MOUSE_MOVE);
	
    //右键点击操作
	_this.handler.setInputAction(function (click) {
    	if(positions){
		  positions.pop();
    	}
    	if(_this.option && _this.option.finish){
    		 _this.option.finish(VFG.Util.c3sToLnLas(_this.viewer,positions));
    	}
    	_this.destroy();
    }, Cesium.ScreenSpaceEventType.RIGHT_CLICK);
}

VFG.Draw.Polygon.prototype.destroy=function(){
    if( this.handler){
    	this.handler.destroy();
    	this.handler = null;
    };
    if(Cesium.defined(this.entity)){
    	this.viewer.entities.remove(this.entity);
    }
    if( this.cesiumTips){
    	this.cesiumTips.destroy();
    	this.cesiumTips=null;
    };
    delete this.viewer,
    delete this.entity,
	delete this.cesiumTips,
	delete this.handler;
	return Cesium.destroyObject(this);
}

//绘制画圆
VFG.Draw.Circle=function(viewer,option){
	this.option=option;
	this.viewer=viewer;
	this.entity;
	this.cesiumTips=new Cesium.Tip(this.viewer);
	this.handler = new Cesium.ScreenSpaceEventHandler(this.viewer.scene.canvas);
	this.init();
	return this;
}

VFG.Draw.Circle.prototype.init=function(){
	var _this=this;
	var positions= [];
	var geodesic = new Cesium.EllipsoidGeodesic();
	_this.handler.setInputAction(function (movement) {
        var cartesian3 =VFG.Util.getScreenToC3(_this.viewer,movement.position,_this.entity);
        if (Cesium.defined(cartesian3)){
            if (positions.length == 0) {
            	positions.push(cartesian3.clone());
            }
            positions.push(cartesian3);
        }
        
        if(positions.length>2){
          	if(_this.option && _this.option.finish){
                var satrt = Cesium.Cartographic.fromCartesian(positions[0]);
                var end = Cesium.Cartographic.fromCartesian(positions[positions.length-1]);    
                geodesic.setEndPoints(satrt, end);
          		_this.option.finish({
          			center:VFG.Util.getLnLaFormC3(_this.viewer,positions[0]),
          			radius: geodesic.surfaceDistance.toFixed(3)*1
          		});
          	}
          	_this.destroy();
        }
        
    }, Cesium.ScreenSpaceEventType.LEFT_CLICK);
	
	_this.handler.setInputAction(function (movement) {
    	var cartesian3 =VFG.Util.getScreenToC3(_this.viewer,movement.endPosition,_this.entity);
        if(Cesium.defined(cartesian3)){
        	positions.pop();
        	positions.push(cartesian3);
        }
        if (positions.length >= 2 && !Cesium.defined(_this.entity)){
        	_this.entity =_this.viewer.entities.add({
        		id:VFG.Util.getUuid(),
        		position:positions[0],
        		point: {
                   color: Cesium.Color.YELLOW,
                   pixelSize: 5,
                },
        		ellipse:{
                	semiMajorAxis:new Cesium.CallbackProperty(function(){
                        var satrt = Cesium.Cartographic.fromCartesian(positions[0]);
                        var end = Cesium.Cartographic.fromCartesian(positions[positions.length-1]);                   
                        geodesic.setEndPoints(satrt, end);
                        return geodesic.surfaceDistance.toFixed(3)*1
                	}, false),
                	semiMinorAxis:new Cesium.CallbackProperty(function(){
                        var satrt = Cesium.Cartographic.fromCartesian(positions[0]);
                        var end = Cesium.Cartographic.fromCartesian(positions[positions.length-1]);                   
                        geodesic.setEndPoints(satrt, end);
                        return geodesic.surfaceDistance.toFixed(3)*1
                	}, false),
                    material: Cesium.Color.RED.withAlpha(0.7),
                    outline: true,
                    outlineColor: Cesium.Color.YELLOW.withAlpha(1),
                    outlineWidth:1,
                }
            });
        }
    	if (positions.length > 1){
    		_this.cesiumTips.showAt(movement.endPosition, `<div class="con" style="color: white;">请绘制下一个点，右键结束！</div>`);
    	}else{
    		_this.cesiumTips.showAt(movement.endPosition, `<div class="con" style="color: white;">左键单击绘制，右键结束绘制！</div>`);
    	}
    }, Cesium.ScreenSpaceEventType.MOUSE_MOVE);
	
    //右键点击操作
	_this.handler.setInputAction(function (click) {
    	if(positions){
		  positions.pop();
    	}
    	if(_this.option && _this.option.finish){
    		 _this.option.finish();
    	}
    	_this.destroy();
    }, Cesium.ScreenSpaceEventType.RIGHT_CLICK);
}


VFG.Draw.Circle.prototype.destroy=function(){
    if( this.handler){
    	this.handler.destroy();
    	this.handler = null;
    };
    if(Cesium.defined(this.entity)){
    	this.viewer.entities.remove(this.entity);
    }
    if( this.cesiumTips){
    	this.cesiumTips.destroy();
    	this.cesiumTips=null;
    };
    delete this.viewer,
    delete this.entity,
	delete this.cesiumTips,
	delete this.handler;
	return Cesium.destroyObject(this);
}

//绘制矩形
VFG.Draw.Rectangle=function(viewer,option){
	this.option=option;
	this.viewer=viewer;
	this.entity;
	this.cesiumTips=new Cesium.Tip(this.viewer);
	this.handler = new Cesium.ScreenSpaceEventHandler(this.viewer.scene.canvas);
	this.init();
	return this;
}

VFG.Draw.Rectangle.prototype.init=function(){
	var _this=this;
	var positions= [];
	var geodesic = new Cesium.EllipsoidGeodesic();
	_this.handler.setInputAction(function (movement) {
        var cartesian3 =VFG.Util.getScreenToC3(_this.viewer,movement.position,_this.entity);
        if (Cesium.defined(cartesian3)){
            if (positions.length == 0) {
            	positions.push(cartesian3.clone());
            }
            positions.push(cartesian3);
        }
        
        if(positions.length>2){
          	if(_this.option && _this.option.finish){
                var satrt = Cesium.Cartographic.fromCartesian(positions[0]);
                var end = Cesium.Cartographic.fromCartesian(positions[positions.length-1]);    
                geodesic.setEndPoints(satrt, end);
          		_this.option.finish({
          			center:positions[0],
          			radius: geodesic.surfaceDistance.toFixed(3)*1
          		});
          	}
          	_this.destroy();
        }
        
    }, Cesium.ScreenSpaceEventType.LEFT_CLICK);
	
	_this.handler.setInputAction(function (movement) {
    	var cartesian3 =VFG.Util.getScreenToC3(_this.viewer,movement.endPosition,_this.entity);
        if(Cesium.defined(cartesian3)){
        	positions.pop();
        	positions.push(cartesian3);
        }
        if (positions.length >= 2 && !Cesium.defined(_this.entity)){
//        	_this.entity =_this.viewer.entities.add({
//        		id:VFG.Util.getUuid(),
//                rectangle:{
//                    coordinates :_self.shape.rect,
//                    material : Cesium.Color.RED.withAlpha(0.4),
//                    height:0,
//                    outline: true,
//                    outlineColor: Cesium.Color.YELLOW.withAlpha(1),
//                    outlineWidth:1,
//                }
//            });
        }
    	if (positions.length > 1){
    		_this.cesiumTips.showAt(movement.endPosition, `<div class="con" style="color: white;">请绘制下一个点，右键结束！</div>`);
    	}else{
    		_this.cesiumTips.showAt(movement.endPosition, `<div class="con" style="color: white;">左键单击绘制，右键结束绘制！</div>`);
    	}
    }, Cesium.ScreenSpaceEventType.MOUSE_MOVE);
	
    //右键点击操作
	_this.handler.setInputAction(function (click) {
    	if(positions){
		  positions.pop();
    	}
    	if(_this.option && _this.option.finish){
    		 _this.option.finish();
    	}
    	_this.destroy();
    }, Cesium.ScreenSpaceEventType.RIGHT_CLICK);
}


VFG.Draw.Rectangle.prototype.destroy=function(){
    if( this.handler){
    	this.handler.destroy();
    	this.handler = null;
    };
    if(Cesium.defined(this.entity)){
    	this.viewer.entities.remove(this.entity);
    }
    if( this.cesiumTips){
    	this.cesiumTips.destroy();
    	this.cesiumTips=null;
    };
    delete this.viewer,
    delete this.entity,
	delete this.cesiumTips,
	delete this.handler;
	return Cesium.destroyObject(this);
}


 