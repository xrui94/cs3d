﻿///<jscompress sourcefile="VFGMap.js" />
/**
 * container:容器Id
 * options： 调用接口：http://localhost:8080/vf/i/scene/get(sceneId) 
 * options.url=http://localhost:8080/vf
 */
VFG.Viewer=function(container, options){
	if(!container || !options){
		console.log('参数必填!!!');
		return;
	}
	this.MAP_STYLE=options.MAP_STYLE;
	this.MAP_LAYER=options.MAP_LAYER;
	this.container=container||''
	this.url=options.url;
	this.SCENE=options||null;
	this.sceneId=this.SCENE.id||''
	this.geocoder=options.geocoder||false;
	this.complete=options.complete||null;
	this.providers=options.maps||null;
	this.viewer=null;
	this.scene=null;
	this.tip=null;
	this.ECEF =new Cesium.ECEF();
	return this.init();
}

VFG.Viewer.prototype.init=function(){
	let _this=this;
	_this.ellipsoid = Cesium.Ellipsoid.WGS84;
	var imageryProvider;
	if(this.providers){
		var p=this.getFirstProvider(this.providers);
		if(p){
			imageryProvider=VFG.Provider.packProvider(p);
			VFG.Provider.Map.set(p.id,imageryProvider);
			_this.cacheProviderMap.set(p.id,p);
		}
	}
	
	_this.globe = new VFG.Globe({
		domId: _this.container,
		geocoder: _this.geocoder,
		imageryProvider:imageryProvider
	});
	
	
   
	_this.viewer=_this.globe.viewer;
	_this.scene=_this.globe.viewer.scene;
	_this.tip=new VFG.Tip(_this.viewer);
	_this.handler = new Cesium.ScreenSpaceEventHandler(_this.viewer.scene.canvas);
	
	var CesiumViewerSceneController = _this.viewer.scene.screenSpaceCameraController;
    CesiumViewerSceneController.inertiaSpin = 0.1;
    CesiumViewerSceneController.inertiaTranslate = 0.1;
    CesiumViewerSceneController.inertiaZoom = 0.1;
    
	if(_this.SCENE.enableLighting && _this.SCENE.enableLighting=='1'){
		 _this.enableLighting(true);
	}else{
		 _this.enableLighting(false);
	}
	if(_this.SCENE.shadows && _this.SCENE.shadows=='1'){
		 _this.showShadows(true);
	}else{
		 _this.showShadows(false);
	}
	if(_this.SCENE.globeShow && _this.SCENE.globeShow=='1'){
		 _this.showGlobe(true);
	}else{
		_this.showGlobe(false);
	}
	if(_this.SCENE.skyBoxShow && _this.SCENE.skyBoxShow=='1'){
	    _this.showSkyBox(true);
	}else{
	    _this.showSkyBox(false);
	}
    
    _this.addProviders(this.providers);
	
    if(_this.SCENE.models){
    	 _this.addModels(_this.SCENE.models);
    }
    _this.MOUSE_MOVE_PRIMITIVE();
	if(this.complete){
		_this.flyToHome({
			complete:this.complete
		});
	}else{
		_this.flyToHome({
			complete:this.complete
		});	
	}
	return this;
}

VFG.Viewer.prototype.getFirstProvider=function(providers){
	var _this=this;
	var provider;
	if(providers){
		for(var i=0;i<providers.length;i++){
			if(providers[i].dataType=='map'){
				return providers[i];
			}
		}
	}
	return provider;
}

VFG.Viewer.prototype.flyToHome=function(option){
	var _this=this;
	if(_this.SCENE.x && _this.SCENE.y && _this.SCENE.z && _this.SCENE.heading && _this.SCENE.pitch && _this.SCENE.roll){
		this.viewer.camera.flyTo({
			destination:Cesium.Cartesian3.fromDegrees(_this.SCENE.x,_this.SCENE.y, _this.SCENE.z),
			orientation:{
		        heading : Cesium.Math.toRadians(_this.SCENE.heading*1),
		        pitch : Cesium.Math.toRadians(_this.SCENE.pitch*1),
		        roll : Cesium.Math.toRadians(_this.SCENE.roll*1),
		    },
	        complete: function () {
	        	if(option.complete){
	        		option.complete(_this);
	        	}
	        },
	        cancle: function () {
	        	if(option.cancle){
	        		option.cancle(_this);
	        	}
	        },
		});
	}else{
    	if(option.complete){
    		option.complete(_this);
    	}
	}
}

VFG.Viewer.prototype.setViewToHome=function(){
	var _this=this;
	if(_this.SCENE.x && _this.SCENE.y && _this.SCENE.z && _this.SCENE.heading && _this.SCENE.pitch && _this.SCENE.roll){
		this.viewer.scene.camera.setView({
			destination:Cesium.Cartesian3.fromDegrees(_this.SCENE.x,_this.SCENE.y, _this.SCENE.z),
			orientation:{
		        heading : Cesium.Math.toRadians(_this.SCENE.heading*1),
		        pitch : Cesium.Math.toRadians(_this.SCENE.pitch*1),
		        roll : Cesium.Math.toRadians(_this.SCENE.roll*1),
		    },
	        complete: function () {
	        	if(option.complete){
	        		option.complete(_this);
	        	}
	        },
	        cancle: function () {
	        	if(option.cancle){
	        		option.cancle(_this);
	        	}
	        },
		});
	}else{
    	if(option.complete){
    		option.complete(_this);
    	}
	}
}

VFG.Viewer.prototype.flyTo=function(option){
	var position=option.position;
	var orientation=option.orientation;
	var param={};
	if(position){
		param.destination=Cesium.Cartesian3.fromDegrees(position.x,position.y, position.z);
	}
	if(orientation){
		param.orientation={
	        heading : Cesium.Math.toRadians(orientation.heading*1),
	        pitch : Cesium.Math.toRadians(orientation.pitch*1),
	        roll : Cesium.Math.toRadians(orientation.roll*1),
	    };
	}
	this.viewer.camera.flyTo(param);
}

VFG.Viewer.prototype.setView=function(option){
	var position=option.position;
	var orientation=option.orientation;
	var param={};
	if(position){
		param.destination=Cesium.Cartesian3.fromDegrees(position.x,position.y, position.z);
	}
	if(orientation){
		param.orientation={
	        heading : Cesium.Math.toRadians(orientation.heading*1),
	        pitch : Cesium.Math.toRadians(orientation.pitch*1),
	        roll : Cesium.Math.toRadians(orientation.roll*1),
	    };
	}
	this.viewer.scene.camera.setView(param);
}

VFG.Viewer.prototype.setView=function(option){
	var position=option.position;
	var orientation=option.orientation;
	var param={};
	if(position){
		param.destination=Cesium.Cartesian3.fromDegrees(position.x,position.y, position.z);
	}
	if(orientation){
		param.orientation={
	        heading : Cesium.Math.toRadians(orientation.heading*1),
	        pitch : Cesium.Math.toRadians(orientation.pitch*1),
	        roll : Cesium.Math.toRadians(orientation.roll*1),
	    };
	}
	this.viewer.scene.camera.setView(param);
}


VFG.Viewer.prototype.hideGlobe=function() {
	this.viewer.scene.sun.show = false; 
	this.viewer.scene.moon.show = false;
	this.viewer.scene.skyBox.show = false;//关闭天空盒，否则会显示天空颜色
	//this.viewer.scene.undergroundMode = true; //重要，开启地下模式，设置基色透明，这样就看不见黑色地球了
	//this.viewer.scene.underGlobe.show = true;
	//this.viewer.scene.underGlobe.baseColor = new Cesium.Color(0, 0, 0, 0);
	this.viewer.scene.globe.show = false; //不显示地球，这条和地球透明度选一个就可以
	this.viewer.scene.globe.baseColor = new Cesium.Color(0, 0, 0, 0);
	this.viewer.scene.backgroundcolor = new Cesium.Color(0, 0, 0, 0)
};

VFG.Viewer.prototype.updateStyle=function(style) {
	if(!this.MAP_STYLE){
		this.MAP_STYLE={};
	}
	this.MAP_STYLE[style.id]=style;
};

VFG.Viewer.prototype.removeStyle=function(styleId) {
	if(this.MAP_STYLE && this.MAP_STYLE[styleId]){
		delete this.MAP_STYLE[styleId]
	}
};
VFG.Viewer.prototype.getStyle=function(styleId) {
	if(this.MAP_STYLE && this.MAP_STYLE[styleId]){
		return this.MAP_STYLE[styleId];
	}
};

VFG.Viewer.prototype.updateLayer=function(layer) {
	if(!this.MAP_LAYER){
		this.MAP_LAYER={};
	}
	this.MAP_LAYER[layer.id]=layer;
};

VFG.Viewer.prototype.removeLayer=function(layerId) {
	if(this.MAP_LAYER && this.MAP_LAYER[layerId]){
		delete this.MAP_LAYER[layerId]
	}
};

VFG.Viewer.prototype.getLayerStyle=function(layerId,eventType) {
	if(this.MAP_LAYER && this.MAP_LAYER[layerId]){
		var layer=this.MAP_LAYER[layerId];
		if('DEFAULT'==eventType){
			return this.getStyle(layer.defaultStyleId);
		}
		else if('HOVER'==eventType){
			return this.getStyle(layer.hoverStyleId);
		}
		else if('SELECTED'==eventType){
			return this.getStyle(layer.selectedStyleId);
		}
	}
};


VFG.Viewer.prototype.createTranslationAxis=function(option) {
	return new VFG.Controls.Drag(this.viewer,{
		id:option.id,
		position:Cesium.Cartesian3.fromDegrees(option.position.x*1,option.position.y*1,option.position.z*1),
		callback:option.callback
	});
};

VFG.Viewer.prototype.destroy=function(){
	var _this = this;
	try{
		_this.closeWebSocket();
		_this.handler.destroy();
		if(_this.handler3D){
			_this.handler3D.destroy();
		}
		if(_this.cameraMoveEnd){
			_this.viewer.scene.camera.moveEnd.removeEventListener(_this.cameraMoveEnd)
		}
		
		_this.globe.destroy();
	}catch (e) {
	}
}
;
///<jscompress sourcefile="VFGMapCluster.js" />
VFG.Viewer.prototype.cacheClusterMap=new Map();

;
///<jscompress sourcefile="VFGMapDraw.js" />
VFG.Viewer.prototype.drawPolyline=function(option){
	return new VFG.Draw.Polyline(this.viewer,option);
}

VFG.Viewer.prototype.drawPolygon=function(option){
	return new VFG.Draw.Polygon(this.viewer,option);
}

VFG.Viewer.prototype.drawCircle=function(option){
	return new VFG.Draw.Circle(this.viewer,option);
}

VFG.Viewer.prototype.drawRectangle=function(option){
	return new VFG.Draw.Rectangle(this.viewer,option);
}

VFG.Viewer.prototype.drawPoint=function(option){
	return new VFG.Draw.Point(this.viewer,option);
}
;
///<jscompress sourcefile="VFGMapEvent.js" />
/**
 * 鼠标左键单击拾取实体
 * complete(c2,data)：回调函数
 */
VFG.Viewer.prototype.LEFT_CLICK_PRIMITIVE=function(callback){
	var _this=this;
	_this.handler.setInputAction(function(movement) {
		var obj=_this.checkPickType(_this.viewer.scene.pick(movement.position),movement.position);
		if (Cesium.defined(obj)){
			if(callback){
				callback(obj);
			}
		}
	}, Cesium.ScreenSpaceEventType.LEFT_CLICK);
}

/**
 * 鼠标双击事件
 */
VFG.Viewer.prototype.LEFT_DOUBLE_CLICK_PRIMITIVE=function(callback){
	var _this=this;
	this.handler.setInputAction(function(movement) {
		var obj=_this.checkPickType(_this.viewer.scene.pick(movement.position),movement.position);
		if (Cesium.defined(obj)){
			if(callback){
				callback(obj);
			}
		}
	}, Cesium.ScreenSpaceEventType.LEFT_DOUBLE_CLICK);
}

/**
 * 鼠标右键
 */
VFG.Viewer.prototype.RIGHT_CLICK_PRIMITIVE=function(callback){
	var _this=this;
	this.handler.setInputAction(function(movement) {
		var obj=_this.checkPickType(_this.viewer.scene.pick(movement.position),movement.position);
		if(callback){
			callback(obj);
		}
	}, Cesium.ScreenSpaceEventType.RIGHT_CLICK);
}

/**
 * 滚轮事件
 */
VFG.Viewer.prototype.WHEEL=function(callback){
	var _this=this;
	this.handler.setInputAction(function(movement) {
	}, Cesium.ScreenSpaceEventType.WHEEL);
}

/**
 * 相机移动停止事件
 */
VFG.Viewer.prototype.CAMERA_MOVE_END=function(callback){
	var _this=this;
	_this.cameraMoveEnd=function(){
		if(callback){
			callback({
				level:_this.getLevel(),
				height: Math.ceil(_this.viewer.camera.positionCartographic.height)
			})
		}
	}
	_this.viewer.scene.camera.moveEnd.addEventListener(_this.cameraMoveEnd)
}

/**
 * 移除相机移动停止事件
 */
VFG.Viewer.prototype.CAMERA_MOVE_END=function(callback){
	var _this=this;
	if(_this.cameraMoveEnd){
		_this.viewer.scene.camera.moveEnd.removeEventListener(_this.cameraMoveEnd)
	}
}

/**
 * 鼠标滑过效果
 */
VFG.Viewer.prototype.MOUSE_MOVE_PRIMITIVE=function(callback){
	var _this=this;
	var preObj = null;
	this.handler.setInputAction(function(movement) {
		var obj=_this.checkPickType(_this.viewer.scene.pick(movement.endPosition),movement.endPosition);
		if (Cesium.defined(obj)){
			_this.tip.showAt(movement.endPosition, `<div class="con" style="color: white;">`+obj.name+`</div>`);
			 if(Cesium.defined(preObj) && preObj.id==obj.id){
				 return;
			 }else{
				if(Cesium.defined(preObj)){
					_this.changeStyle(preObj.id,'DEFAULT',preObj.type);
				}
				_this.changeStyle(obj.id,'HOVER',obj.type);
				preObj=obj;
			 }
		}else{
			if(Cesium.defined(preObj)){
				_this.changeStyle(preObj.id,'DEFAULT',preObj.type);
			}
			_this.tip.setVisible(false);
			preObj=obj;
		}
	}, Cesium.ScreenSpaceEventType.MOUSE_MOVE);
}


VFG.Viewer.prototype.checkPickType=function(obj,position){
	var _this=this;
	if (Cesium.defined(obj) && obj.primitive && obj.id){ 
		if ((obj.primitive instanceof Cesium.Primitive 
				|| obj.primitive instanceof Cesium.GroundPrimitive ) &&!(obj.id instanceof Cesium.Entity)
				){
			if(obj.id && obj.id.PrimitiveType){
				var primitive=obj.id;
				return {
					id:primitive.id,
					layerId:primitive.layerId,
					type:primitive.PrimitiveType,
					name:primitive.name,
					code:primitive.code,
					position:position
				}
			 }
		}
		else if (obj.primitive instanceof Cesium.ClassificationPrimitive && obj.id){
			var primitive=_this.getFeatureById(obj.id);
			if(primitive){
				return {
					id:primitive.id,
					layerId:primitive.layerId,
					type:"Feature",
					name:primitive.name,
					code:primitive.code,
					position:position
				}
			}
		}
		else if (obj.primitive instanceof Cesium.Cesium3DTileset){
//			console.log(obj.id);
//			var primitive=_this.getModelById(obj.id);
//			if(primitive){
//				return {
//					id:primitive.id,
//					layerId:primitive.layerId,
//					type:"Feature",
//					name:primitive.name,
//					code:primitive.code,
//					position:position
//				}
//			}
		}
		else if (obj.primitive instanceof Cesium.Model){
			var primitive=_this.getModelById(obj.id);
			if(primitive){
				return {
					id:primitive.id,
					layerId:primitive.layerId,
					type:"Model",
					name:primitive.name,
					code:primitive.code,
					attribute:primitive.attribute,
					position:position,
				}
			}
		}
		else if ((obj.primitive instanceof Cesium.Label) || (obj.primitive instanceof Cesium.PointPrimitive)|| (obj.primitive instanceof Cesium.Billboard)){
			 var point=_this.getPointById(obj.id);
			 if(Cesium.defined(point)){
				 return {
					id:point.id,
					layerId:point.layerId,
					type:"Point",
					name:point.name,
					code:point.code,
					position:position
				}
			 }
		}
		else if(obj.id instanceof Cesium.Entity){
			if(Cesium.defined(obj.id.point) || Cesium.defined(obj.id.billboard) || Cesium.defined(obj.id.label)){
				var point=_this.getPointById(obj.id.id); 
				if(point){
					return {
						id:point.id,
						layerId:point.layerId|| null,
						type:"Point",
						name:point.name|| null,
						code:point.code|| null,
						position:position
					}
				}else{
					return {
						id:obj.id.id,
						layerId:null,
						type:"Point",
						name:obj.id.name|| null,
						code:obj.id.code|| null,
						position:position
					}
				}
			}
			else if( Cesium.defined(obj.id.polygon)){
				var polygon=_this.getPolygonById(obj.id.id); 
				if(polygon){
					return {
						id:polygon.id,
						layerId:polygon.layerId,
						type:"Polygon",
						name:polygon.name,
						code:polygon.code,
						position:position
					}
				}else{
					return {
						id:obj.id.id,
						layerId:null,
						type:"Polygon",
						name:obj.id.name|| null,
						code:obj.id.code|| null,
						position:position
					}
				}
			}
			else if( Cesium.defined(obj.id.polyline)){
				var polyline=_this.getPolylineById(obj.id.id); 
				if(polyline){
					return {
						id:polyline.id,
						layerId:polyline.layerId,
						type:"Polyline",
						name:polyline.name,
						code:polyline.code,
						position:position
					}
				}else{
					return {
						id:obj.id.id,
						layerId:null,
						type:"Polyline",
						name:obj.id.name|| null,
						code:obj.id.code|| null,
						position:position
					}
				}

			}
			else if( Cesium.defined(obj.id.ellipse)){
				return {
					id:obj.id.id,
					layerId:null,
					type:"Ellipse",
					name:obj.id.name|| null,
					code:obj.id.code|| null,
					position:position
				}
			}
		}
	}
}

;
///<jscompress sourcefile="VFGMapFeature.js" />
VFG.Viewer.prototype.cacheFeatureMap=new Map();
VFG.Viewer.prototype.selectFeatureMap=new Map();

VFG.Viewer.prototype.addFeature=function(option){
	var _this=this;
	var feature={
		id:option.id,
		name:option.name||'',
		code:option.code||'',
		isView:option.isView||false,
		type:'Feature',
		position:{
			x:option.x,
			y:option.y,
			z:option.z,
		},
		dimensions:{
			x:option.dimensionsX||10,
			y:option.dimensionsY||10,
			z:option.dimensionsZ||10,
		},
		rotation:{
			x:option.rotationX||0,
			y:option.rotationY||0,
			z:option.rotationZ||0,
		},
		normalColor:option.normalColor||null,
		hoverColor:option.hoverColor||null,
		heading:option.heading||null,
		pitch:option.pitch||null,
		roll:option.roll||null,
		cameraX:option.cameraX||null,
		cameraY:option.cameraY||null,
		cameraZ:option.cameraZ||null,
	}
	VFG.Feature.add(feature);	
	_this.cacheFeatureMap.set(option.id,option);
}

VFG.Viewer.prototype.addFeatures=function(list){
	var _this=this;
	 for(var i=0;i<list.length;i++){
		 var feature=list[i];
		 _this.addFeature(feature);
	 }
}

VFG.Viewer.prototype.removeFeatures=function(list){
	var _this=this;
	for(var i=0;i<list.length;i++){
		var Feature=list[i];
		_this.removeFeatureById(Feature.id);
	}
}

VFG.Viewer.prototype.showFeatures=function(list,show){
	var _this=this;
	for(var i=0;i<list.length;i++){
		var Feature=list[i];
		VFG.Feature.show(_this.viewer,{
			id:Feature.id,
			show:show,
		});
	}
} 

VFG.Viewer.prototype.showFeature=function(option){
	var _this=this;
	VFG.Feature.show(_this.viewer,option);
}

VFG.Viewer.prototype.removeAllFeature=function(){
	var _this=this;
	_this.cacheFeatureMap.forEach(function(value,key){
		VFG.Feature.removeById(_this.viewer,key);
	});
	_this.cacheFeatureMap.clear();
	_this.selectFeatureMap.clear();
}

VFG.Viewer.prototype.removeFeatureById=function(id){
	var _this=this;
	VFG.Feature.removeById(id);
	this.cacheFeatureMap.delete(id);
	this.selectFeatureMap.delete(id);
}

VFG.Viewer.prototype.removeFeature=function(feature){
	var _this=this;
	if(feature &&feature.id){
		_this.removeFeatureById(feature.id);
		_this.cacheFeatureMap.delete(feature.id);
		_this.selectFeatureMap.delete(feature.id);
	}
}

VFG.Viewer.prototype.changeFeatureStyle=function(id,event){
	var _this=this;
	 var feature=VFG.Feature.getById(id);
	 if(feature!=null){
		 if('DEFAULT'==event){
			 feature.leave();
			 this.selectFeatureMap.delete(id);
		 }
		 else if('HOVER'==event){
			 if(!this.selectFeatureMap.has(id)){
				 feature.enter();
				 this.selectFeatureMap.delete(id);
			 }
		 }
		 else if('SELECTED'==event){
			 feature.enter();
			 this.selectFeatureMap.set(id,id);
		 }else{
			 feature.leave();
			 this.selectFeatureMap.delete(id);
		 }
	 }
}

VFG.Viewer.prototype.getFeatureById=function(id){
	var _this=this;
	if(_this.containFeature(id)){
		 return _this.cacheFeatureMap.get(id);
	}
}

VFG.Viewer.prototype.containFeature=function(id){
	return this.cacheFeatureMap.has(id)
}

VFG.Viewer.prototype.updateFeature=function(option){
	var _this=this;
	if(option.id){
		if(_this.containFeature(option.id)){
			var feature=_this.getFeatureById(option.id);
			if(feature){
				feature.name=option.name||feature.name;
				feature.code=option.code||feature.code;
				feature.isView=option.isView||feature.isView;
				feature.type=option.type||feature.type;
				feature.x=option.x||feature.x
				feature.y=option.y||feature.y
				feature.z=option.z||feature.z
				feature.dimensionsX=option.dimensionsX||feature.dimensionsX
				feature.dimensionsY=option.dimensionsY||feature.dimensionsY
				feature.dimensionsZ=option.dimensionsZ||feature.dimensionsZ
				feature.rotationX=option.rotationX||feature.rotationX
				feature.rotationY=option.rotationY||feature.rotationY
				feature.rotationZ=option.rotationZ||feature.rotationZ
				feature.normalColor=option.normalColor||feature.normalColor;
				feature.hoverColor=option.hoverColor||feature.hoverColor;
				feature.heading=option.heading||feature.heading;
				feature.pitch=option.pitch||feature.pitch;
				feature.roll=option.roll||feature.roll;
				feature.cameraX=option.cameraX||feature.cameraX;
				feature.cameraY=option.cameraY||feature.cameraY;
				feature.cameraZ=option.roll||feature.cameraZ;
				_this.cacheFeatureMap.set(option.id,feature);
				
				if(option.name){
					VFG.Feature.update({
						id:option.id,
						name:option.name||'',
					});
				}
				
				if(option.isView){
					VFG.Feature.update({
						id:option.id,
						isView:option.isView,
					});
				}
				
				if(option.x && option.y && option.z){
					VFG.Feature.update({
						id:option.id,
						position:{
							x:option.x,
							y:option.y,
							z:option.z,
						},
					});
				}
				
				if(option.dimensionsX && option.dimensionsY && option.dimensionsZ){
					VFG.Feature.update({
						id:option.id,
						dimensions:{
							x:option.dimensionsX||10,
							y:option.dimensionsY||10,
							z:option.dimensionsZ||10,
						},
					});
				}
				
				if(option.rotationX && option.rotationY && option.rotationZ){
					VFG.Feature.update({
						id:option.id,
						rotation:{
							x:option.rotationX||0,
							y:option.rotationY||0,
							z:option.rotationZ||0,
						},
					});
				}
				
				if(option.normalColor){
					VFG.Feature.update({
						id:option.id,
						normalColor:option.normalColor,
					});
				}
				if(option.hoverColor){
					VFG.Feature.update({
						id:option.id,
						hoverColor:option.hoverColor,
					});
				}
			}else{
				throw new Error("未找到对象!");
			}
		}else{
			 throw new Error("未找到对象!");
		}
	}else{
	   throw new Error("参数有误!");
	}
}


;
///<jscompress sourcefile="VFGMapKml.js" />
VFG.Viewer.prototype.addKml=function(option){
	let _this=this;
	VFG.Kml.add(_this.viewer,{
		url:'http://localhost:8081/vf/upload/宾馆.kml',
		clampToGround:true
	})
}

;
///<jscompress sourcefile="VFGMapLayer.js" />
/**
 * 添加
 */
VFG.Viewer.prototype.add=function(option){
	var _this=this;
	if('Point'==option.type){
		_this.addPoint(option);
	}
	else if('Polyline'==option.type){
		_this.addPolyline(option)
	}
	else if('Polygon'==option.type){
		_this.addPolygon(option)
	}
	else if('Model'==option.type){
		_this.addModel(option)
	}
	else if('Map'==option.type){
		_this.addProvider(option);
	}
	else if('VideoPlane'==option.type){
		_this.addVPlane(option);
	}
	else if('VideoModel'==layer.type){
		_this.addVModel(option.entity);
	}
	else if('VideoShed3d'==layer.type){
		_this.addVideoShed3d(option.entity);
	}
	else if('Feature'==option.type){
		_this.addFeature(option);
	}
	else if('Roam'==option.type){
		_this.addRoam(option);
	}
}

/**
 * 切换样式
 * id:对象id
 * event:DEFAULT  HOVER SELECTED
 * type: Point、Polyline、Polygon、Model。。。
 */
VFG.Viewer.prototype.changeStyle=function(id,event,type){
	var _this=this;
	if('Point'==type){
		_this.changePointStyle(id,event)
	}
	else if('Polyline'==type){
		_this.changePolylineStyle(id,event)
	}
	else if('Polygon'==type){
		_this.changePolygonStyle(id,event)
	}
	else if('Model'==type){
		_this.changeModelStyle(id,event);
	}
	else if('Map'==type){
	}
	else if('VideoPlane'==type){
	}
	else if('Feature'==type){
		_this.changeFeatureStyle(id,event);
	}
}

/**
* @Description  设置样式
* @param
*/
VFG.Viewer.prototype.setStyle=function(param){
	var _this=this; 
	var style = _this.getStyle(param.styleId);
	 if(style && "1"==style.enabled){
		 if('Point'==param.type){
			 _this.setPointStyle({
				 id:param.id,
				 style:style,
				 cache:param.cache,
			 });
		 }
		 else if('Polyline'==param.type){
			 _this.setPolylineStyle({
				 id:param.id,
				 style:style,
				 cache:param.cache,
			 });
		 }
		 else if('Polygon'==param.type){
			 _this.setPolygonStyle({
				 id:param.id,
				 style:style,
				 cache:param.cache,
			 });
		 }
		 else if('Model'==param.type){
		 }
		 else if('Map'==param.type){
		 }
		 else if('VideoPlane'==param.type){
		 }
	 }
}

/**
 * 获取对象
 */
VFG.Viewer.prototype.getById=function(id,type){
	var _this=this;
	if('Point'==type){
		if(_this.cacheMessageMap.has(id)){
			return _this.cacheMessageMap.get(id);
		}else{
			return _this.getPointById(id);
		}
	}
	else if('Polyline'==type){
		return _this.getPolylineById(id);
	}
	else if('Polygon'==type){
		return _this.getPolygonById(id);
	}
	else if('Model'==type){
		return _this.getModelById(id);
	}
	else if('Map'==type){
		return _this.getProviderById(id);
	}
	else if('VideoPlane'==type){
		return _this.getVPlaneById(id);
	}
	else if('VideoModel'==type){
		return _this.getVModelById(id);
	}
	else if('Feature'==type){
		return _this.getFeatureById(id);
	}
	else if('Roam'==type){
		_this.getRoamById(id);
	}
}

/*
 * 移除对象
 */
VFG.Viewer.prototype.removeById=function(id,type){
	
	if('Point'==type){
		this.removePointById(id);
	}
	else if('Model'==type){
		this.removeModelById(id);
	}
	else if('Polyline'==type){
		this.removePolylineById(id);
	}
	else if('Polygon'==type){
		this.removePolygonById(id);
	}
	else if('Feature'==type){
		this.removeFeatureById(id);
	}
	else if('VideoPlane'==type){
		this.removeVPlaneById(id);
	}
	else if('VideoShed3d'==type){
		this.removeVideoShed3dById(id);
	}
	else if('VideoModel'==type){
		this.removeVModelById(id);
	}
	else if('Roam'==type){
		_this.removeRoamById(id);
	}
}

/**
 * 显示或隐藏
 * option:{
 *   id:对象id,
 *   type:Point..,
 *   state:true/false
 * }
 */
VFG.Viewer.prototype.show=function(option){
	var _this=this;
	var type=option.type;
	var state=option.state;
	if('Point'==type){
		_this.showPoint(option);
	}
	else if('Polyline'==type){
		_this.showPolyline(option);
	}
	else if('Polygon'==type){
		_this.showPolygon(option);
	}
	else if('Models'==type || 'GLTF'==type || '3DTILES'==type || 'Model'==type){
		_this.showModel(option);
	}
	else if('Maps'==type || 'terrain'==type || 'division'==type || 'Map'==type || 'map'==type || 'road'==type ){
		_this.showProvider(option);
	}
}

/**
 * 更新
 */
VFG.Viewer.prototype.update=function(option){
	var _this=this;
	if('Point'==option.type){
		_this.updatePoint(option);
	}
	else if('Polyline'==option.type){
		_this.updatePolyline(option);
	}
	else if('Polygon'==option.type){
		_this.updatePolyline(option);
	}
	else if('Model'==option.type){
		_this.updateModel(option);
	}
	else if('Map'==option.type){
		_this.updateMap(option);
	}
	else if('VideoPlane'==option.type){
		_this.updateVideoPlane(option);
	}
	else if('VideoModel'==option.type){
		_this.updateVideoModel(option);
	}
	else if('Feature'==option.type){
		_this.updateFeature(option);
	}
	else if('Roam'==option.type){
		return _this.updateRoam(option);
	}
}

/**
 * 是否包含
 * option：{
 *   id:对象id,
 *   type:Point、Polyline 。。
 * }
 */
VFG.Viewer.prototype.contain=function(option){
	var _this=this;
	if('Point'==option.type){
		return _this.containPoint(option.id);
	}
	else if('Polyline'==option.type){
		return _this.containPolyline(option.id);
	}
	else if('Polygon'==option.type){
		return _this.containPolyline(option.id);
	}
	else if('Model'==option.type){
		return _this.containModel(option.id);
	}
	else if('Map'==option.type){
		return _this.containMap(option.id);
	}
	else if('VideoPlane'==option.type){
		return _this.containVideoPlane(option.id);
	}
	else if('VideoModel'==option.type){
		return _this.containVideoModel(option.id);
	}
	else if('Feature'==option.type){
		return _this.containFeature(option.id);
	}
	else if('Roam'==option.type){
		return _this.containRoam(option.id);
	}
}

/**
 * 层级
 */
VFG.Viewer.prototype.getLevel=function() {
	 var tilesToRender=this.viewer.scene.globe._surface._tilesToRender;
	if(tilesToRender.length>0){
		return tilesToRender[0].level;
	}else{
		return 0;
	}
};

/**
 * 渲染
 * option{}  调用接口：http://localhost:8080/vf/i/scene/layer/getEntities(layerId)  或 getEntity(entityId)
 */
VFG.Viewer.prototype.render=function(option){
	var _this=this;
	if(!option){
		console.log('参数必填！');
		return;
	}
	if(!option.layer){
		console.log('格式有误！');
		return;
	}
	var layer=option.layer;
	
	if('Point'==layer.type){
		_this.addPoint(option.entity);
	}
	else if('Points'==layer.type){
		_this.addPoints(option.entities)
	}
	else if('Polyline'==layer.type){
		_this.addPolyline(option.entity)
	}
	else if('Polylines'==layer.type){
		_this.addPolylines(option.entities)
	}
	else if('Polygon'==layer.type){
		_this.addPolygon(option.entity)
	}
	else if('Polygons'==layer.type){
		_this.addPolygons(option.entities)
	}
	else if('Model'==layer.type){
		_this.addModel(option.entity)
	}
	else if('Models'==layer.type){
		_this.addModels(option.entities)
	}
	else if('Map'==layer.type){
		_this.addProvider(option.entity);
	}
	else if('Maps'==layer.type){
		_this.addProviders(option.entities);
	}
	else if('VideoPlane'==layer.type){
		_this.addVPlane(option.entity);
	}
	else if('VideoPlanes'==layer.type){
		_this.addVPlanes(option.entities);
	}
	else if('Feature'==layer.type){
		_this.addFeature(option.entity);
	}
	else if('Features'==layer.type){
		_this.addFeatures(option.entities);
	}
	else if('VideoModel'==layer.type){
		_this.addVModel(option.entity);
	}
	else if('VideoModels'==layer.type){
		_this.addVModels(option.entities);
	}
	else if('VideoShed3d'==layer.type){
		_this.addVideoShed3d(option.entity);
	}
	else if('VideoShed3ds'==layer.type){
		_this.addVideoShed3ds(option.entities);
	}
	else if('Roam'==layer.type){
		_this.addRoam(option.entity);
	}
}


//漫游
VFG.Viewer.prototype.creatRoam=function(option){
	return new VFG.Roam(this.viewer,option);
}

//绕点旋转
VFG.Viewer.prototype.creatAroundPoint=function(option){
	return new VFG.AroundPoint(this.viewer,option);
}

VFG.Viewer.prototype.enableRotate=function(enabled){
	this.viewer.scene.screenSpaceCameraController.enableRotate = enabled;
}

/**
 * 平移
 */
VFG.Viewer.prototype.enableTranslate=function(enabled){
	this.viewer.scene.screenSpaceCameraController.enableTranslate = enabled;
}

/**
 * 放大和缩小
 */
VFG.Viewer.prototype.enableZoom=function(enabled){
	this.viewer.scene.screenSpaceCameraController.enableZoom = enabled;
}
/**
 * 倾斜相机
 */
VFG.Viewer.prototype.enableTilt=function(enabled){
	this.viewer.scene.screenSpaceCameraController.enableZoom = enabled;
}

/**
 * 获取视角
 */
VFG.Viewer.prototype.getVisualAngle=function() {
	return {
		heading: Cesium.Math.toDegrees(this.viewer.camera.heading).toFixed(2),
		pitch:Cesium.Math.toDegrees(this.viewer.camera.pitch).toFixed(2),
		roll:Cesium.Math.toDegrees(this.viewer.camera.roll).toFixed(2),
		position:VFG.Util.getLnLaFormC3(this.viewer,this.viewer.camera.position)
	}
};

/**
 * 隐藏球体
 */
VFG.Viewer.prototype.hideGlobe=function() {
	this.viewer.scene.sun.show = false; 
	this.viewer.scene.moon.show = false;
	this.viewer.scene.skyBox.show = false;//关闭天空盒，否则会显示天空颜色
	//this.viewer.scene.undergroundMode = true; //重要，开启地下模式，设置基色透明，这样就看不见黑色地球了
	//this.viewer.scene.underGlobe.show = true;
	//this.viewer.scene.underGlobe.baseColor = new Cesium.Color(0, 0, 0, 0);
	this.viewer.scene.globe.show = false; //不显示地球，这条和地球透明度选一个就可以
	this.viewer.scene.globe.baseColor = new Cesium.Color(0, 0, 0, 0);
	this.viewer.scene.backgroundcolor = new Cesium.Color(0, 0, 0, 0)
};

/**
 * 飞行
 */
VFG.Viewer.prototype.flyTo=function(option){
	var position=option.position;
	var orientation=option.orientation;
	var param={};
	if(position){
		param.destination=Cesium.Cartesian3.fromDegrees(position.x,position.y, position.z);//x:经度,y:纬度,z:高度
	}
	if(orientation){
		param.orientation={
	        heading : Cesium.Math.toRadians(orientation.heading*1),//heading：角度
	        pitch : Cesium.Math.toRadians(orientation.pitch*1),//pitch：角度
	        roll : Cesium.Math.toRadians(orientation.roll*1),//roll：角度
	    };
	}
	if(option.complete){//飞行结束
		param.complete=option.complete(_this);
	}
	if(option.cancle){//飞行取消
		param.cancle=option.cancle(_this);
	}
	this.viewer.camera.flyTo(param);
}

/**
 * 聚合显示
 * option：调用接口：http://localhost:8080/vf/i/scene/layer/getCluster(layerId) 	 
 */
VFG.Viewer.prototype.addCluster=function(option){
	var _this=this;
	if(!_this.cacheClusterMap.has(option.layerId)){
		var points=option.points;
		var Layers=new Cesium.CustomDataSource(option.layerId);
		for(var i=0;i<points.length;i++){
			var point=points[i];
			var entity=VFG.Point.addEntity(_this.viewer,{
				point:point,
				style:point.defaultStyleId?_this.POINT_STYLE[point.defaultStyleId]:null
			},_this.url);
			Layers.entities.add(entity);
		}
		var cluster=new VFG.Layer.Cluster(_this.viewer,{
			Layers:Layers
		});
		_this.cacheClusterMap.set(option.layerId,cluster);
	}
}

/**
 * 移除聚合
 * option：{
 * 	layerId：layerId
 * }
 */
VFG.Viewer.prototype.removeCluster=function(option){
	var _this=this;
	if(_this.cacheClusterMap.has(option.layerId)){
		_this.cacheClusterMap.get(option.layerId).destroy();
		_this.cacheClusterMap.delete(option.layerId);
	}
}


;
///<jscompress sourcefile="VFGMapListener.js" />
/**
 * 监听集合
 */
VFG.Viewer.prototype.ListenerMap=new Map();
/**
 * 监听屏幕坐标
 * 	bizPoint
 *  callBack:fun(c2)回调函数
 */
VFG.Viewer.prototype.addListenerScreenPosition  = function(bizPoint,callBack) {
	if(!bizPoint)return;
	if(!this.ListenerMap.has(bizPoint.id)){
		var option = {
			position:{
		  		x:bizPoint.x*1,
		  		y:bizPoint.y*1,
		  		z:bizPoint.z*1
			},
			id:bizPoint.id,
			callBack:callBack
		}
		var listener=new VFG.Listener(this.viewer,option);
		this.ListenerMap.set(bizPoint.id,listener);
	}
}

/**
 * 移出
 */
VFG.Viewer.prototype.removeListenerScreenPosition= function(id) {
	if(this.ListenerMap.has(id)){
		var listener=this.ListenerMap.get(id).destroy();
		this.ListenerMap.delete(id)
	}
}

VFG.Viewer.prototype.addListenerCamera=function(callback){
	var _this=this;
	_this.removeListenerCamera();
	_this.handler3D = new Cesium.ScreenSpaceEventHandler(_this.viewer.scene.canvas);
	var viewer=this.viewer;
	_this.handler3D.setInputAction(function(movement) {
	    var pick= new Cesium.Cartesian2(movement.endPosition.x,movement.endPosition.y);
	    if(pick){
	        var cartesian = _this.viewer.scene.globe.pick(_this.viewer.camera.getPickRay(pick), viewer.scene);
	        if(cartesian){
	            // 世界坐标转地理坐标（弧度）
	            var cartographic = _this.viewer.scene.globe.ellipsoid.cartesianToCartographic(cartesian);
	            if(cartographic){
	                // 海拔
	                var height = _this.viewer.scene.globe.getHeight(cartographic);
	                // 视角海拔高度
	                var he = Math.sqrt(_this.viewer.scene.camera.positionWC.x * _this.viewer.scene.camera.positionWC.x + _this.viewer.scene.camera.positionWC.y * _this.viewer.scene.camera.positionWC.y + viewer.scene.camera.positionWC.z * _this.viewer.scene.camera.positionWC.z);
	                var he2 = Math.sqrt(cartesian.x * cartesian.x + cartesian.y * cartesian.y + cartesian.z * cartesian.z);
	                // 地理坐标（弧度）转经纬度坐标
	                var point=[ cartographic.longitude / Math.PI * 180, cartographic.latitude / Math.PI * 180];
	                if(!height){
	                    height = 0;
	                }
	                if(!he){
	                    he = 0;
	                }
	                if(!he2){
	                    he2 = 0;
	                }
	                if(!point){
	                    point = [0,0];
	                }
	                var level=0;
                    var tilesToRender = _this.viewer.scene.globe._surface._tilesToRender;
                    if(tilesToRender.length>0){
                    	level= tilesToRender[0].level;
                    }else{
                    	level=0;
                    }
                    
                    var heading = Cesium.Math.toDegrees(_this.viewer.camera.heading).toFixed(2)*1;;
                    var pitch = Cesium.Math.toDegrees(_this.viewer.camera.pitch).toFixed(2)*1;;
                    var roll = Cesium.Math.toDegrees(_this.viewer.camera.roll).toFixed(2)*1;;
                    
                    if(callback){
                    	callback({
                    		position:{
                    			x:point[0].toFixed(6),
                    			y:point[1].toFixed(6),
                    			z:height.toFixed(2)
                    		},
                    		zoom:level,
                    		camera:{
                    			heading:heading,
                    			pitch:pitch,
                    			roll:roll,
                    		}
                    	});
                    }
	            }
	        }
	    }
	    
	}, Cesium.ScreenSpaceEventType.MOUSE_MOVE);
}
VFG.Viewer.prototype.removeListenerCamera=function(){
	var _this=this;
	if(_this.handler3D){
		_this.handler3D.destroy();
	}
}
;
///<jscompress sourcefile="VFGMapMessage.js" />
VFG.Viewer.prototype.cacheEventMap=new Map();
VFG.Viewer.prototype.cacheMessageMap=new Map();
VFG.Viewer.prototype.filterMessageMap=new Map();
VFG.Viewer.prototype.layerMessageMap=new Map();
/**
 * 添加控制图层Key
 */
VFG.Viewer.prototype.addLayerMessage=function(key,value){
	var _this=this;
	if(!this.layerMessageMap.has(key)){
		this.layerMessageMap.set(key,value);
		_this.newMessageMap().forEach(function(value,key){
			_this.addMessageEventPoint(value);
		});
	}
}


VFG.Viewer.prototype.newMessageMap=function(){
	var _this=this;
	var newMap = new Map();
	_this.cacheEventMap.forEach(function(value,key){
		newMap.set(key,value);
	});
	return newMap;
}


/**
 * 移除控制图层Key
 */
VFG.Viewer.prototype.removeLayerMessage=function(key){
	var _this=this;
	if(this.layerMessageMap.has(key)){
		this.layerMessageMap.delete(key);
		_this.newMessageMap().forEach(function(value,key){
			_this.addMessageEventPoint(value);
		});
	}
}

/**
 * 批量移除控制图层Key
 */
VFG.Viewer.prototype.removeAllLayerMessage=function(){
	var _this=this;
	_this.layerMessageMap.clear();
	_this.cacheMessageMap.forEach(function(value,key){
		VFG.Point.removePrimitiveById(key);
		_this.viewer.entities.removeById(key);
	});
	_this.cacheMessageMap.clear();
}

/**
 * 添加过滤条件
 */
VFG.Viewer.prototype.addFilterMessage=function(key,value){
	var _this=this;
	if(!this.filterMessageMap.has(key)){
		this.filterMessageMap.set(key,value);
		_this.newMessageMap().forEach(function(value,key){
			_this.addMessageEventPoint(value);
		});
	}
}

/**
 * 移除过滤条件
 */
VFG.Viewer.prototype.removeFilterMessage=function(key){
	var _this=this;
	if(this.filterMessageMap.has(key)){
		this.filterMessageMap.delete(key);
		_this.newMessageMap().forEach(function(value,key){
			_this.addMessageEventPoint(value);
		});
	}
}

/**
 * 批量移除控制图层Key
 */
VFG.Viewer.prototype.removeAllFilterMessage=function(){
	var _this=this;
	this.filterMessageMap.clear();
	_this.newMessageMap().forEach(function(value,key){
		_this.addMessageEventPoint(value);
	});
}


/**
 * 处理消息
 */
VFG.Viewer.prototype.handleMessage=function(event){
	var _this=this;
	var result=JSON.parse(event.data);
	if(result && result.code=='event'){
		var data=result.data;
		if(data){
			_this.addMessageEventPoint(data);
		}
	}
}

VFG.Viewer.prototype.addMessageEventPoint=function(data){
	var _this=this;
	if(data){
		var id=data.id||null;
		var level=data.level||null;
		var show=data.show||false;
		var code=data.code||null;
		var state=data.state||null;
		var type=data.type||null;
		var event=data.event||null;
		_this.cacheEventMap.delete(data.id);
		_this.cacheEventMap.set(data.id,data);
		if('Point'==type && event){
			if(event.x && event.x.length>0 && event.y && event.y.length>0 && event.z && event.z.length>0 ){
				VFG.Point.removePrimitiveById(event.id);
				_this.cacheMessageMap.delete(event.id) ;
				_this.viewer.entities.removeById(event.id)
				
				if(!show){
					return;
				}
				
				var filters=event.filters || null;
				var layerCode=event.layerCode || null;
				if(_this.filterMessageMap.size>0){
					if(filters && filters.length>0){
						_this.filterMessageMap.forEach(function(value,key){
							if(filters.indexOf(key)!=-1){
								if(_this.layerMessageMap.size>0 && _this.layerMessageMap.has(layerCode)){
									var key=code+state;
									var defaultStyleId='';
									var hoverStyleId='';
									var selectedStyleId='';
									var color=Cesium.Color.RED;
									if(_this.EVENT_CONFIG[key]){
										defaultStyleId=_this.EVENT_CONFIG[key].defaultStyleId;
										hoverStyleId=_this.EVENT_CONFIG[key].hoverStyleId;
										selectedStyleId=_this.EVENT_CONFIG[key].selectedStyleId;
										color=_this.EVENT_CONFIG[key].color? Cesium.Color.fromCssColorString(_this.EVENT_CONFIG[key].color) :Cesium.Color.RED;
									}
									var point={
										id:event.id||id,
										name:event.name||'',
										code:event.layerCode|| '',
										state:state,
										x:event.x*1,
										y:event.y*1,
										z:event.z*1,
										defaultStyleId:defaultStyleId,
										hoverStyleId:hoverStyleId,
										selectedStyleId:selectedStyleId,
										code:code
									};
									
									if('1'==level){
										var position= Cesium.Cartesian3.fromDegrees(event.x*1,event.y*1,event.z*1);
										_this.getEventMessagePoint(event.id,event.name,position,color);
										if(defaultStyleId){
											VFG.Point.addPrimitive(_this.viewer,{
												point:point,
												style:_this.POINT_STYLE[defaultStyleId]
											},_this.url);
										}
									}else{
										VFG.Point.addPrimitive(_this.viewer,{
											point:point,
											style:_this.POINT_STYLE[defaultStyleId]
										},_this.url);
									}
									_this.cacheMessageMap.set(event.id,point);
								}
							}
						});
					}
				}else{
					if(_this.layerMessageMap.size>0 && _this.layerMessageMap.has(layerCode)){
						var key=code+state;
						var defaultStyleId='';
						var hoverStyleId='';
						var selectedStyleId='';
						var color=Cesium.Color.RED;
						if(_this.EVENT_CONFIG[key]){
							defaultStyleId=_this.EVENT_CONFIG[key].defaultStyleId;
							hoverStyleId=_this.EVENT_CONFIG[key].hoverStyleId;
							selectedStyleId=_this.EVENT_CONFIG[key].selectedStyleId;
							color=Cesium.Color.fromCssColorString(_this.EVENT_CONFIG[key].color);
						}
						var point={
							id:event.id||id,
							name:event.name||'',
							code:event.layerCode|| '',
							state:state,
							x:event.x*1,
							y:event.y*1,
							z:event.z*1,
							defaultStyleId:defaultStyleId,
							hoverStyleId:hoverStyleId,
							selectedStyleId:selectedStyleId,
							code:code
						};
						if('1'==level){
							var position= Cesium.Cartesian3.fromDegrees(event.x*1,event.y*1,event.z*1);
							_this.getEventMessagePoint(event.id,event.name,position,color);
							if(defaultStyleId){
								VFG.Point.addPrimitive(_this.viewer,{
									point:point,
									style:_this.POINT_STYLE[defaultStyleId]
								},_this.url);
							}
						}else{
							VFG.Point.addPrimitive(_this.viewer,{
								point:point,
								style:_this.POINT_STYLE[defaultStyleId]
							},_this.url);
						}
						_this.cacheMessageMap.set(event.id,point);
					}

				}
			}
		}
	}
}

VFG.Viewer.prototype.removeAllEventMessage=function(){
	var _this=this;
	_this.cacheMessageMap.forEach(function(value,key){
		VFG.Point.removePrimitiveById(key);
		_this.viewer.entities.removeById(key)
	});
	_this.cacheMessageMap.clear();
	_this.cacheEventMap.clear();
	_this.filterMessageMap.clear();
	_this.layerMessageMap.clear();
}

VFG.Viewer.prototype.getEventMessagePoint=function(id,name,position,color){
	var _this=this;
	var x=1;
	var flog=true;
	_this.viewer.entities.removeById(id);
	_this.viewer.entities.add({
		id:id,
		name:name,
		position:position,
		point : {
			show : true,
			color :new Cesium.CallbackProperty(function () {
				if(flog){
					x=x-0.05;
					if(x<=0){
						flog=false;
					}
					}else{
						x=x+0.05;
						if(x>=1){
						flog=true;
						}
					}
					return color.withAlpha(x);
				},false),
			pixelSize : 20, // default: 1
			outlineWidth :0
		}
	});
}




;
///<jscompress sourcefile="VFGMapModel.js" />
VFG.Viewer.prototype.cacheModelMap=new Map();
VFG.Viewer.prototype.selectModelMap=new Map();

/**
 * 移除所有
 */
VFG.Viewer.prototype.removeAllModels=function(){
	var _this=this;
	_this.cacheModelMap.forEach(function(value,key){
		VFG.Model.removeById(key);
	});
	_this.cacheModelMap.clear();
	_this.selectModelMap.clear();
}

/**
 * 添加模型
 */
VFG.Viewer.prototype.addModel=function(model){
	let _this=this;
	console.log(model);
	VFG.Model.addModel(_this.viewer,model,_this.LocalModelUrl,_this.aliveLocalService);
	_this.cacheModelMap.set(model.id,model);
} 

/**
 * 批量添加
 */
VFG.Viewer.prototype.addModels=function(list){
	var _this=this;
	 for(var i=0;i<list.length;i++){
		 var model=list[i]; 
		 _this.addModel(model);
	 }
} 

VFG.Viewer.prototype.removeModels=function(list){
	var _this=this;
	for(var i=0;i<list.length;i++){
		var model=list[i]; 
		_this.removeModelById(model.id);
	}
}

VFG.Viewer.prototype.removeModel=function(model){
	var _this=this;
	_this.removeModelById(model.id);
}

/**
 * 移除模型
 * id：模型id
 */
VFG.Viewer.prototype.removeModelById=function(id){
	var _this=this;
	VFG.Model.removeById(id);
	_this.cacheModelMap.delete(id);
	_this.selectModelMap.delete(id);
} 

VFG.Viewer.prototype.getModelById=function(id){
	if(this.cacheModelMap.has(id)){
		return this.cacheModelMap.get(id);
	}
}

VFG.Viewer.prototype.setViewModelById=function(id,complete){
	if(this.cacheModelMap.has(id)){
		this.setViewModel(this.cacheModelMap.get(id))
	}
	if(complete){
		complete(this);
	}
}

VFG.Viewer.prototype.flyToModel=function(id,complete){
	if(this.cacheModelMap.has(id)){
		var option=this.cacheModelMap.get(id);
		
		if(option.cameraX && option.cameraY){
			this.viewer.camera.flyTo({
				destination:Cesium.Cartesian3.fromDegrees(option.cameraX*1,option.cameraY*1,option.cameraZ*1),
				orientation:{
			        heading : Cesium.Math.toRadians(option.heading*1),
			        pitch : Cesium.Math.toRadians(option.pitch*1),
			        roll : Cesium.Math.toRadians(option.roll*1),
			    }
			});
		}else{
			this.viewer.camera.flyTo({
				destination:Cesium.Cartesian3.fromDegrees(option.x*1,option.y*1,option.z*1+5)
			});
		}
	}
	if(complete){
		complete(this);
	}
}

VFG.Viewer.prototype.setViewModel=function(model,complete){
	if(model.cameraX && model.cameraX && model.cameraZ && model.heading && model.pitch && model.roll){
		this.viewer.scene.camera.setView({
			destination:new Cesium.Cartesian3(model.cameraX*1,model.cameraY*1,model.cameraZ*1),
			orientation:{
		        heading : model.heading*1,
		        pitch : model.pitch*1,
		        roll : model.roll*1,
		    }
		});
		if(complete){
			complete(this);
		}
	}else{
		this.viewer.scene.camera.setView({
			destination:Cesium.Cartesian3.fromDegrees(model.x*1,model.y*1,model.z*1+10000||10000)
		});
		if(complete){
			complete(this);
		}
	}
}

VFG.Viewer.prototype.changeModelPitch=function(id,rx){
	var _this=this;
	var option=_this.getModelById(id);
	if(option){
		option.rx=rx;
		VFG.Model.update(option);
		this.cacheModelMap.has(option.id,option)
	}
}

VFG.Viewer.prototype.changeModelHeading=function(id,ry){
	var _this=this;
	var option=_this.getModelById(id);
	if(option){
		option.ry=ry;
		VFG.Model.update(option);
		this.cacheModelMap.has(option.id,option)
	}
}

VFG.Viewer.prototype.changeModelRoll=function(id,rz){
	var _this=this;
	var option=_this.getModelById(id);
	if(option){
		option.rz=rz;
		VFG.Model.update(option);
		this.cacheModelMap.has(option.id,option)
	}
}

VFG.Viewer.prototype.changeModelPosition=function(id,position){
	var _this=this;
	var option=_this.getModelById(id);
	if(option){
		option.x=position.x;
		option.y=position.y;
		option.z=position.z;
		VFG.Model.update(option);
		this.cacheModelMap.has(option.id,option)
	}
}

VFG.Viewer.prototype.changeModelHeight=function(id,height){
	var _this=this;
	var option=_this.getModelById(id);
	if(option){
		option.z=height;
		VFG.Model.update(option);
		this.cacheModelMap.has(option.id,option)
	}
}

VFG.Viewer.prototype.changeModelScale=function(id,scale){
	var _this=this;
	var option=_this.getModelById(id);
	if(option){
		option.scale=scale;
		VFG.Model.updateScale(option);
		this.cacheModelMap.has(option.id,option)
	}
}

VFG.Viewer.prototype.changeModelColor=function(id,value){
	var _this=this;
	var option=_this.getModelById(id);
	if(option){
		option.color=value;
		VFG.Model.updateHighlight(option);
		this.cacheModelMap.has(option.id,option)
	}
}

VFG.Viewer.prototype.changeModelColorBlendMode=function(id,value){
	var _this=this;
	var option=_this.getModelById(id);
	if(option){
		option.colorBlendMode=value;
		VFG.Model.updateHighlight(option);
		this.cacheModelMap.has(option.id,option)
	}
}

VFG.Viewer.prototype.changeModelSilhouetteColor=function(id,value){
	var _this=this;
	var option=_this.getModelById(id);
	if(option){
		option.silhouetteColor=value;
		VFG.Model.updateHighlight(option);
		this.cacheModelMap.has(option.id,option)
	}
}

VFG.Viewer.prototype.changeModelSilhouetteSize=function(id,value){
	var _this=this;
	var option=_this.getModelById(id);
	if(option){
		option.silhouetteSize=value;
		VFG.Model.updateHighlight(option);
		this.cacheModelMap.has(option.id,option)
	}
}

VFG.Viewer.prototype.changeModelHighlight=function(id,value){
	var _this=this;
	var option=_this.getModelById(id);
	if(option){
		option.oftenShow=value;
		VFG.Model.updateHighlight(option);
		this.cacheModelMap.has(option.id,option)
	}
}

VFG.Viewer.prototype.showModel=function(param){
	VFG.Model.show(param);
}	

VFG.Viewer.prototype.changeModelStyle=function(id,event){
	var _this=this;
	 var model=_this.getModelById(id);
	 if(model && "1"==model.oftenShow){
		 if('HOVER'==event){
			 if(!_this.selectModelMap.has(id)){
				 VFG.Model.select(model);
			 }
		 }
		 else if('SELECTED'==event){
			 VFG.Model.select(model);
			 _this.selectModelMap.set(id,model);
		 }else{
			 VFG.Model.unselect(model);
			 _this.selectModelMap.delete(id);
		 }
	 }
}	
;
///<jscompress sourcefile="VFGMapPoint.js" />
VFG.Viewer.prototype.cachePointMap=new Map();
VFG.Viewer.prototype.selectPointMap=new Map();

VFG.Viewer.prototype.addPoint=function(option){
	var _this=this;
	VFG.Point.add(_this.viewer,{
		point:option,
		style:option.layerId?_this.getLayerStyle(option.layerId,'DEFAULT'):null
	},_this.url);
	_this.cachePointMap.set(option.id,option);
}

VFG.Viewer.prototype.addPoints=function(list){
	var _this=this;
	 for(var i=0;i<list.length;i++){
		 var point=list[i];
		 _this.addPoint(point);
	 }
}

VFG.Viewer.prototype.removePoints=function(list){
	var _this=this;
	for(var i=0;i<list.length;i++){
		var point=list[i];
		_this.removePointById(point.id);
	}
}

VFG.Viewer.prototype.showPoints=function(list,show){
	var _this=this;
	for(var i=0;i<list.length;i++){
		var point=list[i];
		VFG.Point.show(_this.viewer,{
			id:point.id,
			show:show,
		});
	}
} 

VFG.Viewer.prototype.showPoint=function(option){
	var _this=this;
	VFG.Point.show(_this.viewer,option);
}

VFG.Viewer.prototype.removeAllPoint=function(){
	var _this=this;
	_this.cachePointMap.forEach(function(value,key){
		VFG.Point.removeById(_this.viewer,key);
	});
	_this.cachePointMap.clear();
	_this.selectPointMap.clear();
}

VFG.Viewer.prototype.removePointById=function(id){
	var _this=this;
	VFG.Point.removeById(_this.viewer,id);
	this.cachePointMap.delete(id);
	this.selectPointMap.delete(id);
}

VFG.Viewer.prototype.removePoint=function(point){
	var _this=this;
	VFG.Point.removeById(_this.viewer,point.id);
	this.cachePointMap.delete(point.id);
	this.selectPointMap.delete(point.id);
}

VFG.Viewer.prototype.changePointStyle=function(id,event){
	var _this=this;
	 var point=_this.getPointById(id);
	 if(point!=null){
		 if('DEFAULT'==event && point.layerId){
			 var style=_this.getLayerStyle(point.layerId,'DEFAULT');
			 if(style && "1"==style.enabled){
				 _this.setPointStyle({
					 id:point.id,
					 style:style,
					 cache:false,
				 });
			 }
		 }
		 else if('HOVER'==event && point.layerId){
			 if(!_this.selectPointMap.has(id)){
				 var style=_this.getLayerStyle(point.layerId,'HOVER');
				 if(style && "1"==style.enabled){
					 _this.setPointStyle({
						 id:point.id,
						 style:style,
						 cache:false,
					 });
				 }
			 }
		 }
		 else if('SELECTED'==event && point.layerId){
			 var style=_this.getLayerStyle(point.layerId,'SELECTED');
			 if(style && "1"==style.enabled){
				 _this.setPointStyle({
					 id:point.id,
					 style:style,
					 cache:true,
				 });
			 }
		 }else{
			 if(point.layerId){
				 var style=_this.getLayerStyle(point.layerId,'DEFAULT');
				 if(style && "1"==style.enabled){
					 _this.setPointStyle({
						 id:point.id,
						 style:style,
						 cache:true,
					 });
				 }
			 }
		 }
	 }
}

VFG.Viewer.prototype.flyToPointById=function(id,complete){
	if(this.containPoint(id)){
		this.flyToPoint(this.cachePointMap.get(id))
	}
	if(complete){
		complete(this);
	}
}

VFG.Viewer.prototype.setViewPointById=function(id,complete){
	if(this.containPoint(id)){
		this.setViewPoint(this.cachePointMap.get(id))
	}
	if(complete){
		complete(this);
	}
}

VFG.Viewer.prototype.flyToPoint=function(point,complete){
	var _this=this;
	if(point.cameraX && point.cameraX && point.cameraZ && point.heading && point.pitch && point.roll){
		this.viewer.camera.flyTo({
			destination:new Cesium.Cartesian3(point.cameraX*1,point.cameraY*1,point.cameraZ*1),
			orientation:{
		        heading : point.heading*1,
		        pitch : point.pitch*1,
		        roll : point.roll*1,
		    }
		});
		if(complete){
			complete(this);
		}
	}else{
		this.viewer.camera.flyTo({
			destination:Cesium.Cartesian3.fromDegrees(point.x*1,point.y*1,point.z*1+100||10000)
		});
		if(complete){
			complete(this);
		}
	}
}

VFG.Viewer.prototype.setViewPoint=function(point,complete){
	var _this=this;
	if(point.cameraX && point.cameraX && point.cameraZ && point.heading && point.pitch && point.roll){
		this.viewer.scene.camera.setView({
			destination:new Cesium.Cartesian3(point.cameraX*1,point.cameraY*1,point.cameraZ*1),
			orientation:{
		        heading : point.heading*1,
		        pitch : point.pitch*1,
		        roll : point.roll*1,
		    }
		});
		if(complete){
			complete(this);
		}
	}else{
		this.viewer.scene.camera.setView({
			destination:Cesium.Cartesian3.fromDegrees(point.x*1,point.y*1,point.z*1+100||10000)
		});
		if(complete){
			complete(this);
		}
	}
}

VFG.Viewer.prototype.getPointById=function(id){
	var _this=this;
	if(_this.containPoint(id)){
		 return _this.cachePointMap.get(id);
	}
}

VFG.Viewer.prototype.containPoint=function(id){
	return this.cachePointMap.has(id)
}

VFG.Viewer.prototype.setPointStyle=function(param){
	var _this=this;
	var point=_this.getPointById(param.id);
	if(point && param.style){
	   VFG.Point.update(_this.viewer,{
		point:point,
	 	style:param.style
	   },_this.url);
	   if(param.cache==true){
		   _this.selectPointMap.set(point.id,point);
	   }else{
		   _this.selectPointMap.delete(point.id);
	   }
	}
}

VFG.Viewer.prototype.updatePoint=function(option){
	var _this=this;
	if(option.id){
		if(_this.containPoint(option.id)){
			var point=_this.getPointById(option.id);
			if(point){
				point.layerId=option.layerId||point.layerId
				point.name=option.name||point.name
				point.code=option.code||point.code
				point.telephone=option.telephone||point.telephone
				point.adress=option.adress||point.adress
				point.x=option.x||point.x
				point.y=option.y||point.y
				point.z=option.z||point.z
				point.heading=option.heading||point.heading
				point.pitch=option.pitch||point.pitch
				point.roll=option.roll||point.roll
				point.cameraX=option.cameraX||point.cameraX
				point.cameraY=option.cameraY||point.cameraY
				point.cameraZ=option.cameraZ||point.cameraZ
				point.terrainHeight=option.terrainHeight||point.terrainHeight
				point.modelHeight=option.modelHeight||point.modelHeight
				 var style=_this.getLayerStyle(point.layerId,'DEFAULT');
				 if(style && "1"==style.enabled){
					 _this.setPointStyle({
						 id:point.id,
						 style:style,
						 cache:true,
					 });
				 }else{
				   VFG.Point.update(_this.viewer,{
						point:point,
					 	style:null
					   },_this.url);
					_this.cachePointMap.set(option.id,point);
				 }
			}else{
				throw new Error("未找到对象!");
			}
		}else{
			 throw new Error("未找到对象!");
		}
	}else{
	   throw new Error("参数有误!");
	}
}


;
///<jscompress sourcefile="VFGMapPolygon.js" />
/**********************************************************************************************************/
VFG.Viewer.prototype.cachePolygonMap=new Map();
VFG.Viewer.prototype.selectPolygonMap=new Map();

VFG.Viewer.prototype.addPolygon=function(polygon){
	var _this=this;
	 var style=_this.getStyle(polygon.defaultStyleId);
	 console.log(style);
	 if(style && "1"==style.enabled){
		VFG.Polygon.add({
			polygon:polygon,
			style:style
		},_this.url);
	 }else{
		VFG.Polygon.add({
			polygon:polygon,
			style:null
		},_this.url);
	 }
	_this.cachePolygonMap.set(polygon.id,polygon);
}

VFG.Viewer.prototype.addPolygons=function(list){
	var _this=this;
	 for(var i=0;i<list.length;i++){
		 var polygon=list[i];
		 _this.addPolygon(polygon);
	 }
}

VFG.Viewer.prototype.removePolygon=function(polygon){
	var _this=this;
	if(this.containPolygon(polygon.id)){
		_this.removePolygonById(polygon.id);
	}
}

VFG.Viewer.prototype.removePolygons=function(list){
	var _this=this;
	 for(var i=0;i<list.length;i++){
		 var polygon=list[i];
		 _this.removePolygon(polygon);
	 }
}

VFG.Viewer.prototype.removePolygonById=function(id){
	var _this=this;
	 VFG.Polygon.removeById(id);
	 _this.cachePolygonMap.delete(id);
	 _this.selectPolygonMap.delete(id);
}

/**
 * 移除所有
 */
VFG.Viewer.prototype.removeAllPolygons=function(){
	var _this=this;
	_this.cachePolygonMap.forEach(function(value,key){
		VFG.Polygon.removeById(key);
	});
	_this.cachePolygonMap.clear();
	_this.selectPolygonMap.clear();
}

VFG.Viewer.prototype.changePolygonStyle=function(id,event){
	var _this=this;
	if(_this.containPolygon(id)){
		 var polygon=_this.getPolygonById(id);
		 if('DEFAULT'==event && polygon.defaultStyleId){
			 var style=_this.getStyle(polygon.defaultStyleId);
			 if(style && "1"==style.enabled){
				 _this.setPolygonStyle({
					 id:polygon.id,
					 style:style,
					 cache:false,
				 });
			 }
		 }
		 else if('HOVER'==event && polygon.hoverStyleId){
			 if(!_this.selectPolygonMap.has(polygon.id)){
				 var style=_this.getStyle(polygon.hoverStyleId);
				 if(style && "1"==style.enabled){
					 _this.setPolygonStyle({
						 id:polygon.id,
						 style:style,
						 cache:false,
					 });
				 }
			 }
		 }
		 else if('SELECTED'==event && polygon.selectedStyleId){
			 var style=_this.getStyle(polygon.selectedStyleId);
			 if(style && "1"==style.enabled){
				 _this.setPolygonStyle({
					 id:polygon.id,
					 style:style,
					 cache:true,
				 });
			 }
		 }else{
			 if(polygon.selectedStyleId){
				 var style=_this.getStyle(polygon.defaultStyleId);
				 if(style && "1"==style.enabled){
					 _this.setPolygonStyle({
						 id:polygon.id,
						 style:style,
						 cache:false,
					 });
				 }
			 }
		 }
	}
}

VFG.Viewer.prototype.setPolygonStyle=function(param){
	var _this=this;
	var polygon=_this.getPolygonById(param.id);
	if(polygon && param.style){
	   VFG.Polygon.update({
		polygon:polygon,
	 	style:param.style
	   },_this.url);
	   if(param.cache==true){
		   _this.selectPolygonMap.set(polygon.id,polygon);
	   }else{
		   _this.selectPolygonMap.delete(polygon.id);
	   }
	}
}

VFG.Viewer.prototype.containPolygon=function(id){
	return this.cachePolygonMap.has(id)
}

VFG.Viewer.prototype.getPolygonById=function(id){
	var _this=this;
	if(_this.containPolygon(id)){
		 return _this.cachePolygonMap.get(id);
	}
}

VFG.Viewer.prototype.showPolygon=function(param){
	var _this=this;
	VFG.Polygon.show(param);
}

VFG.Viewer.prototype.showPolygons=function(list,show){
	var _this=this;
	for(var i=0;i<list.length;i++){
		var polygon=list[i];
		VFG.Polygon.show({
			id:polygon.id,
			show:show,
		});
	}
} 

VFG.Viewer.prototype.flyToPolygonById=function(id){
	if(this.containPolygon(id)){
		var e=this.getPolygonById(id);
		if(e.cameraX && e.cameraY && e.cameraZ){
			this.viewer.camera.flyTo({
			    destination : new Cesium.Cartesian3(e.cameraX*1,e.cameraY*1, e.cameraZ*1),
			    orientation : {
			        heading : e.heading*1,
			        pitch : e.pitch*1,
			        roll : e.roll*1
			    }
			});
		}
	}
}
;
///<jscompress sourcefile="VFGMapPolyline.js" />
VFG.Viewer.prototype.cachePolylineMap=new Map();
VFG.Viewer.prototype.selectPolylineMap=new Map();

/**
 * 添加线
 */
VFG.Viewer.prototype.addPolyline=function(param){
	var _this=this;
	_this.cachePolylineMap.set(param.id,param);
	return VFG.Polyline.add({
		polyline:param,
		style:param.layerId?_this.getLayerStyle(param.layerId,'DEFAULT'):null
	},_this.url);
}

VFG.Viewer.prototype.addPolylines=function(list){
	var _this=this;
	for(var i=0;i<list.length;i++){
		var polyline=list[i]; 
		_this.addPolyline(polyline);
	}
}

VFG.Viewer.prototype.removePolyline=function(polyline){
	var _this=this;
	if(_this.cachePolylineMap.has(polyline.id)){
		_this.removePolylineById(polyline.id);
	}
}

VFG.Viewer.prototype.removePolylines=function(list){
	var _this=this;
	for(var i=0;i<list.length;i++){
		var polyline=list[i]; 
		_this.removePolyline(polyline);
	}
}

VFG.Viewer.prototype.removePolylineById=function(id){
	var _this=this;
	 VFG.Polyline.removeById(id);
	 _this.cachePolylineMap.delete(id);
	 _this.selectPolylineMap.delete(id);
}

VFG.Viewer.prototype.removeAllPolylines=function(){
	var _this=this;
	_this.cachePolylineMap.forEach(function(value,key){
		VFG.Polyline.removeById(key);
	});
	_this.cachePolylineMap.clear();
	_this.selectPolylineMap.clear();
}


VFG.Viewer.prototype.changePolylineStyle=function(id,event){
	var _this=this;
	 var polyline=_this.getPolylineById(id);
	 if(polyline!=null && polyline.layerId){
		 if('DEFAULT'==event && polyline.layerId){
			 var style=_this.getLayerStyle(polyline.layerId,'DEFAULT');
			 if(style && "1"==style.enabled){
				 _this.setPolylineStyle({
					 id:polyline.id,
					 style:style,
					 cache:false,
				 });
			 }
		 }
		 else if('HOVER'==event && polyline.layerId){
			 if(!_this.selectPolylineMap.has(polyline.id)){
				 var style=_this.getLayerStyle(polyline.layerId,'HOVER');
				 if(style && "1"==style.enabled){
					 _this.setPolylineStyle({
						 id:polyline.id,
						 style:style,
						 cache:false,
					 });
				 }
			 }
		 }
		 else if('SELECTED'==event && polyline.layerId){
			 var style=_this.getLayerStyle(polyline.layerId,'SELECTED');
			 if(style && "1"==style.enabled){
				 _this.setPolylineStyle({
					 id:polyline.id,
					 style:style,
					 cache:true,
				 });
			 }
		 }else{
			 if(polyline.layerId){
				 var style=_this.getLayerStyle(polyline.layerId,'DEFAULT');
				 if(style && "1"==style.enabled){
					 _this.setPolylineStyle({
						 id:polyline.id,
						 style:style,
						 cache:false,
					 });
				 }
			 }
		 }
	 }
}

VFG.Viewer.prototype.getPolylineById=function(id){
	var _this=this;
	if(_this.cachePolylineMap.has(id)){
		 return _this.cachePolylineMap.get(id);
	}
}

VFG.Viewer.prototype.setPolylineStyle=function(param){
	var _this=this;
	var polyline=_this.getPolylineById(param.id);
	if(polyline && param.style){
	   VFG.Polyline.update({
		   polyline:polyline,
		   style:param.style
	   },_this.url);
	   if(param.cache==true){
		   _this.selectPolylineMap.set(polyline.id,polygon);
	   }else{
		   _this.selectPolylineMap.delete(polyline.id);
	   }
	}
}


VFG.Viewer.prototype.showPolylines=function(list,show){
	var _this=this;
	for(var i=0;i<list.length;i++){
		var polyline=list[i];
		VFG.Polyline.show({
			id:polyline.id,
			show:show,
		});
	}
} 

VFG.Viewer.prototype.showPolyline=function(option){
	var _this=this;
	VFG.Polyline.show(option);
}

;
///<jscompress sourcefile="VFGMapProvider.js" />
VFG.Viewer.prototype.cacheProviderMap=new Map();

/**
 * 添加底图
 */
VFG.Viewer.prototype.addProvider=function(provider){
	var _this=this;
	if(!provider) return;
	if(!VFG.Provider.Map.has(provider.id)){
		if('terrain'==provider.dataType){
			var terrainProvider=VFG.Provider.getTerrainProviderFromServ(provider);
			if(terrainProvider){
				VFG.Provider.hasTerrain=true;
				_this.viewer.terrainProvider=terrainProvider;
				VFG.Provider.Map.set(provider.id,terrainProvider);
				_this.cacheProviderMap.set(provider.id,provider);
			}
		}else{
			var imageryProvider=VFG.Provider.getImageryProviderFromServ(provider);
			if(imageryProvider){
				var sImgPro=_this.viewer.imageryLayers.addImageryProvider(imageryProvider);
				VFG.Provider.Map.set(provider.id,sImgPro);
				_this.cacheProviderMap.set(provider.id,provider);
			}
		}
	}
} 

/**
 * 添加底图
 */
VFG.Viewer.prototype.addProviders=function(providers){
	var _this=this;
	if(providers){
		 for(var i=0;i<providers.length;i++){
			 var provider=providers[i];
			 _this.addProvider(provider);
		 }
	}
}

/**
 * 移除所有
 */
VFG.Viewer.prototype.removeAllProviders=function(){
	var _this=this;
	_this.cacheProviderMap.forEach(function(provider,key){
		if('terrain'==provider.dataType){
			VFG.Provider.removeTerrainProviderById(_this.viewer,provider.id);
		}else{
			VFG.Provider.removeById(_this.viewer,provider.id);
		}
	});
	_this.cacheProviderMap.clear();
}

VFG.Viewer.prototype.removeProviders=function(providers){
	var _this=this;
	if(providers){
		 for(var i=0;i<providers.length;i++){
			 var provider=providers[i];
			 _this.removeProvider(provider.id);
		 }
	}
}

/**
 * 移除
 */
VFG.Viewer.prototype.removeProvider=function(id){
	var _this=this;
	if(_this.cacheProviderMap.has(id)){
		var provider=_this.cacheProviderMap.get(id);
		if('terrain'==provider.dataType){
			VFG.Provider.removeTerrainProviderById(_this.viewer,provider.id);
		}else{
			VFG.Provider.removeById(_this.viewer,id);
		}
	}
	_this.cacheProviderMap.delete(id);
}

VFG.Viewer.prototype.getProviderById=function(id){
	var _this=this;
	if(_this.cacheProviderMap.has(id)){
		 return _this.cacheProviderMap.get(id);
	}
}

VFG.Viewer.prototype.showProvider=function(param){
}	


;
///<jscompress sourcefile="VFGMapRoam.js" />
VFG.Viewer.prototype.cacheRoamMap=new Map();
VFG.Viewer.prototype.selectRoamMap=new Map();

VFG.Viewer.prototype.addRoam=function(option){
	var _this=this;
	var roam=new VFG.Roam(option);
	_this.cacheRoamMap.set(option.id,option);
}

VFG.Viewer.prototype.addRoams=function(list){
	var _this=this;
	 for(var i=0;i<list.length;i++){
		 var roam=list[i];
		 _this.addRoam(roam);
	 }
}

VFG.Viewer.prototype.removeRoams=function(list){
	var _this=this;
	for(var i=0;i<list.length;i++){
		var roam=list[i];
		_this.removeRoamById(roam.id);
	}
}

VFG.Viewer.prototype.showPath=function(list,show){
	var _this=this;
} 


VFG.Viewer.prototype.removeAllRoam=function(){
	var _this=this;
	_this.cacheRoamMap.forEach(function(value,key){
		value.destroy();
	});
	_this.cacheRoamMap.clear();
	_this.selectRoamMap.clear();
}

VFG.Viewer.prototype.removeRoamById=function(id){
	var _this=this;
	var roam=_this.getRoamById(id);
	if(roam){
		roam.destroy();
	}
	this.cacheRoamMap.delete(id);
	this.selectRoamMap.delete(id);
}

VFG.Viewer.prototype.removeRoam=function(roam){
	var _this=this;
	_this.removeRoamById(roam.id);
}

VFG.Viewer.prototype.getRoamById=function(id){
	var _this=this;
	if(_this.containRoam(id)){
		 return _this.cacheRoamMap.get(id);
	}
}

VFG.Viewer.prototype.containRoam=function(id){
	return this.cacheRoamMap.has(id)
}


VFG.Viewer.prototype.updateRoam=function(option){
	var _this=this;
	if(option.id){
		if(_this.containRoam(option.id)){
			var roam=_this.getRoamById(option.id);
			if(roam){
			}else{
				throw new Error("未找到对象!");
			}
		}else{
			 throw new Error("未找到对象!");
		}
	}else{
	   throw new Error("参数有误!");
	}
}


;
///<jscompress sourcefile="VFGMapScene.js" />
VFG.Viewer.prototype.showGlobe=function(value){
	this.viewer.scene.globe.show = value;
}
VFG.Viewer.prototype.showSkyBox=function(value){
	this.viewer.scene.skyBox.show = value;
}
VFG.Viewer.prototype.enableLighting=function(value){
	this.viewer.scene.globe.enableLighting = value;
}
VFG.Viewer.prototype.showShadows=function(value){
	this.viewer.shadows = value
}

VFG.Viewer.prototype.zoomIn=function(value){
	let cameraPos = this.viewer.camera.position;
	let ellipsoid = this.viewer.scene.globe.ellipsoid;
	let cartographic = ellipsoid.cartesianToCartographic(cameraPos);
	let height = cartographic.height;
	this.viewer.camera.zoomIn(height / value);
	
}

VFG.Viewer.prototype.zoomOut=function(value){
	let cameraPos = this.viewer.camera.position;
	let ellipsoid = this.viewer.scene.globe.ellipsoid;
	let cartographic = ellipsoid.cartesianToCartographic(cameraPos);
	let height = cartographic.height;
	this.viewer.camera.zoomOut(height * value);
}


;
///<jscompress sourcefile="VFGMapUtil.js" />
/**
 * 获取UUID
 */
VFG.Viewer.prototype.getUuid = function() {
	var s = [];
	var hexDigits = "0123456789abcdef";
	for (var i = 0; i < 36; i++) {
		s[i] = hexDigits.substr(Math.floor(Math.random() * 0x10), 1);
	}
	s[14] = "4"; 
	s[19] = hexDigits.substr((s[19] & 0x3) | 0x8, 1); 
	s[8] = s[13] = s[18] = s[23] = "-";
	var uuid = s.join("");
	return uuid;
};

/**
 * 笛卡尔坐标转经纬度
 */
VFG.Viewer.prototype.getLnLaFormC3  = function(cartesian3) {
	return VFG.Util.getC3ToLnLa(this.viewer, cartesian3)
}

/**
 * 经纬度装屏幕坐标
 * position:{
 * 	x:经度
 *  y:维度
 *  z:高度
 * }
 */
VFG.Viewer.prototype.getC2FormLnLa = function(position) {
	return Cesium.SceneTransforms.wgs84ToWindowCoordinates(this.viewer.scene,this.getC3FormLnLa(position))
}

/**
 * 经纬度转笛卡尔坐标
 * position:{
 * 	x:经度
 *  y:维度
 *  z:高度
 * }
 */
VFG.Viewer.prototype.getC3FormLnLa = function(position) {
	return Cesium.Cartesian3.fromDegrees(position.x*1,position.y*1,position.z*1);
}


/**
 * Turf多边形
 */
VFG.Viewer.prototype.getTurfPolygon = function (points) {
	return VFG.Util.getTurfPolygon(points);
}

/**
 * Turf点
 */
VFG.Viewer.prototype.getTurfPoint = function (point) {
	return VFG.Util.getTurfPoint(point);
}

/**
 * 点是否在多边形内部
 */
VFG.Viewer.prototype.booleanPointInPolygon = function (polygon,point) {
	return VFG.Util.booleanPointInPolygon(polygon, point);
}

//unit:degrees, radians, miles, or kilometers
VFG.Viewer.prototype.distance = function (point1,point2,unit) {
	var from = turf.point([point1.x*1,point1.y*1]);
	var to = turf.point([point2.x*1,point2.y*1]);
	var options = {units: unit||'miles'};
	var distance = turf.distance(from, to, options);
	return turf.distance(from, to, options);
}
/**
 * 多边形内的点
 */
VFG.Viewer.prototype.getPointInPolygon = function (polygonPoints,points) {
	var polygon =VFG.Util.getTurfPolygon(polygonPoints);
	var inPoints=[];
	for(var i=0;i<points.length;i++){
		if(VFG.Util.booleanPointInPolygon(polygon, VFG.Util.getTurfPoint(points[i]))){
			inPoints.push(points[i]);
		}
	}
	return inPoints;
}
;
///<jscompress sourcefile="VFGMapVModel.js" />
VFG.Viewer.prototype.cacheVModelMap=new Map();

VFG.Viewer.prototype.addVModel=function(option,url){
	var _this=this;
	if(_this.cacheVModelMap.has(option.id)) return;
	option.PrimitiveType='VideoModel';
	option.localUrl=url;
	var vmodel=new VFG.VModel(_this.viewer,option);
	_this.cacheVModelMap.set(option.id,vmodel);
} 

VFG.Viewer.prototype.addVModels=function(list,url){
	var _this=this;
	 for(var i=0;i<list.length;i++){
		 var vmodel=list[i]; 
		 _this.addVModel(vmodel,url);
	 }
} 

VFG.Viewer.prototype.removeVModel=function(vmodel){
	var _this=this;
	if(_this.cacheVModelMap.has(vmodel.id)){
		_this.removeVModelById(vmodel.id);
	}
}

VFG.Viewer.prototype.removeVModels=function(list){
	var _this=this;
	 for(var i=0;i<list.length;i++){
		 var vmodel=list[i]; 
		 _this.removeVModel(vmodel);
	 }
}

VFG.Viewer.prototype.removeVModelById=function(id){
	var _this=this;
	if(_this.cacheVModelMap.has(id)){
		var vmodel=_this.cacheVModelMap.get(id);
		vmodel.destroy();
		_this.cacheVModelMap.delete(id);
		vmodel=null;
	}
}

VFG.Viewer.prototype.getVModelById=function(id){
	var _this=this;
	if(_this.cacheVModelMap.has(id)){
		return _this.cacheVModelMap.get(id);
	}
} 

VFG.Viewer.prototype.removeAllVModel=function(layerId){
	var _this=this;
	_this.cacheVModelMap.forEach(function(vmodel,key){
		vmodel.destroy();
	});
	_this.cacheVModelMap.clear();
}

VFG.Viewer.prototype.flyToVModelById=function(id){
	if(this.cacheVModelMap.has(id)){
		var e=this.cacheVModelMap.get(id).option;
		if(e.cameraX && e.cameraX!='0'  && e.cameraY && e.cameraY!='0' && e.cameraZ){
			this.viewer.camera.flyTo({
			    destination : new Cesium.Cartesian3(e.cameraX*1,e.cameraY*1, e.cameraZ*1 ||10000),
			    orientation : {
			        heading : e.heading*1,
			        pitch : e.pitch*1,
			        roll : e.roll*1
			    }
			});
		}
	}
}

VFG.Viewer.prototype.changeVModelRX=function(id,rx){
	var _this=this;
	var vModel=_this.getVModelById(id);
	if(vModel){
		vModel.option.rotationX=rx;
		vModel.update();
	}
}

VFG.Viewer.prototype.changeVModelRY=function(id,ry){
	var _this=this;
	var vModel=_this.getVModelById(id);
	if(vModel){
		vModel.option.rotationY=ry;
		vModel.update();
	}
}

VFG.Viewer.prototype.changeVModelRZ=function(id,rz){
	var _this=this;
	var vModel=_this.getVModelById(id);
	if(vModel){
		vModel.option.rotationZ=rz;
		vModel.update();
	}
}

VFG.Viewer.prototype.changeVModelPosition=function(id,position){
	var _this=this;
	var vModel=_this.getVModelById(id);
	if(vModel){
		vModel.option.x=position.x;
		vModel.option.y=position.y;
		vModel.option.z=position.z;
		vModel.update();
	}
}

VFG.Viewer.prototype.changeVModelHeight=function(id,height){
	var _this=this;
	var vModel=_this.getVModelById(id);
	if(vModel){
		vModel.option.z=height;
		vModel.update();
	}
}

VFG.Viewer.prototype.changeVModelScale=function(id,scale){
	var _this=this;
	var vModel=_this.getVModelById(id);
	if(vModel){
		vModel.option.scaleX=scale.scaleX;
		vModel.option.scaleY=scale.scaleY;
		vModel.option.scaleZ=scale.scaleZ;
		vModel.update();
	}
}



;
///<jscompress sourcefile="VFGMapVPlane.js" />
VFG.Viewer.prototype.cacheVPlaneMap=new Map();

VFG.Viewer.prototype.addVPlane=function(option){
	var _this=this;
	if(!_this.cacheVPlaneMap.has(option.id)){
		option.PrimitiveType='VideoPlane';
		var vplane=new VFG.VPlane(_this.viewer,option);
		_this.cacheVPlaneMap.set(option.id,vplane);
	}

} 

VFG.Viewer.prototype.addVPlanes=function(list){
	var _this=this;
	 for(var i=0;i<list.length;i++){
		 var vplane=list[i]; 
		 _this.addVPlane(vplane);
	 }
} 

VFG.Viewer.prototype.removeVPlane=function(vplane){
	var _this=this;
	if(_this.cacheVPlaneMap.has(vplane.id)){
		_this.removeVPlaneById(vplane.id);
	}
}

VFG.Viewer.prototype.removeVPlanes=function(list){
	var _this=this;
	 for(var i=0;i<list.length;i++){
		 var vplane=list[i]; 
		 _this.removeVPlane(vplane);
	 }
}

VFG.Viewer.prototype.removeVPlaneById=function(id){
	var _this=this;
	if(_this.cacheVPlaneMap.has(id)){
		var vplane=_this.cacheVPlaneMap.get(id);
		vplane.destroy();
		_this.cacheVPlaneMap.delete(id);
		vplane=null;
	}
}

VFG.Viewer.prototype.getVPlaneById=function(id){
	var _this=this;
	if(_this.cacheVPlaneMap.has(id)){
		return _this.cacheVPlaneMap.get(id);
	}
} 

VFG.Viewer.prototype.removeAllVPlane=function(layerId){
	var _this=this;
	_this.cacheVPlaneMap.forEach(function(vplane,key){
		vplane.destroy();
	});
	_this.cacheVPlaneMap.clear();
}

VFG.Viewer.prototype.flyToVPlaneById=function(id){
	if(this.cacheVPlaneMap.has(id)){
		var e=this.cacheVPlaneMap.get(id).option;
		if(e.cameraX && e.cameraX!='0'  && e.cameraY && e.cameraY!='0' && e.cameraZ){
			this.viewer.camera.flyTo({
			    destination : new Cesium.Cartesian3(e.cameraX*1,e.cameraY*1, e.cameraZ*1 ||10000),
			    orientation : {
			        heading : e.heading*1,
			        pitch : e.pitch*1,
			        roll : e.roll*1
			    }
			});
		}
	}
}

VFG.Viewer.prototype.changeVPlaneDX=function(id,dx){
	var _this=this;
	var VPlane=_this.getVPlaneById(id);
	if(VPlane){
		VPlane.option.dimensionX=dx;
		VPlane.update()
	}
}
VFG.Viewer.prototype.changeVPlaneDY=function(id,dy){
	var _this=this;
	var VPlane=_this.getVPlaneById(id);
	if(VPlane){
		VPlane.option.dimensionY=dy;
		VPlane.update()
	}
}
VFG.Viewer.prototype.changeVPlaneDZ=function(id,dz){
	var _this=this;
	var VPlane=_this.getVPlaneById(id);
	if(VPlane){
		VPlane.option.dimensionZ=dz;
		VPlane.update()
	}
}
VFG.Viewer.prototype.changeVPlaneRX=function(id,rx){
	var _this=this;
	var VPlane=_this.getVPlaneById(id);
	if(VPlane){
		VPlane.option.rotationX=rx;
		VPlane.update()
	}
}
VFG.Viewer.prototype.changeVPlaneRY=function(id,ry){
	var _this=this;
	var VPlane=_this.getVPlaneById(id);
	if(VPlane){
		VPlane.option.rotationY=ry;
		VPlane.update()
	}
}
VFG.Viewer.prototype.changeVPlaneRZ=function(id,rz){
	var _this=this;
	var VPlane=_this.getVPlaneById(id);
	if(VPlane){
		VPlane.option.rotationZ=rz;
		VPlane.update()
	}
}

VFG.Viewer.prototype.changeVPlanePosition=function(id,position){
	var _this=this;
	var VPlane=_this.getVPlaneById(id);
	if(VPlane){
		VPlane.option.x=position.x;
		VPlane.option.y=position.y;
		VPlane.option.z=position.z;
		VPlane.update()
	}
}

VFG.Viewer.prototype.changeVPlaneHeight=function(id,height){
	var _this=this;
	var VPlane=_this.getVPlaneById(id);
	if(VPlane){
		VPlane.option.z=height;
		VPlane.update()
	}
}


;
///<jscompress sourcefile="VFGMapVShed3d.js" />
VFG.Viewer.prototype.cacheVideoShed3dMap=new Map();

VFG.Viewer.prototype.addVideoShed3d=function(option){
	var _this=this;
	
	if(_this.cacheVideoShed3dMap.has(option.id)) return;
	
	option.PrimitiveType='VideoShed3d';
	var vPos=_this.getVideoShed3dViewPosition(option);
	
	var param={
		id:option.id||'', 
		name:option.name||'', 
		code:option.code||'', 
		option:option||{}, 
		cameraPosition:Cesium.Cartesian3.fromDegrees(option.x*1,option.y*1,option.z*1),
		position:Cesium.Cartesian3.fromDegrees(vPos.longitude*1,vPos.latitude*1,vPos.altitude*1), 
		alpha:option.alpha||1, 
		debugFrustum:false, 
		fov:option.fov||null, 
		heading:option.vHeading||null, 
		pitch:option.vPitch||null, 
		roll:option.vRoll||null,
		near:option.near||null,
		aspectRatio:option.aspectRatio||null, 
		maskUrl:option.maskUrl||null, 
	};
	
	var vShed3d=new VFG.VShed3d(_this.viewer,param);
	_this.cacheVideoShed3dMap.set(option.id,vShed3d);
} 

VFG.Viewer.prototype.getVideoShed3dViewPosition=function(option){
	return this.ECEF.enu_to_ecef({ 
		longitude:option.x*1, 
		latitude:option.y*1, 
		altitude:option.z*1
	},
	{ 
		distance: option.far*1, 
		azimuth:option.vHeading*1, 
		elevation:option.vPitch*1
	});
}
VFG.Viewer.prototype.addVideoShed3ds=function(list){
	var _this=this;
	 for(var i=0;i<list.length;i++){
		 var vShed3d=list[i]; 
		 _this.addVideoShed3d(vShed3d);
	 }
}

VFG.Viewer.prototype.removeVideoShed3d=function(vShed3d){
	var _this=this;
	if(_this.cacheVideoShed3dMap.has(vShed3d.id)){
		_this.removeVideoShed3dById(vShed3d.id);
	}
}

VFG.Viewer.prototype.removeVideoShed3ds=function(list){
	var _this=this;
	 for(var i=0;i<list.length;i++){
		 var vShed3d=list[i]; 
		 _this.removeVideoShed3d(vShed3d);
	 }
}

VFG.Viewer.prototype.removeVideoShed3dById=function(id){
	var _this=this;
	if(_this.cacheVideoShed3dMap.has(id)){
		var vShed3d=_this.cacheVideoShed3dMap.get(id);
		vShed3d.destroy();
		_this.cacheVideoShed3dMap.delete(id);
		vShed3d=null;
	}
}

VFG.Viewer.prototype.getVideoShed3dById=function(id){
	var _this=this;
	if(_this.cacheVideoShed3dMap.has(id)){
		return _this.cacheVideoShed3dMap.get(id);
	}
} 

VFG.Viewer.prototype.removeAllVideoShed3d=function(layerId){
	var _this=this;
	_this.cacheVideoShed3dMap.forEach(function(vShed3d,key){
		vShed3d.destroy();
	});
	_this.cacheVideoShed3dMap.clear();
}

VFG.Viewer.prototype.flyToVideoShed3dById=function(id){
	if(this.cacheVideoShed3dMap.has(id)){
		var e=this.cacheVideoShed3dMap.get(id).option;
		if(e.cameraX && e.cameraX!='0' && e.cameraY && e.cameraY!='0' && e.cameraZ){
			this.viewer.camera.flyTo({
			    destination : new Cesium.Cartesian3(e.cameraX*1,e.cameraY*1, e.cameraZ  || 2000 ),
			    orientation : {
			        heading : e.heading*1,
			        pitch : e.pitch*1,
			        roll : e.roll*1
			    }
			});
		}
	}
}

VFG.Viewer.prototype.changeFar=function(id,far){
  var videoShed3d=this.getVideoShed3dById(id);
  if(videoShed3d){
	  videoShed3d.option.far=far;
	  var vPos=this.getVideoShed3dViewPosition(videoShed3d.option);
	  if(vPos){
		  var position=Cesium.Cartesian3.fromDegrees(vPos.longitude*1,vPos.latitude*1,vPos.altitude*1);
		  videoShed3d.option.position=position;
		  videoShed3d.position=position;
	  }
  }
}

VFG.Viewer.prototype.changeNear=function(id,near){
  var videoShed3d=this.getVideoShed3dById(id);
  if(videoShed3d){
	  videoShed3d.option.near=near;
	  videoShed3d.near=near;
  }
}

VFG.Viewer.prototype.changeAlpha=function(id,alpha){
  var videoShed3d=this.getVideoShed3dById(id);
  if(videoShed3d){
	  videoShed3d.option.alpha=alpha;
	  videoShed3d.alpha=alpha;
  }
}
VFG.Viewer.prototype.changeAspectRatio=function(id,aspectRatio){
  var videoShed3d=this.getVideoShed3dById(id);
  if(videoShed3d){
	  videoShed3d.option.aspectRatio=aspectRatio;
	  videoShed3d.aspectRatio=aspectRatio
  }
}

VFG.Viewer.prototype.changeFov=function(id,fov){
  var videoShed3d=this.getVideoShed3dById(id);
  if(videoShed3d){
	  videoShed3d.option.fov=fov;
	  videoShed3d.fov=fov
  }
}

VFG.Viewer.prototype.changePitch=function(id,pitch){
  var videoShed3d=this.getVideoShed3dById(id);
  if(videoShed3d){
	  videoShed3d.option.vPitch=pitch;
	  videoShed3d._pitch=pitch;
	  var vPos=this.getVideoShed3dViewPosition(videoShed3d.option);
	  videoShed3d.position=Cesium.Cartesian3.fromDegrees(vPos.longitude*1,vPos.latitude*1,vPos.altitude*1)
  }
}

VFG.Viewer.prototype.changeHeading=function(id,heading){
  var videoShed3d=this.getVideoShed3dById(id);
  if(videoShed3d){
	  videoShed3d.option.vHeading=heading;
	  videoShed3d._heading=heading;
	  var vPos=this.getVideoShed3dViewPosition(videoShed3d.option);
	  videoShed3d.position=Cesium.Cartesian3.fromDegrees(vPos.longitude*1,vPos.latitude*1,vPos.altitude*1)
  }
}

VFG.Viewer.prototype.changeRoll=function(id,roll){
  var videoShed3d=this.getVideoShed3dById(id);
  if(videoShed3d){
	  videoShed3d.option.vRollg=roll;
	  videoShed3d.roll=roll
  }
}

VFG.Viewer.prototype.changeHeight=function(id,height){
  var videoShed3d=this.getVideoShed3dById(id);
  if(videoShed3d){
	  videoShed3d.option.z=height;
	  videoShed3d.cameraPosition=Cesium.Cartesian3.fromDegrees(videoShed3d.option.x*1,videoShed3d.option.y*1,videoShed3d.option.z*1)
  }
}

VFG.Viewer.prototype.changeMaskUrl=function(id,maskUrl){
  var videoShed3d=this.getVideoShed3dById(id);
  if(videoShed3d){
	  videoShed3d.option.maskUrl=maskUrl;
	  videoShed3d.maskUrl=maskUrl
  }
}

VFG.Viewer.prototype.changeDebugFrustum=function(id,debugFrustum){
  var videoShed3d=this.getVideoShed3dById(id);
  if(videoShed3d){
	  videoShed3d.option.debugFrustum=debugFrustum;
	  videoShed3d.debugFrustum=debugFrustum
  }
}

;
