VFG.Polygon=function(){
}
VFG.Polygon.Layers=new Cesium.PrimitiveCollection();
VFG.Polygon.Primitives=new Map();

VFG.Polygon.removeById=function(id){
	if(this.Primitives.has(id)){
		this.Layers.remove(this.Primitives.get(id));
	}
	this.Primitives.delete(id);
}

VFG.Polygon.getById=function(id){
	if(this.Primitives.has(id)){
		return this.Primitives.get(id);
	}
}

VFG.Polygon.add=function(param,url){
	if(!this.Primitives.has(param.polygon.id)){
		var primitive=this.package(param,url);
		this.Layers.add(primitive)
		this.Primitives.set(param.polygon.id,primitive)
	}
}

VFG.Polygon.package=function(param,url){
	var polygon=param.polygon;
	var style=param.style;
	var hierarchy=this.getHierarchy(polygon.points);
	polygon.PrimitiveType="Polygon";
	if(style){
		var distanceDisplayCondition=null
	    if(style.distanceDisplayConditionX*1!=0 || style.distanceDisplayConditionY*1!=0){
			distanceDisplayCondition=new Cesium.DistanceDisplayConditionGeometryInstanceAttribute(style.distanceDisplayConditionX*1, style.distanceDisplayConditionY*1)
	    }
		//普通面
		if("0"==style.displayEffect){
			var perPositionHeight=style.perPositionHeight?style.perPositionHeight*1:false;
			if(perPositionHeight){
				return new Cesium.GroundPrimitive({
			        geometryInstances: new Cesium.GeometryInstance({
			          geometry: new Cesium.PolygonGeometry({
			            polygonHierarchy: hierarchy,
			            stRotation:style.stRotation?style.stRotation*1:0.0,
			          }),
			          id:polygon,
			          attributes : {
		        	     distanceDisplayCondition : distanceDisplayCondition
		        	  },
			        }),
			        appearance: new Cesium.MaterialAppearance({
			            material: this.getMaterial(param.style,url),
			            translucent: false
			        })
			      })
			}else{
				return new Cesium.Primitive({
			        geometryInstances: new Cesium.GeometryInstance({
			          geometry: new Cesium.PolygonGeometry({
						polygonHierarchy: hierarchy,
						stRotation:style.stRotation?style.stRotation*1:0.0,
						perPositionHeight:false,
						height:style.height?style.height*1:0.0,//相对于椭球表面的高度
						extrudedHeight:style.extrudedHeight?style.extrudedHeight*1:0.0,//定多边形的凸出面相对于椭圆表面的高度
						closeTop:style.closeTop?style.closeTop*1:false,
						closeBottom:style.closeBottom?style.closeBottom*1:false,
			          }),
			          id:polygon,
			          attributes : {
		        	     distanceDisplayCondition : distanceDisplayCondition
		        	  },
			        }),
			        appearance: new Cesium.MaterialAppearance({
			            material: this.getMaterial(param.style,url),
			            translucent: false
			        })
		      })
		   }
		}
		else if("1"==style.displayEffect){
			return new Cesium.Primitive({
		        geometryInstances: new Cesium.GeometryInstance({
		          geometry: new Cesium.PolygonGeometry({
					polygonHierarchy: hierarchy,
					stRotation:style.stRotation?style.stRotation*1:0.0,
					perPositionHeight:false,
					height:style.height?style.height*1:0.0,
					extrudedHeight:style.extrudedHeight?style.extrudedHeight*1:0.0,
		          }),
		          id:polygon,
		          attributes : {
	        	     distanceDisplayCondition : distanceDisplayCondition
	        	  },		          
		        }),
		        appearance: new Cesium.MaterialAppearance({
		            material: this.getMaterial(param.style,url),
		            translucent: false,
		        })
	      })
		}
		else if("2"==style.displayEffect){
			return new Cesium.GroundPrimitive({
		        geometryInstances: new Cesium.GeometryInstance({
		          geometry: new Cesium.PolygonGeometry({
		            polygonHierarchy: hierarchy,
		            stRotation:style.stRotation?style.stRotation*1:0.0,
		          }),
		          id:polygon,
		          attributes : {
	        	     distanceDisplayCondition : distanceDisplayCondition
	        	  },		          
		        }),
		        appearance : new Cesium.EllipsoidSurfaceAppearance({
		            material : new Cesium.Material({
		              fabric : {
		                type : 'Water',
		                uniforms : {
		                  normalMap:  url+'/'+style.mapUrl,
		                  frequency: style.frequency?style.frequency*1:1000.0,
		                  animationSpeed: style.speed?style.speed*1:0.01,
		                  amplitude:style.amplitude?style.amplitude*1:10.0,
		                }
		              }
		            }),
		            fragmentShaderSource:'varying vec3 v_positionMC;\nvarying vec3 v_positionEC;\nvarying vec2 v_st;\nvoid main()\n{\nczm_materialInput materialInput;\nvec3 normalEC = normalize(czm_normal3D * czm_geodeticSurfaceNormal(v_positionMC, vec3(0.0), vec3(1.0)));\n#ifdef FACE_FORWARD\nnormalEC = faceforward(normalEC, vec3(0.0, 0.0, 1.0), -normalEC);\n#endif\nmaterialInput.s = v_st.s;\nmaterialInput.st = v_st;\nmaterialInput.str = vec3(v_st, 0.0);\nmaterialInput.normalEC = normalEC;\nmaterialInput.tangentToEyeMatrix = czm_eastNorthUpToEyeCoordinates(v_positionMC, materialInput.normalEC);\nvec3 positionToEyeEC = -v_positionEC;\nmaterialInput.positionToEyeEC = positionToEyeEC;\nczm_material material = czm_getMaterial(materialInput);\n#ifdef FLAT\ngl_FragColor = vec4(material.diffuse + material.emission, material.alpha);\n#else\ngl_FragColor = czm_phong(normalize(positionToEyeEC), material);\
		            	gl_FragColor.a=0.5;\n#endif\n}\n'//重写shader，修改水面的透明度
		          })
		      })
		}else{
			return new Cesium.GroundPrimitive({
		        geometryInstances: new Cesium.GeometryInstance({
		          geometry: new Cesium.PolygonGeometry({
		            polygonHierarchy: hierarchy,
		            stRotation:style.stRotation?style.stRotation*1:0.0,
		          }),
		          id:polygon,
		          attributes : {
	        	     distanceDisplayCondition : distanceDisplayCondition
	        	  },		          
		        }),
		        appearance: new Cesium.MaterialAppearance({
		            material: this.getMaterial(param.style,url),
		            translucent: false,
		        }),
		        
	       })
		}
	}else{
		return new Cesium.GroundPrimitive({
	        geometryInstances: new Cesium.GeometryInstance({
	          geometry: new Cesium.PolygonGeometry({
	            polygonHierarchy: hierarchy,
	          }),
	          id:polygon
	        }),
	        appearance: new Cesium.MaterialAppearance({
	            material:new Cesium.Material({
				    fabric : {
				        type : 'Color',
				        uniforms : {
				            color : Cesium.Color.RED
				        }
				    }
				}),
	            translucent: false,
	        }),
       })
	}
}

VFG.Polygon.getMaterial=function(style,url){
	if("2"==style.displayEffect){
		return new Cesium.Material({
		    fabric : {
		        type : 'Water',
		        uniforms : {
		            normalMap:  url+'/'+style.mapUrl,
		            frequency: 10000.0,
		            animationSpeed: 1,
		            amplitude: 1.0,
		        }
		    },
		})
	}else{
		if("color"==style.material){
			return new Cesium.Material({
			    fabric : {
			        type : 'Color',
			        uniforms : {
			            color : style.color? Cesium.Color.fromCssColorString(style.color):Cesium.Color.BLACK
			        }
			    }
			})
		}else if("img"==style.material){
			return new Cesium.Material({
			    fabric : {
			        type : 'Image',
			        uniforms : {
			            image : url+'/'+style.mapUrl
			        }
			    }
			})			
		}else{
			return new Cesium.Material({
			    fabric : {
			        type : 'Color',
			        uniforms : {
			            color : style.color? Cesium.Color.fromCssColorString(style.color):Cesium.Color.BLACK
			        }
			    }
			})
		}
	}
}

VFG.Polygon.getHierarchy=function(points){
	if(points){
		var positions=JSON.parse(points);
		if(positions.length>=2){
			var Hierarchy=[];
			for(var i=0;i<positions.length;i++){
				Hierarchy.push(Cesium.Cartesian3.fromDegrees(positions[i].x*1,positions[i].y*1,positions[i].z*1));
			}
			return new Cesium.PolygonHierarchy(Hierarchy);
		}
	}else{
		return [];
	}
}

VFG.Polygon.getPositions=function(points){
	if(points){
		var positions=JSON.parse(points);
		if(positions.length>=2){
			var Hierarchy=[];
			for(var i=0;i<positions.length;i++){
				Hierarchy.push(Cesium.Cartesian3.fromDegrees(positions[i].x*1,positions[i].y*1,positions[i].z*1));
			}
			return Hierarchy;
		}
	}else{
		return [];
	}
}

VFG.Polygon.update=function(param,url){
	var old=this.Primitives.get(param.polygon.id);
	if(old){
		var primitive=this.package(param,url);
		if( old instanceof Cesium.GroundPrimitive && primitive instanceof Cesium.GroundPrimitive){
			old.appearance.material=primitive.appearance.material
			old.geometryInstances=primitive.geometryInstances;
		}
		else if( old instanceof Cesium.Primitive && primitive instanceof Cesium.Primitive){
			old.appearance.material=primitive.appearance.material;
			old.geometryInstances=primitive.geometryInstances;
		}else{
			this.removeById(param.polygon.id);
			this.Layers.add(primitive);
			this.Primitives.set(param.polygon.id,primitive)
		}
	}
}

VFG.Polygon.show=function(param){
	var primitive=this.Primitives.get(param.id);
	if(primitive){
		primitive.show=param.show || true
	}
}
