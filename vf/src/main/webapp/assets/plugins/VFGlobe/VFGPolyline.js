VFG.Polyline=function(){
}
VFG.Polyline.Layers=new Cesium.CustomDataSource("polylinesLayer");
VFG.Polyline.Primitives=new Map();

VFG.Polyline.add=function(param,ctx){
	var _this=this;
	_this.Primitives.set(param.polyline.id,param.polyline);
	return this.Layers.entities.add(new Cesium.Entity(_this.packagePolyline(param,ctx)));
}

VFG.Polyline.packagePolyline=function(param,ctx){
	var _this=this;
	var polyline=param.polyline;
	var style=param.style;
	var positions=this.getPositions(polyline.points);
	if(style){
	    var distanceDisplayCondition =VFG.Util.getDistanceDisplayCondition(style.distanceDisplayConditionX,style.distanceDisplayConditionY);
		var material=_this.getMaterial(style,ctx);
		return {
            id:polyline.id,
			name:polyline.name||'',
			code:polyline.code||'',
            polyline:{
		        show: true,
		        positions: positions,
				width : style.width*1,
				loop:style.loop&&style.loop=='1'?true:false,
				clampToGround:style.clampToGround&&style.clampToGround=='1'?true:false,
				material:material?material:Cesium.Color.RED,
				distanceDisplayCondition : distanceDisplayCondition?distanceDisplayCondition:undefined,
				zIndex:polyline.sort||null,
			}
        }
	}else{
		return {
            id:polyline.id,
			name:polyline.name||'',
			code:polyline.code||'',
            polyline: {
                show: true,
                positions:positions,
                material:Cesium.Color.RED,
                width:2,
                clampToGround:true,
            }
        }
	}
}

VFG.Polyline.getMaterial = function(style,ctx){
	var _this=this;
	if(Cesium.defined(style)){
		if(style.materialType=="solid"){
			return (style.color)?Cesium.Color.fromCssColorString(style.color):Cesium.Color.RED;//线条颜色;
		}
		else if(style.materialType=="dash"){
			return new Cesium.PolylineDashMaterialProperty({
				color:(style.color)?Cesium.Color.fromCssColorString(style.color):Cesium.Color.RED,//线条颜色
				gapColor:(style.gapColor)?Cesium.Color.fromCssColorString(style.gapColor):Cesium.Color.WHITE,//间隔颜色
				dashLength:(style.dashLength)?style.dashLength*1:16,//间隔距离
				dashPattern:(style.dashPattern)?style.dashPattern*1:255.0
			})
		}
		else if(style.materialType=="outline"){
			return new Cesium.PolylineOutlineMaterialProperty({
				color:(style.color)?Cesium.Color.fromCssColorString(style.color):Cesium.Color.RED,//线条颜色
				outlineWidth: (style.outlineWidth)?style.outlineWidth*1:5,
				outlineColor:(style.outlineColor)?Cesium.Color.fromCssColorString(style.outlineColor):Cesium.Color.WHITE,//线条颜色
			});
		}
		else if(style.materialType=="glow"){
			return new Cesium.PolylineGlowMaterialProperty({
					color:(style.color)?Cesium.Color.fromCssColorString(style.color):Cesium.Color.RED,//线条颜色
					glowPower: (style.glowPower)?style.glowPower*1:0.5,//发光强度
				});
		}
		else if(style.materialType=="arrow"){
			return new Cesium.PolylineArrowMaterialProperty(
					(style.color)?Cesium.Color.fromCssColorString(style.color):Cesium.Color.RED
			);
		}
		else if(style.materialType=="od"){
			return new Cesium.ODLineMaterial(
					(style.color)?Cesium.Color.fromCssColorString(style.color):Cesium.Color.RED,style.interval?style.interval*1000:20000,style.direction,style.effect,ctx+"/"+style.imageUrl)
		}
		else{
			return Cesium.Color.BLUE;
		}
	}
}

VFG.Polyline.getPositions=function(points){
	if(points){
		var positions;
		if(typeof(points)=='string'){
			positions=JSON.parse(points);
		}else{
			positions=points;
		}
		if(positions.length>=2){
			var Hierarchy=[];
			for(var i=0;i<positions.length;i++){
				Hierarchy.push(Cesium.Cartesian3.fromDegrees(positions[i].x*1,positions[i].y*1,positions[i].z*1));
			}
			return Hierarchy;
		}
	}else{
		return [];
	}
}

VFG.Polyline.contain=function(id){
	return this.Primitives.has(id)
}

VFG.Polyline.getById=function(id){
	if(this.contain(id)){
		var obj=this.Primitives.get(id);
		if(obj.render && obj.render=="1"){
			return this.Layers.entities.getById(id);
		}else{
			return this.Layers.entities.getById(id);
		}
	}
}

VFG.Polyline.removeById=function(id){
	if(this.contain(id)){
		var obj=this.Primitives.get(id);
		if(obj.render && obj.render=="1"){
			return this.Layers.entities.removeById(id);
		}else{
			return this.Layers.entities.removeById(id);
		}
	}
	this.Primitives.delete(id);
}

VFG.Polyline.show=function(param){
	if(this.contain(param.id)){
		var Entity=this.Layers.entities.getById(param.id);
		if(Entity){
			Entity.show=param.show;
		}
	}
}

VFG.Polyline.update=function(param,ctx){
	if(this.contain(param.polyline.id)){
		var option=this.packagePolyline(param,ctx);
		var Entity=this.Layers.entities.getById(param.polyline.id);
		console.log(Entity);
		Entity.id=option.id;
		Entity.name=option.name;
		Entity.code=option.code;
		Entity.polyline=option.polyline;
	}
}



