var treeSelect;
var scenelayerMap=new Map();
var editMarker;
var sceneTree;
var $ ;
var slider;
var form;
var table;
var upload;
var fileChoose;
var carousel;
var colorpicker;
var selectInput;
var viewer;
var globe;
layui.config({
    version: true,   
    base: '../../assets/module/'
}).extend({
    formSelects: 'formSelects/formSelects-v4',
    treetable: 'treetable-lay/treetable',
    dropdown: 'dropdown/dropdown',
    notice: 'notice/notice',
    step: 'step-lay/step',
    dtree: 'dtree/dtree',
    citypicker: 'city-picker/city-picker',
    tableSelect: 'tableSelect/tableSelect',
    Cropper: 'Cropper/Cropper',
    zTree: 'zTree/zTree',
    introJs: 'introJs/introJs',
    fileChoose: 'fileChoose/fileChoose',
    tagsInput: 'tagsInput/tagsInput',
    CKEDITOR: 'ckeditor/ckeditor',
    Split: 'Split/Split',
    cascader: 'cascader/cascader',
    selectInput: 'selectInput/selectInput',
	treeSelect: 'treeSelect/treeSelect' 
}).use(['layer', 'element', 'config', 'index','selectInput','upload','carousel','colorpicker' ,'admin', 'laytpl','treeSelect','contextMenu','tableX','util','form','treetable','slider','fileChoose','form'], function () {
    $ = layui.jquery;
    var layer = layui.layer;
    var element = layui.element;
    var config = layui.config;
    slider = layui.slider;
    table = layui.table;
    form= layui.form;
    var index = layui.index;
    var admin = layui.admin;
	treeSelect= layui.treeSelect;
	fileChoose= layui.fileChoose;
	upload= layui.upload;
	carousel = layui.carousel;
	colorpicker = layui.colorpicker;
	selectInput = layui.selectInput;
    var laytpl = layui.laytpl;
    //获取场景
	layer.load(2);
    var res=common_ajax.ajaxFunc(ctx+"/api/scene/getById",{
    		sceneId:id,
    		hasMap:'1',
    		hasModel:'1',
    	}, "json",null);
    layer.closeAll('loading');
    if (undefined!=res && 200==res.code) {
    	init(res.data);
    } else {
        layer.msg(res.message, {icon: 2});
    }
});


function init(data){
	data.url=ctx;
	data.geocoder=true;
    globe= new VFG.Viewer('cesiumContainer',data);
	viewer=globe.viewer;
	editMarker=new VFGEditor(viewer,{
		sceneId:id,
		url:ctx,
		globe:globe
	});
	
	
	var options = {};
	// 用于在使用重置导航重置地图视图时设置默认视图控制。接受的值是Cesium.Cartographic 和Cesium.Rectangle.
	options.defaultResetView = Cesium.Cartographic.fromDegrees(110, 30, 2000000);
	// 用于启用或禁用罗盘。true是启用罗盘，false是禁用罗盘。默认值为true。如果将选项设置为false，则罗盘将不会添加到地图中。
	options.enableCompass= true;
	// 用于启用或禁用缩放控件。true是启用，false是禁用。默认值为true。如果将选项设置为false，则缩放控件 将不会添加到地图中。
	options.enableZoomControls= true;
	 // 用于启用或禁用距离图例。true是启用，false是禁用。默认值为true。如果将选项设置为false，距离图例将不会添加到地图中。
	options.enableDistanceLegend= true;
	// 用于启用或禁用指南针外环。true是启用，false是禁用。默认值为true。如果将选项设置为false，则该环将可见但无效。
	options.enableCompassOuterRing= true;
	viewer.extend(Cesium.viewerCesiumNavigationMixin, options);
	//场景信息
	$('#editorForScene').click(function () {
		editMarker.scenePrimitiveForEdit();
	});
	//场景菜单
	$('#editorForSceneMenu').click(function () {
		editMarker.sceneMenuForEdit();
	});	
	//图层管理
	$('#editorForImgLayer').click(function () {
		editMarker.layerForEdit();
	});
	$('#handlePointHeight').click(function () {
		editMarker.drawPolygon();
	});  
	
	$('#handlePointModelHeight').click(function () {
		editMarker.getClampToHeight();
	});
	$('#handlePointTerrainHeight').click(function () {
		editMarker.getSampleTerrain();
	});
	$('#handlePointDemHeight').click(function () {
		editMarker.drawPolygon();
	});
	

	
}







