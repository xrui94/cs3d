# Cesium web三维管理工具
#### 介绍
初衷，基于cesium实现快速搭建GIS三维平台；
#### 软件架构
前端：LayerUI+Jquery+cesium 
后端:Java+JFinal
数据库：MySQL
服务：tomcat9
#### 安装教程
maven工程
　　eclipse:
    1、File->Import->Maven -> Existing Maven Project
    2、导入成功 maven -> update Project
    参考:https://blog.csdn.net/w12345_ww/article/details/52094756
数据库脚本
src/resources/vf.sql
mysql字符集：utf8mb4 -- UTF-8 Unicode 排序规则：utf8mb4_bin

联系开发人员
#### 使用说明
视频融合功能，
投影、广告牌、模型视频
#### 参与贡献
1.  感谢公司提供研究三维的机会！


#### 功能界面截图

![场景信息](https://images.gitee.com/uploads/images/2020/0822/125145_85817dc6_400924.png "场景信息.png")
![图层管理](https://images.gitee.com/uploads/images/2020/0822/125413_5614aede_400924.png "图层管理.png")
![底图管理](https://images.gitee.com/uploads/images/2020/0822/125434_020f8f78_400924.png "底图管理.png")
![地形管理](https://images.gitee.com/uploads/images/2020/0822/125450_e6dfdebd_400924.png "地形管理.png")
![实景三维](https://images.gitee.com/uploads/images/2020/0822/125509_fef32f86_400924.png "实景三维.png")
![人工模型](https://images.gitee.com/uploads/images/2020/0822/125524_25f43138_400924.png "人工模型.png")
![绘制点](https://images.gitee.com/uploads/images/2020/0822/125540_de790d92_400924.png "点.png")
![视频融合-投影](https://images.gitee.com/uploads/images/2020/0918/153819_76106b9a_400924.png "视频融合.png")
![视频融合-广告牌](https://images.gitee.com/uploads/images/2020/0918/154448_3b8f9177_400924.png "广告牌.png")
![调整编辑功能](https://images.gitee.com/uploads/images/2021/0131/222631_fd317a3f_400924.png "微信图片_20210131222530.png")
在线预览地址:http://39.99.162.142/vfs 账号:system密码:sys@123456
咨询qq群：1057643314

不搞cs了，搞UE
https://gitee.com/203014/digital-twin-for-ue/blob/master/README.md




